$(window).on("load", function () {
    //Preloader
    setTimeout("$('.page-loader div').fadeOut();", 200);
    setTimeout("$('.page-loader').delay(200).fadeOut('slow');", 200);
    setTimeout("jQuery('.preloader_hide, .selector_open').animate({'opacity' : '1'},500)", 200);
});

$(function () {
    /*copylink*/
    $('.copylink').click(function () {
        $('#link')[0].select();
        document.execCommand('copy');
        $('#link').append(' ');
        $('#link').val().slice(0, -1);
    });
    /*copylink*/

    /*menu button*/
    $('.menu-button').click(function () {
        $(this).toggleClass('open');
        $('.menu').toggleClass('open');
    });
    /**menu button*/

    /*question*/
    $('.question').click(function () {
        $(this).toggleClass('active');
        $(this).siblings('.answer').toggleClass('active');
    });
    /*question*/

    /*dropdown-item*/
    $('.dropdown-item-lang').click(function () {
        var lang = $(this).data('lang');
        $(this).parent().parent().find('.dropdown-toggle').find('span').removeClass();
        $(this).parent().parent().find('.dropdown-toggle').find('span').addClass('flag-icon ' + lang);
    });
    $('.dropdown-wallet-menu__item').click(function () {
        var class1 = $(this).data('class');
        var title = $(this).data('title');
        var price = $(this).data('price');
        $(this).parent().parent().find('.dropdown-toggle').find('.wallet-menu__item__title').find('b').html(title);
        $(this).parent().parent().find('.dropdown-toggle').find('.wallet-menu__item__amount').html(price);
        $(this).parent().parent().find('.dropdown-toggle').removeClass('comis');
        $(this).parent().parent().find('.dropdown-toggle').removeClass('trade');
        $(this).parent().parent().find('.dropdown-toggle').removeClass('founds');
        $(this).parent().parent().find('.dropdown-toggle').removeClass('total');
        $(this).parent().parent().find('.dropdown-toggle').addClass(class1);
    });
    $('.dropdown-left-menu__item').click(function () {
        var title2 = $(this).data('title');
        $(this).parent().parent().find('.dropdown-toggle').html(title2);
    });
    /*dropdown-item*/

    /*collapse*/
    $('.collapse').collapse({
        toggle: false
    });
    /*collapse*/

    $('[data-toggle="popover"]').popover();
    $(".phone").mask("+38 999 999-99-99");
    $(".styled").selectbox;
});


/*function windowSize(){
    
}*/

//$(window).load(windowSize); // при загрузке
//$(window).resize(windowSize); // при изменении размеров

/*upload-photo*/
function readURL(e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        $(reader).load(function (e) {
            $('#upload-img').attr('src', e.target.result);
        });
        reader.readAsDataURL(this.files[0]);
    }
}

$("#upload").change(readURL);
/*upload-photo*/

//my script

$('.ticket__detail_btn').click(e => {
    const $th = $(e.delegateTarget);
    $th.parents('.ticket__detail').fadeOut(100);
    $('.modal-backdrop.show').fadeOut();
});

$('.open-image-file').click(function(e){
    e.preventDefault();
    var dataId = $(this).attr('data-id');
    var csrf = $('#csrf-token').val();

    $.ajax({
        type:"post",
        cache:false,
        url:"ajax-user-ticket",
        data:{ id: dataId,'_token':csrf},
        success: function (html) {
            if(html){
                $('#modal_ticket').html(html)
                $('#modal_ticket').modal('show')
            }
        }
    });
})

const fakeAPI = 'http://elittrade.loc/add-order-ticket';
if (document.getElementById('form_buy_ticket_standart')) {
    $('#form_buy_ticket_standart').validate({
        ignore: [],
        errorElement: 'error',
        rules: {
            checkbox_standart: {
                required: true,
            },
            select_ticket_buy_standart: {
                required: true,
            },
        },
        messages: {
            checkbox_standart: {},
            select_ticket_buy_standart: {},
        },
        submitHandler: form => {
            $.ajax({
                type: 'post',
                url: fakeAPI,
                data: $(form).serialize()
            })
                .done((response) => {
                         if(response){
                             if (response.status == 'success') {
                                 //$('#download_pdf_btn_thx a').attr('href','/'+response.id+'/ticket-pdf');
                                 $('#modal_ticket_buy_standart').modal('hide');
                                 $('#modal_ticket_buy_thx').modal('show');
                                 $('.ticket__checkbox input').prop('checked', false)
                                 $('.ticket__checkbox').removeClass('valid').removeClass('error');
                             }else if(response.status == 'error'){
                                 alert(response.message);
                             }
                         }

                })

                .fail(response => {
                    console.log('error', response)
                });
            return false;
        },
        // add error from parents block
        highlight: function (element) {
            $(element).parent().addClass('error').removeClass('valid');
        },
        unhighlight: function (element) {
            $(element).parent().addClass('valid').removeClass('error');
        }
    });
}
if (document.getElementById('form_buy_ticket_vip')) {
    $('#form_buy_ticket_vip').validate({
        ignore: [],
        errorElement: 'error',
        rules: {
            checkbox_vip: {
                required: true,
            },
            select_ticket_buy_vip: {
                required: true,
            },
        },
        messages: {
            checkbox_vip: {},
            select_ticket_buy_vip: {},
        },
        submitHandler: form => {
            $.ajax({
                type: 'post',
                url: fakeAPI,
                data: $(form).serialize()
            })
                .done(function (response) {
                    if(response){
                        if (response.status == 'success') {
                            //$('#download_pdf_btn_thx a').attr('href','/'+response.id+'/ticket-pdf');
                            $('#download_pdf_btn_thx a').attr('href','/my-ticket');
                            $('#modal_ticket_buy_vip').modal('hide');
                            $('#modal_ticket_buy_thx').modal('show');
                            $('.ticket__checkbox input').prop('checked', false)
                            $('.ticket__checkbox').removeClass('valid').removeClass('error');
                        }else if(response.status == 'error'){
                            alert(response.message);
                        }
                    }

                })
                .fail(response => {
                    console.log('error', response)
                });
            return false;
        },
        // add error from parents block
        highlight: function (element) {
            $(element).parent().addClass('error').removeClass('valid');
        },
        unhighlight: function (element) {
            $(element).parent().addClass('valid').removeClass('error');
        }
    });
}
if (document.getElementById('form_buy_ticket_elite')) {
    $('#form_buy_ticket_elite').validate({
        errorElement: 'error',
        ignore: [],
        rules: {
            checkbox_elite: {
                required: true,
            },
            select_ticket_buy_elite: {
                required: true,
            },
        },
        messages: {
            checkbox_elite: {},
            select_ticket_buy_elite: {},
        },
        submitHandler: form => {
            $.ajax({
                type: 'post',
                url: fakeAPI,
                data: $(form).serialize()
            })
                .done((response) => {
                        if(response){
                            if (response.status == 'success') {
                                //$('#download_pdf_btn_thx a').attr('href','/'+response.id+'/ticket-pdf');
                                $('#download_pdf_btn_thx a').attr('href','/my-ticket');
                                $('#modal_ticket_buy_elite').modal('hide');
                                $('#modal_ticket_buy_thx').modal('show');
                                $('.ticket__checkbox input').prop('checked', false)
                                $('.ticket__checkbox').removeClass('valid').removeClass('error');
                            }else if(response.status == 'error'){
                                alert(response.message);
                            }
                        }
                })
                .fail(response => {
                    console.log('error', response)
                });

            return false;
        },
        // add error from parents block
        highlight: function (element) {
            $(element).parent().addClass('error').removeClass('valid');
        },
        unhighlight: function (element) {
            $(element).parent().addClass('valid').removeClass('error');
        }
    });
}
//add src from download pdf
$('.ticket__buy .ticket__detail-bottom_btn').click(e => {
    const $th = $(e.delegateTarget);
    let srcPdf = $th.attr('data-pdf');
    $('#download_pdf_btn_thx a').attr('href', srcPdf)
});
