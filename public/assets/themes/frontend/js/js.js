$(window).on("load",function(){
    //Preloader
	setTimeout("$('.page-loader div').fadeOut();",200);
	setTimeout("$('.page-loader').delay(200).fadeOut('slow');",200);
	setTimeout("jQuery('.preloader_hide, .selector_open').animate({'opacity' : '1'},500)",200);
});



$(function(){
    /*copylink*/
    $('.copylink').click(function() {
        $('#link')[0].select(); 
        document.execCommand('copy');
        $('#link').append(' ');
        $('#link').val().slice(0, -1);
    });
    /*copylink*/
    
    /*menu button*/    
    $('.menu-button').click(function(){
        $(this).toggleClass('open');
        $('.menu').toggleClass('open');
    });
    /**menu button*/
    
    /*dropdown-item*/
    $('.dropdown-item-lang').click(function(){
        var lang = $(this).data('lang');
        $(this).parent().parent().find('.dropdown-toggle').find('span').removeClass();
        $(this).parent().parent().find('.dropdown-toggle').find('span').addClass('flag-icon ' + lang);
    });
    $('.dropdown-wallet-menu__item').click(function(){
        var class1 = $(this).data('class');
        var title = $(this).data('title');
        var price = $(this).data('price');
        $(this).parent().parent().find('.dropdown-toggle').find('.wallet-menu__item__title').find('b').html(title);
        $(this).parent().parent().find('.dropdown-toggle').find('.wallet-menu__item__amount').html(price);
        $(this).parent().parent().find('.dropdown-toggle').removeClass('comis');
        $(this).parent().parent().find('.dropdown-toggle').removeClass('trade');
        $(this).parent().parent().find('.dropdown-toggle').removeClass('founds');
        $(this).parent().parent().find('.dropdown-toggle').removeClass('total');
        $(this).parent().parent().find('.dropdown-toggle').addClass(class1);
    });
    $('.dropdown-left-menu__item').click(function(){
        var title2 = $(this).data('title');
        $(this).parent().parent().find('.dropdown-toggle').html(title2);
    });
    /*dropdown-item*/
    
    /*collapse*/
    $('.collapse').collapse({
        toggle: false
    });
    /*collapse*/
    
    $('[data-toggle="popover"]').popover(); 
    // $(".phone").mask("+9999999999999999");
    $(".styled").selectbox;
});


/*function windowSize(){
    
}*/
    
//$(window).load(windowSize); // при загрузке
//$(window).resize(windowSize); // при изменении размеров

/*upload-photo*/
        function readURL(e) {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                $(reader).load(function(e) { 
                    $('#upload-img').attr('src', e.target.result); 
                });
                reader.readAsDataURL(this.files[0]);
            }
        }
        $("#upload").change(readURL);
/*upload-photo*/