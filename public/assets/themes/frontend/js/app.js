var referralsLoader, _EliteAiTrade;
_EliteAiTrade.ajax = _EliteAiTrade.ajax || {}, _EliteAiTrade.ajax.processError = function (e) {
    return e = e.responseJSON, e.errors || ("" === e.message || void 0 === e.message ? flashMessage(_EliteAiTrade.lang.internalError, "error") : flashMessage(e.message, "error")), e.errors && $.each(e.errors, function (e, t) {
        return $.each(t, function (e, t) {
            return flashMessage(t, "error")
        })
    }), void 0 !== e.redirect ? setTimeout(function () {
        return window.location.href = e.redirect
    }, 2e3) : void 0
}, $(window).on("load", function () {
    return console.log("init ajax")
}), _EliteAiTrade = _EliteAiTrade || {}, _EliteAiTrade.stopLazyLoad = !1, _EliteAiTrade.routes = _EliteAiTrade.routes || {}, _EliteAiTrade.isSafariBrowser = function () {
    var e;
    return e = window.navigator.vendor, e.indexOf("Apple") > -1 && window.navigator.userAgent.indexOf("Safari") > -1
}, $(window).on("load", function () {
    var e;
    return e = new ClipboardJS(".btn-mini.copy"), e.on("success", function () {
        flashMessage(_EliteAiTrade.lang.copied, "success")
    }), console.log("init app")
}), _EliteAiTrade.auth = _EliteAiTrade.auth || {}, _EliteAiTrade.auth.$authForm = $(".auth-form"), _EliteAiTrade.auth.initAuthForm = function () {
    return $(document).on("click", ".auth-form-submit", function (e) {
        var t, r;
        $('.alertify-logs').html('');
        return e.preventDefault(), t = $(this).closest("form"), r = _EliteAiTrade.form.getData(t), $.ajax({
            method: "post",
            url: t.attr("action"),
            dataType: "json",
            data: r,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return e.message && flashMessage(e.message, e.status), "success" !== e.status ? !1 : (t.closest(".modal").find(".close").click(), "show-two-factor" === e.action ? ($(".two-factor-login-modal").modal("show"), void 0) : ("redirect" === e.action && (window.location.href = e.redirect), !1))
            }
        }), !1
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.auth.$authForm.length ? (_EliteAiTrade.auth.initAuthForm(), _EliteAiTrade.auth.showPasswordForm && $(".password-modal").modal("show"), console.log("init auth")) : void 0
}), _EliteAiTrade.binaryTree = _EliteAiTrade.binaryTree || {}, _EliteAiTrade.binaryTree.$block = $(".binary-tree-block"), _EliteAiTrade.binaryTree.$navigation = $(".binary-tree-navigation"), _EliteAiTrade.binaryTree.history = [], _EliteAiTrade.binaryTree.get = function (e) {
    return $.ajax({
        method: "get", url: e, dataType: "json", error: function (e) {
            return _EliteAiTrade.binaryTree.$block.fadeIn(), _EliteAiTrade.ajax.processError(e)
        }
    })
}, _EliteAiTrade.binaryTree.init = function () {
    return $(document).on("click", "a.one-people", function (e) {
        var t, r;
        return e.preventDefault(), t = $(this), r = t.attr("href"), "" === r ? !1 : (_EliteAiTrade.binaryTree.$block.fadeOut(), $.when(_EliteAiTrade.binaryTree.get(r)).then(function (e) {
            return e.html && _EliteAiTrade.binaryTree.$block.find(".main-user").html(e.html), _EliteAiTrade.binaryTree.$block.fadeIn(), _EliteAiTrade.binaryTree.history.push(r), _EliteAiTrade.binaryTree.$navigation.find(".binary-navigation-button").show(), !1
        }), !1)
    }), $(document).on("click", ".binary-back a", function (e) {
        var t;
        return e.preventDefault(), 1 === _EliteAiTrade.binaryTree.history.length ? (_EliteAiTrade.binaryTree.$navigation.find(".binary-home a").click(), !1) : (t = _EliteAiTrade.binaryTree.history[_EliteAiTrade.binaryTree.history.length - 2], _EliteAiTrade.binaryTree.history.pop(), _EliteAiTrade.binaryTree.$block.fadeOut(), $.when(_EliteAiTrade.binaryTree.get(t)).then(function (e) {
            return e.html && _EliteAiTrade.binaryTree.$block.find(".main-user").html(e.html), _EliteAiTrade.binaryTree.$block.fadeIn(), 0 === _EliteAiTrade.binaryTree.history.length && _EliteAiTrade.binaryTree.$navigation.find(".binary-navigation-button").hide(), !1
        }), !1)
    }), $(document).on("click", ".binary-home a", function (e) {
        return e.preventDefault(), _EliteAiTrade.binaryTree.$block.fadeOut(), $.when(_EliteAiTrade.binaryTree.get($(this).attr("href"))).then(function (e) {
            return e.html && _EliteAiTrade.binaryTree.$block.find(".main-user").html(e.html), _EliteAiTrade.binaryTree.$block.fadeIn(), _EliteAiTrade.binaryTree.history = [], _EliteAiTrade.binaryTree.$navigation.find(".binary-navigation-button").hide(), !1
        }), !1
    }), $(document).on("click", ".binary-volume-filter", function () {
        var e;
        return e = $(this).val() + "-volume", _EliteAiTrade.binaryTree.$block.attr("data-volume", e)
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.binaryTree.$block.length > 0 ? (_EliteAiTrade.binaryTree.init(), console.log("init binary tree")) : void 0
}), _EliteAiTrade.bot = _EliteAiTrade.bot || {}, _EliteAiTrade.bot.$modalButton = $(".run-bot"), _EliteAiTrade.bot.$button = $(".run-bot-button"), _EliteAiTrade.bot.$modal = $(".run-bot-modal"), _EliteAiTrade.bot.init = function () {
    return _EliteAiTrade.bot.$button.on("click", function (e) {
        var t;
        return e.preventDefault(), t = {_token: _EliteAiTrade.bot.$button.attr("data-_token")}, $.ajax({
            method: "post",
            url: _EliteAiTrade.bot.$button.attr("href"),
            dataType: "json",
            data: t,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return e.message && flashMessage(e.message, e.status), "success" === e.status && (_EliteAiTrade.bot.$modalButton.addClass("spin").text(_EliteAiTrade.lang.running).removeAttr("data-toggle").removeAttr("data-target"), _EliteAiTrade.bot.$modal.find(".close").click()), !1
            }
        }), !1
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.bot.$button.hasClass(".spin") ? void 0 : (_EliteAiTrade.bot.init(), console.log("init bot"))
}), _EliteAiTrade.currencyChart = _EliteAiTrade.currencyChart || {}, _EliteAiTrade.currencyChart.chart = ".chart svg", _EliteAiTrade.currencyChart.$chart = $(_EliteAiTrade.currencyChart.chart), _EliteAiTrade.currencyChart.$loader = $(".chart .spiner"), _EliteAiTrade.currencyChart.$tooltip = $(".currency-tooltip"), _EliteAiTrade.currencyChart.width = parseFloat(_EliteAiTrade.currencyChart.$chart.width()), _EliteAiTrade.currencyChart.height = parseFloat(_EliteAiTrade.currencyChart.$chart.height()), _EliteAiTrade.currencyChart.xTiks = 7, _EliteAiTrade.currencyChart.initXScaleFormats = function () {
    return _EliteAiTrade.formatSecond = d3.timeFormat("%M:%S"), _EliteAiTrade.formatMinute = d3.timeFormat("%H:%M"), _EliteAiTrade.formatHour = d3.timeFormat("%H:%M"), _EliteAiTrade.formatDay = d3.timeFormat("%a %d"), _EliteAiTrade.formatWeek = d3.timeFormat("%a %d"), _EliteAiTrade.formatMonth = d3.timeFormat("%b %d")
}, _EliteAiTrade.currencyChart.xFormat = function (e) {
    return d3.timeMinute(e) < e ? _EliteAiTrade.formatSecond(e) : d3.timeHour(e) < e ? _EliteAiTrade.formatMinute(e) : d3.timeDay(e) < e ? _EliteAiTrade.formatHour(e) : d3.timeMonth(e) < e ? d3.timeWeek(e) < e ? _EliteAiTrade.formatDay(e) : _EliteAiTrade.formatWeek(e) : d3.timeYear(e) < e ? _EliteAiTrade.formatMonth(e) : void 0
}, _EliteAiTrade.currencyChart.padding = {
    left: 0,
    right: 0,
    top: 5,
    bottom: 35
}, _EliteAiTrade.currencyChart.initSize = function () {
    return _EliteAiTrade.currencyChart.xScale = d3.scaleTime().range([_EliteAiTrade.currencyChart.padding.left, _EliteAiTrade.currencyChart.width - _EliteAiTrade.currencyChart.padding.left]), _EliteAiTrade.currencyChart.yScale = d3.scaleLinear().range([_EliteAiTrade.currencyChart.height - _EliteAiTrade.currencyChart.padding.bottom - _EliteAiTrade.currencyChart.padding.top, _EliteAiTrade.currencyChart.padding.top])
}, _EliteAiTrade.currencyChart.prepareItem = function (e) {
    var t;
    return t = d3.timeParse("%Y-%m-%d %H:%M:%S"), e.date = t(e.date), e
}, _EliteAiTrade.currencyChart.initCurrencyRatePoints = function () {
    return $(document).on("mouseover", ".rate", function () {
        var e;
        return e = $(this), e.attr("r", 5), e.attr("fill", "#000000"), _EliteAiTrade.currencyChart.$tooltip.find(".dealing-rate").text(e.attr("data-dealing_rate")), _EliteAiTrade.currencyChart.$tooltip.css({
            top: e.attr("cy") - parseInt(_EliteAiTrade.currencyChart.$tooltip.outerHeight() + 20) + "px",
            left: e.attr("cx") - parseInt(_EliteAiTrade.currencyChart.$tooltip.outerWidth()) / 2 + "px"
        }), _EliteAiTrade.currencyChart.$currencyLine.attr("y1", e.attr("cy") + "px").attr("x2", _EliteAiTrade.currencyChart.width + "px").attr("y2", e.attr("cy") + "px"), _EliteAiTrade.currencyChart.$currencyLine.show(), _EliteAiTrade.currencyChart.$tooltip.show()
    }), $(document).on("mouseleave", ".rate", function () {
        var e;
        return e = $(this), e.attr("r", 1), e.attr("fill", "transparent"), _EliteAiTrade.currencyChart.$tooltip.hide(), _EliteAiTrade.currencyChart.$currencyLine.hide()
    })
}, _EliteAiTrade.currencyChart.prepareData = function (e) {
    return $.each(e, function (t, r) {
        return e[t] = _EliteAiTrade.currencyChart.prepareItem(r)
    }), e
}, _EliteAiTrade.currencyChart.update = function (e) {
    var t;
    return $.each(e, function (e, t) {
        return _EliteAiTrade.currencyChart.data.push(_EliteAiTrade.currencyChart.prepareItem(t))
    }), _EliteAiTrade.currencyChart.data.splice(0, e.length), _EliteAiTrade.currencyChart.xScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, function (e) {
        return e.date
    })), _EliteAiTrade.currencyChart.yScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, function (e) {
        return e.rate
    })), t = d3.select(_EliteAiTrade.currencyChart.chart), t.select(".line").attr("d", _EliteAiTrade.currencyChart.line(_EliteAiTrade.currencyChart.data)), t.select(".area").attr("d", _EliteAiTrade.currencyChart.area(_EliteAiTrade.currencyChart.data)), _EliteAiTrade.currencyChart.$chart.find(".rate").remove(), t.selectAll(".rate").data(_EliteAiTrade.currencyChart.data).enter().append("circle").attr("fill", "transparent").attr("stroke", "transparent").attr("cx", function (e) {
        return _EliteAiTrade.currencyChart.xScale(e.date)
    }).attr("cy", function (e) {
        return _EliteAiTrade.currencyChart.yScale(e.rate)
    }).attr("r", function () {
        return 2
    }).attr("class", function () {
        return "rate"
    }).attr("data-dealing_rate", function (e) {
        return "$ " + _EliteAiTrade.helpers.format(e.rate)
    }), t.select(".axis-x").attr("stroke-width", 0).attr("transform", "translate(0," + parseInt(_EliteAiTrade.currencyChart.height) + ")").call(d3.axisBottom(_EliteAiTrade.currencyChart.xScale).ticks(_EliteAiTrade.currencyChart.xTiks).tickFormat(_EliteAiTrade.currencyChart.xFormat)).selectAll("text").attr("transform", "translate(0, -25)"), t.select(".axis-y").attr("stroke-width", 0).attr("transform", "translate(0, 0)").call(d3.axisLeft(_EliteAiTrade.currencyChart.yScale).tickFormat(function (e) {
        return "$ " + _EliteAiTrade.helpers.format(e)
    }).tickPadding(8).tickSize(-_EliteAiTrade.currencyChart.width)).selectAll("text").attr("transform", "translate(80, -15)"), _EliteAiTrade.currencyChart.initCurrencyRatePoints()
}, _EliteAiTrade.currencyChart.initChart = function () {
    var e, t;
    return _EliteAiTrade.currencyChart.data = _EliteAiTrade.currencyChart.prepareData(_EliteAiTrade.currencyChart.data), t = d3.select(_EliteAiTrade.currencyChart.chart), e = t.append("g"), _EliteAiTrade.currencyChart.initSize(), _EliteAiTrade.currencyChart.area = d3.area().x(function (e) {
        return _EliteAiTrade.currencyChart.xScale(e.date)
    }).y1(function (e) {
        return _EliteAiTrade.currencyChart.yScale(e.rate)
    }).curve(d3.curveCardinal.tension(1)), _EliteAiTrade.currencyChart.line = d3.line().x(function (e) {
        return _EliteAiTrade.currencyChart.xScale(e.date)
    }).y(function (e) {
        return _EliteAiTrade.currencyChart.yScale(e.rate)
    }).curve(d3.curveCardinal.tension(1)), _EliteAiTrade.currencyChart.area.y0(_EliteAiTrade.currencyChart.yScale(0)), _EliteAiTrade.currencyChart.xScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, function (e) {
        return e.date
    })), _EliteAiTrade.currencyChart.yScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, function (e) {
        return e.rate
    })), e.append("path").datum(_EliteAiTrade.currencyChart.data).attr("class", "area").attr("fill", "#F5F5F5").attr("stroke", "transparent").attr("stroke-width", 0).attr("d", _EliteAiTrade.currencyChart.area), e.append("path").datum(_EliteAiTrade.currencyChart.data).attr("class", "line").attr("fill", "none").attr("stroke", "#DBDDDC").attr("stroke-width", 1.5).attr("d", _EliteAiTrade.currencyChart.line), t.append("line").attr("class", "currency-line").attr("x1", 0).attr("y1", 0).attr("x2", 0).attr("y2", 0), _EliteAiTrade.currencyChart.$chart.find(".rate").remove(), t.selectAll(".rate").data(_EliteAiTrade.currencyChart.data).enter().append("circle").attr("fill", "transparent").attr("stroke", "transparent").attr("cx", function (e) {
        return _EliteAiTrade.currencyChart.xScale(e.date)
    }).attr("cy", function (e) {
        return _EliteAiTrade.currencyChart.yScale(e.rate)
    }).attr("r", function () {
        return 2
    }).attr("class", function () {
        return "rate"
    }).attr("data-dealing_rate", function (e) {
        return "$ " + _EliteAiTrade.helpers.format(e.rate)
    }), e.append("g").attr("class", "axis axis-x").attr("stroke-width", 0).attr("transform", "translate(0," + parseInt(_EliteAiTrade.currencyChart.height) + ")").call(d3.axisBottom(_EliteAiTrade.currencyChart.xScale).ticks(_EliteAiTrade.currencyChart.xTiks).tickFormat(_EliteAiTrade.currencyChart.xFormat)).selectAll("text").attr("transform", "translate(0, -25)"), e.append("g").attr("class", "axis axis-y").attr("stroke-width", 0).attr("transform", "translate(0, 0)").call(d3.axisLeft(_EliteAiTrade.currencyChart.yScale).tickFormat(function (e) {
        return "$ " + _EliteAiTrade.helpers.format(e)
    }).tickPadding(8).tickSize(-_EliteAiTrade.currencyChart.width)).selectAll("text").attr("transform", "translate(80, -15)"), _EliteAiTrade.currencyChart.$loader.hide(), _EliteAiTrade.currencyChart.initCurrencyRatePoints(), _EliteAiTrade.currencyChart.$currencyLine = _EliteAiTrade.currencyChart.$chart.find(".currency-line"), setInterval(function () {
        return $.ajax({
            method: "get",
            url: _EliteAiTrade.currencyChart.$chart.attr("data-update_url"),
            dataType: "json",
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return e.length ? _EliteAiTrade.currencyChart.update(e) : void 0
            }
        })
    }, parseInt(1e3 * _EliteAiTrade.currencyChart.updateTimeout))
}, $(window).on("load", function () {
    return _EliteAiTrade.currencyChart.$chart.length ? (_EliteAiTrade.currencyChart.initXScaleFormats(), _EliteAiTrade.currencyChart.initChart(), $(window).resize(function () {
        return _EliteAiTrade.currencyChart.width = parseFloat(_EliteAiTrade.currencyChart.$chart.closest(".chart").width() / 100 * 99), _EliteAiTrade.currencyChart.height = parseFloat(_EliteAiTrade.currencyChart.$chart.closest(".chart").height() / 100 * 99), _EliteAiTrade.currencyChart.initSize(), _EliteAiTrade.currencyChart.update([])
    }), console.log("init trades")) : void 0
}), _EliteAiTrade.contract = _EliteAiTrade.contract || {}, _EliteAiTrade.contract.blocked = !1, _EliteAiTrade.contract.$modal = $("#modal_new_contract"), _EliteAiTrade.contract.$button = _EliteAiTrade.contract.$modal.find('[type="submit"]'), _EliteAiTrade.contract.reinvestButton = ".contract-reinvest-button", _EliteAiTrade.contract.contractPriceBtc = $(".contract-price-btc span"), _EliteAiTrade.contract.contractTotal = $(".contract-total span"), _EliteAiTrade.contract._initAmountField = function () {
    return _EliteAiTrade.contract.$modal.find('[name="amount"]').on("keyup", function (e) {
        var t, r, a, i, n;
        return e.preventDefault(), t = $(this), r = t.val(), r = "" === r ? 0 : parseFloat(r), a = "0.0", $.each(_EliteAiTrade.contract.levels, function (e, t) {
            var i, n;
            return i = parseFloat(t.contract_amount_from), n = parseFloat(t.contract_amount_to), r >= i && r <= t.contract_amount_to && (a = t.contract_price), r >= i && 0 === n ? a = t.contract_price : void 0
        }), i = a / _EliteAiTrade.contract.currencyRate, _EliteAiTrade.contract.contractPriceBtc.text(a + " = " + _EliteAiTrade.helpers.format(i, 8)), r > 0 ? (n = r + i, n += "", n = n.split("."), n = n[0] + "." + n[1].substring(0, 8)) : n = 0, _EliteAiTrade.contract.contractTotal.text(n), !1
    })
}, _EliteAiTrade.contract._initFormSubmit = function () {
    return _EliteAiTrade.contract.$button.on("click", function (e) {
        var t, r, a;
        if (_EliteAiTrade.contract.blocked !== !0) return _EliteAiTrade.contract.blocked = !0, r = $(this), r.attr("disabled", "disabled"), e.preventDefault(), t = _EliteAiTrade.contract.$modal.find("form"), a = _EliteAiTrade.form.getData(t), $.ajax({
            method: "post",
            url: t.attr("action"),
            dataType: "json",
            data: a,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e), _EliteAiTrade.contract.blocked = !1, r.removeAttr("disabled")
            },
            success: function (e) {
                return _EliteAiTrade.contract.blocked = !1, r.removeAttr("disabled"), e.message && flashMessage(e.message, e.status), "success" === e.status && (_EliteAiTrade.contract.$modal.find(".close").click(), window.location.reload()), !1
            }
        }), !1
    })
}, _EliteAiTrade.contract._initReinvest = function () {
    return $(document).on("click", _EliteAiTrade.contract.reinvestButton, function (e) {
        var t, r, a;
        return e.preventDefault(), t = $(this), r = t.closest("form"), a = _EliteAiTrade.form.getData(r), $.ajax({
            method: "post",
            url: r.attr("action"),
            dataType: "json",
            data: a,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return e.message && flashMessage(e.message, e.status), "success" === e.status && t.closest(".contract-actions").html(e.html), !1
            }
        }), !1
    })
}, _EliteAiTrade.contract.init = function () {
    return _EliteAiTrade.contract._initAmountField(), _EliteAiTrade.contract._initFormSubmit(), _EliteAiTrade.contract._initReinvest()
}, $(window).on("load", function () {
    return _EliteAiTrade.contract.$modal.length ? (_EliteAiTrade.contract.init(), _EliteAiTrade.contract.$modal.find('[name="amount"]').keyup(), console.log("init contract")) : void 0
}), _EliteAiTrade.cookie = {}, _EliteAiTrade.cookie.get = function (e, t) {
    var r;
    return void 0 === t && (t = null), r = document.cookie.match(new RegExp(e + "=([^;]+)")), r ? r[1] : null !== t ? t : null
}, _EliteAiTrade.cookie.set = function (e, t, r) {
    return r = r || !1, void 0 !== t && "undefined" !== t ? document.cookie = e + "=" + t + "; path=/;" + (null != r ? r : "expires=" + {time: ""}) : void 0
}, _EliteAiTrade.deposit = _EliteAiTrade.deposit || {}, _EliteAiTrade.deposit.$page = $(".deposit-page"), _EliteAiTrade.deposit.initDepositPage = function () {
    return $('[name="amount"]').on("keyup", function (e) {
        var t;
        return e.preventDefault(), t = $(this).val(), "" !== t ? (t = parseFloat(t), (0 > t || "" === t) && (t = .01), $(".amount-text").text(t), !1) : void 0
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.deposit.$page.length ? (_EliteAiTrade.deposit.initDepositPage(), console.log("init deposit")) : void 0
}), _EliteAiTrade.faq = _EliteAiTrade.faq || {}, _EliteAiTrade.faq.$block = $(".questions"), _EliteAiTrade.faq.initFaqWidget = function () {
    return $(".question").on("click", function (e) {
        var t;
        return e.preventDefault(), t = $(this), _EliteAiTrade.faq.$block.find("li").removeClass("active"), _EliteAiTrade.faq.$block.find(".answer").text(t.attr("data-text")), t.closest("li").addClass("active"), !1
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.faq.$block.length ? (_EliteAiTrade.faq.initFaqWidget(), console.log("init faq")) : void 0
}), _EliteAiTrade.files = _EliteAiTrade.files || {}, _EliteAiTrade.files.itemClass = ".user-verification-file", _EliteAiTrade.files._initFilesUpload = function () {
    return $(document).on("change", _EliteAiTrade.files.itemClass, function (e) {
        var t, r, a;
        return e.preventDefault(), r = $(this), t = r.closest("form"), a = _EliteAiTrade.form.getFormData(t), $.ajax({
            method: t.attr("method"),
            dataType: "json",
            url: t.attr("action"),
            data: a,
            processData: !1,
            contentType: !1,
            error: function (e) {
                return r.closest(".documents__item").find(".documents__item__status").removeClass("uploaded disclined").addClass("error").text(_EliteAiTrade.lang.errorUpload), _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return r.closest(".documents__item").find(".documents__item__status").removeClass("uploaded disclined error").text(""), flashMessage(e.message, e.status), "success" === e.status && t.attr("action", e.action), r.closest(".documents__item").find(".documents__item__status").addClass("uploaded").text(_EliteAiTrade.lang.uploaded), !1
            }
        }), !1
    })
}, $(window).on("load", function () {
    return $(_EliteAiTrade.files.itemClass).length ? (_EliteAiTrade.files._initFilesUpload(), console.log("init files")) : void 0
}), window.flashMessage = function (e, t, r, a) {
    var i;
    switch ("" === t && (t = "notice"), void 0 === r && (r = 8e3), void 0 === a && (a = "bottom right"), i = alertify.logPosition(a).delay(r).maxLogItems(10).closeLogOnClick(!0), t) {
        case"success":
            i.success(e);
            break;
        case"error":
            i.error(e);
            break;
        default:
            i.log(e)
    }
}, _EliteAiTrade.form = _EliteAiTrade.form || {}, _EliteAiTrade.form.getData = function (e) {
    var t;
    return t = new Object, $.each(e.serializeArray(), function (e, r) {
        return t[r.name] = r.value
    }), t
}, _EliteAiTrade.form.getFormData = function (e) {
    var t, r, a, i, n, o;
    if (_EliteAiTrade.isSafariBrowser()) for (n = {}, r = e.get(0), a = r.elements, i = void 0, t = void 0, o = 0; o < a.length;) i = $(a.item(o)), t = i.attr("type"), i.attr("name") && ("checkbox" !== t && "radio" !== t || i.prop("checked")) ? (n[i.attr("name")] = i.val(), o++) : o++; else n = new FormData, $.each(e.serializeArray(), function (e, t) {
        return n.append(t.name, t.value)
    }), $.each(e.find('[type="file"]'), function (e, t) {
        return n.append(t.name, t.files[0])
    });
    return n
}, _EliteAiTrade.helpers = _EliteAiTrade.helpers || {}, _EliteAiTrade.helpers.format = function (e, t, r, a, i) {
    var n, o, l;
    return t = t || 0, r = r || ".", a = a || " ", i = i || 3, l = Math.pow(10, t), o = "\\d(?=(\\d{" + (i || 3) + "})+" + (t > 0 ? "\\D" : "$") + ")", n = Math.round(e * l) / l, n += "", n = (r ? n.replace(".", r) : n).replace(new RegExp(o, "g"), "$&" + (a || ","))
}, $(window).on("load", function () {
    var e, t;
    return e = $(".izimodal"), t = {
        group: "izimodal",
        loop: !0,
        arrowKeys: !1,
        navigateCaption: !1,
        overlayClose: !1,
        pauseOnHover: !0
    }, e.length && e.each(function (e, r) {
        var a, i, n, o;
        return a = $(r), o = a.attr("id"), n = "ai_trade_modal_" + o, i = _EliteAiTrade.cookie.get(n), i ? (a.remove(), void 0) : a.on("click", ".close-modal", function () {
            var e;
            return e = a.find(".button-hide"), e.is(":checked") && _EliteAiTrade.cookie.set(n, !0), $(".izimodal").length > 1 ? (a.iziModal("next"), a.fadeOut(300), setTimeout(function () {
                return a.remove(), $(".izimodal").iziModal(t)
            }, 400)) : (a.fadeOut(300), setTimeout(function () {
                return a.iziModal("destroy"), $(".iziModal-navigate").remove(), $(".iziModal-overlay").remove(), a.remove()
            }, 400), void 0)
        })
    }), e = $(".izimodal"), e.length && ($(".izimodal").iziModal(t), e.first().iziModal("open")), console.log("init modals")
}), _EliteAiTrade.profile = _EliteAiTrade.profile || {}, _EliteAiTrade.profile.$legSwitcher = $('[name="active_leg"]'), _EliteAiTrade.profile.$activeLegTitle = $(".active-leg-title"), _EliteAiTrade.profile.initLegSwitcher = function () {
    return _EliteAiTrade.profile.$legSwitcher.on("change", function () {
        var e;
        return e = {
            _token: _EliteAiTrade.profile.$legSwitcher.attr("data-_token"),
            active_leg: $(this).val()
        }, $.ajax({
            method: "post",
            url: _EliteAiTrade.profile.$legSwitcher.attr("data-url"),
            dataType: "json",
            data: e,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return e.message && flashMessage(e.message, e.status), "success" === e.status && _EliteAiTrade.profile.$activeLegTitle.text(e.leg_title), !1
            }
        }), !1
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.profile.$legSwitcher.length ? (_EliteAiTrade.profile.initLegSwitcher(), console.log("init leg switcher")) : void 0
}), _EliteAiTrade.questions = _EliteAiTrade.questions || {}, _EliteAiTrade.questions.$page = $(".faq-page"), _EliteAiTrade.questions._initFaqPage = function () {
    return $(".question-category").on("click", function (e) {
        var t, r;
        return e.preventDefault(), $(".question-category").removeClass("active"), $(this).addClass("active"), r = $(this).attr("data-category"), t = $(".question-item", ".questions-list"), "all" !== r ? (t.fadeOut(0), t.filter(".category-" + r).fadeIn(0)) : t.fadeIn(0), $(".questions-list .active").removeClass("active"), !1
    }), $(".question").on("click", function (e) {
        return e.preventDefault(), $(".questions-list .active").removeClass("active"), $(this).siblings(".answer").addClass("active"), !1
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.questions.$page.length ? (_EliteAiTrade.questions._initFaqPage(), console.log("init questions")) : void 0
}), _EliteAiTrade.referrals = _EliteAiTrade.referrals || {}, _EliteAiTrade.referrals.$block = $(".referrals-items"), _EliteAiTrade.referrals.filters = ["generation", "contracts", "keywords"], _EliteAiTrade.referrals.loader = {}, _EliteAiTrade.referrals.filter = function () {
    var e, t, r, a, i;
    a = _EliteAiTrade.routes.referralsIndex + "?";
    for (t in _EliteAiTrade.referrals.filters) r = _EliteAiTrade.referrals.filters[t], t > 0 && (a += "&"), e = $('[name="' + r + '"]'), i = e.hasClass("text-filter") ? e.val() : e.filter(":checked").val(), a += r + "=" + i;
    return _EliteAiTrade.referrals.$block.attr("data-url", a), _EliteAiTrade.referrals.$block.attr("data-page", 1), _EliteAiTrade.stopLazyLoad = !0, $.ajax({
        method: "get",
        url: a,
        dataType: "json",
        error: function (e) {
            return _EliteAiTrade.stopLazyLoad = !1, _EliteAiTrade.ajax.processError(e)
        },
        success: function (e) {
            var t;
            return _EliteAiTrade.stopLazyLoad = !1, e.message && flashMessage(e.message, e.status), "" !== e.html ? (t = $(e.html).css("opacity", 0), _EliteAiTrade.referrals.$block.find(".append-list").html(t), t.slideDown(2 * _EliteAiTrade.referrals.loader.showTime).animate({opacity: 1}, _EliteAiTrade.referrals.loader.showTime)) : _EliteAiTrade.referrals.$block.find(".append-list").html(""), !1
        }
    })
}, _EliteAiTrade.referrals.init = function () {
    return _EliteAiTrade.referrals.loader = new referralsLoader(_EliteAiTrade.referrals.$block), $(".referrals-filter").on("change", function (e) {
        return e.preventDefault(), _EliteAiTrade.referrals.filter()
    }), $(".referrals-filter").on("keyup", function (e) {
        var t, r;
        return e.preventDefault(), t = $(this), r = t.val().length, setTimeout(function () {
            return r === $('[name="' + t.attr("name") + '"]').val().length ? _EliteAiTrade.referrals.filter() : void 0
        }, 1e3)
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.referrals.$block.length > 0 ? (_EliteAiTrade.referrals.init(), console.log("init referrals")) : void 0
}), _EliteAiTrade.register = _EliteAiTrade.register || {}, _EliteAiTrade.register.$button = $(".modal-register-button"), _EliteAiTrade.register.$modal = $(".modal-register"), _EliteAiTrade.register.$form = $(".register-form"), _EliteAiTrade.register.initRegisterForm = function () {
    return $(document).on("click", ".register-form-submit", function (e) {
        var t;
        $('.alertify-logs').html('');
        return e.preventDefault(), t = _EliteAiTrade.form.getData(_EliteAiTrade.register.$form), $.ajax({
            method: "post",
            url: _EliteAiTrade.register.$form.attr("action"),
            dataType: "json",
            data: t,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                var stayTime = e.hasOwnProperty('stayTime') ?  e.stayTime : 8000;
                var position = e.hasOwnProperty('position') ?  e.position : 'bottom right';
                return flashMessage(e.message, e.status, stayTime, position), "success" === e.status ? (_EliteAiTrade.register.$modal.find(".close").click(), !1) : void 0
            }
        }), !1
    })
}, $(window).on("load", function () {
    return _EliteAiTrade.register.$form.length ? (_EliteAiTrade.register.initRegisterForm(), _EliteAiTrade.register.$modal.hasClass("active") && _EliteAiTrade.register.$button.click(), console.log("init register")) : void 0
}), _EliteAiTrade.trades = _EliteAiTrade.trades || {}, _EliteAiTrade.trades.$block = $(".trades-history"),_EliteAiTrade.trades.$list = _EliteAiTrade.trades.$block.find("ul"),_EliteAiTrade.trades.$template = _EliteAiTrade.trades.$block.find(".template"),_EliteAiTrade.trades.show = function (e) {
    var t, r, a, i, n;
    return n = e[0], e.splice(0, 1), t = _EliteAiTrade.trades.$template.clone(), i = _EliteAiTrade.lang[n.operation], r = i + " " + n.amount + " " + ("sold" === n.operation ? n.currency_from : n.currency_to), a = _EliteAiTrade.lang["for"] + " " + ("sold" === n.operation ? n.currency_to : n.currency_from) + " " + n.currency_rate + " / " + ("sold" === n.operation ? n.currency_from : n.currency_to), t.find(".currency").text(r).end().find(".currency-rate").text(a).end().find(".exchange").text(n.exchange), _EliteAiTrade.trades.$list.prepend(t), t.fadeIn(300, function () {
        return e.length > 0 ? _EliteAiTrade.trades.renderTrades(e) : void 0
    })
},_EliteAiTrade.trades.renderTrades = function (e) {
    var t;
    return _EliteAiTrade.trades.$list.find("li").length >= 5 ? (t = _EliteAiTrade.trades.$list.find("li:last-child"), t.fadeOut(300, function () {
        return t.remove(), _EliteAiTrade.trades.show(e)
    })) : _EliteAiTrade.trades.show(e)
},_EliteAiTrade.trades.initTradesHistory = function () {
    return setInterval(function () {
        return $.ajax({
            method: "get",
            dataType: "json",
            url: _EliteAiTrade.trades.$block.attr("data-url"),
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return "success" === e.status && e.trades.length ? _EliteAiTrade.trades.renderTrades(e.trades) : void 0
            }
        })
    }, 1e3 * _EliteAiTrade.trades.updateTimeout)
},$(window).on("load", function () {
    return _EliteAiTrade.trades.$block.length > 0 ? (_EliteAiTrade.trades.initTradesHistory(), console.log("init trades")) : void 0
}),_EliteAiTrade.transaction = _EliteAiTrade.transaction || {},_EliteAiTrade.transaction.$block = $(".finances-table"),_EliteAiTrade.transaction.$table = _EliteAiTrade.transaction.$block.find(".table"),_EliteAiTrade.transaction.$button = _EliteAiTrade.transaction.$block.find(".show-more"),_EliteAiTrade.transaction.page = 1,_EliteAiTrade.transaction.busy = !1,_EliteAiTrade.transaction.initTransactionTable = function () {
    return _EliteAiTrade.transaction.$button.on("click", function (e) {
        return e.preventDefault(), _EliteAiTrade.transaction.busy ? !1 : (_EliteAiTrade.transaction.busy || (_EliteAiTrade.transaction.busy = !0), $.ajax({
            method: "get",
            dataType: "json",
            data: {page: _EliteAiTrade.transaction.page + 1},
            error: function (e) {
                return _EliteAiTrade.transaction.busy = !1, _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return _EliteAiTrade.transaction.page += 1, "success" === e.status && _EliteAiTrade.transaction.$table.append(e.html), "" !== e.html ? _EliteAiTrade.transaction.busy = !1 : _EliteAiTrade.transaction.$button.hide(), !1
            }
        }), !1)
    })
},$(window).on("load", function () {
    return _EliteAiTrade.transaction.$table.length ? (_EliteAiTrade.transaction.initTransactionTable(), console.log("init transaction")) : void 0
}),_EliteAiTrade.userTransfer = _EliteAiTrade.userTransfer || {},_EliteAiTrade.contract.userTransfer = !1,_EliteAiTrade.userTransfer.$page = $(".user-transfer-page"),_EliteAiTrade.userTransfer.$form = _EliteAiTrade.userTransfer.$page.find("form"),_EliteAiTrade.userTransfer.$walletSelect = _EliteAiTrade.userTransfer.$page.find('[name="wallet_id"]'),_EliteAiTrade.userTransfer.$amountField = _EliteAiTrade.userTransfer.$page.find('[name="amount"]'),_EliteAiTrade.userTransfer.$dollarAmount = _EliteAiTrade.userTransfer.$page.find(".dollar-amount"),_EliteAiTrade.userTransfer.dealingRate = parseFloat(_EliteAiTrade.userTransfer.$dollarAmount.attr("data-dealing_rate")),_EliteAiTrade.userTransfer.$modalButton = _EliteAiTrade.userTransfer.$page.find(".user-transfer-modal-button"),_EliteAiTrade.userTransfer.$modal = $("#modal_confirm"),_EliteAiTrade.userTransfer.$walletBalance = _EliteAiTrade.userTransfer.$page.find(".calculator .wallet-balance"),_EliteAiTrade.userTransfer.$transferAmount = _EliteAiTrade.userTransfer.$page.find(".calculator .transfer-amount"),_EliteAiTrade.userTransfer.$transferCommission = _EliteAiTrade.userTransfer.$page.find(".calculator .transfer-commission"),_EliteAiTrade.userTransfer.$totalAmount = _EliteAiTrade.userTransfer.$page.find(".calculator .total-amount"),_EliteAiTrade.userTransfer.calculate = function (e) {
    var t, r;
    return _EliteAiTrade.userTransfer.$transferAmount.text(e), t = e / 100 * _EliteAiTrade.userTransfer.$transferCommission.attr("data-commission"), _EliteAiTrade.userTransfer.$transferCommission.text(t.toFixed(8)), r = parseFloat(e) + t, _EliteAiTrade.userTransfer.$totalAmount.text(r.toFixed(8))
},_EliteAiTrade.userTransfer.initWithdrawPage = function () {
    return _EliteAiTrade.userTransfer.$walletSelect.on("change", function () {
        return _EliteAiTrade.userTransfer.$walletBalance.text($(this).find("option:selected").attr("data-balance"))
    }), _EliteAiTrade.userTransfer.$amountField.on("keyup", function (e) {
        var t, r;
        return e.preventDefault(), r = $(this).val(), "" === r ? (_EliteAiTrade.userTransfer.$dollarAmount.text("= 0 $"), void 0) : (r = parseFloat(r), 0 > r || "" === r ? (_EliteAiTrade.userTransfer.$dollarAmount.text("= 0 $"), void 0) : (t = _EliteAiTrade.userTransfer.dealingRate * r, t = 100 > t ? _EliteAiTrade.helpers.format(t, 2) : _EliteAiTrade.helpers.format(t, 0), _EliteAiTrade.userTransfer.$dollarAmount.text("= " + t + " $"), _EliteAiTrade.userTransfer.calculate(r), !1))
    }), _EliteAiTrade.userTransfer.$form.on("click", ".submit-user-transfer-form", function (e) {
        var t, r;
        return e.preventDefault(), _EliteAiTrade.userTransfer.blocked !== !0 ? (t = $(this), _EliteAiTrade.userTransfer.blocked = !0, t.attr("disabled", "disabled"), r = _EliteAiTrade.form.getData(_EliteAiTrade.userTransfer.$form), $.ajax({
            method: "post",
            url: _EliteAiTrade.userTransfer.$form.attr("action"),
            dataType: "json",
            data: r,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e), _EliteAiTrade.userTransfer.blocked = !1, t.removeAttr("disabled")
            },
            success: function (e) {
                return _EliteAiTrade.userTransfer.blocked = !1, t.removeAttr("disabled"), e.message && flashMessage(e.message, e.status), "success" === e.status ? (_EliteAiTrade.userTransfer.$modal.find("[name='wallet_id']").val(e.wallet_id), _EliteAiTrade.userTransfer.$modal.find("[name='user']").val(e.user), _EliteAiTrade.userTransfer.$modal.find("[name='amount']").val(e.amount), _EliteAiTrade.userTransfer.$modal.find("[name='_token']").val(e._token), _EliteAiTrade.userTransfer.$modalButton.click(), !1) : void 0
            }
        }), !1) : void 0
    }), _EliteAiTrade.userTransfer.$modal.on("click", ".submit-confirm-form", function (e) {
        var t, r, a;
        return e.preventDefault(), _EliteAiTrade.userTransfer.blocked !== !0 ? (r = $(this), _EliteAiTrade.userTransfer.blocked = !0, r.attr("disabled", "disabled"), t = _EliteAiTrade.userTransfer.$modal.find("form"), a = _EliteAiTrade.form.getData(t), $.ajax({
            method: "post", url: t.attr("action"), dataType: "json", data: a, error: function (e) {
                return _EliteAiTrade.ajax.processError(e), _EliteAiTrade.userTransfer.blocked = !1, r.removeAttr("disabled")
            }, success: function (e) {
                return _EliteAiTrade.userTransfer.blocked = !1, r.removeAttr("disabled"), e.message && flashMessage(e.message, e.status), "success" === e.status && (_EliteAiTrade.userTransfer.$modal.find(".close").click(), setTimeout(function () {
                    return window.location.reload()
                }, 1e3)), !1
            }
        }), !1) : void 0
    })
},$(window).on("load", function () {
    return _EliteAiTrade.userTransfer.$page.length ? (_EliteAiTrade.userTransfer.initWithdrawPage(), console.log("init user transfer")) : void 0
}),_EliteAiTrade.verification = _EliteAiTrade.verification || {},_EliteAiTrade.verification.$page = $(".verification-page"),_EliteAiTrade.verification.$form = $("form.verification-form"),_EliteAiTrade.verification.$2FaForm = $("form.verification-2fa-form"),_EliteAiTrade.verification.submitButton = ".verification-form-submit",_EliteAiTrade.verification.initVerification = function () {
    return $(".verification-2fa-form").on("submit", function (e) {
        return e.preventDefault(), $(_EliteAiTrade.verification.submitButton).click(), !1
    }), $(document).on("click", _EliteAiTrade.verification.submitButton, function (e) {
        var t, r;
        return e.preventDefault(), t = _EliteAiTrade.form.getData(_EliteAiTrade.verification.$form), r = _EliteAiTrade.form.getData(_EliteAiTrade.verification.$2FaForm), $.each(r, function (e, r) {
            return t[e] = r
        }), $.ajax({
            method: "post",
            url: _EliteAiTrade.verification.$form.attr("action"),
            dataType: "json",
            data: t,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e)
            },
            success: function (e) {
                return e.message && flashMessage(e.message, e.status), "success" === e.status && window.location.reload(), !1
            }
        }), !1
    })
},$(window).on("load", function () {
    return _EliteAiTrade.verification.$page.length ? (_EliteAiTrade.verification.initVerification(), console.log("init verification")) : void 0
}),$(window).on("load", function () {
    var e;
    return e = $(".video-modal"), $(document).on("click", ".video-item", function () {
        var t, r, a;
        return a = $(this).attr("data-src"), r = $('<iframe src="' + a + '"></iframe>'), t = e.find(".modal-body"), t.html(r), setTimeout(function () {
            return r.css({opacity: 1})
        }, 1500)
    }), e.on("hidden.bs.modal", function () {
        return e.find("iframe").remove()
    }), console.log("init videos")
}),_EliteAiTrade.withdraw = _EliteAiTrade.withdraw || {},_EliteAiTrade.withdraw.blocked = !1,_EliteAiTrade.withdraw.$page = $(".withdraw-page"),_EliteAiTrade.withdraw.$form = _EliteAiTrade.withdraw.$page.find("form"),_EliteAiTrade.withdraw.$walletSelect = _EliteAiTrade.withdraw.$page.find('[name="wallet_id"]'),_EliteAiTrade.withdraw.$amountField = _EliteAiTrade.withdraw.$page.find('[name="amount"]'),_EliteAiTrade.withdraw.$dollarAmount = _EliteAiTrade.withdraw.$page.find(".dollar-amount"),_EliteAiTrade.withdraw.dealingRate = parseFloat(_EliteAiTrade.withdraw.$dollarAmount.attr("data-dealing_rate")),_EliteAiTrade.withdraw.$modalButton = _EliteAiTrade.withdraw.$page.find(".withdraw-modal-button"),_EliteAiTrade.withdraw.$modal = $("#modal_confirm"),_EliteAiTrade.withdraw.$walletBalance = _EliteAiTrade.withdraw.$page.find(".calculator .wallet-balance"),_EliteAiTrade.withdraw.$transferAmount = _EliteAiTrade.withdraw.$page.find(".calculator .transfer-amount"),_EliteAiTrade.withdraw.$transferCommission = _EliteAiTrade.withdraw.$page.find(".calculator .transfer-commission"),_EliteAiTrade.withdraw.$totalAmount = _EliteAiTrade.withdraw.$page.find(".calculator .total-amount"),_EliteAiTrade.withdraw.calculate = function (e) {
    var t, r;
    return _EliteAiTrade.withdraw.$transferAmount.text(e), t = e / 100 * _EliteAiTrade.withdraw.$transferCommission.attr("data-commission"), _EliteAiTrade.withdraw.$transferCommission.text(t.toFixed(8)), r = parseFloat(e) + t, _EliteAiTrade.withdraw.$totalAmount.text(r.toFixed(8))
},_EliteAiTrade.withdraw.initWithdrawPage = function () {
    return _EliteAiTrade.withdraw.$walletSelect.on("change", function () {
        return _EliteAiTrade.withdraw.$walletBalance.text($(this).find("option:selected").attr("data-balance"))
    }), _EliteAiTrade.withdraw.$amountField.on("keyup", function (e) {
        var t, r;
        return e.preventDefault(), r = $(this).val(), "" === r ? (_EliteAiTrade.withdraw.$dollarAmount.text("= 0 $"), void 0) : (r = parseFloat(r), 0 > r || "" === r ? (_EliteAiTrade.withdraw.$dollarAmount.text("= 0 $"), void 0) : (t = _EliteAiTrade.withdraw.dealingRate * r, t = 100 > t ? _EliteAiTrade.helpers.format(t, 2) : _EliteAiTrade.helpers.format(t, 0), _EliteAiTrade.withdraw.$dollarAmount.text("= " + t + " $"), _EliteAiTrade.withdraw.calculate(r), !1))
    }), _EliteAiTrade.withdraw.$form.on("click", ".submit-withdraw-form", function (e) {
        var t, r;
        return e.preventDefault(), _EliteAiTrade.withdraw.blocked !== !0 ? (t = $(this), _EliteAiTrade.withdraw.blocked = !0, t.attr("disabled", "disabled"), r = _EliteAiTrade.form.getData(_EliteAiTrade.withdraw.$form), $.ajax({
            method: "post",
            url: _EliteAiTrade.withdraw.$form.attr("action"),
            dataType: "json",
            data: r,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e), _EliteAiTrade.withdraw.blocked = !1, t.removeAttr("disabled")
            },
            success: function (e) {
                return _EliteAiTrade.withdraw.blocked = !1, t.removeAttr("disabled"), e.message && flashMessage(e.message, e.status), "success" === e.status ? (_EliteAiTrade.withdraw.$modal.find("[name='id']").val(e.id), _EliteAiTrade.withdraw.$modal.find("[name='_token']").val(e._token), _EliteAiTrade.withdraw.$modalButton.click(), !1) : void 0
            }
        }), !1) : void 0
    }), _EliteAiTrade.withdraw.$modal.on("click", ".submit-confirm-form", function (e) {
        var t, r, a;
        return e.preventDefault(), _EliteAiTrade.withdraw.blocked !== !0 ? (r = $(this), _EliteAiTrade.withdraw.blocked = !0, r.attr("disabled", "disabled"), t = _EliteAiTrade.withdraw.$modal.find("form"), a = _EliteAiTrade.form.getData(t), $.ajax({
            method: "post",
            url: t.attr("action"),
            dataType: "json",
            data: a,
            error: function (e) {
                return _EliteAiTrade.ajax.processError(e), _EliteAiTrade.withdraw.blocked = !1, r.removeAttr("disabled")
            },
            success: function (e) {
                return _EliteAiTrade.withdraw.blocked = !1, r.removeAttr("disabled"), e.message && flashMessage(e.message, e.status), "success" === e.status && (_EliteAiTrade.withdraw.$modal.find(".close").click(), setTimeout(function () {
                    return window.location.reload()
                }, 1e3)), !1
            }
        }), !1) : void 0
    })
},$(window).on("load", function () {
    return _EliteAiTrade.withdraw.$page.length ? (_EliteAiTrade.withdraw.initWithdrawPage(), console.log("init withdraw")) : void 0
}),referralsLoader = function () {
    function e(e, t) {
        r = e, t = t || null, $(window).on("DOMMouseScroll scroll", function () {
            var o, l, d;
            return d = document.documentElement.clientWidth / window.innerWidth, l = window.innerHeight * d, o = e.attr("data-page") || 1, n = e.attr("data-url"), $(window).scrollTop() + $("footer").outerHeight() > $(document).height() - l && !a && !_EliteAiTrade.stopLazyLoad ? $.ajax({
                url: n,
                type: "GET",
                data: {page: ++o},
                beforeSend: function () {
                    return a = !0
                },
                error: function (e) {
                    return a = !1, _EliteAiTrade.ajax.processError(e)
                },
                success: function (n) {
                    var l;
                    return "success" !== n.status ? (flashMessage(n.message, n.status), _EliteAiTrade.stopLazyLoad = !0, a = !1) : (e.attr("data-page", o), "" !== n.html ? (l = $(n.html).css("opacity", 0), r.find(".append-list").append(l), l.slideDown(2 * i).animate({opacity: 1}, i), a = !1) : (_EliteAiTrade.stopLazyLoad = !0, a = !1), "function" == typeof t ? t(n) : void 0)
                }
            }) : void 0
        })
    }

    var t, r, a, i, n;
    return a = !1, i = 300, r = null, n = null, t = null, e
}();