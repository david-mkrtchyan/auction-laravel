_dashboard = _dashboard || {}

_dashboard.initContractsCountChart = () ->
  $chart = $('#contractsCount')

  if $chart.length > 0
    pieData = {
      datasets: [{
        data: _dashboard.contractsCount
        backgroundColor: [
          '#00c0ef',
          '#00a65a',
          '#f39c12',
          '#f56954',
        ],
      }],
      labels: ["Count"]
    }

    pieOptions =
      segmentShowStroke: true
      segmentStrokeColor: '#fff'
      segmentStrokeWidth: 1
      percentageInnerCutout: 50
      animationSteps: 100
      animationEasing: 'easeOutBounce'
      animateRotate: true
      animateScale: false
      responsive: true
      maintainAspectRatio: false
      legend: {
        display: true,
      },
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) ->
            label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]

            return ' ' + label + ' шт.'
        }
      }

    pieChart = new Chart($chart.get(0).getContext('2d'), {
      type: 'doughnut',
      data: pieData,
      options: pieOptions
    })

_dashboard.initContractsAmountDollarChart = () ->
  $chart = $('#contractsAmountDollar')

  if $chart.length > 0
    pieData = {
      datasets: [{
        data: _dashboard.contractsAmountDollar
        backgroundColor: [
          '#00c0ef',
          '#00a65a',
          '#f39c12',
          '#f56954',
        ]
      }],
      labels: ["Amount $"]
    }

    pieOptions =
      segmentShowStroke: true
      segmentStrokeColor: '#fff'
      segmentStrokeWidth: 1
      percentageInnerCutout: 50
      animationSteps: 100
      animationEasing: 'easeOutBounce'
      animateRotate: true
      animateScale: false
      responsive: true
      maintainAspectRatio: false
      legend: {
        display: true,
      },
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) ->
            label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]

            return ' ' + label + ' $'
        }
      }

    pieChart = new Chart($chart.get(0).getContext('2d'), {
      type: 'doughnut',
      data: pieData,
      options: pieOptions
    })

_dashboard.initContractsAmountBtcChart = () ->
  $chart = $('#contractsAmountBtc')

  if $chart.length > 0
    pieData = {
      datasets: [{
        data: _dashboard.contractsAmountBtc
        backgroundColor: [
          '#00c0ef',
          '#00a65a',
          '#f39c12',
          '#f56954',
        ]
      }],
      labels: ["Amount BTC"]
    }

    pieOptions =
      segmentShowStroke: true
      segmentStrokeColor: '#fff'
      segmentStrokeWidth: 1
      percentageInnerCutout: 50
      animationSteps: 100
      animationEasing: 'easeOutBounce'
      animateRotate: true
      animateScale: false
      responsive: true
      maintainAspectRatio: false
      legend: {
        display: true,
      },
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) ->
            label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]

            return ' ' + label + ' BTC'
        }
      }

    pieChart = new Chart($chart.get(0).getContext('2d'), {
      type: 'doughnut',
      data: pieData,
      options: pieOptions
    })

_dashboard.initWithdrawalChart = () ->
  $chart = $('#withdrawal')

  if $chart.length > 0
    pieData = {
      datasets: [{
        data: _dashboard.withdrawal
        backgroundColor: [
          '#f56954',
          '#00a65a',
        ]
      }],
      labels: ["Amount BTC"]
    }

    pieOptions =
      segmentShowStroke: true
      segmentStrokeColor: '#fff'
      segmentStrokeWidth: 1
      percentageInnerCutout: 50
      animationSteps: 100
      animationEasing: 'easeOutBounce'
      animateRotate: true
      animateScale: false
      responsive: true
      maintainAspectRatio: false
      legend: {
        display: true,
      },
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) ->
            label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]

            return ' ' + label + ' BTC'
        }
      }

    pieChart = new Chart($chart.get(0).getContext('2d'), {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

_dashboard.initTransfersChart = () ->
  $chart = $('#transfer')

  if $chart.length > 0
    pieData = {
      datasets: [{
        data: _dashboard.transfer
        backgroundColor: [
          '#00c0ef',
          '#00a65a',
        ]
      }],
      labels: ["Amount BTC"]
    }

    pieOptions =
      segmentShowStroke: true
      segmentStrokeColor: '#fff'
      segmentStrokeWidth: 1
      percentageInnerCutout: 50
      animationSteps: 100
      animationEasing: 'easeOutBounce'
      animateRotate: true
      animateScale: false
      responsive: true
      maintainAspectRatio: false
      legend: {
        display: true,
      },
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) ->
            label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]

            return ' ' + label + ' BTC'
        }
      }

    pieChart = new Chart($chart.get(0).getContext('2d'), {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

_dashboard.initDepositsChart = () ->
  $chart = $('#deposit')

  if $chart.length > 0
    pieData = {
      datasets: [{
        data: _dashboard.deposit
        backgroundColor: [
          '#00a65a',
        ]
      }],
      labels: ["Amount BTC"]
    }

    pieOptions =
      segmentShowStroke: true
      segmentStrokeColor: '#fff'
      segmentStrokeWidth: 1
      percentageInnerCutout: 50
      animationSteps: 100
      animationEasing: 'easeOutBounce'
      animateRotate: true
      animateScale: false
      responsive: true
      maintainAspectRatio: false
      legend: {
        display: true,
      },
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) ->
            label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]

            return ' ' + label + ' BTC'
        }
      }

    pieChart = new Chart($chart.get(0).getContext('2d'), {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

_dashboard.init = () ->
  this.initContractsCountChart()
  this.initContractsAmountDollarChart()
  this.initContractsAmountBtcChart()
  this.initWithdrawalChart()
  this.initTransfersChart()
  this.initDepositsChart()

$(document).ready ->
  _dashboard.init()
  
  console.log('init dashboard')