$(document).ready ->
  if $('.user-files-table').length > 0
    $(document).on 'click', '.user-files-table .action', (e) ->
      e.preventDefault()

      $that = $(this)

      $.ajax
        method: 'post'
        url: $that.attr('data-url')
        dataType: 'json'
        data: {
          _token: $that.attr('data-_token')
        }
        error: (response) ->
          processError(response)
        success: (response) ->
          message.show response.message, response.status

          if response.status is 'success'
            $that.closest('.file-row').find('.coll-status').text(response.label)

          return false

      return false

    console.log "init user files"