window.initDateTimePickers = () ->
  $('.datepicker-birthday').datepicker
    autoclose: true
    language: window.lang
    todayHighlight: true
    format: birthday_format
    todayBtn: true

  console.log('date pickers init')

$(document).on "ready", () ->
  initDateTimePickers()