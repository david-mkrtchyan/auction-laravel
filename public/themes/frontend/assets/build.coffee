###
Base imports and vars
###
path = require 'path'
gulp = require 'gulp'
prefix = require 'gulp-autoprefixer'
coffee = require 'gulp-coffee'
util = require 'gulp-util'
concat = require 'gulp-concat'
uglify = require 'gulp-uglify'
imagemin = require 'gulp-imagemin'
cleanCSS = require 'gulp-clean-css'
plumber = require 'gulp-plumber'
sass = require('gulp-sass')
nib = require('nib')

# get the theme name
theme = path.basename(path.dirname(__dirname))

projectRoot = __dirname.slice(0, __dirname.indexOf('/resources/'))

console.log(projectRoot);

dev_path =
  fonts: __dirname.concat('/fonts/**')
  vendorJs: __dirname.concat('/vendor/js/**')
  vendorCss: __dirname.concat('/vendor/css/**')
  images: __dirname.concat('/images/**')
  img: __dirname.concat('/img/**')
  flags: __dirname.concat('/flags/**')
  js: __dirname.concat('/js/**.js')
  css: __dirname.concat('/css/**')
  coffee: __dirname.concat('/coffee/**')
  sass: __dirname.concat('/sass/')

prod_path =
  fonts: projectRoot.concat('/public/assets/themes/' + theme + '/fonts/')
  vendorJs: projectRoot.concat('/public/assets/themes/' + theme + '/vendor/js/')
  vendorCss: projectRoot.concat('/public/assets/themes/' + theme + '/vendor/css/')
  images: projectRoot.concat('/public/assets/themes/' + theme + '/images/')
  img: projectRoot.concat('/public/assets/themes/' + theme + '/img/')
  flags: projectRoot.concat('/public/assets/themes/' + theme + '/flags/')
  js: projectRoot.concat('/public/assets/themes/' + theme + '/js/')
  css: projectRoot.concat('/public/assets/themes/' + theme + '/css/')
  coffee: projectRoot.concat('/public/assets/themes/' + theme + '/js/')
  sass: projectRoot.concat('/public/assets/themes/' + theme + '/css/')


# Export tasks #
module.exports =
  default: [
    theme.concat('::css'),
    theme.concat('::sass'),
    theme.concat('::coffee'),
    theme.concat('::purejs'),
    theme.concat('::images'),
    theme.concat('::img'),
    theme.concat('::flags'),
    theme.concat('::fonts'),
    theme.concat('::vendorJs')
    theme.concat('::vendorCss')
  ]
  dev: [
    theme.concat('::css:dev'),
    theme.concat('::sass:dev'),
    theme.concat('::coffee:dev'),
    theme.concat('::purejs'),
    theme.concat('::images'),
    theme.concat('::img'),
    theme.concat('::flags'),
    theme.concat('::fonts'),
    theme.concat('::vendorJs')
    theme.concat('::vendorCss')
  ]
  watch: [
    theme.concat('::css:watch'),
    theme.concat('::sass:watch'),
    theme.concat('::coffee:watch'),
    theme.concat('::purejs:watch'),
    theme.concat('::images:watch'),
    theme.concat('::img:watch'),
    theme.concat('::flags:watch'),
    theme.concat('::fonts:watch'),
    theme.concat('::vendorJs')
    theme.concat('::vendorCss:watch')
  ]

gulp.task theme, [
  theme.concat('::css'),
  theme.concat('::sass'),
  theme.concat('::coffee'),
  theme.concat('::purejs'),
  theme.concat('::images'),
  theme.concat('::img'),
  theme.concat('::fonts'),
  theme.concat('::vendorJs')
  theme.concat('::vendorCss')
]


# CSS #
gulp.task theme.concat("::css"), ->
  gulp.src(dev_path.css.concat("*.css"))
    .pipe(plumber())
    .pipe(cleanCSS(removeEmpty: true))
    .pipe(gulp.dest(prod_path.css))
    .on('error', plumber)

gulp.task theme.concat('::css:dev'), ->
  gulp.src(dev_path.css.concat('*.css'))
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.css))
    .on('error', plumber)

gulp.task theme.concat('::css:watch'), ->
  gulp.watch dev_path.css.concat('*.css'), [theme.concat('::css:dev')]


## SASS #
gulp.task theme.concat("::sass"), ->
  gulp.src(dev_path.sass.concat("*.sass"))
    .pipe(plumber())
    .pipe(sass({use: [nib()]}))
    .pipe(cleanCSS(removeEmpty: true))
    .pipe(concat("app.css"))
    .pipe(gulp.dest(prod_path.css))
    .on('error', plumber)

gulp.task theme.concat('::sass:dev'), ->
  gulp.src(dev_path.sass.concat('*.sass'))
    .pipe(plumber())
    .pipe(sass({use: [nib()]}))
    .pipe(concat('app.css'))
    .pipe(gulp.dest(prod_path.css))
    .on('error', plumber)

gulp.task theme.concat('::sass:watch'), ->
  gulp.watch dev_path.sass.concat('**/*.sass'), [theme.concat('::sass:dev')]


# COFFEE #
gulp.task theme.concat('::coffee'), ->
  gulp.src(dev_path.coffee)
    .pipe(plumber())
    .pipe(concat('app.js'))
    .pipe(coffee({bare: true}))
    .pipe(uglify({outSourceMap: true}))
    .pipe(gulp.dest(prod_path.js))
    .on('error', plumber)

gulp.task theme.concat('::coffee:dev'), ->
  gulp.src(dev_path.coffee)
    .pipe(plumber())
    .pipe(concat('app.js'))
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest(prod_path.js))
    .on('error', plumber)

gulp.task theme.concat('::coffee:watch'), ->
  gulp.watch dev_path.coffee, [theme.concat('::coffee:dev')]


# PUREJS #
gulp.task theme.concat('::purejs'), ->
  gulp.src(dev_path.js)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.js))
    .on('error', plumber)

gulp.task theme.concat('::purejs:watch'), ->
  gulp.watch dev_path.js, [theme.concat('::purejs')]


# IMAGES #
gulp.task theme.concat('::images'), ->
  gulp.src(dev_path.images)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.images))
    .on('error', plumber)

gulp.task theme.concat('::images:watch'), ->
  gulp.watch dev_path.images, [theme.concat('::images')]


# IMG #
gulp.task theme.concat('::img'), ->
  gulp.src(dev_path.img)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.img))
    .on('error', plumber)

gulp.task theme.concat('::img:watch'), ->
  gulp.watch dev_path.img, [theme.concat('::img')]


# FLAGS #
gulp.task theme.concat('::flags'), ->
  gulp.src(dev_path.flags)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.flags))
    .on('error', plumber)

gulp.task theme.concat('::flags:watch'), ->
  gulp.watch dev_path.flags, [theme.concat('::flags')]


# FONTS #
gulp.task theme.concat('::fonts'), ->
  gulp.src(dev_path.fonts)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.fonts))
    .on('error', plumber)

gulp.task theme.concat('::fonts:watch'), ->
  gulp.watch dev_path.fonts, [theme.concat('::fonts')]


# VENDOR JS #
gulp.task theme.concat('::vendorJs'), ->
  gulp.src(dev_path.vendorJs)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.js))
    .on('error', plumber)

gulp.task theme.concat('::vendorJs:watch'), ->
  gulp.watch dev_path.vendorJs, [theme.concat('::vendorJs')]


# VENDOR CSS #
gulp.task theme.concat('::vendorCss'), ->
  gulp.src(dev_path.vendorCss)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.css))
    .on('error', plumber)

gulp.task theme.concat('::vendorCss:watch'), ->
  gulp.watch dev_path.vendorCss, [theme.concat('::vendorCss')]