_EliteAiTrade.helpers = _EliteAiTrade.helpers || {}

###*
# Number.prototype.format(decimals, dec_point, thousands_sep, dec_point, thousands_count)
#
# @param integer number         : number to format
# @param integer decimals       : length of decimal
# @param mixed   dec_point      : decimal delimiter
# @param mixed   thousands_sep  : sections delimiter
# @param integer thousands_count: length of whole part
###

_EliteAiTrade.helpers.format = (number, decimals, dec_point, thousands_sep, thousands_count) ->
  decimals = decimals || 0
  dec_point = dec_point || '.'
  thousands_sep = thousands_sep || ' '
  thousands_count = thousands_count || 3

  x = Math.pow(10, decimals)
  re = '\\d(?=(\\d{' + (thousands_count or 3) + '})+' + (if decimals > 0 then '\\D' else '$') + ')'
  num = (Math.round(number * x)/x)
  num += ''
  num = (if dec_point then num.replace('.', dec_point) else num).replace new RegExp(re, 'g'), '$&' + (thousands_sep or ',')

  return num
