_EliteAiTrade.withdraw = _EliteAiTrade.withdraw || {}

_EliteAiTrade.withdraw.blocked = false

_EliteAiTrade.withdraw.$page = $('.withdraw-page')

_EliteAiTrade.withdraw.$form = _EliteAiTrade.withdraw.$page.find('form')

_EliteAiTrade.withdraw.$walletSelect = _EliteAiTrade.withdraw.$page.find('[name="wallet_id"]')

_EliteAiTrade.withdraw.$amountField = _EliteAiTrade.withdraw.$page.find('[name="amount"]')

_EliteAiTrade.withdraw.$dollarAmount = _EliteAiTrade.withdraw.$page.find('.dollar-amount')

_EliteAiTrade.withdraw.dealingRate = parseFloat(_EliteAiTrade.withdraw.$dollarAmount.attr('data-dealing_rate'))

_EliteAiTrade.withdraw.$modalButton = _EliteAiTrade.withdraw.$page.find('.withdraw-modal-button')

_EliteAiTrade.withdraw.$modal = $('#modal_confirm')

_EliteAiTrade.withdraw.$walletBalance = _EliteAiTrade.withdraw.$page.find('.calculator .wallet-balance')
_EliteAiTrade.withdraw.$transferAmount = _EliteAiTrade.withdraw.$page.find('.calculator .transfer-amount')
_EliteAiTrade.withdraw.$transferCommission = _EliteAiTrade.withdraw.$page.find('.calculator .transfer-commission')
_EliteAiTrade.withdraw.$totalAmount = _EliteAiTrade.withdraw.$page.find('.calculator .total-amount')

_EliteAiTrade.withdraw.calculate = (amount) ->
  _EliteAiTrade.withdraw.$transferAmount.text(amount)

  commission = amount / 100 * _EliteAiTrade.withdraw.$transferCommission.attr('data-commission')

  _EliteAiTrade.withdraw.$transferCommission.text(commission.toFixed(8))

  totalAmount =  parseFloat(amount) + commission

  _EliteAiTrade.withdraw.$totalAmount.text(totalAmount.toFixed(8))

_EliteAiTrade.withdraw.initWithdrawPage = () ->
  _EliteAiTrade.withdraw.$walletSelect.on 'change', (e) ->
    _EliteAiTrade.withdraw.$walletBalance.text($(this).find('option:selected').attr('data-balance'))

  _EliteAiTrade.withdraw.$amountField.on 'keyup', (e) ->
    e.preventDefault()

    value = $(this).val()

    if value == ''
      _EliteAiTrade.withdraw.$dollarAmount.text('= 0 $')

      return

    value = parseFloat(value)

    if value < 0 or value == ''
      _EliteAiTrade.withdraw.$dollarAmount.text('= 0 $')

      return

    amount = _EliteAiTrade.withdraw.dealingRate * value;

    amount = if amount < 100 then _EliteAiTrade.helpers.format(amount, 2) else _EliteAiTrade.helpers.format(amount, 0)

    _EliteAiTrade.withdraw.$dollarAmount.text('= ' + amount + ' $')

    _EliteAiTrade.withdraw.calculate(value)

    return false;

  _EliteAiTrade.withdraw.$form.on 'click', '.submit-withdraw-form', (e) ->
    e.preventDefault()

    if _EliteAiTrade.withdraw.blocked == true
      return

    $that = $(this)

    _EliteAiTrade.withdraw.blocked = true

    $that.attr('disabled', 'disabled');

    data = _EliteAiTrade.form.getData(_EliteAiTrade.withdraw.$form)

    $.ajax
      method: 'post',
      url: _EliteAiTrade.withdraw.$form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)

        _EliteAiTrade.withdraw.blocked = false

        $that.removeAttr('disabled');
      success: (response) ->
        _EliteAiTrade.withdraw.blocked = false

        $that.removeAttr('disabled');
        
        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.withdraw.$modal.find('[name=\'id\']').val(response.id)
          _EliteAiTrade.withdraw.$modal.find('[name=\'_token\']').val(response._token)

          _EliteAiTrade.withdraw.$modalButton.click()

          return false

    return false

  _EliteAiTrade.withdraw.$modal.on 'click', '.submit-confirm-form', (e) ->
    e.preventDefault()

    if _EliteAiTrade.withdraw.blocked == true
      return

    $that = $(this)

    _EliteAiTrade.withdraw.blocked = true

    $that.attr('disabled', 'disabled');

    $form = _EliteAiTrade.withdraw.$modal.find('form')

    data = _EliteAiTrade.form.getData($form)

    $.ajax
      method: 'post',
      url: $form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)

        _EliteAiTrade.withdraw.blocked = false

        $that.removeAttr('disabled');
      success: (response) ->
        _EliteAiTrade.withdraw.blocked = false

        $that.removeAttr('disabled');

        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.withdraw.$modal.find('.close').click()

          setTimeout () ->
              window.location.reload()
            , 1000

        return false

    return false

$(window).on 'load', ->
  if _EliteAiTrade.withdraw.$page.length
    _EliteAiTrade.withdraw.initWithdrawPage()

    console.log 'init withdraw'