_EliteAiTrade.form = _EliteAiTrade.form || {}

_EliteAiTrade.form.getData = ($form) ->
  data = new Object()

  $.each $form.serializeArray(), (i, field) ->
    data[field.name] = field.value

  return data

_EliteAiTrade.form.getFormData = ($form) ->
  if _EliteAiTrade.isSafariBrowser()
    data = {}

    FormElement = $form.get(0)

    FormElementCollection = FormElement.elements

    JQEle = undefined
    EleType = undefined
    ele = 0

    while ele < FormElementCollection.length
      JQEle = $(FormElementCollection.item(ele))
      EleType = JQEle.attr('type')

      if !JQEle.attr('name') or (EleType == 'checkbox' or EleType == 'radio') and !JQEle.prop('checked')
        ele++

        continue

      data[JQEle.attr('name')] = JQEle.val()

      ele++
  else
    data = new FormData()

    $.each $form.serializeArray(), (i, field) ->
      data.append field.name, field.value

    $.each $form.find('[type="file"]'), (i, field) ->
      data.append field.name, field.files[0]

  return data