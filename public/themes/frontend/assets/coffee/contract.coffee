_EliteAiTrade.contract = _EliteAiTrade.contract || {}

_EliteAiTrade.contract.blocked = false

_EliteAiTrade.contract.$modal = $('#modal_new_contract')

_EliteAiTrade.contract.$button = _EliteAiTrade.contract.$modal.find('[type="submit"]')

_EliteAiTrade.contract.reinvestButton = '.contract-reinvest-button'

_EliteAiTrade.contract.contractPriceBtc = $('.contract-price-btc span')
_EliteAiTrade.contract.contractTotal = $('.contract-total span')

_EliteAiTrade.contract.actualLevel = (amount) ->
  _level = null

  $.each _EliteAiTrade.contract.levels, (index, level) ->
    contract_amount_from = level.contract_amount_from
    contract_amount_to = level.contract_amount_to

    if amount >= contract_amount_from && amount <= contract_amount_to
      _level = level

      return

    if amount >= contract_amount_from && contract_amount_to == 0
      _level = level

      return

  return _level

_EliteAiTrade.contract._initAmountField = () ->
  _EliteAiTrade.contract.$modal.find('[name="amount"]').on "keyup", (e) ->
    e.preventDefault()

    $that = $(this)

    amount = parseFloat($that.val());
    amount = if amount == '' then 0 else parseFloat(amount)

    level = _EliteAiTrade.contract.actualLevel(amount)
    price = if level then parseFloat(level.contract_price) else 0;

    priceBtc = price / _EliteAiTrade.contract.currencyRate

    _EliteAiTrade.contract.contractPriceBtc.text(price + ' = ' + _EliteAiTrade.helpers.format(priceBtc, 8))

    if amount > 0
      total = amount + priceBtc
      total = total + ''
      total = total.split('.')
      total = total[0] + '.' + (total[1].substring(0, 8))
    else
      total = 0

    _EliteAiTrade.contract.contractTotal.text(total)

    return false

_EliteAiTrade.contract._initFormSubmit = () ->
  _EliteAiTrade.contract.$button.on 'click', (e) ->
    if _EliteAiTrade.contract.blocked == true
      return

    _EliteAiTrade.contract.blocked = true

    $that = $(this)

    $that.attr('disabled', 'disabled');

    e.preventDefault()

    $form = _EliteAiTrade.contract.$modal.find('form')

    data = _EliteAiTrade.form.getData($form)

    $.ajax
      method: 'post',
      url: $form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)

        _EliteAiTrade.contract.blocked = false

        $that.removeAttr('disabled');
      success: (response) ->
        _EliteAiTrade.contract.blocked = false

        $that.removeAttr('disabled')

        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.contract.$modal.find('.close').click()

          window.location.reload()

        return false

    return false

_EliteAiTrade.contract._initReinvest = () ->
  $(document).on 'click', _EliteAiTrade.contract.reinvestButton, (e) ->
    e.preventDefault()

    $button = $(this)
    $form = $button.closest('form')

    data = _EliteAiTrade.form.getData($form)

    $.ajax
      method: 'post',
      url: $form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          $button.closest('.contract-actions').html(response.html)

        return false

    return false

_EliteAiTrade.contract.init = () ->
  _EliteAiTrade.contract._initAmountField()

  _EliteAiTrade.contract._initFormSubmit()

  _EliteAiTrade.contract._initReinvest()

$(window).on "load", () ->
  if _EliteAiTrade.contract.$modal.length
    _EliteAiTrade.contract.init()

    _EliteAiTrade.contract.$modal.find('[name="amount"]').keyup()

    console.log('init contract')