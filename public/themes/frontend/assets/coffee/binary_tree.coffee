_EliteAiTrade.binaryTree = _EliteAiTrade.binaryTree || {}

_EliteAiTrade.binaryTree.$block = $('.binary-tree-block')

_EliteAiTrade.binaryTree.$navigation = $('.binary-tree-navigation')

_EliteAiTrade.binaryTree.history = []

_EliteAiTrade.binaryTree.get = (url) ->
  $.ajax
    method: 'get',
    url: url
    dataType: 'json'
    error: (response) ->
      _EliteAiTrade.binaryTree.$block.fadeIn()

      _EliteAiTrade.ajax.processError(response)

_EliteAiTrade.binaryTree.init = () ->
  $(document).on 'click', 'a.one-people', (e) ->
    e.preventDefault()

    $that = $(this)
    url = $that.attr('href')

    if url is ''
      return false

    _EliteAiTrade.binaryTree.$block.fadeOut()

    $.when(_EliteAiTrade.binaryTree.get(url))
      .then((data) ->
        if data.html
          _EliteAiTrade.binaryTree.$block.find('.main-user').html(data.html)

        _EliteAiTrade.binaryTree.$block.fadeIn()

        _EliteAiTrade.binaryTree.history.push url

        _EliteAiTrade.binaryTree.$navigation.find('.binary-navigation-button').show()

        return false
      )

    return false

  $(document).on 'click', '.binary-back a', (e) ->
    e.preventDefault()

    if _EliteAiTrade.binaryTree.history.length == 1
      _EliteAiTrade.binaryTree.$navigation.find('.binary-home a').click()

      return false
    else   
      url = _EliteAiTrade.binaryTree.history[_EliteAiTrade.binaryTree.history.length - 2]

      _EliteAiTrade.binaryTree.history.pop()

    _EliteAiTrade.binaryTree.$block.fadeOut()

    $.when(_EliteAiTrade.binaryTree.get(url))
      .then((data) ->
        if data.html
          _EliteAiTrade.binaryTree.$block.find('.main-user').html(data.html)
          
        _EliteAiTrade.binaryTree.$block.fadeIn()

        if _EliteAiTrade.binaryTree.history.length == 0
          _EliteAiTrade.binaryTree.$navigation.find('.binary-navigation-button').hide()

        return false  
      )

    return false

  $(document).on 'click', '.binary-home a', (e) ->
    e.preventDefault()

    _EliteAiTrade.binaryTree.$block.fadeOut()

    $.when(_EliteAiTrade.binaryTree.get($(this).attr('href')))
      .then((data) ->
        if data.html
          _EliteAiTrade.binaryTree.$block.find('.main-user').html(data.html)
          
        _EliteAiTrade.binaryTree.$block.fadeIn()

        _EliteAiTrade.binaryTree.history = []
        _EliteAiTrade.binaryTree.$navigation.find('.binary-navigation-button').hide()

        return false  
      )

    return false

  $(document).on 'click', '.binary-volume-filter', (e) ->
    volume = $(this).val() + '-volume'

    _EliteAiTrade.binaryTree.$block.attr('data-volume', volume)

$(window).on 'load', () ->
  if _EliteAiTrade.binaryTree.$block.length > 0
    _EliteAiTrade.binaryTree.init()

    console.log 'init binary tree'
