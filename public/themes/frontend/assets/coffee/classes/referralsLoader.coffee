class referralsLoader
  inProgress = false
  showTime = 300
  container = null
  url = null
  closure = null

  constructor: ($container, closure)->
    container = $container
    closure = closure || null

    $(window).on "DOMMouseScroll scroll", ->
      zoomLevel = document.documentElement.clientWidth / window.innerWidth
      windowHeight = window.innerHeight * zoomLevel

      page = $container.attr('data-page') || 1
      url = $container.attr('data-url')

      if ($(window).scrollTop() + $('footer').outerHeight() > $(document).height() - windowHeight) and not inProgress and not _EliteAiTrade.stopLazyLoad

        $.ajax
          url: url
          type: "GET"
          data:
            page: ++page
          beforeSend: ->
            inProgress = true
          error: (response) ->
            inProgress = false

            _EliteAiTrade.ajax.processError(response)
          success: (response) ->
            if response.status == 'success'
              $container.attr('data-page', page)

              unless response.html is ''
                html = $(response.html).css("opacity", 0)

                container.find('.append-list').append html

                html.slideDown(showTime * 2).animate({opacity: 1}, showTime)

                inProgress = false
              else
                _EliteAiTrade.stopLazyLoad = true

                inProgress = false

              if typeof closure == 'function'
                closure response
            else
              flashMessage(response.message, response.status)

              _EliteAiTrade.stopLazyLoad = true

              inProgress = false