_EliteAiTrade.referrals = _EliteAiTrade.referrals || {}

_EliteAiTrade.referrals.$block = $('.referrals-items')

_EliteAiTrade.referrals.filters = ['generation', 'contracts', 'keywords']

_EliteAiTrade.referrals.loader = {}

_EliteAiTrade.referrals.filter = () ->
  url = _EliteAiTrade.routes.referralsIndex + '?';

  for i of _EliteAiTrade.referrals.filters
    key = _EliteAiTrade.referrals.filters[i]

    if i > 0
      url += '&'

    $filter = $('[name="' + key + '"]')
    
    value = if $filter.hasClass('text-filter') then $filter.val() else $filter.filter(':checked').val()

    url += key + '=' + value

  _EliteAiTrade.referrals.$block.attr('data-url', url)
  _EliteAiTrade.referrals.$block.attr('data-page', 1)

  _EliteAiTrade.stopLazyLoad = true

  $.ajax
    method: 'get',
    url: url
    dataType: 'json'
    error: (response) ->
      _EliteAiTrade.stopLazyLoad = false

      _EliteAiTrade.ajax.processError(response)
    success: (response) ->
      _EliteAiTrade.stopLazyLoad = false

      if response.message
        flashMessage(response.message, response.status)

      unless response.html is ''
        html = $(response.html).css("opacity", 0)

        _EliteAiTrade.referrals.$block.find('.append-list').html html

        html.slideDown(_EliteAiTrade.referrals.loader.showTime * 2).animate({opacity: 1}, _EliteAiTrade.referrals.loader.showTime)
      else
        _EliteAiTrade.referrals.$block.find('.append-list').html ''

      return false

_EliteAiTrade.referrals.init = () ->
  _EliteAiTrade.referrals.loader = new referralsLoader _EliteAiTrade.referrals.$block

  $('.referrals-filter').on 'change', (e) ->
    e.preventDefault()

    _EliteAiTrade.referrals.filter()

  $('.referrals-filter').on 'keyup', (e) ->
    e.preventDefault()

    $that = $(this)
    count = $that.val().length

    setTimeout () ->
        if count == $('[name="' + $that.attr('name') + '"]').val().length
          _EliteAiTrade.referrals.filter()
      , 1000


$(window).on 'load', ->
  if _EliteAiTrade.referrals.$block.length > 0
    _EliteAiTrade.referrals.init()

    console.log 'init referrals'