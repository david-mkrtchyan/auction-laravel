_EliteAiTrade.verification = _EliteAiTrade.verification || {}

_EliteAiTrade.verification.$page = $('.verification-page')

_EliteAiTrade.verification.$form = $('form.verification-form')

_EliteAiTrade.verification.$2FaForm = $('form.verification-2fa-form')

_EliteAiTrade.verification.submitButton = '.verification-form-submit'

_EliteAiTrade.verification.initVerification = () ->
  $('.verification-2fa-form').on 'submit', (e) ->
    e.preventDefault()

    $(_EliteAiTrade.verification.submitButton).click()

    return false

  $(document).on 'click', _EliteAiTrade.verification.submitButton, (e) ->
    e.preventDefault()

    data = _EliteAiTrade.form.getData(_EliteAiTrade.verification.$form)
    _data = _EliteAiTrade.form.getData(_EliteAiTrade.verification.$2FaForm)

    $.each _data, (index, value) ->
      data[index] = value

    $.ajax
      method: 'post',
      url: _EliteAiTrade.verification.$form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          window.location.reload()  
          
        return false

    return false

$(window).on 'load', ->
  if _EliteAiTrade.verification.$page.length
    _EliteAiTrade.verification.initVerification()

    console.log 'init verification'