_EliteAiTrade = _EliteAiTrade || {}

_EliteAiTrade.stopLazyLoad = false

_EliteAiTrade.routes = _EliteAiTrade.routes || {}

_EliteAiTrade.isSafariBrowser = () ->
  VendorName = window.navigator.vendor

  VendorName.indexOf('Apple') > -1 and window.navigator.userAgent.indexOf('Safari') > -1

$(window).on 'load', ->
  clipboard = new ClipboardJS('.btn-mini.copy')

  clipboard.on 'success', () ->
    flashMessage(_EliteAiTrade.lang.copied, 'success')

    return

  console.log 'init app'