_EliteAiTrade.faq = _EliteAiTrade.faq || {}

_EliteAiTrade.faq.$block = $('.questions')

_EliteAiTrade.faq.initFaqWidget = () ->
  $('.question').on 'click', (e) ->
    e.preventDefault()

    $that = $(this)

    _EliteAiTrade.faq.$block.find('li').removeClass('active')

    _EliteAiTrade.faq.$block.find('.answer').text($that.attr('data-text'));

    $that.closest('li').addClass('active')

    return false;

$(window).on 'load', ->
  if _EliteAiTrade.faq.$block.length
    _EliteAiTrade.faq.initFaqWidget()

    console.log 'init faq'