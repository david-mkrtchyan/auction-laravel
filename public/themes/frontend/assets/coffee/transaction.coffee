_EliteAiTrade.transaction = _EliteAiTrade.transaction || {}

_EliteAiTrade.transaction.$block = $('.finances-table')

_EliteAiTrade.transaction.$table = _EliteAiTrade.transaction.$block.find('.table')

_EliteAiTrade.transaction.$button = _EliteAiTrade.transaction.$block.find('.show-more')

_EliteAiTrade.transaction.page = 1

_EliteAiTrade.transaction.busy = false

_EliteAiTrade.transaction.initTransactionTable = () ->
  _EliteAiTrade.transaction.$button.on 'click', (e) ->
    e.preventDefault()

    if _EliteAiTrade.transaction.busy
      return false

    if !_EliteAiTrade.transaction.busy
      _EliteAiTrade.transaction.busy = true

    $.ajax
      method: 'get',
      dataType: 'json'
      data: {
        page: _EliteAiTrade.transaction.page + 1
      }
      error: (response) ->
        _EliteAiTrade.transaction.busy = false

        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        _EliteAiTrade.transaction.page += 1

        if response.status is 'success'
          _EliteAiTrade.transaction.$table.append(response.html)

        unless response.html is ''
          _EliteAiTrade.transaction.busy = false
        else
          _EliteAiTrade.transaction.$button.hide()

        return false

    return false

$(window).on 'load', ->
  if _EliteAiTrade.transaction.$table.length
    _EliteAiTrade.transaction.initTransactionTable()

    console.log 'init transaction'