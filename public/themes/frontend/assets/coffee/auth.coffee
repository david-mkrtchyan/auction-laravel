_EliteAiTrade.auth = _EliteAiTrade.auth || {}

_EliteAiTrade.auth.$authForm = $('.auth-form')

_EliteAiTrade.auth.initAuthForm = () ->
  $(document).on 'click', '.auth-form-submit', (e) ->
    e.preventDefault()

    $form = $(this).closest('form');

    data = _EliteAiTrade.form.getData($form)

    $.ajax
      method: 'post',
      url: $form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        if response.message
          flashMessage(response.message, response.status)

        unless response.status is 'success'
          return false

        $form.closest('.modal').find('.close').click()

        if response.action == 'show-two-factor'
          $('.two-factor-login-modal').modal('show')

          return

        if response.action == 'redirect'
          window.location.href = response.redirect

        return false

    return false

$(window).on 'load', ->
  if _EliteAiTrade.auth.$authForm.length
    _EliteAiTrade.auth.initAuthForm()

    if (_EliteAiTrade.auth.showPasswordForm)
      $('.password-modal').modal('show')

    console.log 'init auth'