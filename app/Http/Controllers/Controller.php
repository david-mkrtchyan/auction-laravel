<?php

namespace App\Http\Controllers;

use Exception;
use FlashMessages;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Theme;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{

    use DispatchesJobs, ValidatesRequests;

    /**
     * @var array
     */
    public $viewData = [];

    /**
     * @var string
     */
    public $_theme = '';

    /**
     * @var string
     */
    public $_view = 'home';

    /**
     * @var array
     */
    public $breadcrumbs = [];

    /**
     * @var null
     */
    public $messageBag = null;

    /**
     * @var \App\Entities\User|null
     */
    public $user = null;

    /**
     * @var \App\Entities\Currency
     */
    public $default_currency;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_theme = empty($this->_theme) ? config('app.theme') : $this->_theme;

        $this->middleware(
            function ($request, $next) {
                $this->user = \Sentinel::getUser();

                if ($this->user) {
                    //$this->user->load('setting', 'wallets', 'binary_referrals');
                }

                return $next($request);
            }
        );

        $this->_init();
    }

    /**
     * @param string $view
     * @param array  $data
     *
     * @return \Illuminate\Contracts\View\View|string
     * @throws \Throwable
     */
    public function render($view = '', array $data = [])
    {
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $this->data($k, $v);
            }
        }

        if (empty($view) || !view()->exists($view)) {
            $view = $this->_view;
        }

        if (view()->exists($view)) {
            return view($view, $this->viewData)->with('messages', FlashMessages::retrieve())->render();
        }

        throw new Exception('View not found', 500);
    }

    /**
     * Push data to templates
     *
     * @return bool
     */
    public function data( /*array or pair of values*/)
    {
        $data = func_get_args();

        if (!empty($data)) {
            if (count($data) > 1) {
                $this->viewData[$data[0]] = $data[1];
            } elseif (is_array($data[0])) {
                $this->viewData = array_merge($this->viewData, $data[0]);
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $name
     * @param bool   $url
     */
    public function breadcrumbs($name, $url = false)
    {
        $this->breadcrumbs[] = ['name' => $name, 'url' => $url];
    }

    /**
     *  Init theme
     */
    private function _init()
    {
        Theme::init($this->_theme);
    }
}
