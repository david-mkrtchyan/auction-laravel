<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use FlashMessages;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use JavaScript;
use Meta;
use Request;
use Response;

/**
 * Class BackendController
 * @package App\Http\Controllers\Backend
 */
class BackendController extends Controller
{

    /**
     * @var string
     */
    public $_theme = "admin";

    /**
     * @var string
     */
    public $module = "admin";

    /**
     * Карта доступа к методам.
     * Пара метод => права доступа
     * Ключ all - права доступа ков сем методам класса
     */
    public $accessMap = [];

    /**
     * BackendController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        app()->setLocale('ru');

        $this->middleware(
            function ($request, $next) {
                view()->share('user', $this->user);

                return $next($request);
            }
        );
    }

    /**
     * Call controller with the specified parameters.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return bool|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        if (!empty($this->user)) {
            $permission = array_get($this->accessMap, $method, false);

            // если нет соотв. прав - проверим, может заданы права к абсолютно всем методам
            if (!$permission) {
                $permission = array_get($this->accessMap, 'all', false);
            }

            if ($permission) {

                $protect = $this->protect($permission);

                if ($protect !== true) {
                    return $protect;
                }
            }
        }

        return parent::callAction($method, $parameters);
    }

    /**
     * @param $permission
     *
     * @return bool
     */
    public function isActionAllowed($permission)
    {
        return $this->user->hasAccess($permission);
    }

    /**
     * @param      $permission
     * @param bool $verbose
     *
     * @return bool|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function protect($permission, $verbose = true)
    {
        if (!$this->isActionAllowed($permission)) {
            $message = trans('messages.access not allowed');

            if (Request::ajax()) {
                return Response::json(['message' => $message, 'status' => 'warning']);
            } else {
                if ($verbose) {
                    FlashMessages::add('warning', $message);

                    return redirect()->route('admin.home');
                }

                return false;
            }
        }

        return true;
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        $this->data('page_title', 'Dashboard');

        return $this->render();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadFile()
    {
        $file = request('file', null);

        if ($file && File::exists(public_path($file))) {
            return response()->download(public_path($file));
        }

        if (URL::isValidUrl($file)) {
            return redirect($file);
        }

        return redirect()->back();
    }

    /**
     * @param string $view
     * @param array  $data
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render($view = '', array $data = [])
    {
        $this->data('breadcrumbs', $this->breadcrumbs);

        $this->fillThemeData();

        return parent::render($view, $data);
    }

    /**
     * fill theme data
     */
    private function fillThemeData()
    {
        Meta::title(config('app.name'));

        $this->data('module', $this->module);

        view()->share('lang', app()->getLocale());

        view()->share('elfinder_link_name', 'link');

        $upload_max_filesize = (int) ini_get("upload_max_filesize") * 1024 * 1024;
        view()->share('upload_max_filesize', $upload_max_filesize);

        $no_image = 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text=no+image';
        view()->share('no_image', $no_image);

        JavaScript::put(
            [
                'no_image'                         => $no_image,
                'select2_per_page'                 => 15,
                'lang'                             => app()->getLocale(),
                'lang_yes'                         => trans('labels.yes'),
                'lang_save'                        => trans('labels.save'),
                'lang_cancel'                      => trans('labels.cancel'),
                'lang_error'                       =>
                    trans('messages.an error has occurred, please reload the page and try again'),
                'lang_errorSelectedFileIsTooLarge' => trans('messages.trying to load is too large file'),
                'lang_errorIncorrectFileType'      => trans('messages.trying to load unsupported file type'),
                'lang_errorFormSubmit'             => trans('messages.error form submit'),
                'lang_errorValidation'             => trans('messages.validation_failed'),
                'lang_errorEmptyData'              => trans('messages.you have not entered any data'),
                'lang_errorEmptyNameField'         => trans('messages.name field not set'),
                'upload_max_filesize'              => $upload_max_filesize,
                'elfinderConnectorUrl'             => route('admin.elfinder.connector'),
                'birthday_format'                  => 'dd.mm.yyyy',
            ]
        );

        view()->share('translation_groups', config('translation.visible_groups'));
    }
}