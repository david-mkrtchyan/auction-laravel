<?php

namespace App\Http\Controllers\Backend;

use ImageUploader;
use App\Entities\Group;
use App\Entities\Images;
use App\Entities\Product;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\Product\ProductCreateRequest;
use App\Http\Requests\Backend\Product\ProductUpdateRequest;
use DataTables;
use FlashMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Meta;

/**
 * Class ProductController
 * @package App\Http\Controllers\Backend
 */
class ProductController extends BackendController
{

    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "product";

    /**
     * @var array
     */
    public $accessMap = [
        'index' => 'product.read',
        'create' => 'product.create',
        'store' => 'product.create',
        'show' => 'product.read',
        'edit' => 'product.read',
        'update' => 'product.write',
        'destroy' => 'product.delete',
        'ajaxFieldChange' => 'product.write',
    ];


    public function __construct()
    {
        parent::__construct();


        Meta::title(trans('labels.products'));

        $this->breadcrumbs(trans('labels.products'), route('admin.product.index'));
    }

    /**
     * Display a listing of the resource.
     * GET /product
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */

    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = Product::joinTranslations('products', 'product_translations')
                ->select(
                    'products.id as id',
                    'products.price as price',
                    'products.start_price as start_price',
                    'product_translations.title as title',
                    'product_translations.description as description',
                    'products.position as position',
                    'products.view_count as view_count',
                    'products.status as status'
                );

            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'id',
                    function ($model) {
                        return;
                    }
                )
                ->filterColumn(
                    'title',
                    function ($query, $keyword) {
                        $query->whereRaw("product_translations.title like ?", ["%{$keyword}%"]);
                    }
                )
                ->filterColumn(
                    'description',
                    function ($query, $keyword) {
                        $query->whereRaw("product_translations.description like ?", ["%{$keyword}%"]);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->addColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]  //, 'without_delete' => true
                        )->render();
                    }
                )
                ->rawColumns(['actions', 'status', 'start_time', 'end_time'])
                ->make();
        }

        $this->data('product_title', trans('labels.products'));
        $this->breadcrumbs(trans('labels.products_list'));

        return $this->render('views.product.index');
    }

    /**
     * Show the form for creating a new resource.
     * GET /product/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->_fillAdditionTemplateData();

        $this->data('model', new Product());

        $this->data('product_title', trans('labels.product_create'));

        $this->breadcrumbs(trans('labels.product_create'));

        return $this->render('views.product.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /product
     *
     * @param ProductCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(ProductCreateRequest $request)
    {
        $input = $request->all();


        $model = new Product($input);

        if ($model->save() && isset($input['images']) && ($images = $input['images'])) {
            Images::uploadAndSaveImages($images, $model->id, $this->module, Images::typeProduct);
        }

        FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.product.index');
    }

    /**
     * Display the specified resource.
     * GET /product/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /product/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        $model = Product::findOrFail($id);

        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.product.index');
        }

        $this->data('product_title', '"' . $model->name . '"');

        $this->breadcrumbs(trans('labels.product_editing'));

        $this->_fillAdditionTemplateData($model);

        return $this->render('views.product.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     * PUT /product/{id}
     *
     * @param  int $id
     * @param ProductUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     */
    public function update($id, ProductUpdateRequest $request)
    {
        $model = Product::findOrFail($id);

        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.product.index');
        }

        $input = $request->all();

        if ($model->update($input) && isset($input['images']) && ($images = $input['images'])) {
            Images::uploadAndSaveImages($images,$model->id,$this->module, Images::typeProduct);
        }

        FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.product.index');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /product/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $model = Product::findOrFail($id);

        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.product.index');
        }

        $model->delete();

        FlashMessages::add('success', trans("messages.destroy_ok"));

        return redirect()->route('admin.product.index');
    }

    /**
     * set to template addition variables for add\update product
     *
     * @param \App\Entities\Product   |null $model
     */
    private function _fillAdditionTemplateData($model = null)
    {

        $this->data('groups', Group::list());

        /* images */
        $images = $imagesConfig = null;

        if($model){
            if($imagesFromModel = $model->images){
                foreach ($imagesFromModel as $key => $image){
                    $images.= '"'.$image->name.'",';
                    $imagesConfig .=  "{'url': '".route('admin.image.ajax_delete_image')."', 'key':'".$image->id."', 'extra' : {'_token':'".csrf_token()."','key':'".$image->id."','type': '".Images::typeProduct."' } },";
                }
            }
        }

        $this->data('images',$images);
        $this->data('imagesConfig',$imagesConfig);
        /* end images */

        $this->data(
            'templates',
            get_templates(
                base_path('resources/themes/' . config('app.theme') . '/views/' . $this->module . '/templates'),
                true
            )
        );
    }
}