<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Images;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use DataTables;
use FlashMessages;
use Illuminate\Http\Request;
use Meta;
use Orchestra\Support\Arr;

/**
 * Class ImageController
 * @package App\Http\Controllers\Backend
 */
class ImageController extends BackendController
{

    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "image";

    /**
     * @var array
     */
    public $accessMap = [
        'ajax_delete_image' => 'image.ajax_delete_image',
    ];

    public function ajaxDeleteImage(Request $request)
    {
        if ($request->isMethod('post') && ($key = $request->post('key')) && ($type = $request->post('type'))) {
            if (is_numeric(Arr::get(array_flip(Images::$types), $type))) {
                if (Images::deleteImage($key, $type)) {
                    return response()->json([
                        'status' => 'true',
                        'message' => trans('messages.delete_error'),
                    ], 200);
                }
            }
        }

        return response()->json(['status' => 'error'], 403);
    }

    public function sort(Request $request)
    {
        if ($request->isMethod('post') && ($stacks = $request->post('stack')) && is_array($stacks)) {

                foreach ($stacks as $key => $value) {
                    if (isset($value['key']) && ($keyStack = intval($value['key']))) {
                        if ($model = Images::where('id', $keyStack)->first()) {

                            $model->sort = $key + 1;

                            $model->save();
                        }
                    }
                }

                return response()->json([
                    'status' => 'true',
                    'message' => trans('messages.success_sort'),
                ], 200);

        }

        return response()->json(['status' => 'error'], 403);
    }
}