<?php

namespace App\Http\Controllers\Backend;

use App\Entities\User;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Controllers\Traits\SaveImageTrait;
use App\Http\Requests\Backend\User\PasswordChangeRequest;
use App\Http\Requests\Backend\User\UserCreateRequest;
use App\Http\Requests\Backend\User\UserUpdateRequest;
use App\Jobs\SendUserVerificationApprovedEmail;
use App\Services\UserService;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Cartalyst\Sentinel\Users\UserInterface;
use DataTables;
use Exception;
use FlashMessages;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Meta;
use Redirect;
use Sentinel;

/**
 * Class UserController
 * @package App\Http\Controllers\Backend
 */
class UserController extends BackendController
{
    
    use SaveImageTrait;
    use AjaxFieldsChangerTrait;
    
    /**
     * @var string
     */
    public $module = "user";
    
    /**
     * @var array
     */
    public $accessMap = [
        'index'                => 'user.read',
        'create'               => 'user.create',
        'store'                => 'user.create',
        'show'                 => 'user.read',
        'edit'                 => 'user.read',
        'update'               => 'user.write',
        'getNewPassword'       => 'user.write',
        'postNewPassword'      => 'user.write',
        'localAjaxFieldChange' => 'user.write',
        'ajaxFieldChange'      => 'user.write',
    ];
    
    /**
     * @var \App\Services\UserService
     */
    private $userService;

    /**
     * UserController constructor.
     *
     * @param \App\Services\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        
        Meta::title(trans('labels.users'));

        $this->breadcrumbs(trans('labels.users'), route('admin.user.index'));
        
        $this->middleware('prepare.phone', ['only' => ['store', 'update']]);
        
        $this->userService = $userService;

    }
    
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = User::select(
                [
                    'users.id as id',
                    'users.user_name as user_name',
                    'users.first_name as first_name',
                    'users.last_name as last_name',
                    'users.email as email',
                    'users.phone as phone',
                    'users.verified as verified',
                ]
            );
            
            return $this->_datatable($list);
        }
        
        $this->data('page_title', trans('labels.users'));
        $this->breadcrumbs(trans('labels.users_list'));
        
        return $this->render('views.user.index');
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|mixed
     * @throws \Exception
     */
    public function indexUnverified(Request $request)
    {
        if ($request->get('draw')) {
            $list = User::where('users.verified', false)
                ->select(
                    [
                        'users.id as id',
                        'users.user_name as user_name',
                        'users.first_name as first_name',
                        'users.last_name as last_name',
                        'users.email as email',
                        'users.phone as phone',
                        'users.verified as verified',
                    ]
                );
            
            return $this->_datatable($list);
        }
        
        $this->data('page_title', trans('labels.unverified users'));
        $this->breadcrumbs(trans('labels.unverified_users_list'));
        
        return $this->render('views.user.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new User());
        
        $this->fillAdditionalTemplateData(__FUNCTION__);
        
        $this->data('page_title', trans('labels.user_create'));
        
        $this->breadcrumbs(trans('labels.user_create'));
        
        return $this->render('views.user.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Backend\User\UserCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\ImageSaveException
     */
    public function store(UserCreateRequest $request)
    {

        if (!$this->validateImage('avatar')) {
            FlashMessages::add('warning', trans('messages.bad image'));

            return Redirect::back();
        }

        $user = $user = Sentinel::register($request->validated());

        if ($request->hasFile('avatar')) {
            $this->setImage($user, 'avatar', $this->module);
        }
        
        if ($this->user->hasAccess('user.approve')) {
            $user->verified = $request->input('verified');
        }
        
        $user->user_name = $request->input('user_name');

        $user->save();

        $this->_processRoles($user, $request->input('roles', []));

        if ($request->input('activated', false)) {
            $this->userService->activate($user);
        }

        FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.user.edit', $user->getUserId());
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     *
     */
    public function show($id)
    {
        return $this->edit($id);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        /**
         * @var User $model
         */
        $model = Sentinel::getUserRepository()->findById($id);
        
        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.user.index');
        }
        
        $model->load('files');
        
        $this->data(
            'page_title',
            trim(
                (!$model->verified ? '<span class="text-red">'.trans('labels.not verified').'</span>' : '')
                .' '.
                $model->getFullName()
            )
        );
        
        $this->breadcrumbs(trans('labels.user_edit'));
        
        $this->fillAdditionalTemplateData(__FUNCTION__, $model);
        
        $this->data('model', $model);
        
        return $this->render('views.user.edit');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int                                              $id
     * @param \App\Http\Requests\Backend\User\UserUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     * @throws \App\Exceptions\ImageSaveException
     */
    public function update($id, UserUpdateRequest $request)
    {
        $user = Sentinel::getUserRepository()->findById($id);
        
        if (!$user) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.user.index');
        }
        
        if (!$this->validateImage('avatar')) {
            FlashMessages::add('warning', trans('messages.bad image'));
            
            return Redirect::back();
        }
        
        $original_verification = $user->verified;

        $user->fill($request->validated());
        
        $user->user_name = $request->input('user_name');

        if ($request->hasFile('avatar')) {
            $this->setImage($user, 'avatar', $this->module);
        } else {
            $user->avatar = $request->input('avatar');
        }
        
        if ($this->user->hasAccess('user.approve')) {
            $user->verified = $request->input('verified');
        }
        
        $user->save();
        
        if ($request->input('activated', false)) {
            $this->userService->activate($user);
        } else {
            $this->userService->deactivate($user);
        }
        
        $this->_processRoles($user, $request->get('roles', []));
        
        $this->_checkVerification($original_verification, $user);
        
        FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.user.edit', $user->getUserId());
    }
    
    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function getNewPassword($id)
    {
        $model = Sentinel::getUserRepository()->findById($id);
        
        if (!$model) {
            FlashMessages::add('error', trans("messages.record_not_found"));
            
            return redirect()->route('admin.user.index');
        }
        
        $this->data('model', $model);
        
        $this->data('page_title', trans('labels.password_edit'));
        
        return $this->render('views.user.new_password');
    }
    
    /**
     * @param                       $id
     * @param PasswordChangeRequest $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postNewPassword($id, PasswordChangeRequest $request)
    {
        $user = Sentinel::getUserRepository()->findById($id);
        
        Sentinel::update($user, ['password' => $request->input('password')]);
        
        Sentinel::logout($user, true);
        
        FlashMessages::add("success", trans("messages.save_ok"));
        
        return redirect()->route('admin.user.edit', $id);
    }
    
    /**
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function twoFactorAuthenticationReset(int $id)
    {
        User::whereId($id)->update(['two_factor_secret' => null]);
        
        FlashMessages::add("success", trans("messages.2fa successfully disabled"));
        
        return redirect()->route('admin.user.edit', $id);
    }
    
    /**
     * change field = $field of record with id = $id
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function localAjaxFieldChange($id)
    {
        $field = request('field', null);
        
        if ($field != 'activated') {
            return $this->ajaxFieldChange($id);
        }
        
        $model = Sentinel::getUserRepository()->findById($id);
        
        if ($model) {
            if (request('value', false)) {
                $this->userService->activate($model);
            } else {
                $this->userService->deactivate($model);
            }
            
            $event = '\App\Events\Backend\UserEdit';
            
            if (class_exists($event)) {
                event(new $event($model));
            }
            
            return response()->json(
                [
                    "error"   => 0,
                    'message' => trans('messages.field_value_successfully_saved'),
                    'type'    => 'success',
                ]
            );
        }
        
        return response()->json(
            [
                "error"   => 1,
                'message' => trans('messages.error_in_field_value_saving'),
                'type'    => 'error',
            ]
        );
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {
        try {
            $search_text = $request->get('search_text', '');
            
            if ($search_text) {
                $users = User::where('first_name', 'like', '%'.$request->get('text', '').'%')
                    ->orWhere('last_name', 'like', '%'.$request->get('text', '').'%')
                    ->orWhere('email', 'like', '%'.$request->get('text', '').'%')
                    ->orWhere('phone', 'like', '%'.$request->get('text', '').'%');
            } else {
                $users = User::whereNotNull('email');
            }
            
            $users = $users->select('id', 'user_name', 'email', 'phone', 'first_name', 'last_name')->paginate(15);
            
            $items = [];
            
            /**
             * @var User $user
             */
            foreach ($users as $user) {
                $full_name = $user->getFullName();
                $full_name = ($full_name ? $full_name : $user->user_name);
                
                $info = '('.$user->email.($user->phone ? ', '.$user->phone : '').')';
                
                $items[] = [
                    'id'        => $user->id,
                    'full_name' => '# '.$user->id.' '.trim($full_name.' '.$info),
                ];
            }
            
            return [
                'status' => 'success',
                'items'  => $items,
            ];
        } catch (Exception $e) {
            return [
                'status' => 'error',
                'items'  => [],
            ];
        }
    }
    
    /**
     * @param           $function
     * @param User|null $model
     */
    public function fillAdditionalTemplateData($function, $model = null)
    {
        switch ($function) {
            case 'create' :
                $this->data('user_roles', []);
                break;
            case 'edit' :
                $this->data('user_roles', $model->roles->pluck('id')->toArray());
                break;
        }
        
        $roles = [];
        foreach (EloquentRole::all() as $item) {
            $roles[$item['id']] = $item['name'];
        }
        $this->data('roles', $roles);
    }
    
    /**
     * @param \Cartalyst\Sentinel\Users\UserInterface $user
     * @param array                                   $roles
     */
    private function _processRoles(UserInterface $user, $roles = [])
    {
        if ($this->user->hasAccess('user.role.write')) {
            $user->roles()->sync($roles);
        }
    }
    
    /**
     * @param bool               $original_verification
     * @param \App\Entities\User $user
     */
    private function _checkVerification(bool $original_verification, User $user)
    {
        if ($original_verification == false && $user->verified == true) {
            dispatch(new SendUserVerificationApprovedEmail($user))->onQueue('users');
        }
    }
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $list
     *
     * @return mixed
     * @throws \Exception
     */
    private function _datatable(Builder $list)
    {
        return $dataTables = DataTables::of($list)
            ->filterColumn(
                'actions',
                function ($query, $keyword) {
                    $query->whereRaw('users.id like ?', ['%'.$keyword.'%']);
                }
            )
            ->filterColumn(
                'full_name',
                function ($query, $keyword) {
                    $query->whereRaw(
                        'users.first_name like ? or users.last_name like ?',
                        ['%'.$keyword.'%', '%'.$keyword.'%']
                    );
                }
            )
            ->editColumn(
                'verified',
                function ($model) {
                    return view(
                        'partials.datatables.icon_status',
                        [
                            'model' => $model,
                            'type'  => $this->module,
                            'field' => 'verified',
                        ]
                    )->render();
                }
            )
            ->addColumn(
                'full_name',
                function ($model) {
                    return $model->getFullName();
                }
            )
            ->addColumn(
                'actions',
                function ($model) {
                    return view(
                        'views.'.$this->module.'.partials.control_buttons',
                        ['model' => $model, 'type' => 'user', 'without_delete' => true]
                    )->render();
                }
            )
            ->rawColumns(['actions', 'verified'])
            ->make();
    }
}
