<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Page;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\Page\PageCreateRequest;
use App\Http\Requests\Backend\Page\PageUpdateRequest;
use App\Services\PageService;
use DataTables;
use FlashMessages;
use Illuminate\Http\Request;
use Meta;

/**
 * Class PageController
 * @package App\Http\Controllers\Backend
 */
class PageController extends BackendController
{
    
    use AjaxFieldsChangerTrait;
    
    /**
     * @var string
     */
    public $module = "page";
    
    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'page.read',
        'create'          => 'page.create',
        'store'           => 'page.create',
        'show'            => 'page.read',
        'edit'            => 'page.read',
        'update'          => 'page.write',
        'destroy'         => 'page.delete',
        'ajaxFieldChange' => 'page.write',
    ];
    
    /**
     * @var PageService
     */
    private $pageService;
    
    /**
     * @param \App\Services\PageService $pageService
     */
    public function __construct(PageService $pageService)
    {
        parent::__construct();
        
        $this->pageService = $pageService;
        
        Meta::title(trans('labels.pages'));
        
        $this->breadcrumbs(trans('labels.pages'), route('admin.page.index'));
        
        $this->middleware('set.slug', ['only' => ['store', 'update']]);
    }
    
    /**
     * Display a listing of the resource.
     * GET /page
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = Page::joinTranslations('pages', 'page_translations')
                ->select(
                    'pages.id as id',
                    'page_translations.name as name',
                    'pages.status as status',
                    'pages.rank as rank',
                    'pages.position as position',
                    'pages.parent_id as parent_id',
                    'pages.slug as slug'
                );
            
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'name',
                    function ($query, $keyword) {
                        $query->whereRaw("page_translations.name like ?", ["%{$keyword}%"]);
                    }
                )
                ->filterColumn(
                    'actions',
                    function ($query, $keyword) {
                        $query->whereRaw('pages.id like ?', ['%'.$keyword.'%']);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'position',
                    function ($model) {
                        return view(
                            'partials.datatables.select',
                            ['model' => $model, 'type' => $this->module, 'field' => 'position', 'list' => $model->positions()]
                        )->render();
                    }
                )
                ->editColumn(
                    'rank',
                    function ($model) {
                        return view(
                            'partials.datatables.text_input',
                            ['model' => $model, 'type' => $this->module, 'field' => 'rank']
                        )->render();
                    }
                )
                ->editColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'front_link' => true, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'rank', 'position', 'actions'])
                ->make();
        }
        
        $this->data('page_title', trans('labels.pages'));
        $this->breadcrumbs(trans('labels.pages_list'));
        
        return $this->render('views.page.index');
    }
    
    /**
     * Show the form for creating a new resource.
     * GET /page/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->_fillAdditionTemplateData();
        
        $this->data('model', new Page);
        
        $this->data('page_title', trans('labels.page_create'));
        
        $this->breadcrumbs(trans('labels.page_create'));
        
        return $this->render('views.page.create');
    }
    
    /**
     * Store a newly created resource in storage.
     * POST /page
     *
     * @param PageCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(PageCreateRequest $request)
    {
        $input = $request->all();
        $input['parent_id'] = isset($input['parent_id']) ? $input['parent_id'] : null;
        
        $model = new Page($input);
        
        $model->save();
        
        $this->pageService->setExternalUrl($model);
        
        FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.page.index');
    }
    
    /**
     * Display the specified resource.
     * GET /page/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return $this->edit($id);
    }
    
    /**
     * Show the form for editing the specified resource.
     * GET /page/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        $model = Page::findOrFail($id);
        
        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.page.index');
        }
        
        $this->data('page_title', '"'.$model->name.'"');
        
        $this->breadcrumbs(trans('labels.page_editing'));
        
        $this->_fillAdditionTemplateData($model);
        
        return $this->render('views.page.edit', compact('model'));
    }
    
    /**
     * Update the specified resource in storage.
     * PUT /page/{id}
     *
     * @param  int              $id
     * @param PageUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     */
    public function update($id, PageUpdateRequest $request)
    {
        $model = Page::findOrFail($id);
        
        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.page.index');
        }
        
        $input = $request->all();
        $input['parent_id'] = isset($input['parent_id']) ? $input['parent_id'] : null;
        
        $model->update($input);
        
        FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.page.index');
    }
    
    /**
     * Remove the specified resource from storage.
     * DELETE /page/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $model = Page::findOrFail($id);
        
        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.page.index');
        }
        
        $model->delete();
        
        FlashMessages::add('success', trans("messages.destroy_ok"));
        
        return redirect()->route('admin.page.index');
    }
    
    /**
     * set to template addition variables for add\update page
     *
     * @param \App\Entities\Page|null $model
     */
    private function _fillAdditionTemplateData($model = null)
    {

        $this->data('positions',Page::positions());

        $this->data(
            'templates',
            get_templates(
                base_path('resources/themes/'.config('app.theme').'/views/'.$this->module.'/templates'),
                true
            )
        );
    }
}