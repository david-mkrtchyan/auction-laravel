<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Group;
use App\Entities\Images;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\Group\GroupCreateRequest;
use App\Http\Requests\Backend\Group\GroupUpdateRequest;
use DataTables;
use FlashMessages;
use Illuminate\Http\Request;
use Meta;

/**
 * Class GroupController
 * @package App\Http\Controllers\Backend
 */
class GroupController extends BackendController
{
    
    use AjaxFieldsChangerTrait;
    
    /**
     * @var string
     */
    public $module = "group";
    
    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'group.read',
        'create'          => 'group.create',
        'store'           => 'group.create',
        'show'            => 'group.read',
        'edit'            => 'group.read',
        'update'          => 'group.write',
        'destroy'         => 'group.delete',
        'ajaxFieldChange' => 'group.write',
    ];
    

    public function __construct()
    {
        parent::__construct();


        Meta::title(trans('labels.groups'));
        
        $this->breadcrumbs(trans('labels.groups'), route('admin.group.index'));
    }
    
    /**
     * Display a listing of the resource.
     * GET /group
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */

    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = Group::joinTranslations('groups', 'group_translations')
                ->select(
                    'groups.id as id',
                    'group_translations.name as name',
                    'group_translations.description as description',
                    'groups.start_time as start_time',
                    'groups.end_time as end_time',
                    'groups.status as status'
                );

            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'id',
                    function ($model) {
                        return ;
                    }
                )
                ->filterColumn(
                    'name',
                    function ($query, $keyword) {
                        $query->whereRaw("group_translations.name like ?", ["%{$keyword}%"]);
                    }
                )
                ->filterColumn(
                    'description',
                    function ($query, $keyword) {
                        $query->whereRaw("group_translations.description like ?", ["%{$keyword}%"]);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )

                ->addColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]  //, 'without_delete' => true
                        )->render();
                    }
                )
                ->rawColumns(['actions','status','start_time','end_time'])
                ->make();
        }
        
        $this->data('group_title', trans('labels.groups'));
        $this->breadcrumbs(trans('labels.groups_list'));
        
        return $this->render('views.group.index');
    }
    
    /**
     * Show the form for creating a new resource.
     * GET /group/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->_fillAdditionTemplateData();
        
        $this->data('model', new Group());
        
        $this->data('group_title', trans('labels.group_create'));
        
        $this->breadcrumbs(trans('labels.group_create'));
        
        return $this->render('views.group.create');
    }
    
    /**
     * Store a newly created resource in storage.
     * POST /group
     *
     * @param GroupCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(GroupCreateRequest $request)
    {
        $input = $request->all();

        $model = new Group($input);

        if ($model->save() && isset($input['images']) && ($images = $input['images'])) {
            Images::uploadAndSaveImages($images, $model->id, $this->module, Images::typeGroup);
        }

        FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.group.index');
    }
    
    /**
     * Display the specified resource.
     * GET /group/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return $this->edit($id);
    }
    
    /**
     * Show the form for editing the specified resource.
     * GET /group/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        $model = Group::findOrFail($id);
        
        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.group.index');
        }
        
        $this->data('group_title', '"'.$model->name.'"');
        
        $this->breadcrumbs(trans('labels.group_editing'));
        
        $this->_fillAdditionTemplateData($model);
        
        return $this->render('views.group.edit', compact('model'));
    }
    
    /**
     * Update the specified resource in storage.
     * PUT /group/{id}
     *
     * @param  int              $id
     * @param GroupUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     */
    public function update($id, GroupUpdateRequest $request)
    {
        $model = Group::findOrFail($id);

        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.group.index');
        }
        
        $input = $request->all();

        if ($model->update($input) && isset($input['images']) && ($images = $input['images'])) {
            Images::uploadAndSaveImages($images,$model->id,$this->module,Images::typeGroup);
        }
        
        FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.group.index');
    }
    
    /**
     * Remove the specified resource from storage.
     * DELETE /group/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $model = Group::findOrFail($id);
        
        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.group.index');
        }
        
        $model->delete();
        
        FlashMessages::add('success', trans("messages.destroy_ok"));
        
        return redirect()->route('admin.group.index');
    }
    
    /**
     * set to template addition variables for add\update group
     *
     * @param \App\Entities\Group   |null $model
     */
    private function _fillAdditionTemplateData($model = null)
    {
        /* images */
        $images = $imagesConfig = null;

        if($model){
            if($imagesFromModel = $model->images){
                foreach ($imagesFromModel as $key => $image){
                    $images.= '"'.$image->name.'",';
                    $imagesConfig .=  "{'url': '".route('admin.image.ajax_delete_image')."', 'key':'".$image->id."', 'extra' : {'_token':'".csrf_token()."','key':'".$image->id."','type': '".Images::typeGroup."' } },";
                }
            }
        }

        $this->data('images',$images);
        $this->data('imagesConfig',$imagesConfig);
        /* end images */


        $this->data(
            'templates',
            get_templates(
                base_path('resources/themes/'.config('app.theme').'/views/'.$this->module.'/templates'),
                true
            )
        );
    }
}