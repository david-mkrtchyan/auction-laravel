<?php

namespace App\Http\Controllers\Backend;

use Activation;
use FlashMessages;
use Illuminate\Http\Request;
use Sentinel;
use View;

/**
 * Class AuthController
 * @package App\Http\Controllers\Backend
 */
class AuthController extends BackendController
{
    
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        View::share('body_css_class', 'sidebar-collapse');
    }
    
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function getLogin()
    {
        $this->_layout = 'admin.auth';
        
        return $this->render('views.auth.login');
    }
    
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function postLogin(Request $request)
    {
        $user = Sentinel::findByCredentials($request->only('email'));

        if (!$user) {
            FlashMessages::add('error', trans('messages.user with such email was not found'));
            
            return redirect()->back();
        }
        
        if (!Activation::completed($user)) {
            FlashMessages::add('error', trans('messages.user not activated'));
            
            return redirect()->back();
        }
        
        $credentials = $request->only(['email', 'password']);
        
        if (!Sentinel::validateCredentials($user, $credentials)) {
            FlashMessages::add('error', trans('messages.you have entered a wrong password'));
            
            return redirect()->back();
        }
        
        if (!$user->hasAccess('administrator')) {
            FlashMessages::add('error', trans("messages.access_denied"));
            
            return redirect()->back();
        }

        Sentinel::login($user, $request->get('remember', false));
        
        return redirect()->route('admin.home');
    }
    
    /**
     * @return mixed
     */
    public function getLogout()
    {
        Sentinel::logout();
        
        return redirect()->route('admin.auth.login');
    }
}