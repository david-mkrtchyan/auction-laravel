<?php

namespace App\Http\Controllers\Backend;

use App\Entities\File;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use Illuminate\Http\JsonResponse;

/**
 * Class UserFileController
 * @package App\Http\Controllers\Backend
 */
class UserFileController extends BackendController
{
    
    use AjaxFieldsChangerTrait;
    
    /**
     * @var string
     */
    public $module = "user_file";
    
    /**
     * @var array
     */
    public $accessMap = [
        'show'    => 'userfile.read',
        'approve' => 'userfile.write',
        'decline' => 'userfile.write',
    ];
    
    /**
     * @param $user_id
     * @param $file_id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $user_id, int $file_id)
    {
        $file = File::whereUserId($user_id)->whereId($file_id)->first();
        
        return response(
            file_get_contents($file->getSrc()),
            200,
            [
                'content-type' => 'image/png',
            ]
        );
    }
    
    /**
     * @param int $user_id
     * @param int $file_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(int $user_id, int $file_id): JsonResponse
    {
        $file = File::whereUserId($user_id)->whereId($file_id)->first();
        
        abort_if(!$file, 404);
        
        $this->_setStatus($file, 'approved');
        
        return response()->json(
            [
                'message' => trans('messages.file successfully approved'),
                'status'  => 'success',
                'label'   => trans('labels.file_status_approved'),
            ]
        );
    }
    
    /**
     * @param int $user_id
     * @param int $file_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function decline(int $user_id, int $file_id): JsonResponse
    {
        $file = File::whereUserId($user_id)->whereId($file_id)->first();
        
        abort_if(!$file, 404);
        
        $this->_setStatus($file, 'declined');
        
        return response()->json(
            [
                'message' => trans('messages.file successfully declined'),
                'status'  => 'success',
                'label'   => trans('labels.file_status_declined'),
            ]
        );
    }
    
    /**
     * @param \App\Entities\File $file
     * @param string             $status
     *
     * @return \App\Entities\File
     */
    private function _setStatus(File $file, string $status): File
    {
        $file->status = $file->getStatusIdByKey($status);
        $file->save();
        
        return $file;
    }
}
