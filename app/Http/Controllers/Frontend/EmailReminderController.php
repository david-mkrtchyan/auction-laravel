<?php

namespace App\Http\Controllers\Frontend;

use Activation;
use App\Http\Requests\EmailReminderComplete;
use App\Http\Requests\EmailReminderCreate;
use App\Jobs\SendReminderEmail;
use FlashMessages;
use JavaScript;
use Reminder;
use Sentinel;

/**
 * Class EmailReminderController
 * @package App\Http\Controllers\Frontend
 */
class EmailReminderController extends FrontendController
{
    
    /**
     * @var string
     */
    public $module = 'reminder_email';
    
    /**
     * @param \App\Http\Requests\EmailReminderCreate $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EmailReminderCreate $request)
    {
        $user = Sentinel::findByCredentials($request->only('email'));
        
        if (!$user) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not found'),
                ]
            );
        }
        
        if (!Activation::completed($user)) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not activated'),
                ]
            );
        }
        
        $reminder = Reminder::create($user);
        
        dispatch(new SendReminderEmail($user, $reminder))->onQueue('users');
        
        return response()->json(
            [
                'status'  => 'success',
                'message' => trans('front_messages.reminder send'),
            ]
        );
    }
    
    /**
     * @param string $email
     * @param string $code
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function check(string $email, string $code)
    {
        $user = Sentinel::findByCredentials(['email' => $email]);
        
        if (!$user) {
            FlashMessages::add('error', trans('front_messages.user not found'));
            
            return redirect()->route('home');
        }
        
        if (!Activation::completed($user)) {
            FlashMessages::add('error', trans('front_messages.user not activated'));
            
            return redirect()->route('home');
        }
        
        return redirect()->route('reminder.show.email', [$email, $code]);
    }
    
    /**
     * @param string $email
     * @param string $code
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show(string $email, string $code)
    {
        $user = Sentinel::findByCredentials(['email' => $email]);
        
        if (!$user) {
            FlashMessages::add('error', trans('front_messages.user not found'));
            
            return redirect()->route('home');
        }
        
        if (!Activation::completed($user)) {
            FlashMessages::add('error', trans('front_messages.user not activated'));
            
            return redirect()->route('home');
        }
        
        $this->data('g2fa', $user->twoFactorEnabled());
        $this->data('email', $email);
        $this->data('code', $code);
        
        JavaScript::put(
            [
                'auth' => [
                    'showPasswordForm' => true,
                ],
            ]
        );
        
        return $this->render('page.templates.home');
    }
    
    /**
     * @param \App\Http\Requests\EmailReminderComplete $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function complete(EmailReminderComplete $request)
    {
        $user = Sentinel::findByCredentials($request->only('email'));
        
        if (!$user) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not found'),
                ]
            );
        }
        
        if (!Activation::completed($user)) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not activated'),
                ]
            );
        }
        
        if (!Reminder::complete($user, $request->input('code'), $request->input('password'))) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.reminder complete error'),
                ]
            );
        }
        
        Sentinel::logout($user, true);
        
        Sentinel::login($user);
        
        return response()->json(
            [
                'status'   => 'success',
                'action'   => 'redirect',
                'message'  => trans('front_messages.reminder complete'),
                'redirect' => $this->getRedirectTo(),
            ]
        );
    }
}