<?php

namespace App\Http\Controllers\Frontend;

use App\Entities\Group;

/**
 * Class GroupController
 * @package App\Http\Controllers\Frontend
 */
class GroupController extends FrontendController
{

    /**
     * @var string
     */
    public $module = 'group';

    public function show($id)
    {
        $group = Group::findOrFail($id);

        return $this->render($this->module.'.view',[
            'products' => $group->product()->paginate(35),
            'group' => $group,
        ]);
    }

}