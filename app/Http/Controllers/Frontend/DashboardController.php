<?php

namespace App\Http\Controllers\Frontend;

use App\Contracts\CurrencyRateRepository as CurrencyRateRepositoryContract;
use App\Facades\Currency;
use App\Services\CurrencyRateService;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class DashboardController extends FrontendController
{
    
    /**
     * @var string
     */
    public $module = 'dashboard';


    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return $this->render($this->module.'.index');
    }
    
}