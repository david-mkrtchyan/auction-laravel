<?php

namespace App\Http\Controllers\Frontend;

use App\Entities\Group;
use App\Entities\Product;

/**
 * Class ProductController
 * @package App\Http\Controllers\Frontend
 */
class ProductController extends FrontendController
{

    /**
     * @var string
     */
    public $module = 'product';

    public function show($id)
    {
        $product  = Product::joinTranslations('products','product_translations')
            ->select(
                'products.id as id',
                'products.group_id as group_id',
                'products.price as price',
                'products.start_price as start_price',
                'product_translations.title as title',
                'product_translations.description as description',
                'products.position as position'
            )
            ->where('products.id',$id)->visible()->first();

        return $this->render($this->module.'.view',[
            'product' => $product
        ]);
    }

}