<?php

namespace App\Http\Controllers\Frontend;

use Activation;
use App\Http\Requests\SessionStore;
use App\Http\Requests\TwoFactorCheck;
use App\Services\UserService;
use FlashMessages;
use Sentinel;
use App\Entities\User;

/**
 * Class SessionController
 * @package App\Http\Controllers\Frontend
 */
class SessionController extends FrontendController
{
    
    /**
     * @var string
     */
    public $module = 'session';
    
    /**
     * @var \App\Services\UserService
     */
    private $userService;
    
    /**
     * UserController constructor.
     *
     * @param \App\Services\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        
        $this->userService = $userService;
    }
    
    /**
     * @param \App\Http\Requests\SessionStore $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SessionStore $request)
    {
        $user = Sentinel::findByCredentials($request->only('email'));
        
        if (!$user) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not found'),
                ]
            );
        }

        if (!Activation::completed($user)) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not activated'),
                ]
            );
        }

        if (!$user->verified) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not activated'),
                ]
            );
        }

        $credentials = $request->only(['email', 'password']);
        
        if (!Sentinel::validateCredentials($user, $credentials)) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.wrong authorization credential'),
                ]
            );
        }
        
        if ($user->twoFactorEnabled()) {
            session()->put('session_check_two_factor', $user->id);
            
            return response()->json(
                [
                    'status' => 'success',
                    'action' => 'show-two-factor',
                ]
            );
        }
        
        Sentinel::loginAndRemember($user);
        
        FlashMessages::add('success', trans('front_messages.you have successfully log in'));
    
        session()->put('profile_modal', true);
        session()->put('announcement_modal', true);
        
        return response()->json(
            [
                'status'   => 'success',
                'action'   => 'redirect',
                'redirect' => $this->getRedirectTo(),
            ]
        );
    }
    
    /**
     * @param \App\Http\Requests\TwoFactorCheck $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function twoFactorCheck(TwoFactorCheck $request)
    {
        session()->forget('session_check_two_factor');
        
        $user = $request->getUser();
        
        if (!$user) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not found'),
                ]
            );
        }
        
        if (!Activation::completed($user)) {
            return response()->json(
                [
                    'status'  => 'error',
                    'message' => trans('front_messages.user not activated'),
                ]
            );
        }
        
        Sentinel::loginAndRemember($user);
        
        FlashMessages::add('success', trans('front_messages.you have successfully log in'));
    
        session()->put('profile_modal', true);
        session()->put('announcement_modal', true);
        
        return response()->json(
            [
                'status'   => 'success',
                'action'   => 'redirect',
                'redirect' => $this->getRedirectTo(),
            ]
        );
    }
    
    /**
     * @param int $user_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function adminLogin($user_id)
    {
        if ($this->user->hasAccess('superuser')) {
            $user = User::find($user_id);
            
            if (!$user) {
                FlashMessages::add('error', trans('messages.record_not_found'));
                
                return redirect()->back();
            }
            
            Sentinel::login($user, false);
            
            return redirect()->route('dashboard.index');
        }
        
        FlashMessages::add('error', trans('messages.you do not have access for this action'));
        
        return redirect()->back();
    }
    
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete()
    {
        Sentinel::logout();
        
        FlashMessages::add('notice', trans('front_messages.you have successfully log out'));
        
        return redirect()->route('home');
    }
}
