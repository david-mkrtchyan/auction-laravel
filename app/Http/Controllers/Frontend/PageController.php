<?php

namespace App\Http\Controllers\Frontend;

use App\Entities\Page;

/**
 * Class PageController
 * @package App\Http\Controllers\Frontend
 */
class PageController extends FrontendController
{
    
    /**
     * @var string
     */
    public $module = 'page';
    
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function home()
    {
        $model = Page::with('translations')->whereSlug('home')->first();

        abort_unless($model, 404);
        
        $this->data('model', $model);
        
        $this->fillMeta($model);

        if (!$this->user) {
            $this->data('_home', true);
        }
    
        $this->data('home_page', true);

        return $this->render($this->module.'.'.$model->getTemplate());

       //return $this->render($this->module.'.templates.'.$model->getTemplate());
    }
    
    /**
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function show()
    {
        $slug = func_get_args();

        $slug = array_pop($slug);

        if ($slug == 'home') {
            return redirect(route('home'), 301);
        }
        
        $model = Page::with(['translations', 'parent', 'parent.translations'])->visible()->whereSlug($slug)->first();

        abort_unless($model, 404);
        
        $this->data('model', $model);
        
        $this->fillMeta($model);

        return $this->render($this->module.'.'.$model->getTemplate());

        //return $this->render($this->module.'.templates.'.$model->getTemplate());
    }
    
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function notFound()
    {
        $view = view('errors.404')->render();
        
        return response($view, 404);
    }
}