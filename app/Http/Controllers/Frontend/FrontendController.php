<?php

namespace App\Http\Controllers\Frontend;

use App\Contracts\MetaGettable;
use App\Facades\Currency;
use App\Http\Controllers\Controller;
use JavaScript;
use App\Facades\Meta;

/**
 * Class FrontendController
 * @package App\Http\Controllers\Frontend
 */
class FrontendController extends Controller
{
    /**
     * @var string
     */
    public $module = "";
    
    /**
     * @var array
     */
    public $breadcrumbs = [];
    
    /**
     * constructor
     */
    function __construct()
    {
        $this->_theme = config('app.theme');
        
        parent::__construct();
        
        Meta::title(config('app.name', ''));
        
        $this->breadcrumbs(config('app.name', ''), route('home'));
    }
    
    /**
     * @param MetaGettable $model
     */
    public function fillMeta(MetaGettable $model)
    {

        Meta::title($model->getMetaTitle());
        Meta::description($model->getMetaDescription());
        Meta::keywords($model->getMetaKeywords());
        Meta::image($model->getMetaImage());
        Meta::canonical($model->getUrl());
    }
    
    /**
     * @param string $view
     * @param array  $data
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render($view = '', array $data = [])
    {
        $this->fillThemeData();
        
        return parent::render($view, $data);
    }
    
    /**
     * fill additional template data
     */
    public function fillThemeData()
    {
        view()->share('breadcrumbs', $this->breadcrumbs);
        
        $upload_max_filesize = (int) ini_get("upload_max_filesize") * 1024 * 1024;
        
        JavaScript::put(
            [
                'routes'            => [
                    'appUrl'         => route('home'),
                ],
                'lang'              => [
                    'amountFieldIsRequired' => trans('front_messages.amount is required'),
                    'uploaded'              => trans('front_labels.uploaded'),
                    'running'               => trans('front_labels.running'),
                    'errorUpload'           => trans('front_labels.error upload'),
                    'bought'                => trans('front_labels.bought'),
                    'sold'                  => trans('front_labels.sold'),
                    'for'                   => trans('front_labels.for'),
                    'internalError'         => trans('front_messages.an error has occurred try again later'),
                    'copied'                => trans('front_messages.copied'),
                ],
                'locale'            => app()->getLocale(),
                'uploadMaxFilesize' => $upload_max_filesize,
               // 'currencyTitle'     => Currency::title(),
            ]
        );

        $no_image = 'http://www.placehold.it/250x250/EFEFEF/AAAAAA&text=no+image';
        view()->share('no_image', $no_image);
        
        view()->share('_user', $this->user);
        //view()->share('currency_title', Currency::title());
        view()->share('upload_max_filesize', $upload_max_filesize);

        view()->share('locale', app()->getLocale());
        view()->share('locales', variable('languages', []));
        
        view()->share('app_name', config('app.name'));
        
        view()->share('profile_modal', session()->pull('profile_modal'));

        view()->share('announcement_modal', session()->pull('announcement_modal'));
    }
    
    /**
     * @return string
     */
    public function getRedirectTo()
    {
        return route('dashboard.index');
    }
}