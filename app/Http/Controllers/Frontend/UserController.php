<?php

namespace App\Http\Controllers\Frontend;

use Activation;
use App\Http\Controllers\Traits\SaveImageTrait;
use App\Http\Requests\UserDataUpdateRequest;
use App\Http\Requests\UserStore;
use App\Jobs\SaveReferral;
use App\Jobs\SendUserActivation;
use App\Jobs\UserRegister;
use App\Services\UserService;
use FlashMessages;
use Sentinel;

/**
 * Class UserController
 * @package App\Http\Controllers\Frontend
 */
class UserController extends FrontendController
{
    use SaveImageTrait;
    
    /**
     * @var string
     */
    public $module = 'user';
    
    /**
     * @var \App\Services\UserService
     */
    private $userService;
    
    /**
     * UserController constructor.
     *
     * @param \App\Services\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();
        
        $this->userService = $userService;
    }

    /**
     * @param \App\Http\Requests\UserStore $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserStore $request)
    {
        $data = $request->only(['email', 'first_name', 'last_name', 'password', 'phone', 'user_name', 'country_id']);

        $user = $this->userService->create($data, true);

        /*dispatch(new SendUserActivation($user))->onQueue('users');*/

        dispatch(new UserRegister($user))->onQueue('users');

        return response()->json(
            [
                'status'  => 'success',
                'stayTime'  => 40000,
                'position'  => 'bottom center',
                'message' => trans(
                    'front_emails.activation :full_name :site_link :pay',
                    [
                        'full_name' => 	$user->getFullName(),
                        'site_link' => link_to(route('home'), config('app.name')),
                        'pay' => variable('price_for_activate_account'),
                    ]
                ),
            ]
        );
    }

    /**
     * @param string $email
     * @param string $code
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(string $email, string $code)
    {
        $user = Sentinel::findByCredentials(['email' => $email]);
        
        if (!$user) {
            FlashMessages::add('error', trans('front_messages.user not found'));
            
            return redirect()->route('home');
        }
        
        if (!Activation::complete($user, $code)) {
            FlashMessages::add('error', trans('front_messages.activation error'));
            
            return redirect()->route('home');
        }
        
        FlashMessages::add('success', trans('front_messages.activation successful'));
        
        $this->data('announcement_modal', true);
        
        return redirect()->route('home');
    }

    public function edit()
    {
        /**
         * @var User $model
         */
        $model = Sentinel::getUser();

        if (!$model) {
            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.user.index');
        }

        $this->data('model', $model);

        return $this->render('views.user.edit');
    }


    public function update(UserDataUpdateRequest $request)
    {
        $user = Sentinel::getUser();

        $user->fill($request->validated());


        $user->save();

        return response()->json(
            [
                'status'  => 'success',
                'message' =>  trans('messages.save_ok')
            ]
        );
    }




}