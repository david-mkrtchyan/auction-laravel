<?php

namespace App\Http;

use App\Http\Middleware\AdminAuthenticate;
use App\Http\Middleware\AjaxMiddleware;
use App\Http\Middleware\Auth;
use App\Http\Middleware\Guest;
use App\Http\Middleware\PreparePhone;
use App\Http\Middleware\SessionAllowedAccess;
use App\Http\Middleware\SetSlug;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter;
use Mcamara\LaravelLocalization\Middleware\LocaleSessionRedirect;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */


    protected $routeMiddleware = [
        'bindings'               => SubstituteBindings::class,
        'throttle'               => ThrottleRequests::class,
        'localizationRedirect'   => LaravelLocalizationRedirectFilter::class,
        'localeSessionRedirect'  => LocaleSessionRedirect::class,
        'auth'                   => Auth::class,
        'session_allowed_access' => SessionAllowedAccess::class,
        'guest'                  => Guest::class,
        'ajax'                   => AjaxMiddleware::class,
        'verified'               => Verified::class,
        'prepare.phone'          => PreparePhone::class,

        //Admin
        'admin.auth'             => AdminAuthenticate::class,
        'set.slug'               => SetSlug::class,
    ];
}
