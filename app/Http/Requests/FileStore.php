<?php

namespace App\Http\Requests;

use App\Entities\File;

/**
 * Class FileStore
 * @package App\Http\Requests
 */
class FileStore extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = array_keys(app(File::class)->getTypes());
        
        return [
            'src'  => [
                'required',
                'image',
                'max:'.((int) ini_get("upload_max_filesize") * 1024 * 1024),
                'dimensions:'.
                    'max_width='.config('image.max_upload_width').','.
                    'max_height='.config('image.max_upload_height'),
                ],
            'type' => 'required|integer|in:'.implode(',', $types),
        ];
    }
}
