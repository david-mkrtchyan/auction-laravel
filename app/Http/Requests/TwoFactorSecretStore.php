<?php

namespace App\Http\Requests;

use App\Entities\User;
use Sentinel;

/**
 * Class TwoFactorSecretStore
 * @package App\Http\Requests
 */
class TwoFactorSecretStore extends BaseRequest
{
    
    /**
     * @var User|null
     */
    private $user;
    
    /**
     * @var string
     */
    public $redirectRoute = 'two_factor_authentication.create';
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'otp'    => [
                'required',
                'digits:6',
                'valid_otp:'.$this->request->get('secret'),
                'used_otp:'.$this->user->getUserId(),
            ],
            'secret' => 'required|string',
        ];
    }
    
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->user = Sentinel::getUser();
    }
}
