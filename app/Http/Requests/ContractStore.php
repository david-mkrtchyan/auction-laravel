<?php

namespace App\Http\Requests;

use App\Entities\Level;
use App\Entities\Wallet;
use App\Facades\Currency;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

/**
 * Class ContractStore
 * @package App\Http\Requests
 */
class ContractStore extends BaseRequest
{
    
    /**
     * @var float
     */
    protected $contract_max_amount;
    
    /**
     * @var array
     */
    protected $wallets;
    
    /**
     * @var Level
     */
    public $actual_level;
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'wallet'   => 'required|string|in:'.implode(',', $this->wallets),
            'amount'   => [
                'required',
                'numeric',
                'min:'.config('contracts.min_amount'),
                'crypto_amount',
            ],
            'reinvest' => 'boolean',
            'terms'    => 'accepted',
        ];
        
        if ($this->contract_max_amount !== null) {
            $rules['amount'][] .= 'max:'.$this->contract_max_amount;
        }
        
        return $rules;
    }
    
    /**
     * @return array
     */
    public function messages()
    {
        return [
            'amount.max' => trans('validation.not_enough_money'),
        ];
    }
    
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->wallets = app(Wallet::class)->getAllowedForContractTypes();
    
        /**
         * @var \App\Entities\User $user
         */
        $user = Sentinel::getUser();
        
        $amount = floatval($this->input('amount', 0));
        
        if ($amount >= config('contracts.min_amount')) {
            $this->actual_level = Level::actualForAmount($amount)->first();
            
            $contract_price = Currency::toDefaultCurrency($this->actual_level->contract_price);
            
            $wallet = $this->input('wallet', false);
            
            if ($wallet !== false) {
                /**
                 * @var Wallet $wallet
                 */
                $wallet = $user->wallets->firstWhere('type', app(Wallet::class)->getTypeIdByKey($wallet));
                
                if ($wallet) {
                    $this->contract_max_amount = cbcsub($wallet->getBalance(), $contract_price, 8);
                }
            }
        }
    }
}
