<?php

namespace App\Http\Requests;

/**
 * Class EmailReminderCreate
 * @package App\Http\Requests
 */
class EmailReminderCreate extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => $this->defaultRules['email'],
        ];
    }
}
