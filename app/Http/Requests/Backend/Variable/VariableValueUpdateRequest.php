<?php

namespace App\Http\Requests\Backend\Variable;

use App\Entities\Variable;
use App\Http\Requests\BaseRequest;

/**
 * Class VariableValueUpdateRequest
 * @package App\Http\Requests\Backend\Variable
 */
class VariableValueUpdateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        
        $type = app(Variable::class)->getTypeKeyById($this->request->get('type', 1));

        $multilingual = $this->request->get('multilingual', false);
        
        if ($multilingual) {
            foreach (config('app.locales') as $locale) {
                $rules[$locale.'.text'] = 'nullable|string';
            }
        } else {
            if ($type == 'multi_select') {
                $rules['value'] = 'nullable|array';
            } elseif ($type == 'range') {
                $rules['value.from'] = 'nullable|present|numeric';
                $rules['value.to'] = 'nullable|present|numeric';
            } else {
                $rules['value'] = 'nullable|string';
            }
        }
        
        if ($type == 'image') {
            $regex = '/^.*\.('.implode('|', config('image.allowed_image_extension')).')$/';
            
            $rules = [
                'value' => 'required|regex:'.$regex,
            ];
        }
        
        $rules['status'] = 'required|boolean';
        $rules['variable_id'] = 'required|exists:variables,id';
        
        return $rules;
    }
}