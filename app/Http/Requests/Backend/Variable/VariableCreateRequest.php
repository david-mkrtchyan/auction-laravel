<?php

namespace App\Http\Requests\Backend\Variable;

use App\Http\Requests\BaseRequest;
use App\Entities\Variable;

/**
 * Class VariableCreateRequest
 * @package App\Http\Requests\Backend\Variable
 */
class VariableCreateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'         => 'required|in:'.implode(',', array_keys(app(Variable::class)->getTypes())),
            'key'          => 'required|unique:variables,key',
            'name'         => 'required',
            'multilingual' => 'required|boolean',
        ];
    }
}