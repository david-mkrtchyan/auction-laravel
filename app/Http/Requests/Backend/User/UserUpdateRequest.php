<?php

namespace App\Http\Requests\Backend\User;

use App\Entities\User;
use App\Http\Requests\BaseRequest;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\Backend\User
 */
class UserUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('user');
        
        $to = carbon()->now()->year;
        $from = $to - 100;
        
        $rules = [
            'activated'   => 'required|boolean',
            'verified'    => 'required|boolean',
            'user_name'   => ['required', 'string', 'user_name', 'regex:~[a-zA-Z]+~', 'unique:users,user_name,'.$id],
            'email'       => 'required|email|unique:users,email,'.$id,
            'phone'       => 'nullable|phone',
            'first_name'  => 'nullable|string',
            'last_name'   => 'nullable|string',
            'gender'      => 'nullable|integer|in:'.implode(',', array_keys(app(User::class)->getGenders())),
            'birth_day'   => 'nullable|integer|min:1|max:31',
            'birth_month' => 'nullable|integer|min:1|max:12',
            'birth_year'  => 'nullable|integer|min:'.$from.'|max:'.$to,
            'address'     => 'nullable|string|min:2',
            'city'        => 'nullable|string|min:2',
            'postal_code' => 'nullable|string',
            'country_id'  => 'nullable|integer|exists:countries,id',
            'roles'       => 'array',
        ];
        
        return $rules;
    }
    
    /**
     * @return array
     */
    public function messages()
    {
        return [
            'user_name.regex' => trans('validation.user_name_regex')
        ];
    }
}