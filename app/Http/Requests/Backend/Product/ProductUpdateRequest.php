<?php

namespace App\Http\Requests\Backend\Product;

use App\Http\Requests\BaseRequest;

/**
 * Class ProductUpdateRequest
 * @package App\Http\Requests\Backend\Group
 */
class ProductUpdateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'position'      => 'required|integer',
            'group_id'      => 'required|exists:groups,id',
            'status'        => 'required|integer',
            'price'         => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'start_price'   => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];

        $languageRules = [
            'title'         => 'required',
            'description'   => 'required',
        ];

        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }

        return $rules;
    }
}