<?php

namespace App\Http\Requests\Backend\Role;

use App\Http\Requests\BaseRequest;

/**
 * Class RoleCreateRequest
 * @package App\Http\Requests\Role
 */
class RoleCreateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|unique:roles',
            'slug' => 'required|min:2|unique:roles',
        ];
    }
    
    /**
     * @return array
     */
    public function messages()
    {
        return [
            'slug.required' => trans('validation.required', ['attribute' => trans('labels.role_slug')]),
            'slug.min'      => trans('validation.min.string', ['attribute' => trans('labels.role_slug'), 'min' => 2]),
            'slug.unique'   => trans('validation.unique', ['attribute' => trans('labels.role_slug')]),
        ];
    }
}