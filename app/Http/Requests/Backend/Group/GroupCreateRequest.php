<?php

namespace App\Http\Requests\Backend\Group;

use App\Http\Requests\BaseRequest;

/**
 * Class GroupCreateRequest
 * @package App\Http\Requests\Backend\Group
 */
class GroupCreateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'start_time' => 'required|date_format:Y-m-d H:i:s',
            'end_time'   => 'required|date_format:Y-m-d H:i:s',
            'status'     => 'required|integer',
        ];
        
        $languageRules = [
            'name'         => 'required',
            'description'  => 'required',
        ];
        
        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }
        
        return $rules;
    }
}