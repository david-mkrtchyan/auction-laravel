<?php

namespace App\Http\Requests\Backend\Translation;

use App\Http\Requests\BaseRequest;

/**
 * Class TranslationUpdateRequest
 * @package App\Http\Requests\Backend\Translation
 */
class TranslationUpdateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
