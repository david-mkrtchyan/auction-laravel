<?php

namespace App\Http\Requests;

use App\Entities\User;
use Sentinel;

/**
 * Class TwoFactorCheck
 * @package App\Http\Requests
 */
class TwoFactorCheck extends BaseRequest
{
    
    /**
     * @var string
     */
    protected $session_key = 'session_check_two_factor';
    
    /**
     * @var User|null
     */
    protected $user;
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'otp' => [
                'required',
                'digits:6',
                'valid_otp:'.$this->user->two_factor_secret,
                'used_otp:'.$this->user->getUserId(),
            ],
        ];
    }
    
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->user = Sentinel::findById($this->session()->get($this->session_key));
    }
    
    /**
     * Prepare the data for validation.
     *
     * @return \Cartalyst\Sentinel\Users\UserInterface|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
