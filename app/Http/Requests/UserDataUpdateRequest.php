<?php

namespace App\Http\Requests;

use Sentinel;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\Backend\User
 */
class UserDataUpdateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Sentinel::getUser()->getUserId();

        $password = [];
        if($this->input('password')){
            $password = [
                'password' => array_merge($this->defaultRules['password'], ['confirmed']),
                'password_confirmation' => $this->defaultRules['password_confirmation']
            ];
        }


        $rules = [
            //'user_name' => ['required', 'string', 'user_name', 'regex:~[a-zA-Z]+~', 'unique:users,user_name,' . $id],
            'phone' => ['required', 'string', 'unique:users,phone,' . $id],
            'phone_validate'        => ['required', 'string', 'in:true'],
            'email' => ['required', 'string', 'email', 'unique:users,email,' . $id],
            'avatar' => 'nullable|image',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'country_id' => 'required|integer|exists:countries,id',
        ];

        return array_merge($password,$rules);
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'user_name.regex' => trans('validation.user_name_regex'),
            'phone_validate.in' => trans('validation.phone validate is wrong'),
        ];
    }
}