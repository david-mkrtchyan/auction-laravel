<?php

namespace App\Http\Requests;

/**
 * Class PasswordUpdate
 * @package App\Http\Requests
 */
class PasswordUpdate extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'password'              => array_merge($this->defaultRules['password'], ['confirmed']),
            'password_confirmation' => $this->defaultRules['password_confirmation'],
        ];
        
        if (!session('password_restore', false)) {
            $rules['old_password'] = 'required|string';
        }
        
        return $rules;
    }
}
