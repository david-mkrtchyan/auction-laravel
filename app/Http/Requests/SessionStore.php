<?php

namespace App\Http\Requests;

/**
 * Class SessionStore
 * @package App\Http\Requests
 */
class SessionStore extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => $this->defaultRules['email'],
            'password' => 'required|string',
        ];
    }
}
