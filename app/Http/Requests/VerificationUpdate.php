<?php

namespace App\Http\Requests;

use App\Entities\User;

/**
 * Class VerificationUpdate
 * @package App\Http\Requests
 */
class VerificationUpdate extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $to = carbon()->now()->year;
        $from = $to - 100;
        
        return [
            'country_id'  => 'nullable|integer|exists:countries,id',
            'first_name'  => 'required|string|min:2|max:50',
            'last_name'   => 'required|string|min:2|max:50',
            'gender'      => 'nullable|integer|in:'.implode(',', array_keys(app(User::class)->getGenders())),
            'birth_day'   => 'nullable|integer|min:1|max:31',
            'birth_month' => 'nullable|integer|min:1|max:12',
            'birth_year'  => 'nullable|integer|min:'.$from.'|max:'.$to,
            'postal_code' => 'nullable|string',
            'address'     => 'nullable|string|min:2',
            'city'        => 'nullable|string|min:2',
        ];
    }
}
