<?php

namespace App\Http\Requests;

use App\Entities\User;
use Sentinel;

/**
 * Class EmailReminderComplete
 * @package App\Http\Requests
 */
class EmailReminderComplete extends BaseRequest
{
    
    /**
     * @var User|null
     */
    protected $user;
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code'                  => 'required|string',
            'email'                 => $this->defaultRules['email'],
            'password'              => array_merge($this->defaultRules['password'], ['confirmed']),
            'password_confirmation' => $this->defaultRules['password_confirmation'],
        ];
        
        if ($this->user && $this->user->two_factor_secret) {
            $rules['otp'] = [
                'required',
                'digits:6',
                'valid_otp:'.$this->user->two_factor_secret,
                'used_otp:'.$this->user->getUserId(),
            ];
        }
        
        return $rules;
    }
    
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->user = Sentinel::getUserRepository()->findByCredentials(['email' => $this->input('email')]);
    }
}
