<?php

namespace App\Http\Requests;

use App\Entities\User;
use Sentinel;

/**
 * Class TwoFactorSecretDestroy
 * @package App\Http\Requests
 */
class TwoFactorSecretDestroy extends BaseRequest
{
    
    /**
     * @var User|null
     */
    private $user;
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'otp' => [
                'required',
                'digits:6',
                'valid_otp:'.$this->user->two_factor_secret,
                'used_otp:'.$this->user->getUserId(),
            ],
        ];
    }
    
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->user = Sentinel::getUser();
    }
}
