<?php

namespace App\Http\Requests;

/**
 * Class UserStore
 * @package App\Http\Requests
 */
class UserStore extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email'                 => array_merge($this->defaultRules['email'], ['unique:users,email']),
            'password'              => array_merge($this->defaultRules['password'], ['confirmed']),
            'password_confirmation' => $this->defaultRules['password_confirmation'],
            'first_name'            => 'required|string',
            'last_name'             => 'required|string',
            'country_id'            => 'required|integer|exists:countries,id',
            'user_name'             => 'required|unique:users|min:5',
            'phone'                 => 'required|unique:users',
            'phone_validate'        => ['required', 'string', 'in:true'],
        ];
        
        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'phone_validate.in' => trans('validation.phone validate is wrong'),
        ];
    }

}
