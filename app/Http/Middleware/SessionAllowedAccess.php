<?php

namespace App\Http\Middleware;

use Closure;
use FlashMessages;

/**
 * Class SessionAllowedAccess
 * @package App\Http\Middleware
 */
class SessionAllowedAccess
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param string                    $session_key
     *
     * @return mixed
     */
    public function handle($request, Closure $next, string $session_key)
    {
        if (!session()->has($session_key)) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(
                    [
                        'status'  => 'error',
                        'message' => trans('front_messages.forbidden'),
                    ]
                    ,
                    403
                );
            }
            
            FlashMessages::add('error', trans('front_messages.forbidden'));
            
            return redirect()->route('home', [], 403);
        }
        
        return $next($request);
    }
}
