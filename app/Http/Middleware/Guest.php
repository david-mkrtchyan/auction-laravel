<?php

namespace App\Http\Middleware;

use Closure;
use FlashMessages;
use Sentinel;

class Guest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Sentinel::guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(
                    [
                        'status'   => 'error',
                        'message'  => trans('front_messages.guest error'),
                        'redirect' => route('dashboard.index'),
                    ],
                    403
                );
            }
            
            FlashMessages::add('error', trans('front_messages.guest error'));
            
            return redirect()->route('dashboard.index');
        }
        
        return $next($request);
    }
}
