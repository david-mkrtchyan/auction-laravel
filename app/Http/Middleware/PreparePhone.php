<?php

namespace App\Http\Middleware;

use App\Decorators\Phone;
use Closure;

/**
 * Class PreparePhone
 * @package App\Http\Middleware
 */
class PreparePhone
{
    
    /**
     * @var array
     */
    protected $phone_fields = ['phone'];
    
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param null|string              $key
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $key = null)
    {
        $input = $request->all();
        
        $input = $this->_process($input, $key);
        
        $request->merge($input);
        
        return $next($request);
    }
    
    /**
     * @param array       $array
     * @param null|string $key
     *
     * @return array
     */
    private function _process($array = [], $key = null)
    {
        if ($key && array_has($array, $key)) {
            array_set($array, $key, (new Phone($array[$key]))->getDecorated());
        } else {
            array_walk_recursive(
                $array,
                function ($value, $key) use (&$array) {
                    if (in_array($key, $this->phone_fields)) {
                        array_set($array, $key, (new Phone($value))->getDecorated());
                    }
                }
            );
        }
        
        return $array;
    }
}
