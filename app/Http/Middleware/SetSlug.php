<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class SetSlug
 * @package App\Http\Middleware
 */
class SetSlug
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name = $request->input(app()->getLocale().'.name', '');
        $slug = $request->input('slug', '');
        
        $request->request->set('slug', !empty($slug) ? $slug : (!empty($name) ? str_slug($name) : ''));
        
        return $next($request);
    }
    
}
