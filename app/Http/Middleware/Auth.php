<?php

namespace App\Http\Middleware;

use Closure;
use FlashMessages;
use Sentinel;

/**
 * Class Auth
 * @package App\Http\Middleware
 */
class Auth
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Sentinel::check()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(
                    [
                        'status'   => 'error',
                        'message'  => trans('front_messages.auth error'),
                        'redirect' => route('home'),
                    ],
                    403
                );
            }
            
            FlashMessages::add('error', trans('front_messages.auth error'));
            
            return redirect()->route('home');
        }
        
        return $next($request);
    }
}
