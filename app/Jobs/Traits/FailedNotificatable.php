<?php

namespace App\Jobs\Traits;

use Exception;
use Log;

/**
 * Trait FailedNotificatable
 * @package App\Jobs\Traits
 */
trait FailedNotificatable
{
    
    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     *
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::error(
            'Job failed',
            [
                'message'     => $exception->getMessage(),
                'job_message' => method_exists($this, 'getFailedMessage')
                    ? $this->getFailedMessage()
                    : 'failed message not specified',
            ]
        );
    }
}