<?php

namespace App\Jobs;

use App\Entities\User;
use App\Mail\Reminder;
use Cartalyst\Sentinel\Reminders\EloquentReminder;
use Mail;

class SendReminderEmail extends BaseJob
{
    
    /**
     * @var \Cartalyst\Sentinel\Users\UserInterface
     */
    public $user;
    
    /**
     * @var \Cartalyst\Sentinel\Reminders\EloquentReminder
     */
    public $reminder;
    
    /**
     * Create a new job instance.
     *
     * @param \App\Entities\User                             $user
     * @param \Cartalyst\Sentinel\Reminders\EloquentReminder $reminder
     */
    public function __construct(User $user, EloquentReminder $reminder)
    {
        $this->user = $user;
        $this->reminder = $reminder;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(new Reminder($this->user, $this->reminder->code));
    }
}
