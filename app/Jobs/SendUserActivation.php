<?php

namespace App\Jobs;

use Activation;
use App\Entities\User;
use App\Mail\Activation as ActivationMail;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendUserActivation
 * @package App\Jobs
 */
class SendUserActivation extends BaseJob
{

    /**
     * @var \App\Entities\User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param \App\Entities\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (Activation::completed($this->user)) {
            return;
        }

        $activation = Activation::create($this->user);

        Mail::send(new ActivationMail($this->user, $activation));
    }
}
