<?php

namespace App\Jobs;

use App\Entities\User;
use ImageUploader;

/**
 * Class SaveUserAvatar
 * @package App\Jobs
 */
class SaveUserAvatar extends BaseJob
{
    /**
     * @var \App\Entities\User
     */
    private $user;
    
    /**
     * @var string
     */
    private $avatar;
    
    /**
     * Create a new job instance.
     *
     * @var \App\Entities\User $user
     * @var string             $avatar
     *
     * @return void
     */
    public function __construct(User $user, string $avatar = '')
    {
        $this->user = $user;
        $this->avatar = $avatar;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->avatar) {
            return;
        }
        
        $avatar = ImageUploader::copy($this->avatar, 'user');
        
        $this->user->avatar = $avatar;
        
        $this->user->save();
    }
}
