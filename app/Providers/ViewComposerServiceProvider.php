<?php

namespace App\Providers;


use App\ViewComposers\Catalog;
use App\ViewComposers\CountriesArray;
use App\ViewComposers\DashboardComposer;
use App\ViewComposers\Dates;
use App\ViewComposers\LanguagesArrayComposer;
use App\ViewComposers\PageMenu;
use Illuminate\Support\ServiceProvider;

/**
 * Class ViewComposer
 * @package App\Http\Middleware;
 */
class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function boot()
    {
        //admin
        view()->composer('user.tabs.verification', CountriesArray::class);
        view()->composer('variable.types.multi_select', LanguagesArrayComposer::class);
        view()->composer('dashboard.index', DashboardComposer::class);
        view()->composer('user.tabs.verification', Dates::class);
        view()->composer(['partials.modals.catalog', 'partials.components.catalog-group'], Catalog::class);
        view()->composer(['partials.menus.main','partials.menus.bottom'], PageMenu::class);

        // front
        view()->composer(['views.user.edit', 'partials.modals.register'], CountriesArray::class);

    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }
}