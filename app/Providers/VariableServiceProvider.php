<?php

namespace App\Providers;

use App\Classes\Variable;
use Illuminate\Support\ServiceProvider;

/**
 * Class VariableServiceProvider
 * @package App\Providers
 */
class VariableServiceProvider extends ServiceProvider
{
    
    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->singleton(
            'variable',
            function () {
                return new Variable();
            }
        );
    }
    
    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        variable()->mergeWithConfig();
    }
}