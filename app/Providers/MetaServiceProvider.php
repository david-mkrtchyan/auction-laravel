<?php

namespace App\Providers;

use App\Classes\Meta;
use Illuminate\Support\ServiceProvider;

/**
 * Class MetaServiceProvider
 * @package App\Providers
 */
class MetaServiceProvider extends ServiceProvider
{
    
    /**
     * register
     */
    public function register()
    {
        $this->app->singleton(
            'meta',
            function () {
                return new Meta();
            }
        );
    }
}