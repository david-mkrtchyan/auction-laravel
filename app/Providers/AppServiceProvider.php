<?php

namespace App\Providers;

use App\Classes\ConfigPermissions;
use App\Contracts\Permissions;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Classes\AppValidator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->_registerValidators();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->_registerDevDependencies();

        $this->app->bind(Permissions::class, ConfigPermissions::class);

        if (config('app.force_ssl')) {
            URL::forceScheme('https');
        }
    }

    /**
     * Register any development application services.
     *
     * @return void
     */
    private function _registerDevDependencies()
    {
        if (config('app.env') != 'production') {
            //
        }
    }

    /**
     * Register any custom validation rule
     *
     * @return void
     */
    private function _registerValidators()
    {
        Validator::resolver(
            function ($translator, $data, $rules, $messages) {
                return new AppValidator($translator, $data, $rules, $messages);
            }
        );
    }
}
