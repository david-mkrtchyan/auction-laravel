<?php

namespace App\Providers;

use App\Classes\Text;
use Illuminate\Support\ServiceProvider;

/**
 * Class TextServiceProvider
 * @package App\Providers
 */
class TextServiceProvider extends ServiceProvider
{
    /**
     * register
     */
    public function register()
    {
        $this->app->bind('text', Text::class);
    }

}