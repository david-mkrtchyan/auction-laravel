<?php

namespace App\Entities;

use App\Entities\Traits\MetaGettable as MetaGettableTrait;
use App\Entities\Traits\PositionSortedTrait;
use App\Entities\Traits\VisibleTrait;
use App\Entities\Traits\WithImages;
use App\Entities\Traits\WithTranslationsTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Entities
 */
class Product extends Model
{
    
    use Translatable;
    use WithTranslationsTrait;
    use VisibleTrait;
    use MetaGettableTrait;
    use PositionSortedTrait;
    use PositionSortedTrait;
    use WithImages;

    const STATUS_TRUE  = 1;
    const STATUS_FALSE = 0;
    const typeImage = Images::typeProduct;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'title',
        'description',
    ];
    
    /**
     * @var array
     */
    protected $fillable = [
        'position',
        'view_count',
        'status',
        'group_id',
        'start_price',
        'price',
        'price_step',
    ];
    
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_FALSE,
        self::STATUS_TRUE,
    ];



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function group()
    {
        return $this->hasOne('App\Entities\Group', 'id', 'group_id');
    }

    public function checkImageExists($image)
    {
        if( Images::checkExists($image)){
            return $image;
        }

        return false;
    }

}