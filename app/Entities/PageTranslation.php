<?php

namespace App\Entities;

use Eloquent;

/**
 * Class PageTranslation
 * @package App\Entities
 */
class PageTranslation extends Eloquent
{
    
    /**
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'short_content',
        'content',
        'meta_keywords',
        'meta_title',
        'meta_description',
    ];
}