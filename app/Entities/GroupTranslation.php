<?php

namespace App\Entities;

use Eloquent;

/**
 * Class PageTranslation
 * @package App\Entities
 */
class GroupTranslation extends Eloquent
{
    
    /**
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];
}