<?php

namespace App\Entities;

use Eloquent;

/**
 * Class ProductTranslation
 * @package App\Entities
 */
class ProductTranslation extends Eloquent
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
    ];
}