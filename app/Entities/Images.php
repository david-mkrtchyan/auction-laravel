<?php

namespace App\Entities;

use App\Entities\Traits\WithTypes;
use Illuminate\Database\Eloquent\Model;
use ImageUploader;
use phpDocumentor\Reflection\Types\Array_;

/**
 * Class File
 * @package App\Entities
 */
class Images extends Model
{

    use  WithTypes;

    const typeGroup = 'group';
    const typeProduct = 'product';
    /**
     * @var array
     */
    protected $fillable = [ 
        'name',
        'object_id',
        'type',
        'sort',
    ];

    /**
     * @var array
     */
    public static $types = [
        self::typeGroup,
        self::typeProduct,
    ];

    public static function deleteImage($id, $type)
    {
        $model = Images::whereType($type)->where('id', $id)->first();

        if (!$model) {
            return false;
        }

        if ($model->delete()) {
            if (self::unlinkImage($model->name)) {
                return true;
            }
        }

        return false;
    }

    public static function unlinkImage($path)
    {
        if (self::checkExists($path) && unlink($path)) {
            return true;
        }

        return false;
    }

    public static function checkExists($path)
    {
        $path = public_path() . $path;

        if (is_file($path)) {
            return $path;
        }

        return false;
    }

    public static function uploadAndSaveImages($images, $objectId, $direct = null, $type)
    {
        if (!is_array($images)) {
            $images = [$images];
        }

        foreach ($images as $image) {
            if ($path = ImageUploader::upload($image, $direct, null, null)) {
                $image = new self();
                $image->name = $path;
                $image->object_id = $objectId;
                $image->type = $type;
                $image->save();
            }
        }
    }
}