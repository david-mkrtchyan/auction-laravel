<?php

namespace App\Entities;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Translation model
 * @package App\Entities
 */
class Translation extends Model
{
    
    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $group
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfTranslatedGroup($query, $group)
    {
        return $query->where('group', $group)->whereNotNull('value');
    }
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $ordered
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderByGroupKeys($query, $ordered)
    {
        if ($ordered) {
            $query->orderBy('group')->orderBy('key');
        }
        
        return $query;
    }
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSelectDistinctGroup($query)
    {
        $select = '';
        
        switch (DB::getDriverName()) {
            case 'mysql':
                $select = 'DISTINCT `group`';
                break;
            default:
                $select = 'DISTINCT "group"';
                break;
        }
        
        return $query->select(DB::raw($select));
    }
    
}
