<?php

namespace App\Entities;

use App\Entities\Traits\PositionSortedTrait;
use App\Entities\Traits\WithTranslationsTrait;
use App\Entities\Traits\WithTypes;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Variable
 * @package App\Entities
 */
class Variable extends Model
{
    use Translatable;
    use WithTranslationsTrait;
    use PositionSortedTrait;
    use WithTypes;
    
    /**
     * @var array
     */
    public $translatedAttributes = ['text'];
    
    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'key',
        'name',
        'description',
        'multilingual',
        'value',
        'text',
        'status',
        'position',
    ];
    
    /**
     * @var int
     */
    public static $defaultType = 1;
    
    /**
     * @var array
     */
    protected $types = [
        1  => 'text',
        2  => 'textarea',
        3  => 'image',
        4  => 'weekday',
        5  => 'time',
        6  => 'boolean',
        7  => 'commission_type',
        8  => 'multi_select',
        9  => 'file',
        10 => 'range',
        11 => 'number',
    ];
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHidden(Builder $query): Builder
    {
        return $query->where('is_hidden', false);
    }
    
    /**
     * @param string $value
     */
    public function setKeyAttribute($value)
    {
        $this->attributes['key'] = preg_replace('/\W+\./', '_', $value);
    }
    
    /**
     * @param string $value
     */
    public function setValueAttribute($value)
    {
        switch ($this->getStringType()) {
            case 'multi_select':
                $value = (empty($value) ? '[]' : json_encode($value));
                
                break;
            
            case 'range':
                $value = is_array($value) ? $value : [];
                
                $value = [
                    'from' => array_get($value, 'from'),
                    'to'   => array_get($value, 'to'),
                ];
                
                $value = json_encode($value);
                
                break;
        };
        
        $this->attributes['value'] = $value;
    }
    
    /**
     * @param mixed $value
     *
     * @return  mixed
     */
    public function getValueAttribute($value)
    {
        if ($this->multilingual) {
            return $this->text;
        }
        
        if ($this->getStringType() == 'multi_select') {
            if (empty($value)) {
                return [];
            }
            
            $values = [];
            
            foreach (json_decode($value, true) as $value) {
                $values[$value] = $value;
            }
            
            return $values;
        }
        
        if ($this->getStringType() == 'range') {
            return json_decode($value, true);
        }
        
        return $value;
    }
    
    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeVisible($query)
    {
        return $query->where($this->getTable().'.status', true);
    }
    
    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}