<?php

namespace App\Entities;

use Activation;
use App\Entities\Traits\ExtendedMutator;
use Cartalyst\Sentinel\Users\EloquentUser;
use Crypt;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App
 */
class User extends EloquentUser
{
    
    use Notifiable,
        ExtendedMutator;
    
    /**
     * @var array
     */
    protected $fillable = [
        'email',
        'phone',
        'first_name',
        'last_name',
        'avatar',
        'gender',
        'birth_day',
        'birth_month',
        'birth_year',
        'verified',
        'address',
        'city',
        'postal_code',
        'country_id',
        'user_name',
        'password',
        'permissions',
    ];
    
    /**
     * {@inheritDoc}
     */
    protected $hidden = [
        'password',
        'two_factor_secret',
    ];
    
    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'array',
    ];
    
    /**
     * @var array
     */
    protected $genders = [
        'male',
        'female',
    ];
    
    /**
     * Dynamically pass missing methods to the user.
     *
     * @param  string $method
     * @param  array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $methods = ['hasAccess'];

        if (in_array($method, $methods)) {
            if (isset($this->permissions['superuser']) && $this->permissions['superuser'] == true) {
                return true;
            }
        }
        
        return parent::__call($method, $parameters);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socials()
    {
        return $this->hasMany(UserSocial::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wallets()
    {
        return $this->hasMany(Wallet::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function referrals()
    {
        return $this->hasOne(Referral::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function binary_referrals()
    {
        return $this->hasOne(BinaryReferral::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function matrix()
    {
        return $this->hasOne(UserMatrix::class);
    }
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeJoinActivations(Builder $query)
    {
        return $query->leftJoin('activations', 'activations.user_id', '=', $this->getTable().'.id');
    }
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeJoinSetting(Builder $query)
    {
        return $query->leftJoin('user_setting', 'user_setting.user_id', '=', $this->getTable().'.id');
    }
    
    /**
     * @param mixed $value
     */
    public function setTwoFactorSecretAttribute($value)
    {
        $this->attributes['two_factor_secret'] = $value ? Crypt::encrypt($value) : null;
    }
    
    /**
     * @param mixed $value
     *
     * @return null|string
     */
    public function getTwoFactorSecretAttribute($value)
    {
        return $value ? Crypt::decrypt($value) : null;
    }
    
    /**
     * @param mixed $value
     *
     * @return null|string
     */
    public function getActivatedAttribute($value)
    {
        return (bool) Activation::completed($this);
    }
    
    /**
     * @return array
     */
    public function getGenders(): array
    {
        return $this->genders;
    }
    
    /**
     * @param int $id
     *
     * @return string|null
     */
    public function getGenderKeyById(int $id)
    {
        foreach ($this->genders as $_id => $key) {
            if ($_id != $id) {
                continue;
            }
            
            return $key;
        }
        
        return null;
    }
    
    /**
     * @return string
     */
    public function getReferralCode()
    {
        return $this->user_name ? $this->user_name : $this->id;
    }
    
    /**
     * @return string
     */
    public function getFullName()
    {
        return trim($this->last_name.' '.$this->first_name);
    }
    
    /**
     * @return string
     */
    public function getStringGender(): string
    {
        return $this->getGenderKeyById($this->gender);
    }
    
    /**
     * @return string
     */
    public function getCountry()
    {
        if (!$this->country) {
            $this->load('country');
        }
        
        return $this->country ? $this->country->getTitle() : trans('front_labels.not selected');
    }
    
    /**
     * @return float
     */
    public function getBalance()
    {
        $balance = 0;
        
        if (!$this->wallets) {
            $this->load('wallets');
        }
        
        foreach ($this->wallets as $wallet) {
            $balance += $wallet->getBalance();
        }
        
        return $balance;
    }
    
    /**
     * @return int
     */
    public function getLevel()
    {
        $level = null;
        
        if (!$this->setting || !$this->setting->level) {
            $this->load('setting.level');
        }
        
        $level = $this->setting->level;
        
        return $level;
    }
    
    /**
     * @return string
     */
    public function getActiveLeg()
    {
        return $this->setting
            ? ($this->setting->active_leg ? $this->setting->active_leg : 'left')
            : 'left';
    }
    
    /**
     * @return bool
     */
    public function botIsRunning()
    {
        return $this->setting ? $this->setting->bot_status : false;
    }


    /**
     * @return bool
     */
    public function hasContracts()
    {
        return $this->setting ? (bool) $this->setting->has_contracts : false;
    }
    
    /**
     * @return bool
     */
    public function twoFactorEnabled()
    {
        return (bool) $this->two_factor_secret;
    }
    
    /**
     * @return bool
     */
    public function isVerified()
    {
        return (bool) $this->verified || $this->hasAccess('verified');
    }
}
