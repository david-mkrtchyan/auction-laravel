<?php

namespace App\Entities;

use App\Entities\Traits\WithStatuses;
use App\Entities\Traits\WithTypes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 * @package App\Entities
 */
class File extends Model
{
    
    use WithStatuses,
        WithTypes;
    
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
        'src',
        'status',
    ];
    
    /**
     * @var array
     */
    protected $types = [
        'passport',
        'selfie',
        'address',
    ];
    
    /**
     * @var array
     */
    protected $statuses = [
        'new',
        'approved',
        'declined',
    ];
    
    /**
     * @return string
     */
    public function getSrc()
    {
        return storage_path($this->src);
    }
    
    /**
     * @return string
     */
    public function getLink()
    {
        return route('admin.user.file.show', [$this->user_id, $this->id]);
    }
}