<?php

namespace App\Entities;

use App\Entities\Traits\ExtendedMutator;
use App\Entities\Traits\PositionSortedTrait;
use App\Entities\Traits\VisibleTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * @package App\Entities
 */
class Currency extends Model
{
    
    use VisibleTrait,
        PositionSortedTrait,
        ExtendedMutator;
    
    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'name',
        'icon',
        'position',
        'status',
        'is_trading',
        'trade_amount_from',
        'trade_amount_to',
    ];
    
    /**
     * @var array
     */
    protected $casts_integer = [
        'dealing_rate',
    ];
    
    /**
     * @var array
     */
    protected $casts_currency = [
        'trade_amount_from',
        'trade_amount_to',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rates()
    {
        return $this->hasMany(CurrencyRate::class);
    }
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDefault(Builder $query): Builder
    {
        return $query->where('key', config('currency.default'));
    }
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTrading(Builder $query): Builder
    {
        return $query->where('is_trading', true);
    }
    
    /**
     * @param $value
     */
    public function setKeyAttribute($value)
    {
        if (empty($value)) {
            $value = $this->attributes['name'];
        }
        
        $this->attributes['key'] = str_slug($value, '_');
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return mb_strtoupper($this->key);
    }
    
    /**
     * @return float
     */
    public function getRate()
    {
        return $this->dealing_rate;
    }
    
    /**
     * @return float
     */
    public function getFormattedRate()
    {
        return number_format($this->getRate(), 8, '.', ' ');
    }
}