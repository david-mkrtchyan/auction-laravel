<?php

namespace App\Entities\Traits;

/**
 * Trait ExtendedMutator
 * @package App\Entities\Traits
 */
trait ExtendedMutator
{
    
    /**
     * @return array
     */
    public function getCastsInteger()
    {
        return isset($this->casts_integer) ? (array) $this->casts_integer : [];
    }
    
    /**
     * @return array
     */
    public function getCastsCurrency()
    {
        return isset($this->casts_currency) ? (array) $this->casts_currency : [];
    }
    
    /**
     * @return int
     */
    public function getCastsCurrencyDecimalPoints()
    {
        return 8;
    }
    
    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        if (!empty($this->getCastsInteger()) && in_array($key, $this->getCastsInteger())) {
            $this->attributes[$key] = intval(cbcmul($value, 100, 0));
            
            return $this;
        }
        
        if (!empty($this->getCastsCurrency()) && in_array($key, $this->getCastsCurrency())) {
            $decimal_points = $this->getCastsCurrencyDecimalPoints();
            
            $this->attributes[$key] = intval(cbcmul($value, pow(10, $decimal_points), $decimal_points));
            
            return $this;
        }
        
        return parent::setAttribute($key, $value);
    }
    
    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (!empty($this->getCastsInteger()) && in_array($key, $this->getCastsInteger())) {
            return isset($this->attributes[$key])
                ? round(cbcdiv($this->attributes[$key], 100, 2), 2)
                : 0;
        }
        
        if (!empty($this->getCastsCurrency()) && in_array($key, $this->getCastsCurrency())) {
            $decimal_points = $this->getCastsCurrencyDecimalPoints();
            
            return isset($this->attributes[$key])
                ? round(cbcdiv($this->attributes[$key], pow(10, $decimal_points), $decimal_points), $decimal_points)
                : 0;
        }
        
        return parent::getAttribute($key);
    }
    
    /**
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        
        $attributes = $this->addCastsIntegerAttributesToArray($attributes);
        
        $attributes = $this->addCastsCurrencyAttributesToArray($attributes);
        
        return $attributes;
    }
    
    /**
     * @param array $attributes
     *
     * @return array
     */
    protected function addCastsIntegerAttributesToArray(array $attributes = [])
    {
        foreach ($this->getCastsInteger() as $attribute) {
            if (!isset($attributes[$attribute])) {
                continue;
            }
            
            $attributes[$attribute] = $this->getAttribute($attribute);
        }
        
        return $attributes;
    }
    
    /**
     * @param array $attributes
     *
     * @return array
     */
    protected function addCastsCurrencyAttributesToArray(array $attributes = [])
    {
        foreach ($this->getCastsCurrency() as $attribute) {
            if (!isset($attributes[$attribute])) {
                continue;
            }
            
            $attributes[$attribute] = $this->getAttribute($attribute);
        }
        
        return $attributes;
    }
}