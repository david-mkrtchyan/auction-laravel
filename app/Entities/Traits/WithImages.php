<?php
namespace App\Entities\Traits;

trait WithImages
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Entities\Images', 'object_id', 'id')->whereType(self::typeImage)->orderBy('sort');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne('App\Entities\Images', 'object_id', 'id')->whereType(self::typeImage)->orderBy('sort');
    }
}