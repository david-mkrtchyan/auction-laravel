<?php
namespace App\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait RankSorted{

    public function scopeRankSorted(Builder $query,  $order = 'ASC')
    {
        return $query->orderBy($this->getTable().'.rank', $order);
    }

}