<?php

namespace App\Entities\Traits;

use App\Entities\Media;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Trait WithMedia
 * @package App\Entities\Traits
 */
trait WithMedia
{
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function media(): MorphMany
    {
        return $this->morphMany(Media::class, 'mediable')->positionSorted();
    }
}