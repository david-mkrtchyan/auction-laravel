<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VariableTranslation
 * @package App\Entities
 */
class VariableTranslation extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * @var array
     */
    protected $fillable = ['text'];
}