<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 * @package App\Entities
 */
class Country extends Model
{
    
    /**
     * @param bool $with_native
     *
     * @return string
     */
    public function getTitle($with_native = false)
    {
        return $with_native
            ? ($this->native ? $this->title.' ('.$this->native.')' : $this->title)
            : $this->title;
    }
    
    /**
     * @return string
     */
    public function getFlagKey()
    {
        return mb_strtolower($this->slug);
    }
}