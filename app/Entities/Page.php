<?php

namespace App\Entities;

use App\Contracts\MetaGettable;
use App\Entities\Traits\MetaGettable as MetaGettableTrait;
use App\Entities\Traits\RankSorted;
use App\Entities\Traits\VisibleTrait;
use App\Entities\Traits\WithStatuses;
use App\Entities\Traits\WithTranslationsTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 * @package App\Entities
 */
class Page extends Model implements MetaGettable
{
    
    use Translatable;
    use WithTranslationsTrait;
    use VisibleTrait;
    use MetaGettableTrait;
    use RankSorted;

    const STATUS_TRUE  = 1;
    const STATUS_FALSE = 0;

    const POSITION_TOP     = 0;
    const POSITION_BOTTOM  = 1;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'short_content',
        'content',
        'h1',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];
    
    /**
     * @var array
     */
    protected $fillable = [
        'slug',
        'template',
        'parent_id',
        'status',
        'position',
        'name',
        'image',
        'template',
        'short_content',
        'content',
        'h1',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'rank',
    ];


    /**
     * @var array
     */
    protected $guarded = [];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Page::class, 'parent_id')->with('parent');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(Page::class, 'parent_id');
    }
    
    /**
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        if (empty($value)) {
            $value = $this->attributes['name'];
        }
        
        $this->attributes['slug'] = str_slug($value);
    }
    
    /**
     * @param $value
     */
    public function setParentIdAttribute($value)
    {
        $this->attributes['parent_id'] = empty($value) ? null : intval($value);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->name;
    }
    
    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    /**
     * @return string
     */
    public function getUrl()
    {
        return route('page.show', $this->getSlug());
    }
    
    /**
     * @return string
     */
    public function getTemplate()
    {
        return empty($this->template) ? 'default' : $this->template;
    }

    /**
    * @return array
    */
    public static function positions()
    {
        return [
            self::POSITION_TOP     =>   trans('front_labels.top'),
            self::POSITION_BOTTOM  =>   trans('front_labels.bottom'),
        ];
    }


    /**
     * @var array
     */
    protected $statuses = [
        self::STATUS_FALSE,
        self::STATUS_TRUE,
    ];
}