<?php

namespace App\Entities;

use App\Entities\Traits\MetaGettable as MetaGettableTrait;
use App\Entities\Traits\VisibleTrait;
use App\Entities\Traits\WithImages;
use App\Entities\Traits\WithTranslationsTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class Group
 * @package App\Entities
 */
class Group extends Model
{

    use Translatable;
    use WithTranslationsTrait;
    use VisibleTrait;
    use MetaGettableTrait;
    use WithImages;

    const STATUS_TRUE = 1;
    const STATUS_FALSE = 0;
    const typeImage = Images::typeGroup;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'description',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'start_time',
        'end_time',
        'status',
    ];

    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @var array
     */
    protected $statuses = [
        self::STATUS_FALSE,
        self::STATUS_TRUE,
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product()
    {
        return $this->hasMany('App\Entities\Product', 'group_id', 'id')->whereStatus(true)->orderByDesc('position')->orderByDesc('id');
    }

    public function checkImageExists()
    {
        if (($image = $this->image) && Images::checkExists($image->name)) {
            return $image->name;
        }

        return false;
    }

    public static function scopeList()
    {
        $groups = self::joinTranslations('groups', 'group_translations')->select(
            'groups.id as id',
            'group_translations.name as name'
        )->get();

        return Arr::pluck($groups, 'name', 'id');
    }

    public function scopeCheckTime()
    {

        if ($this->setFormatDate($this->end_time, "Y.m.d H:i:s") > date('Y.m.d H:i:s')) {
            return true;
        }

        return false;
    }

    public function setFormatDate($date, $format = "d/m/Y")
    {
        return \Carbon\Carbon::parse($date)->format($format);
    }
}