<?php

namespace App\Exceptions;

use Exception;

/***
 * Class ExchangeApiException
 * @package App\Exceptions
 */
class ExchangeApiException extends Exception
{
    
    /**
     * @var int
     */
    protected $code = 502;
}