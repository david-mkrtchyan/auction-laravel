<?php

namespace App\Exceptions;

use App\Facades\FlashMessages;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use OAuth\Common\Http\Exception\TokenResponseException;

/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];
    
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
    
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     *
     * @return void
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $exception
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        if (
            $exception instanceof TokenResponseException
            ||
            $exception instanceof TokenMismatchException
        ) {

            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(
                    [
                        'status'  => 'error',
                        'message' => trans('front_messages.session expired'),
                    ]
                );
            }
            
            FlashMessages::add('error', trans('front_messages.session expired'));
            
            return redirect()->back();
        }
        
        return parent::render($request, $exception);
    }
}
