<?php

namespace App\Events;

use App\Entities\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserVerificationDataChanged
 * @package App\Events
 */
class UserVerificationDataChanged
{
    
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    /**
     * @var \App\Entities\User
     */
    public $user;
    
    /**
     * Create a new event instance.
     *
     * @param \App\Entities\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    
    /**
     * Get the channels the event should broadcast on.
     *
     * @return void
     */
    public function broadcastOn()
    {
        //return new PrivateChannel('channel-name');
    }
}