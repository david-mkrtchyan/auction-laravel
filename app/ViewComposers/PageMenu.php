<?php

namespace App\ViewComposers;

use App\Entities\Page;
use Illuminate\View\View;

/**
 * Class Dates
 * @package App\ViewComposers
 */
class PageMenu
{
    const CATALOG = 'catalog';

    /**
     * Bind data to the view.
     *
     * @param  View $view
     */
    public function compose(View $view)
    {

        $pagesTop = Page::with(['translations', 'parent', 'parent.translations'])->wherePosition(Page::POSITION_TOP)->visible()->rankSorted()->get();

        $pagesBottom = Page::with(['translations', 'parent', 'parent.translations'])->wherePosition(Page::POSITION_BOTTOM)->visible()->rankSorted()->get();

        $view->with('pagesTop', $pagesTop);

        $view->with('pagesBottom', $pagesBottom);

        $view->with('catalog', self::CATALOG);
    }
}
