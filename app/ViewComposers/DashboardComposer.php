<?php

namespace App\ViewComposers;

use App\Entities\User;
use Illuminate\View\View;
use JavaScript;
use Sentinel;

/**
 * Class DashboardComposer
 * @package App\ViewComposers
 */
class DashboardComposer
{
    /**
     * @var \Cartalyst\Sentinel\Users\UserInterface
     */
    private $user;
    
    /**
     * @var array
     */
    private $javascript = ['_dashboard'];
    
    /**
     * DashboardComposer constructor.
     *
     */
    public function __construct()
    {
        $this->user = Sentinel::getUser();
    }
    
    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        if ($this->user->hasAccess('dashboard.general')) {
            $this->_blocks($view);
        }

        if (isset($this->javascript['_dashboard']) && count($this->javascript['_dashboard'])) {
            JavaScript::put(
                [
                    '_dashboard' => $this->javascript['_dashboard'],
                ]
            );
        }
    }

    /**
     * @param \Illuminate\View\View $view
     */
    private function _blocks(View $view)
    {
        $ttl = carbon()->now()->subHours(24);
        
        $users_count = User::count();
        $users_count_24 = User::where('created_at', '>=', $ttl)->count();

        $view->with(
            [
                'users_count'        => $users_count,
                'users_count_24'     => $users_count_24,
            ]
        );
    }

}