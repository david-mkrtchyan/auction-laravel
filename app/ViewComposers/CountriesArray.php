<?php

namespace App\ViewComposers;

use App\Entities\Country;
use Illuminate\View\View;

/**
 * Class CountriesArray
 * @package App\ViewComposers
 */
class CountriesArray
{
    
    /**
     * Bind data to the view.
     *
     * @param  View $view
     */
    public function compose(View $view)
    {
        $countries = [];
    
        $_countries = Country::select('id', 'title', 'native')
            ->whereNotIn('id', [120])
            ->get();
        
        /**
         * @var Country $country
         */
        foreach ($_countries as $country) {
            $countries[$country->id] = $country->getTitle(true);
        }
        
        $view->with('countries', $countries);
    }
}
