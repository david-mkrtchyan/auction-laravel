<?php
namespace App\ViewComposers;

use App\Entities\Group;
use Illuminate\View\View;

class Catalog{

    public function compose(View $view)
    {
        $groups  = Group::joinTranslations('groups','group_translations')->select(
            'groups.id as id',
            'groups.start_time as start_time',
            'groups.end_time as end_time',
            'group_translations.name as name',
            'group_translations.name as description'
        )->visible()->paginate(35);

        $view->with('groups',$groups);
    }

}