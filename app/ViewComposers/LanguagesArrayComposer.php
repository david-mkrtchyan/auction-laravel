<?php

namespace App\ViewComposers;

use Illuminate\View\View;

/**
 * Class LanguagesArrayComposer
 * @package App\ViewComposers
 */
class LanguagesArrayComposer
{
    
    /**
     * Bind data to the view.
     *
     * @param  View $view
     */
    public function compose(View $view)
    {
        $values = [];
        
        foreach (config('app.locales') as $locale) {
            $values[$locale] = trans('labels.tab_'.$locale);
        }
        
        $view->with('values', $values);
    }
}