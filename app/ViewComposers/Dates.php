<?php

namespace App\ViewComposers;

use Illuminate\View\View;

/**
 * Class Dates
 * @package App\ViewComposers
 */
class Dates
{
    
    /**
     * Bind data to the view.
     *
     * @param  View $view
     */
    public function compose(View $view)
    {
        $years = [];
        
        $from = carbon()->now()->year;
        $to = $from - 100;
        
        for ($i = $from; $i >= $to; $i--) {
            $years[$i] = $i;
        }
        
        $view->with('years', $years);
        
        $months = [];
        
        for ($i = 1; $i <= 12; $i++) {
            $months[$i] = $i;
        }
        
        $view->with('months', $months);
        
        $days = [];
        
        for ($i = 1; $i <= 31; $i++) {
            $days[$i] = $i;
        }
        
        $view->with('days', $days);
    }
}
