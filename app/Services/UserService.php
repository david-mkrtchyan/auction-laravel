<?php

namespace App\Services;

use Activation;
use App\Entities\User;
use App\Jobs\SaveUserAvatar;
use Sentinel;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    
    /**
     * @param array $data
     * @param bool  $activate
     *
     * @return bool|\Cartalyst\Sentinel\Users\UserInterface|User
     */
    public function create(array $data, $activate = false)
    {
        if ($activate) {
            $user = Sentinel::registerAndActivate($data);
        } else {
            $user = Sentinel::register($data);
        }
        
        $role = $this->getClientsGroup();
        $role->users()->attach($user);

        if (isset($data['avatar'])) {
            dispatch(new SaveUserAvatar($user, $data['avatar']))->onQueue('users');
        }

        return $user;
    }
    
    /**
     * @param array $data
     *
     * @return \App\Entities\User
     */
    public function findOrCreateActivatedUser(array $data)
    {
        $user = Sentinel::findByCredentials(['email' => $data['email']]);
        
        if (!$user) {
            $user = $this->create($data, true);
        }
        
        return $user;
    }
    
    /**
     * @param string $referral_key
     *
     * @return \App\Entities\User
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findUserByReferralKey(string $referral_key)
    {
        return User::with('referrals')
            ->where(
                preg_match('~[\D]+~', $referral_key) ? 'user_name' : 'id',
                $referral_key
            )
            ->firstOrFail();
    }
    
    /**
     * @param \App\Entities\User $user
     *
     * @return \App\Entities\User $user
     */
    public function activate(User $user)
    {
        if (Activation::completed($user)) {
            return $user;
        }
        
        $activation = Activation::exists($user);
        
        if (!$activation) {
            $activation = Activation::create($user);
        }
        
        Activation::complete($user, $activation->getCode());
        
        return $user;
    }
    
    /**
     * @param \App\Entities\User $user
     *
     * @return \App\Entities\User $user
     */
    public function deactivate(User $user)
    {
        $activation = Activation::completed($user);
        
        if ($activation) {
            $activation->delete();
        }
        
        return $user;
    }
    
    /**
     * @return \Cartalyst\Sentinel\Roles\RoleInterface
     */
    public function getClientsGroup()
    {
        return Sentinel::getRoleRepository()->findBySlug('clients');
    }

}
