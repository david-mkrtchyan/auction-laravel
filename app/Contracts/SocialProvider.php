<?php

namespace App\Contracts;

/**
 * Class SocialProvider
 * @package App\Contracts
 */
interface SocialProvider
{
    
    /**
     * @return string
     */
    public function loginUrl();
    
    /**
     * @param string $code
     *
     * @return array
     */
    public function profile($code);
}