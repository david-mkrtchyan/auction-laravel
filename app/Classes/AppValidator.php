<?php

namespace App\Classes;

use App\Services\UserService;
use Illuminate\Validation\Validator;
use PragmaRX\Google2FA\Google2FA;

/**
 * Class AppValidator
 * @package App\Classes
 */
class AppValidator extends Validator
{
    
    /**
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     *
     * @return bool
     */
    public function validatePhone($attribute, $value, $parameters)
    {
        if (empty($value)) {
            return true;
        }
        
        return preg_match('/^[0-9]{10,15}$/', $value);
    }
    
    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool|int
     */
    public function validateValidOtp($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'valid_otp');
        
        return app(Google2FA::class)->verifyKey($parameters['0'], $value);
    }
    
    /**
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     *
     * @return bool
     */
    public function validateGreaterThan($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'greater_than');
        
        return $value > $parameters[0];
    }
    
    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     * @throws \Exception
     */
    public function validateUsedOtp($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'used_otp');
        
        return !cache()->has($parameters[0].':'.$value);
    }
    
    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     * @throws \Exception
     */
    public function validateCryptoAddress($attribute, $value, $parameters)
    {
        $value = (string) $value;
        
        return strlen($value) >= 33 && strlen($value) <= 34;
    }
    
    /**
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     *
     * @return bool
     */
    public function validateUserName($attribute, $value, $parameters)
    {
        return preg_match('/^[a-zA-Z0-9]+$/', $value);
    }
    
    /**
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     *
     * @return bool
     */
    public function validateCryptoAmount($attribute, $value, $parameters)
    {
        $parts = explode('.', $value);
        
        if (!isset($parts[1])) {
            return true;
        }
        
        return strlen($parts[1]) <= 8;
    }
    
    /**
     * @param string $attribute
     * @param string $value
     * @param array  $parameters
     *
     * @return bool
     */
    public function validateExistsInBinaryTree($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'exists_in_binary_tree');
        
        $user = app(UserService::class)->findUserByReferralKey($parameters[0]);
        
        if (!$user) {
            return false;
        }
        
        return $user->binary_referrals()->count();
    }
    
    /**
     * @param  string $message
     * @param  string $attribute
     * @param  string $rule
     * @param  array  $parameters
     *
     * @return string
     */
    public function replacePhone($message, $attribute, $rule, $parameters)
    {
        return str_replace(':attribute', $attribute, $message);
    }
    
    /**
     * @param  string $message
     * @param  string $attribute
     * @param  string $rule
     * @param  array  $parameters
     *
     * @return string
     */
    public function replaceGreaterThan($message, $attribute, $rule, $parameters)
    {
        return str_replace([':attribute', ':value'], [$attribute, $parameters[0]], $message);
    }
}