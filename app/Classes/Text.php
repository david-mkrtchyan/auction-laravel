<?php

namespace App\Classes;

class Text{

    //Вырезать текст
    public static function cutText($string, $lenght = 0)
    {
        $string = strip_tags($string);

        if (mb_strlen($string, 'UTF-8') > $lenght) {
            $content = str_replace('\n', '', mb_substr($string,
                0, $lenght, 'UTF-8'));
            return $content . '…';
        } else {
            return str_replace('\n', '', $string);
        }
    }

}
