<?php

namespace App\Classes;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;

/**
 * Class ImageUploader
 * @package App\Classes
 */
class ImageUploader
{

    /**
     * @var bool
     */
    public $overwrite = false;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $files;

    /**
     * ImageUploader constructor.
     *
     * @param \Illuminate\Filesystem\Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param string                        $module
     * @param null                          $dir
     * @param null                          $filename
     *
     * @return string
     */
    public function upload(UploadedFile $file, $module = "page", $dir = null, $filename = null)
    {
        if ($file) {
            $filename = $filename
                ? $filename
                : md5(microtime(true).str_slug($file->getClientOriginalName())).".".
                strtolower($file->getClientOriginalExtension());

            $dir = $this->_getRandomDir($dir, $filename);

            $destination = public_path().'/uploads/images/'.$module.'/'.$dir;
            $path = '/uploads/images/'.$module.'/'.$dir.'/'.$filename;

            $uploaded = $file->move($destination, $filename);


            if ($uploaded) {
                return $path;
            }
        }

        return '';
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param int                           $user_id
     *
     * @return string
     */
    public function uploadUserFile(UploadedFile $file, int $user_id)
    {
        if ($file) {
            $filename = md5(microtime(true).str_slug($file->getClientOriginalName())).".".
                strtolower($file->getClientOriginalExtension());

            $dir = $this->_getRandomDir($user_id, $filename);

            $destination = storage_path().'/user_files/'.$dir;
            $path = '/user_files/'.$dir.'/'.$filename;

            $uploaded = $file->move($destination, $filename);

            if ($uploaded) {
                return $path;
            }
        }

        return '';
    }

    /**
     * @param string $filePath
     * @param string $module
     * @param null   $dir
     * @param null   $filename
     *
     * @return string
     *
     * Copy local file on server
     */
    public function copy($filePath, $module = "page", $dir = null, $filename = null)
    {
        if ($filePath) {
            $pathInfo = pathinfo($filePath);

            $filename = $filename
                ? $filename
                : md5(microtime(true).str_slug($filePath)).".".strtolower(
                    $this->_parseExtension($pathInfo['extension'])
                );

            $dir = $this->_getRandomDir($dir, $filename);

            $destination = public_path().'/uploads/images/'.$module.'/'.$dir;
            $path = '/uploads/images/'.$module.'/'.$dir.'/'.$filename;

            $file = $destination.'/'.$filename;

            if (!@$this->files->exists($file)) {
                @$this->files->makeDirectory($destination, 0755, true);

                @$this->files->copy($filePath, $file);
            }

            return $path;
        }

        return '';
    }

    /**
     * @param string $dir
     * @param string $filename
     *
     * @return string
     */
    private function _getRandomDir($dir, $filename)
    {
        if (is_null($dir)) {
            $dir = substr($filename, 0, 2).'/'.substr($filename, 2, 2);

            return $dir;
        }

        return $dir;
    }

    /**
     * @param string $extension
     *
     * @return string
     */
    private function _parseExtension(string $extension)
    {
        $extension = explode('?', $extension);

        return $extension[0];
    }
}