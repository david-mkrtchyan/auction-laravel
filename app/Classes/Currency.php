<?php

namespace App\Classes;

use App\Entities\Currency as CurrencyEntity;

/**
 * Class Currency
 * @package App\Classes
 */
class Currency
{
    
    /**
     * @var CurrencyEntity
     */
    protected $default_currency;
    
    /**
     * Currency constructor.
     *
     * @param \App\Entities\Currency $default_currency
     */
    public function __construct(CurrencyEntity $default_currency)
    {
        $this->default_currency = $default_currency;
    }
    
    /**
     * @return int
     */
    public function getId(): string
    {
        return $this->default_currency->id;
    }
    
    /**
     * @return string
     */
    public function title(): string
    {
        return $this->default_currency->getTitle();
    }
    
    /**
     * @return float
     */
    public function dealingRate(): float
    {
        return $this->default_currency->getRate();
    }
    
    /**
     * @param string|float|int $amount
     *
     * @return string
     */
    public function prepare($amount): string
    {
        return sprintf('%.8f', $amount);
    }
    
    /**
     * @param float $amount
     *
     * @return float
     */
    public function toDBCurrency(float $amount): float
    {
        return round(cbcmul($amount, pow(10, 8)), 0);
    }
    
    /**
     * @param float $amount
     *
     * @return float
     */
    public function toDefaultCurrency(float $amount): float
    {
        return round(cbcdiv($amount, $this->default_currency->getRate(), 8), 8);
    }
    
    /**
     * @param float $amount
     *
     * @return float
     */
    public function toDollarCurrency(float $amount): float
    {
        return round(cbcmul($amount, $this->default_currency->getRate(), 2), 2);
    }
    
    /**
     * @param float $amount
     *
     * @return string
     */
    public function toFormattedCurrency(float $amount): string
    {
        $amount = $this->prepare($amount);
        
        $amount = explode('.', $amount);
    
        $amount[1] = isset($amount[1]) ? rtrim($amount[1], '0') : '0';
        $amount[1] = $amount[1] == '' ? '0' : $amount[1];
        
        return number_format($amount[0], 0, '.', ' ').
            '.'.$amount[1];
    }
    
    /**
     * @param float $amount
     *
     * @return string
     */
    public function getMenuFormattedBalance(float $amount): string
    {
        $amount = $this->prepare($amount);
    
        $amount = explode('.', $amount);
    
        $amount[1] = isset($amount[1]) ? rtrim($amount[1], '0') : '0';
        $amount[1] = $amount[1] == '' ? '0' : $amount[1];
        
        return '<b>'.number_format($amount[0], 0, '.', ' ').'</b>'.
            '.'.$amount[1];
    }
    
    /**
     * @param float $amount
     *
     * @return string
     */
    public function toFormattedDollarCurrency(float $amount): string
    {
        $amount = $this->toDollarCurrency($amount);
        
        $amount = $this->prepare($amount);
        
        $amount = explode('.', $amount);
        
        return number_format($amount[0], 0, '.', ' ').
            (isset($amount[1]) ? '.'.$amount[1] : '');
    }
}