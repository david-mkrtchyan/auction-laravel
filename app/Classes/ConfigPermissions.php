<?php

namespace App\Classes;

use App\Contracts\Permissions as PermissionsContract;

/**
 * Class ConfigPermissions
 * @package App\Classes
 */
class ConfigPermissions implements PermissionsContract
{
    
    /**
     * Load permissions array from app config
     *
     * @return array
     */
    public function load()
    {
        $perm = config('permissions');
        
        if (!is_array($perm)) {
            return [];
        }
        
        $perm = array_fill_keys(array_values($perm), 0);
        
        return $perm;
    }
    
    /**
     * Load permissions list in multi-dimensional array
     *
     * @return array
     */
    public function loadArray()
    {
        $permissions = $this->load();
        
        $result = [];
        
        foreach ($permissions as $key => $value) {
            array_set($result, $key, 0);
        }
        
        return $result;
    }
    
    /**
     * Load actual permissions
     *
     * @param array $groupPermissions
     *
     * @return array
     */
    public function loadAndCombine($groupPermissions = [])
    {
        $permissions = $this->load();
        
        // оставляем только те ключи, которые есть в актуальном списке прав
        // другими словами, все старые права, которые более не требуются, будут недоступны
        $intersect = array_intersect_key($groupPermissions, $permissions);
        
        $permissions = array_merge($permissions, $intersect);
        
        return $this->_MDArray($permissions);
    }
    
    /**
     * convert multi-dimensional array
     *
     * @param array $permissions
     *
     * @return array
     */
    private function _MDArray($permissions = [])
    {
        $result = [];
        
        foreach ($permissions as $key => $value) {
            array_set($result, $key, $value);
        }
        
        return $result;
    }
}