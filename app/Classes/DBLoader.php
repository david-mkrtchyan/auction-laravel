<?php

namespace App\Classes;

use App\Entities\Translation;
use Exception;
use Illuminate\Contracts\Translation\Loader;
use Log;

/**
 * Class DBLoader
 * @package App\Classes
 */
class DBLoader implements Loader
{
    
    /**
     * Load the messages for the given locale.
     *
     * @param  string $locale
     * @param  string $group
     * @param  string $namespace
     *
     * @return array
     */
    public function load($locale, $group, $namespace = null)
    {
        try {
            $result = cache()->tags('translations')->get($locale.'_'.$group, null);

            if ($result === null) {
                $result = Translation::whereLocale($locale)
                    ->whereGroup($group)
                    ->get(['key', 'value'])
                    ->keyBy('key')
                    ->map(
                        function ($item) {
                            return $item['value'];
                        }
                    )
                    ->toArray();
                
                if (app()->environment('production')) {
                    cache()->tags('translations')->put($locale.'_'.$group, $result, 60);
                }
            }
            
            return $result;
        } catch (Exception $e) {
            // just insure themselves in case of problems with the database
            Log::critical(
                'message: '.$e->getMessage().', line: '.$e->getLine().', file: '.$e->getFile(),
                [
                    'locale' => $locale,
                    'group'  => $group,
                ]
            );
            
            return [];
        }
    }
    
    /**
     * Add a new namespace to the loader.
     *
     * @param  string $namespace
     * @param  string $hint
     *
     * @return void
     */
    public function addNamespace($namespace, $hint)
    {
        // TODO: Implement addNamespace() method.
    }
    
    /**
     * Add a new JSON path to the loader.
     *
     * @param  string $path
     *
     * @return void
     */
    public function addJsonPath($path)
    {
        // TODO: Implement addJsonPath() method.
    }
    
    /**
     * Get an array of all the registered namespaces.
     *
     * @return array
     */
    public function namespaces()
    {
        // TODO: Implement namespaces() method.
    }
}