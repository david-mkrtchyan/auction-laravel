<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Text extends Facade
{
    /**
     * Class Text
     * @package App\Facades
     */

    protected static function getFacadeAccessor()
    {
        return 'text';
    }
}