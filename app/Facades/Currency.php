<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Currency
 * @package App\Facades
 */
class Currency extends Facade
{
    
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'currency';
    }
}