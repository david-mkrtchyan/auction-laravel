<?php

namespace App\Mail;

use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class VerificationApprovedEmail
 * @package App\Mail
 */
class VerificationApprovedEmail extends Mailable
{
    
    use Queueable, SerializesModels;
    
    /**
     * @var string
     */
    public $view = 'emails.user.verification_approved';
    
    /**
     * @var \App\Entities\User
     */
    public $user;
    
    /**
     * Create a new message instance.
     *
     * @param \App\Entities\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        
        $this->to($user->email, $user->getFullName());
        $this->subject(trans('front_subjects.verification approved email'));
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this;
    }
}
