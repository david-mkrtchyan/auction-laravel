<?php

namespace App\Mail;

use App\Entities\User;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class Activation
 * @package App\Mail
 */
class Activation extends Mailable
{
    
    use Queueable, SerializesModels;
    
    /**
     * @var string
     */
    public $view = 'emails.user.activation';
    
    /**
     * @var \App\Entities\User
     */
    public $user;
    
    /**
     * @var \Cartalyst\Sentinel\Activations\EloquentActivation
     */
    public $activation;
    
    /**
     * Create a new message instance.
     *
     * @param \App\Entities\User                                 $user
     * @param \Cartalyst\Sentinel\Activations\EloquentActivation $activation
     */
    public function __construct(User $user, EloquentActivation $activation)
    {
        $this->user = $user;
        $this->activation = $activation;
        
        $this->to($user->email, $user->getFullName());
        $this->subject(trans('front_subjects.activation email'));
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->with(
            [
                'full_name' => $this->user->getFullName(),
                'email'     => $this->user->email,
                'code'      => $this->activation->code,
            ]
        );
    }
}
