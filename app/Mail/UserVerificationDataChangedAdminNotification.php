<?php

namespace App\Mail;

use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserVerificationDataChangedAdminNotification
 * @package App\Mail
 */
class UserVerificationDataChangedAdminNotification extends Mailable
{
    
    use Queueable, SerializesModels;
    
    /**
     * @var string
     */
    public $view = 'emails.admin.user_verification';
    
    /**
     * @var \App\Entities\User
     */
    public $user;
    
    /**
     * Create a new message instance.
     *
     * @param array              $emails
     * @param \App\Entities\User $user
     */
    public function __construct(array $emails, User $user)
    {
        $this->user = $user;
        
        foreach ($emails as $email) {
            $this->to($email, config('app.name'));
        }
        
        $this->subject(trans('front_subjects.admin user verification email'));
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this;
    }
}
