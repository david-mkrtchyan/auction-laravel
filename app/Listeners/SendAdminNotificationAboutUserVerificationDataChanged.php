<?php

namespace App\Listeners;

use App\Events\UserVerificationDataChanged;
use App\Mail\UserVerificationDataChangedAdminNotification;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendAdminNotificationAboutUserVerificationDataChanged
 * @package App\Listeners
 */
class SendAdminNotificationAboutUserVerificationDataChanged extends BaseListener
{
    
    /**
     * Handle the event.
     *
     * @param \App\Events\UserVerificationDataChanged $event
     */
    public function handle(UserVerificationDataChanged $event)
    {
        $emails = admin_emails();
        
        if ($emails->count()) {
            Mail::send(new UserVerificationDataChangedAdminNotification($emails->toArray(), $event->user));
        }
    }
}
