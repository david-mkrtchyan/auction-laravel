<?php

namespace App\Listeners;

use App\Listeners\Traits\FailedNotificatable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

/**
 * Class BaseListener
 * @package App\Listeners
 */
abstract class BaseListener implements ShouldQueue
{
    
    use InteractsWithQueue, FailedNotificatable;
}