<?php

namespace App\Listeners\Traits;

use Exception;
use Log;

/**
 * Trait FailedNotificatable
 * @package App\Listeners\Traits
 */
trait FailedNotificatable
{
    
    /**
     * The job failed to process.
     *
     * @param            $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed($event, Exception $exception)
    {
        Log::error(
            'Job failed',
            [
                'event'       => get_class($event),
                'listener'    => static::class,
                'exception'   => $exception->getMessage(),
                'trace'       => $exception->getTraceAsString(),
                'job_message' => method_exists($this, 'getFailedMessage')
                    ? $this->getFailedMessage()
                    : 'failed message not specified',
            ]
        );
    }
}