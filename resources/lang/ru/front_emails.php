<?php

return [
    
    'reminder :link' => 'Чтобы сбросить старый пароль, перейдите по :link',
    'activation :full_name :site_link :activation_link' => 'Поздравляем :full_name! Вы успешно зарегистрировались на сайте :site_link. Для активации Вашего профиля перейдите по :activation_link',
    'reminder page header' => 'Добро пожаловать',
    'activation page header' => 'Добро пожаловать',
    'footer :site_name' => 'Мы ценим наших пользователей, с уважением Команда :site_name.',

];



