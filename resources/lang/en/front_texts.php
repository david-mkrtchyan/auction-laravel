<?php

return [

    'home title' => 'Artificial Intelligence Trade',
    'home h1' => 'Automatic AI Robots',
    'home register text' => 'Register now for free',
    'deposit address helper text :currency_title' => 'We’ve generated a Bitcoin wallet special for you.<br/> For adding funds send <span class="amount-text">0.01</span> :currency_title to the address',
    'our benefit 1' => 'Protected<br/>money transfer',
    'our benefit 2' => 'Safe SSL certified<br/>256-bit encryption',
    'our benefit 3' => '24/7 on-line<br/>support',
    'run bot popup text' => 'After startup, the bot runs for 7 days. The user can start the bot<br/> 1 day a week — every Monday from 00:00 to 23:59,<br/> if a person does not launch the bot, in the specified time interval,<br/> the next 7 days, no profit is accrued.',
    'you cannot withdraw money' => 'You cannot withdraw money,<br/> until you',
    'verification form submit text' => 'Verification process usually goes 2 - 7<br/> business days after you upload all 3 files.',
	'password restore helper text' => 'Enter your email',
	'enter new password' => 'Please, enter the new password',
    '2fa qr code helper text' => 'Scan the code by Your device',
    '2fa secret helper text' => 'or enter code manually',
    'two factor login helper text' => 'Enter the two factor code from Your device',
];