<?php

return [
    
    'reminder email'                => 'Password reset',
    'activation email'              => 'Profile activation',
    'register email'                => 'Profile register',
    'admin user verification email' => 'User verification',
    'verification approved email'   => 'Verification approved!',

];
