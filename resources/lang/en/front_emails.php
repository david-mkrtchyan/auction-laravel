<?php

return [
    
    'reminder :link' => 'To reset your old password, go to the :link',
    'activation :full_name :site_link :pay' => 'Congratulation :full_name! You have successfully registered on the site :site_link. To activate your profile you must pay :pay',
    'reminder email header' => 'Hello',
    'activation email header' => 'Welcome',
    'verification email header' => 'Verification required',
    'verification approved email header' => 'Congratulation',
    'footer :site_name' => 'We value our users, with respect Team :site_name.',
    'user verification data changed :user_link' => 'User :user_link changed their data, please confirm them.',
    'verification approved email :full_name :site_link' => ':full_name, your account has been successfully verified on the site :site_link.',
    
];
