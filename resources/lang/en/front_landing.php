<?php

return [

    'calculator'                   => 'Calculator',
    'calculator title'             => 'Profit calculator',
    'calculator text 1 :site_name' => 'Find out how much you earn per day/month/ year with :site_name',
    'calculator text 2'            => 'Invest in the technology of the future and «skim the best» right now',
    'agreement amount'             => 'Agreement amount',
    'profit per day'               => 'Profit/Day',
    'profit per year'              => '1 Year profit',
    'profit and reinvest'          => 'Profit + Reinvest',
    'clear profit'                 => 'Clear profit',
    'not reinvest'                 => 'Not reinvest',
    'reinvest'                     => 'Reinvest',
    'difference'                   => 'Difference',
    'login'                        => 'Login',
    'registration'                 => 'Registration',
    'create_account'               => 'Create account',
    'my profile'                   => 'My Profile',
    'logout'                       => 'Log Out',
    'catalog list'                 => 'Catalog List',
    'catalog'                      => 'Catalog',
    'select directory'             => 'Select the directory you want to see',
    'my details'                   => 'My Details',
];
