<?php

return [
    
    'you have successfully registered'                   => 'Вы успешно зарегистрировались. На Вашу почту отправлено письмо с ссылкой для активации Вашего аккаунта',
    'you have successfully log out'                      => 'Вы успешно вышли',
    'you have successfully log in'                       => 'Вы успешно вошли',
    'user not found'                                     => 'Пользователь не найден',
    'wrong authorization credential'                     => 'Вы ввели неправильные учетные данные',
    'user not activated'                                 => 'Пользователь не активирован',
    'user blocked'                                       => 'Пользователь временно заблокирован',
    'activation successful'                              => 'Вы успешно активировали свой аккаунт',
    'activation error'                                   => 'Активация не найдена или уже завершена',
    'reminder send'                                      => 'Письмо с инструкцией по сбросу старого пароля отправлено на Ваш адрес электронной почты',
    'reminder complete'                                  => 'Вы успешно восстановили пароль',
    'reminder complete error'                            => 'Код недействителен или уже использован',
    'auth error'                                         => 'Это действие разрешено только для авторизованных пользователей',
    'guest error'                                        => 'Это действие разрешено только для неавторизованных пользователей',
    'changes successfully saved'                         => 'Изменения успешно сохранены',
    'wrong old password'                                 => 'Вы ввели неверный старый пароль',
    'password successfully saved'                        => 'Пароль успешно сохранен',
    'validation failed'                                  => 'Ошибка валидации',
    '2fa successfully enabled'                           => 'Двухфакторная аутентификация успешно включена',
    '2fa successfully disabled'                          => 'Двухфакторная аутентификация успешно отключена',
    'amount is required'                                 => 'Поле сумма обязательно для заполнения и не может быть меньше минимального значения',
    'internal server error'                              => 'На данный момент не получается отправить запрос. Попробуйте немного позже. Ведутся технические работы.',
    'confirmation otp is resend'                         => 'Код подтверждения повторно отпрален на Ваш e-mail',
    'confirmation limit reached'                         => 'Вы уже отправили максимальное количество подтверждений',
    'confirmation timeout error'                         => 'Вы не можете отправлять подтверждения чаще одного раза в минуту',
    'forbidden'                                          => 'Доступ запрещен',
    'file successfully uploaded'                         => 'Файл успешно загружен',
    'data successfully saved'                            => 'Данные успешно сохранены',
    'an error has occurred try again later'              => 'Произошла ошибка. Пожалуйста, повторите попытку позже',
    'the contract successfully created'                  => 'Контракт успешно создан',
    'there is not enough money on your wallet'           => 'На Вашем кошельке недостаточно средств',
    'wallets temporary unavailable'                      => 'Ваши кошельки временно недоступны',
    'contract not found'                                 => 'Выбранный контракт не найден',
    'contract already reinvested'                        => 'Реинвестирование уже включено для выбранного контракта',
    'reinvestment not allowed for the selected contract' => 'Реинвестирование больше не разрешено для выбранного контракта',
    'reinvestment successfully on'                       => 'Реинвестирование успешно включено',
    'copied'                                             => 'Скопировано',
    'session expired'                                    => 'Сессия истекла. Пожалуйста, перезагрузите страницу и попробуйте снова',

];
