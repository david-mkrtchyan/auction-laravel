<?php

return [
    
    'platform'                => 'Платформа',
    'what we do'              => 'Что мы делаем',
    'products'                => 'Продукты',
    'command'                 => 'Команда',
    'telegram channel'        => 'Телеграм-канал',
    'registration'            => 'Регистрация',
    'login'                   => 'Вход',
    'profile'                 => 'Профиль',
    'platform text 1'         => 'Новая Эра<br> Криптовалютной<br> Индустрии',
    'platform text 2'         => 'Новая Эра<br> Крипто-<br>валютной<br> Индустрии',
    'platform text 3'         => 'Инновационная Технология Искусственного Интеллекта —<br> Величайшее Открытие для Получения Прибыли.<br> Будущее, Которое Все Ждали, Теперь Здесь.',
    'platform text 4'         => 'Инновационная Технология Искусствен-<br>ного Интеллекта — Величайшее Открытие<br> для Получения Прибыли. Будущее,<br> Которое Все Ждали, Теперь Здесь.',
    'what we do question'     => 'Что мы делаем?',
    'trading'                 => 'Трейдинг',
    'trading text'            => 'Покупка, хранение и продажа<br> криптовалюты в абсолютной<br> безопасности',
    'arbitrage'               => 'Арбитраж',
    'arbitrage text'          => 'Анализ наиболее выгодных<br> предложений на рынке<br> с помощью ИИ',
    'mining'                  => 'Майнинг',
    'mining text'             => 'Разработка роботов ИИ<br> для выбора и добычи<br> перспективной криптовалюты',
    'atm'                     => 'АТМ',
    'atm text'                => 'Собственное производство<br> криптоматов для комфортного<br> использования личных<br> цифровых средств',
    'altcoins'                => 'Альткоины',
    'altcoins text'           => 'Инвестиции в наиболее<br> перспективные альткоины<br> для получения максимальной<br> прибыли',
    'ico'                     => 'ICO',
    'ico text'                => 'Исследование проектов которые<br> выходят на рынок и участие<br> в самых успешных из них',
    'umbrellas title'         => 'Все под<br> одним<br> зонтом',
    'products of ai trade'    => 'Продукты<br> платформы AI Trade',
    'products text 1'         => 'products text 1',
    'products text 2 mobile'  => 'Роботы ИИ для<br> трейдинга с любыми<br> криптовалютными парами',
    'products text 2 desktop' => 'Роботы ИИ <br>для трейдинга <br>с любыми<br> криптовалют-<br>ными парами',
    'products text 3 mobile'  => 'Роботы ИИ для<br> маржинальной торговли<br> на биржах (в разработке)',
    'products text 3 desktop' => 'Роботы ИИ для<br> маржинальной<br> торговли на<br> биржах<br> (в разработке)',
    'products text 4'         => 'Роботы ИИ<br> для майнинга<br> (в разработке)',
    'products text 5 mobile'  => 'Роботы ИИ для анализа рынка<br> и поиска финансово-выгодных<br> альткоинов (в разработке)',
    'products text 5 desktop' => 'Роботы ИИ для<br> анализа рынка<br> и поиска финан-<br>сово-выгодных<br> альткоинов <br>(в разработке)',
    'products text 6'         => 'ИИ ноды',
    'command text 1 mobile'   => '<strong>Наша платформа была создана предпринимателями<br> из разных континентов, ИТ-специалистами,<br> профессиональными трейдерами<br> и высококвалифицированными юристами</strong>',
    'command text 2 mobile'   => 'Это помогает нам лучше понимать менталитет людей в разных<br> странах и использовать эти знания для масштабирования бизнеса.',
    'command text 3 mobile'   => 'Мы разрабатываем самое продвинутое программное обеспечение<br> для того, чтобы вместе достичь новых финансовых высот.',
    'command text 4 mobile'   => 'Добро пожаловать в AI Trade!',
    'command text 1 desktop'  => '<strong>Наша платформа была создана<br> предпринимателями из<br> разных континентов,<br> ИТ-специалистами, профес-<br> сиональными трейдерами и<br> высококвалифицированными<br> юристами.</strong>',
    'command text 2 desktop'  => ' Это помогает нам лучше понимать<br> менталитет людей в разных<br> странах и использовать эти знания<br> для масштабирования бизнеса.',
    'command text 3 desktop'  => ' Мы разрабатываем самое продвинутое<br> программное обеспечение<br> для того, чтобы вместе достичь новых<br> финансовых высот.',
    'command text 4 desktop'  => ' Добро пожаловать в AI Trade!',

];