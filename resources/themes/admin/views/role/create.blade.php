@extends('layouts.editable')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => 'admin.role.store', 'role' => 'form']) !!}

                @include('views.role.partials._form')

            {!! Form::close() !!}
        </div>
    </div>
@endsection