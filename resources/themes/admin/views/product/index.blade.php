@extends('layouts.listable')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="products-table">
                        {!!
                            TablesBuilder::create(
                                ['id' => "datatable1", 'class' => "table table-bordered table-striped table-hover"],
                                [
                                    'bStateSave' => true,
                                    'order' => [[ 0, 'desc' ]],
                                    "columns" => [
                                         [ "data" => "id" ],
                                        [ "data" => "status" ],
                                        [ "data" => "price" ],
                                        [ "data" => "start_price" ],
                                        [ "data" => "title" ],
                                        [ "data" => "view_count" ],
                                        [ "data" => "position" ],
                                        [ "data" => "actions" ],
                                    ],
                                ]
                            )
                            ->addHead([
                                ['text' => trans('labels.id')],
                                ['text' => trans('labels.status')],
                                ['text' => trans('labels.price')],
                                ['text' => trans('labels.start_price')],
                                ['text' => trans('labels.title')],
                                ['text' => trans('labels.view_count')],
                                ['text' => trans('labels.position')],
                                ['text' => trans('labels.actions')]
                            ])
                            ->addFoot([
                                ['attr' => ['colspan' => 5]]
                            ])
                             ->make()
                        !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection