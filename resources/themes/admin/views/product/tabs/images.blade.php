
<div class="form-group required @if ($errors->has('images')) has-error @endif">

    <div class="col-md-12">

        {!! Form::file('images[]', ['required' => true, 'id' => 'input-1a','accept' => 'image/*','multiple' => true]) !!}

        {!! $errors->first('images', '<p class="help-block error">:message</p>') !!}

    </div>

    @include('partials.tabs.fileInput', ['id' => 'input-1a', 'images' => $images, 'imagesConfig' => $imagesConfig ])
</div>