<div class="form-group required @if ($errors->has('start_time')) has-error @endif">
    {!! Form::label('start_time', trans('labels.start_time'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']) !!}

    <div class="col-xs-12 col-sm-7 col-md-4">
        {!! Form::text('start_time', null, ['placeholder' => trans('labels.start_time'), 'required' => true, 'class' => 'form-control input-sm']) !!}

        {!! $errors->first('start_time', '<p class="help-block error">:message</p>') !!}
    </div>

    @include('partials.tabs.datetimepicker', ['id' => 'start_time'])
</div>

<div class="form-group required @if ($errors->has('end_time')) has-error @endif">
    {!! Form::label('end_time', trans('labels.end_time'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']) !!}

    <div class="col-xs-12 col-sm-7 col-md-4">
        {!! Form::text('end_time', null, ['placeholder' => trans('labels.end_time'), 'required' => true, 'class' => 'form-control input-sm']) !!}

        {!! $errors->first('end_time', '<p class="help-block error">:message</p>') !!}
    </div>

    @include('partials.tabs.datetimepicker', ['id' => 'end_time'])
</div>

<div class="form-group required @if ($errors->has('status')) has-error @endif">
    {!! Form::label('status', trans('labels.status'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']) !!}

    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
        {!! Form::select('status', ['1' => trans('labels.status_on'), '0' => trans('labels.status_off')], null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}

        {!! $errors->first('status', '<p class="help-block error">:message</p>') !!}
    </div>
</div>
