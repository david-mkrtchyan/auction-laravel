@include('partials.datatables.control_buttons', ['type' => $type])

@if ($user->hasAccess('superuser'))
    <a class="btn btn-primary btn-sm btn-flat" href="{!! route('session.admin_login', $model->id) !!}"
       title="{!! trans('labels.login_as_user') !!}">
        <i class="fa fa-external-link" aria-hidden="true"></i>
    </a>
@endif