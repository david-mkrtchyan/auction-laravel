<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active">
            <a aria-expanded="false" href="#personal_data" data-toggle="tab">@lang('labels.tab_personal_data')</a>
        </li>

        <li>
            <a class="@if (!$model->verified) text-red @endif" aria-expanded="false" href="#verification"
               data-toggle="tab">@lang('labels.tab_verification')</a>
        </li>

        @if ($user->hasAccess('userfile.read') && $model->exists)
            <li>
                <a class="@if (!$model->verified) text-red @endif" aria-expanded="false" href="#files"
                   data-toggle="tab">@lang('labels.tab_files')</a>
            </li>
        @endif


        <li class="@if ($errors->has('roles')) tab-with-errors @endif">
            <a aria-expanded="false" href="#roles" data-toggle="tab">@lang('labels.tab_roles')</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="personal_data">
            @include('user.tabs.personal_data')
        </div>

        <div class="tab-pane" id="verification">
            @include('user.tabs.verification')
        </div>

        @if ($user->hasAccess('userfile.read') && $model->exists)
            <div class="tab-pane" id="files">
                @include('user.tabs.files')
            </div>
        @endif


        <div class="tab-pane" id="roles">
            @include('user.tabs.roles')
        </div>
    </div>
</div>
