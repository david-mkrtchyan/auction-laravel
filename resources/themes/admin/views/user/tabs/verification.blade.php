<div class="form-group required @if ($errors->has('verified')) has-error @endif">
    {!! Form::label('verified', trans('labels.verified'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-xs-12 col-sm-2 col-md-2">
        {!! Form::select('verified', ['0' => trans('labels.no'), '1' => trans('labels.yes')], null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true', 'required' => true]) !!}

        {!! $errors->first('verified', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class="form-group @if ($errors->has('first_name')) has-error @endif">
    {!! Form::label('first_name', trans('labels.first_name'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-md-5">
        {!! Form::text('first_name', null, ['placeholder' => trans('labels.first_name'), 'class' => 'form-control input-sm']) !!}

        {!! $errors->first('first_name', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class="form-group @if ($errors->has('last_name')) has-error @endif">
    {!! Form::label('last_name', trans('labels.last_name'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-md-5">
        {!! Form::text('last_name', null, ['placeholder' => trans('labels.last_name'), 'class' => 'form-control input-sm']) !!}

        {!! $errors->first('last_name', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class="form-group @if ($errors->has('gender')) has-error @endif">
    {!! Form::label('gender', trans('labels.gender'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-xs-12 col-sm-2 col-md-2">
        {!! Form::select('gender', [0 => trans('labels.gender_male'), 1 => trans('labels.gender_female')], null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}

        {!! $errors->first('gender', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class=" form-group
    @if ($errors->has('birth_day')) has-error @endif
@if ($errors->has('birth_month')) has-error @endif
@if ($errors->has('birth_year')) has-error @endif ">
    {!! Form::label('birthday', trans('labels.birthday'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-md-5">
        <div class="col-md-12">
            <div class="col-md-3">
                {!! Form::select('birth_day', $days, null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}
            </div>
            <div class="col-md-3">
                {!! Form::select('birth_month', $months, null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}
            </div>
            <div class="col-md-4">
                {!! Form::select('birth_year', $years, null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}
            </div>
        </div>

        {!! $errors->first('birth_day', '<p class="help-block error">:message</p>') !!}
        {!! $errors->first('birth_month', '<p class="help-block error">:message</p>') !!}
        {!! $errors->first('birth_year', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class="form-group @if ($errors->has('address')) has-error @endif">
    {!! Form::label('address', trans('labels.address'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-md-5">
        {!! Form::text('address', null, ['placeholder' => trans('labels.address'), 'class' => 'form-control input-sm']) !!}

        {!! $errors->first('address', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class="form-group @if ($errors->has('city')) has-error @endif">
    {!! Form::label('city', trans('labels.city'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-md-5">
        {!! Form::text('city', null, ['placeholder' => trans('labels.city'), 'class' => 'form-control input-sm']) !!}

        {!! $errors->first('city', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class="form-group @if ($errors->has('postal_code')) has-error @endif">
    {!! Form::label('postal_code', trans('labels.postal_code'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-md-5">
        {!! Form::text('postal_code', null, ['placeholder' => trans('labels.postal_code'), 'class' => 'form-control input-sm']) !!}

        {!! $errors->first('postal_code', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<div class="form-group @if ($errors->has('country_id')) has-error @endif">
    {!! Form::label('country_id', trans('labels.country'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-xs-12 col-sm-4">
        {!! Form::select('country_id', $countries, null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}

        {!! $errors->first('country_id', '<p class="help-block error">:message</p>') !!}
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('2fa', trans('labels.2fa'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-xs-12 col-sm-4">
        <div class="col-sm-3 margin-top-7">
            @lang('labels.2fa status '.((int) $model->twoFactorEnabled()))
        </div>

        <div class="col-sm-9">
            <div class="col-xl-6 col-sm-5 col-md-2">
                @if ($model->twoFactorEnabled())
                    <a href="{!! route('admin.user.two_factor_authentication.reset', $model->id) !!}"
                       data-title="@lang('labels.2fa disabling')"
                       data-message="@lang('messages.2fa disabling question')"
                       class="btn btn-sm btn-warning btn-flat simple-link-dialog">
                        @lang('labels.reset')
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>