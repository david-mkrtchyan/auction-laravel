<div class="box-body table-responsive no-padding user-files-table">
    <table class="table table-hover">
        <tbody>
        <tr>
            <th class="col-xs-4 text-center">{!! trans('labels.image') !!}</th>
            <th class="col-xs-3 text-center">{!! trans('labels.type') !!}</th>
            <th class="col-xs-3 text-center">{!! trans('labels.status') !!}</th>
            <th class="col-xs-2 text-center">{!! trans('labels.actions') !!}</th>
        </tr>

        @if (count($model->files))
            @foreach($model->files as $file)
                <tr class="file-row">
                    <td class="text-center">
                        <div class="form-group">
                            @include('partials.image', ['src' => $file->getLink(), 'attributes' => ['width' => 250, 'height' => 150]])
                            <br>
                            <a class="display-block margin-top-10" target="_blank" href="{!! $file->getLink() !!}">@lang('labels.show in full size')</a>
                        </div>
                    </td>
                    <td class="text-center">
                        @lang('labels.file_type_'.$file->getStringType())
                    </td>
                    <td class="text-center coll-status">
                        @lang('labels.file_status_'.$file->getStringStatus())
                    </td>
                    <td class="coll-actions text-center">
                        <a class="btn btn-flat btn-success btn-sm action margin-bottom-10"
                           data-url="{!! route('admin.user.file.approve', [$user->id, $file->id]) !!}"
                           data-_token="{!! csrf_token() !!}">
                            @lang('labels.approve')
                        </a>
                        <br>
                        <a class="btn btn-flat btn-danger btn-sm action"
                           data-url="{!! route('admin.user.file.decline', [$user->id, $file->id]) !!}"
                           data-_token="{!! csrf_token() !!}">
                            @lang('labels.decline')
                        </a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">
                    <div class="text-center text-red margin">
                        @lang('messages.no user files')
                    </div>
                </td>
            </tr>
        @endif

        </tbody>
    </table>
</div>