<div class="row">
    <div class="col-md-3">
        <div class="box-body box-profile">
            @include('partials.tabs.user_avatar')

            <h3 class="profile-username text-center">{!! $model->getFullName() !!}</h3>

            @if (!empty($model->roles()->first()))
                <p class="text-muted text-center">{!! $model->roles()->first()->name !!} </p>
            @endif
        </div>
    </div>

    <div class="col-md-9">
        <div class="box-body">
            <div class="form-group required @if ($errors->has('user_name')) has-error @endif">
                {!! Form::label('user_name', trans('labels.user_name'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-md-5">
                    {!! Form::text('user_name', null, ['placeholder' => trans('labels.user_name'), 'required' => true, 'class' => 'form-control input-sm']) !!}

                    {!! $errors->first('user_name', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            <div class="form-group required @if ($errors->has('email')) has-error @endif">
                {!! Form::label('email', trans('labels.email'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-md-3">
                    {!! Form::text('email', null, ['placeholder' => trans('labels.email'), 'required' => true, 'class' => 'form-control input-sm']) !!}

                    {!! $errors->first('email', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            <div class="form-group @if ($errors->has('phone')) has-error @endif">
                {!! Form::label('phone', trans('labels.phone'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-md-3">
                    {!! Form::text('phone', null, ['placeholder' => trans('labels.phone'), 'class' => 'form-control input-sm inputmask-2']) !!}

                    {!! $errors->first('phone', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            @if(empty($model->id))
                <div class="form-group @if ($errors->has('password')) has-error @endif">
                    {!! Form::label('password', trans('labels.password'), ['class' => 'col-md-3 control-label']) !!}

                    <div class="col-md-3">
                        {!! Form::text('password', null, ['placeholder' => trans('labels.password'), 'required' => true, 'class' => 'form-control input-sm']) !!}

                        {!! $errors->first('password', '<p class="help-block error">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
                    {!! Form::label('password_confirmation', trans('labels.password_confirmation'), ['class' => 'col-md-3 control-label']) !!}

                    <div class="col-md-3">
                        {!! Form::text('password_confirmation', null, ['placeholder' => trans('labels.password_confirmation'), 'required' => true, 'class' => 'form-control input-sm']) !!}

                        {!! $errors->first('password_confirmation', '<p class="help-block error">:message</p>') !!}
                    </div>
                </div>
            @endif

            <div class="form-group required @if ($errors->has('activated')) has-error @endif">
                {!! Form::label('activated', trans('labels.activated'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-xs-12 col-sm-2 col-md-2">
                    {!! Form::select('activated', ['0' => trans('labels.no'), '1' => trans('labels.yes')], null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true', 'required' => true]) !!}

                    {!! $errors->first('activated', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            @if ($method == 'create')
                <div class="form-group @if ($errors->has('referral_id')) has-error @endif">
                    {!! Form::label('referral_id', trans('labels.referral_id'), ['class' => 'col-md-3 control-label']) !!}

                    <div class="col-xs-5">
                        {!! Form::select('referral_id',  $model->referral_id ? [$model->referral_id => $model->referral->getFullName()] : [], null, ['id' => 'referral_select', 'class' => 'form-control select2 select2-ajax referral-select input-sm', 'data-url' => route('admin.user.find'), 'aria-hidden' => 'true']) !!}

                        {!! $errors->first('referral_id', '<p class="help-block error">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group required @if ($errors->has('add_to_binary_tree')) has-error @endif">
                    {!! Form::label('add_to_binary_tree', trans('labels.add_to_binary_tree'), ['class' => 'col-md-3 control-label']) !!}

                    <div class="col-xs-5">
                        {!! Form::select('add_to_binary_tree', ['0' => trans('labels.no'), '1' => trans('labels.yes')], null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true', 'required' => true]) !!}

                        {!! $errors->first('add_to_binary_tree', '<p class="help-block error">:message</p>') !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>