@extends('layouts.listable')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="variables-table">
                        {!!
                           TablesBuilder::create(
                               ['id' => "datatable1", 'class' => "table table-bordered table-striped table-hover"],
                               [
                                   'bStateSave' => true,
                                   'order' => [[ 0, 'desc' ]],
                                   "columns" => [
                                       [ "data" => "id" ],
                                       [ "data" => "name" ],
                                       [ "data" => "description" ],
                                       [ "data" => "type" ],
                                       [ "data" => "key" ],
                                       [ "data" => "multilingual" ],
                                       [ "data" => "actions" ],
                                   ],
                               ]
                           )
                           ->addHead([
                               ['text' => trans('labels.id')],
                               ['text' => trans('labels.name')],
                               ['text' => trans('labels.description')],
                               ['text' => trans('labels.type')],
                               ['text' => trans('labels.key')],
                               ['text' => trans('labels.multilingual')],
                               ['text' => trans('labels.actions')]
                           ])
                           ->addFoot([
                               ['attr' => ['colspan' => 7]]
                           ])
                           ->make()
                       !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection