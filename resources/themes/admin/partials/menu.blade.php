<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            @if ($user->hasAccess(['roles', 'user.read']))
                <li class="header">@lang('labels.users')</li>
            @endif

            @if ($user->hasAccess('user.read'))
                <li class="{!! active_class('admin.user.unverified*') !!}">
                    <a href="{!! route('admin.user.unverified') !!}">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <span>@lang('labels.unverified users')</span>
                    </a>
                </li>
                <li class="{!! active_class('admin.user.index*') !!}">
                    <a href="{!! route('admin.user.index') !!}">
                        <i class="fa fa-user"></i>
                        <span>@lang('labels.users')</span>

                        @if ($user->hasAccess('user.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_user')"
                                   data-href="{!! route('admin.user.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif
            @if ($user->hasAccess('group'))
                <li class="{!! active_class('admin.role.index*') !!}">
                    <a href="{!! route('admin.role.index') !!}">
                        <i class="fa fa-users"></i>
                        <span>@lang('labels.roles')</span>

                        @if ($user->hasAccess('group.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_group')"
                                   data-href="{!! route('admin.role.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif


            @if (
                    $user->hasAccess(['page.read']) ||
                    $user->hasAccess(['supportpage.read']) ||
                    $user->hasAccess(['question.read']) ||
                    $user->hasAccess(['department.read'])
                )
                <li class="header">@lang('labels.content')</li>
            @endif

            @if ($user->hasAccess('page.read'))
                <li class="{!! active_class('admin.page*') !!}">
                    <a href="{!! route('admin.page.index') !!}">
                        <i class="fa fa-file-text"></i>
                        <span>@lang('labels.pages')</span>

                        @if ($user->hasAccess('page.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_page')"
                                   data-href="{!! route('admin.page.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif



          @if ($user->hasAccess(['group.read']))
              <li class="header">@lang('labels.groups')</li>
          @endif

          @if ($user->hasAccess('product'))
              <li class="{!! active_class('admin.product.index*') !!}">
                  <a href="{!! route('admin.product.index') !!}">
                      <i class="fa fa-tasks" aria-hidden="true"></i>
                      <span>@lang('labels.products')</span>

                      @if ($user->hasAccess('product.create'))
                          <small class="label create-label pull-right bg-green" title="@lang('labels.add_product')"
                                 data-href="{!! route('admin.product.create') !!}">
                              <i class="fa fa-plus"></i>
                          </small>
                      @endif
                  </a>
              </li>
          @endif


          @if ($user->hasAccess('group'))
              <li class="{!! active_class('admin.group.index*') !!}">
                  <a href="{!! route('admin.group.index') !!}">
                      <i class="fa fa-users"></i>
                      <span>@lang('labels.groups')</span>

                      @if ($user->hasAccess('group.create'))
                          <small class="label create-label pull-right bg-green" title="@lang('labels.add_group')"
                                 data-href="{!! route('admin.group.create') !!}">
                              <i class="fa fa-plus"></i>
                          </small>
                      @endif
                  </a>
              </li>
          @endif




            @if ($user->hasAccess(['variablevalue.read', 'translation.read']))
                <li class="header">@lang('labels.settings')</li>
            @endif

            @if ($user->hasAccess('variablevalue.read'))
                <li class="{!! active_class('admin.variable*') !!}">
                    <a href="{!! route('admin.variable.value.index') !!}">
                        <i class="fa fa-cog"></i>
                        <span>@lang('labels.variables')</span>
                    </a>
                </li>
            @endif

            @if ($user->hasAccess('translation.read'))
                <li class="treeview {!! active_class('admin.translation.index*') !!}">
                    <a href="#">
                        <i class="fa fa-language"></i>
                        <span>@lang('labels.translations')</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @foreach(config('translation.visible_groups') as $group)
                            <li class="{!! front_active_class(route('admin.translation.index', $group)) !!}">
                                <a href="{!! route('admin.translation.index', $group) !!}">
                                    <span>@lang('labels.translation_group_' . $group)</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endif
        </ul>
    </section>
</aside>