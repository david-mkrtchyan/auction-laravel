
<script>
    $('#{!! $id !!}').datetimepicker({
        format: 'Y-m-d H:i:s',
        separator: ' @ ',
        minDate: new Date()
    });
</script>