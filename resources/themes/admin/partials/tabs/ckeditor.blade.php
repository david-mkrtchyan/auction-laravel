<script>
    CKEDITOR.replace('{!! $id !!}',
        {
            filebrowserImageBrowseUrl: '{!! route('admin.elfinder.ckeditor4') !!}',
            allowedContent: true,
        }
    );
</script>