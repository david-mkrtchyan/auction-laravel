<!doctype html>
<html lang="{!! $locale !!}" xmlns="http://www.w3.org/1999/xhtml">
<head>

    @include('partials.head')

</head>

<body>

@push('landing.css')
    <meta name="format-detection" content="telephone=no">
@endpush

@include('partials.header')

<div class="content container">
    @yield('main')
</div>



@include('partials.footer')

@push('assets.bottom')
@endpush

@include('partials.messages')

@include('partials.foot')


</body>

</html>
