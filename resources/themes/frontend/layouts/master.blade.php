@extends('layouts.app')

@section('main')

	<div class="page-grid">
		<div class="page-grid-in">

			@include('partials.header')

			<section class="main">
				<div class="container-fluid-my">
					<div class="my-row">

						@yield('content')

					</div>
				</div>
			</section>

			@include('partials.footer')

		</div>
	</div>

@endsection