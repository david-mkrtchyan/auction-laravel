_EliteAiTrade.profile = _EliteAiTrade.profile || {}

_EliteAiTrade.profile.$legSwitcher = $('[name="active_leg"]')

_EliteAiTrade.profile.$activeLegTitle = $('.active-leg-title')

_EliteAiTrade.profile.initLegSwitcher = () ->
  _EliteAiTrade.profile.$legSwitcher.on 'change', () ->
    data = {
      _token: _EliteAiTrade.profile.$legSwitcher.attr('data-_token')
      active_leg: $(this).val()
    }

    $.ajax
      method: 'post',
      url: _EliteAiTrade.profile.$legSwitcher.attr('data-url')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.profile.$activeLegTitle.text(response.leg_title)

        return false

    return false

$(window).on 'load', ->
  if _EliteAiTrade.profile.$legSwitcher.length
    _EliteAiTrade.profile.initLegSwitcher()

    console.log 'init leg switcher'