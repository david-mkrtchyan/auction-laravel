_EliteAiTrade.deposit = _EliteAiTrade.deposit || {}

_EliteAiTrade.deposit.$page = $('.deposit-page')

_EliteAiTrade.deposit.initDepositPage = () ->
  $('[name="amount"]').on 'keyup', (e) ->
    e.preventDefault()

    value = $(this).val()

    if value == ''
      return

    value = parseFloat(value)

    if value < 0 or value == ''
      value = 0.01

    $('.amount-text').text(value)

    return false;

$(window).on 'load', ->
  if _EliteAiTrade.deposit.$page.length
    _EliteAiTrade.deposit.initDepositPage()

    console.log 'init deposit'