$(window).on 'load', ->
  $modals = $('.izimodal')
  iziModalConfig =
    group: 'izimodal'
    loop: true
    arrowKeys: false
    navigateCaption: false
    overlayClose: false
    pauseOnHover: true

  if $modals.length
    $modals.each (index, item) ->
      $item = $(item)
      id = $item.attr('id')
      cookieKey = 'ai_trade_modal_' + id
      cookie = _EliteAiTrade.cookie.get(cookieKey)

      if cookie
        $item.remove()

        return

      $item.on "click", '.close-modal', () ->
        $checker = $item.find('.button-hide')

        if $checker.is(':checked')
          _EliteAiTrade.cookie.set cookieKey, true

        if $('.izimodal').length > 1
          $item.iziModal('next');
        else
          $item.fadeOut(300)

          setTimeout ->
            $item.iziModal('destroy');

            $('.iziModal-navigate').remove()
            $('.iziModal-overlay').remove()

            $item.remove()
          , 400

          return

        $item.fadeOut(300)

        setTimeout ->
            $item.remove()

            $('.izimodal').iziModal iziModalConfig
          , 400

  $modals = $('.izimodal');

  if $modals.length
    $('.izimodal').iziModal iziModalConfig

    $modals.first().iziModal 'open'
  
  console.log('init modals')