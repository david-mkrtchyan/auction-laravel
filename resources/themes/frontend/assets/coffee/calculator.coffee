_EliteAiTrade.calculator = _EliteAiTrade.calculator || {}

_EliteAiTrade.calculator.$block = $('#calculator-block')

_EliteAiTrade.calculator.$amountInput = $('#agreement_amount')

_EliteAiTrade.calculator.$textAmount = $('#text_amount')

_EliteAiTrade.calculator._calculate = () ->
  amount = $('#agreement_amount').val()

  duration = parseInt(_EliteAiTrade.contract.duration)
  reinvestDuration = parseInt(_EliteAiTrade.contract.reinvestDuration)

  level = _EliteAiTrade.contract.actualLevel(amount)
  profit = if level then parseFloat(level.contract_profit) else 0

  profit = profit / 100

  profitPerDayNormal = amount * profit
  profitPerDayNormal = _EliteAiTrade.helpers.format(profitPerDayNormal, 8)

  profitPerYear = profitPerDayNormal * duration
  profitPerYear = _EliteAiTrade.helpers.format(profitPerYear, 8)

  profitClearNormal = 0
  if amount > 0
    profitClearNormal = (profitPerYear - amount) * 100 / amount
    profitClearNormal = _EliteAiTrade.helpers.format(profitClearNormal, 2)

  i = 0  
  reinvestedAmount = amount
  while i < reinvestDuration
    reinvestedAmount = parseFloat(reinvestedAmount) + reinvestedAmount * profit
    i++
  profitPerDayReinvest = reinvestedAmount * profit
  profitPerDayReinvest = _EliteAiTrade.helpers.format(profitPerDayReinvest, 8)

  profitReinvest = profitPerDayReinvest * (duration - reinvestDuration)
  profitReinvest = _EliteAiTrade.helpers.format(profitReinvest, 8)

  profitClearReinvest = 0
  if amount > 0
    profitClearReinvest = (profitReinvest - amount) * 100 / amount
    profitClearReinvest = _EliteAiTrade.helpers.format(profitClearReinvest, 2)

  profitDifference = 0
  if amount > 0
    profitDifference = _EliteAiTrade.helpers.format(profitClearReinvest - profitClearNormal, 2)

  $('#profit_per_day_normal .value-text-value').text(profitPerDayNormal)
  $('#profit_per_year .value-text-value').text(profitPerYear)
  $('#profit_clear_normal .value-text-value').text(profitClearNormal)
  $('#profit_per_day_reinvest .value-text-value').text(profitPerDayReinvest)
  $('#profit_reinvest .value-text-value').text(profitReinvest)
  $('#profit_clear_reinvest .value-text-value').text(profitClearReinvest)
  $('#profit_difference .value-text-value').text(profitDifference)

  return

_EliteAiTrade.calculator.initCalculator = () ->
  _EliteAiTrade.calculator.$amountInput.rangeslider
    polyfill: false,
    onInit: () ->
      _EliteAiTrade.calculator.$textAmount.text(_EliteAiTrade.calculator.$amountInput.val())

      _EliteAiTrade.calculator._calculate()
    onSlide: (position, value) ->
      _EliteAiTrade.calculator.$textAmount.text(value)

      _EliteAiTrade.calculator._calculate()

  return

$(window).on 'load', ->
  if _EliteAiTrade.calculator.$block.length
    _EliteAiTrade.calculator.initCalculator()

    console.log 'init calculator'