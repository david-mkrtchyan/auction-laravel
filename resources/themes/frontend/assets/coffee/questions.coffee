_EliteAiTrade.questions = _EliteAiTrade.questions || {}

_EliteAiTrade.questions.$page = $('.faq-page')

_EliteAiTrade.questions._initFaqPage = () ->
  $('.question-category').on "click", (e) ->
    e.preventDefault();

    $('.question-category').removeClass('active')

    $(this).addClass('active')

    category = $(this).attr('data-category')

    $questions = $('.question-item', '.questions-list')

    unless category == 'all'
      $questions.fadeOut(0)

      $questions.filter('.category-' + category).fadeIn(0)
    else
      $questions.fadeIn(0)

    $('.questions-list .active').removeClass('active')

    return false

  $('.question').on "click", (e) ->
    e.preventDefault();

    $('.questions-list .active').removeClass('active')

    $(this).siblings('.answer').addClass('active');

    return false

$(window).on 'load', ->
  if _EliteAiTrade.questions.$page.length
    _EliteAiTrade.questions._initFaqPage()

    console.log 'init questions'

