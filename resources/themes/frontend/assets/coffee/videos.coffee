$(window).on 'load', ->
  $videoModal = $('.video-modal')

  if $videoModal.length
    $(document).on "click", ".video-item", () ->
      src = $(this).attr('data-src')
      $player = $('<iframe src="' + src + '"></iframe>')
      $body = $videoModal.find('.modal-body')

      $body.html($player)

      setTimeout () ->
          $player.css({opacity: 1})
        , 1500

    $videoModal.on "hidden.bs.modal", () ->
      $videoModal.find('iframe').remove()

    console.log('init videos')