_EliteAiTrade.register = _EliteAiTrade.register || {}

_EliteAiTrade.register.$button = $('.modal-register-button')

_EliteAiTrade.register.$modal = $('.modal-register')

_EliteAiTrade.register.$form = $('.register-form')

_EliteAiTrade.register.initRegisterForm = () ->
  $(document).on 'click', '.register-form-submit', (e) ->
    e.preventDefault()

    data = _EliteAiTrade.form.getData(_EliteAiTrade.register.$form)

    $.ajax
      method: 'post',
      url: _EliteAiTrade.register.$form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.register.$modal.find('.close').click()

          return false

    return false

$(window).on 'load', ->
  if _EliteAiTrade.register.$form.length
    _EliteAiTrade.register.initRegisterForm()

    if _EliteAiTrade.register.$modal.hasClass('active')
      _EliteAiTrade.register.$button.click()

    console.log 'init register'