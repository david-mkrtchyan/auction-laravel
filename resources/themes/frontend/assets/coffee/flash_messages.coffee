window.flashMessage = (text, type, stayTime, position) ->
  if type == ''
    type = 'notice'
  if stayTime == undefined
    stayTime = 8000
  if position == undefined
    position = 'bottom right'

  messageContainer = alertify.logPosition(position)
    .delay(stayTime)
    .maxLogItems(10)
    .closeLogOnClick(true)

  switch type
    when 'success'
      messageContainer.success(text)
      break
    when 'error'
      messageContainer.error(text)
      break
    else
      messageContainer.log(text)
      break