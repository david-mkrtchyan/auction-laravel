_EliteAiTrade.ajax = _EliteAiTrade.ajax || {}

_EliteAiTrade.ajax.processError = (response) ->
  response = response.responseJSON

  unless response.errors
    if (response.message is '') or (response.message is undefined)
      flashMessage(_EliteAiTrade.lang.internalError, 'error')
    else
      flashMessage(response.message, 'error')

  if response.errors
    $.each response.errors, (index, items) ->
      $.each items, (i, item) ->
        flashMessage(item, 'error')

  unless response.redirect is undefined
    setTimeout( () ->
        window.location.href = response.redirect
      , 2000)

$(window).on 'load', ->
  console.log 'init ajax'