_EliteAiTrade.cookie = {}

_EliteAiTrade.cookie.get = (key, defaultValue) ->
  if defaultValue is undefined
    defaultValue = null

  match = document.cookie.match(new RegExp(key + '=([^;]+)'));

  if match
    return match[1]

  unless defaultValue is null
    return defaultValue

  return null

_EliteAiTrade.cookie.set = (key, value, time) ->
  time = time || false

  if value is undefined
    return

  if value == 'undefined'
    return

  document.cookie = key + "=" + value + "; path=/;" + (time ? "expires=" + time : "");

