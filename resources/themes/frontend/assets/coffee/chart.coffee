_EliteAiTrade.currencyChart = _EliteAiTrade.currencyChart || {}

_EliteAiTrade.currencyChart.chart = '.chart svg'

_EliteAiTrade.currencyChart.$chart = $(_EliteAiTrade.currencyChart.chart)

_EliteAiTrade.currencyChart.$loader = $('.chart .spiner')

_EliteAiTrade.currencyChart.$tooltip = $('.currency-tooltip')

_EliteAiTrade.currencyChart.width = parseFloat(_EliteAiTrade.currencyChart.$chart.width())

_EliteAiTrade.currencyChart.height = parseFloat(_EliteAiTrade.currencyChart.$chart.height())

_EliteAiTrade.currencyChart.xTiks = 7

_EliteAiTrade.currencyChart.initXScaleFormats = () ->
  _EliteAiTrade.formatSecond = d3.timeFormat("%M:%S")
  _EliteAiTrade.formatMinute = d3.timeFormat("%H:%M")
  _EliteAiTrade.formatHour = d3.timeFormat("%H:%M")
  _EliteAiTrade.formatDay = d3.timeFormat("%a %d")
  _EliteAiTrade.formatWeek = d3.timeFormat("%a %d")
  _EliteAiTrade.formatMonth = d3.timeFormat("%b %d")

_EliteAiTrade.currencyChart.xFormat = (date) ->
  if d3.timeMinute(date) < date
    return _EliteAiTrade.formatSecond(date)
  else
    if d3.timeHour(date) < date
      return _EliteAiTrade.formatMinute(date)
    else
      if d3.timeDay(date) < date
        _EliteAiTrade.formatHour(date)
      else
        if d3.timeMonth(date) < date
          if d3.timeWeek(date) < date
            return _EliteAiTrade.formatDay(date)
          else
            return _EliteAiTrade.formatWeek(date)
        else
          if d3.timeYear(date) < date
            return _EliteAiTrade.formatMonth(date)

_EliteAiTrade.currencyChart.padding =
  left: 0
  right: 0
  top: 5
  bottom: 35

_EliteAiTrade.currencyChart.initSize = () ->
  _EliteAiTrade.currencyChart.xScale = d3.scaleTime().range([
    _EliteAiTrade.currencyChart.padding.left,
    _EliteAiTrade.currencyChart.width - _EliteAiTrade.currencyChart.padding.left
  ])

  _EliteAiTrade.currencyChart.yScale = d3.scaleLinear().range([
    _EliteAiTrade.currencyChart.height - _EliteAiTrade.currencyChart.padding.bottom - _EliteAiTrade.currencyChart.padding.top,
    _EliteAiTrade.currencyChart.padding.top
  ])

_EliteAiTrade.currencyChart.prepareItem = (item) ->
  parseTime = d3.timeParse("%Y-%m-%d %H:%M:%S")

  item.date = parseTime(item.date)

  return item

_EliteAiTrade.currencyChart.initCurrencyRatePoints = () ->
  $(document).on 'mouseover', '.rate', () ->
    point = $(this)

    point.attr('r', 5)
    point.attr('fill', '#000000')

    _EliteAiTrade.currencyChart.$tooltip.find('.dealing-rate').text(point.attr('data-dealing_rate'))

    _EliteAiTrade.currencyChart.$tooltip.css({
      top: point.attr('cy') - parseInt(_EliteAiTrade.currencyChart.$tooltip.outerHeight() + 20) + 'px'
      left: point.attr('cx') - (parseInt(_EliteAiTrade.currencyChart.$tooltip.outerWidth()) / 2) + 'px'
    })

    _EliteAiTrade.currencyChart.$currencyLine
      .attr('y1', point.attr('cy') + 'px')
      .attr('x2', _EliteAiTrade.currencyChart.width + 'px')
      .attr('y2', point.attr('cy') + 'px')

    _EliteAiTrade.currencyChart.$currencyLine.show()

    _EliteAiTrade.currencyChart.$tooltip.show()

  $(document).on 'mouseleave', '.rate', () ->
    point = $(this)

    point.attr('r', 1)
    point.attr('fill', 'transparent')

    _EliteAiTrade.currencyChart.$tooltip.hide()

    _EliteAiTrade.currencyChart.$currencyLine.hide()

_EliteAiTrade.currencyChart.prepareData = (input) ->
  $.each input, (index, item) ->
    input[index] = _EliteAiTrade.currencyChart.prepareItem item

  input

_EliteAiTrade.currencyChart.update = (data) ->
  $.each data, (index, item) ->
    _EliteAiTrade.currencyChart.data.push _EliteAiTrade.currencyChart.prepareItem item

  _EliteAiTrade.currencyChart.data.splice 0, data.length

  _EliteAiTrade.currencyChart.xScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, (d) -> d.date))
  _EliteAiTrade.currencyChart.yScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, (d) -> d.rate))

  svg = d3.select(_EliteAiTrade.currencyChart.chart)

  svg.select(".line")
    .attr("d", _EliteAiTrade.currencyChart.line(_EliteAiTrade.currencyChart.data));

  svg.select(".area")
    .attr("d", _EliteAiTrade.currencyChart.area(_EliteAiTrade.currencyChart.data));

  _EliteAiTrade.currencyChart.$chart.find('.rate').remove()

  svg.selectAll('.rate')
    .data(_EliteAiTrade.currencyChart.data)
    .enter()
    .append('circle')
    .attr("fill", "transparent")
    .attr("stroke", "transparent")
    .attr('cx', (d) ->
      _EliteAiTrade.currencyChart.xScale(d.date)
    ).attr('cy', (d) ->
      _EliteAiTrade.currencyChart.yScale(d.rate)
    ).attr('r', (d) ->
      2
    ).attr('class', (d) ->
      'rate'
    ).attr('data-dealing_rate', (d) ->
      '$ ' + _EliteAiTrade.helpers.format(d.rate)
    )

  svg.select(".axis-x")
    .attr("stroke-width", 0)
    .attr('transform', 'translate(0,' + parseInt(_EliteAiTrade.currencyChart.height) + ')')
    .call(
      d3.axisBottom(_EliteAiTrade.currencyChart.xScale)
        .ticks(_EliteAiTrade.currencyChart.xTiks)
        .tickFormat(_EliteAiTrade.currencyChart.xFormat)
    ).selectAll("text")
      .attr('transform', 'translate(0, -25)')

  svg.select(".axis-y")
    .attr("stroke-width", 0)
    .attr('transform', 'translate(0, 0)')
    .call(
      d3.axisLeft(_EliteAiTrade.currencyChart.yScale)
        .tickFormat((d) -> '$ ' + _EliteAiTrade.helpers.format(d))
        .tickPadding(8)
        .tickSize(-_EliteAiTrade.currencyChart.width)
    ).selectAll("text")
      .attr('transform', 'translate(80, -15)')

  _EliteAiTrade.currencyChart.initCurrencyRatePoints()

_EliteAiTrade.currencyChart.initChart = () ->
  _EliteAiTrade.currencyChart.data = _EliteAiTrade.currencyChart.prepareData(_EliteAiTrade.currencyChart.data)

  svg = d3.select(_EliteAiTrade.currencyChart.chart)

  g = svg.append("g");

  _EliteAiTrade.currencyChart.initSize()

  _EliteAiTrade.currencyChart.area = d3.area().x((d) ->
    _EliteAiTrade.currencyChart.xScale(d.date)
  ).y1((d) ->
    _EliteAiTrade.currencyChart.yScale(d.rate)
  ).curve(d3.curveCardinal.tension(1))

  _EliteAiTrade.currencyChart.line = d3.line().x((d) ->
    _EliteAiTrade.currencyChart.xScale(d.date)
  ).y((d) ->
    _EliteAiTrade.currencyChart.yScale(d.rate)
  ).curve(d3.curveCardinal.tension(1))

  _EliteAiTrade.currencyChart.area.y0 _EliteAiTrade.currencyChart.yScale(0)

  _EliteAiTrade.currencyChart.xScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, (d) -> d.date))
  _EliteAiTrade.currencyChart.yScale.domain(d3.extent(_EliteAiTrade.currencyChart.data, (d) -> d.rate))

  g.append('path')
    .datum(_EliteAiTrade.currencyChart.data)
    .attr("class", "area")
    .attr("fill", "#F5F5F5")
    .attr("stroke", "transparent")
    .attr("stroke-width", 0)
    .attr('d', _EliteAiTrade.currencyChart.area)

  g.append("path")
    .datum(_EliteAiTrade.currencyChart.data)
    .attr("class", "line")
    .attr("fill", "none")
    .attr("stroke", "#DBDDDC")
    .attr("stroke-width", 1.5)
    .attr("d", _EliteAiTrade.currencyChart.line)

  svg.append("line")
    .attr('class', 'currency-line')
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("x2", 0)
    .attr("y2", 0)

  _EliteAiTrade.currencyChart.$chart.find('.rate').remove()

  svg.selectAll('.rate')
    .data(_EliteAiTrade.currencyChart.data)
    .enter()
    .append('circle')
    .attr("fill", "transparent")
    .attr("stroke", "transparent")
    .attr('cx', (d) ->
      _EliteAiTrade.currencyChart.xScale(d.date)
    ).attr('cy', (d) ->
      _EliteAiTrade.currencyChart.yScale(d.rate)
    ).attr('r', (d) ->
      2
    ).attr('class', (d) ->
      'rate'
    ).attr('data-dealing_rate', (d) ->
      '$ ' + _EliteAiTrade.helpers.format(d.rate)
    )

  g.append('g')
    .attr('class', 'axis axis-x')
    .attr("stroke-width", 0)
    .attr('transform', 'translate(0,' + parseInt(_EliteAiTrade.currencyChart.height) + ')')
    .call(
      d3.axisBottom(_EliteAiTrade.currencyChart.xScale)
        .ticks(_EliteAiTrade.currencyChart.xTiks)
        .tickFormat(_EliteAiTrade.currencyChart.xFormat)
    ).selectAll("text")
      .attr('transform', 'translate(0, -25)')

  g.append('g')
    .attr('class', 'axis axis-y')
    .attr("stroke-width", 0)
    .attr('transform', 'translate(0, 0)')
    .call(
      d3.axisLeft(_EliteAiTrade.currencyChart.yScale)
        .tickFormat((d) -> '$ ' + _EliteAiTrade.helpers.format(d))
        .tickPadding(8)
        .tickSize(-_EliteAiTrade.currencyChart.width)
    ).selectAll("text")
      .attr('transform', 'translate(80, -15)')

  _EliteAiTrade.currencyChart.$loader.hide()

  _EliteAiTrade.currencyChart.initCurrencyRatePoints()

  _EliteAiTrade.currencyChart.$currencyLine = _EliteAiTrade.currencyChart.$chart.find('.currency-line')

  setInterval (->
    $.ajax
      method: 'get',
      url: _EliteAiTrade.currencyChart.$chart.attr('data-update_url')
      dataType: 'json'
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        if !response.length
          return

        _EliteAiTrade.currencyChart.update(response)

  ), parseInt(_EliteAiTrade.currencyChart.updateTimeout * 1000)

$(window).on 'load', ->
  if _EliteAiTrade.currencyChart.$chart.length
    _EliteAiTrade.currencyChart.initXScaleFormats()

    _EliteAiTrade.currencyChart.initChart()

    $(window).resize () ->
      _EliteAiTrade.currencyChart.width = parseFloat(_EliteAiTrade.currencyChart.$chart.closest('.chart').width() / 100 * 99)
      _EliteAiTrade.currencyChart.height = parseFloat(_EliteAiTrade.currencyChart.$chart.closest('.chart').height() / 100 * 99)

      _EliteAiTrade.currencyChart.initSize()

      _EliteAiTrade.currencyChart.update([])

    console.log 'init trades'