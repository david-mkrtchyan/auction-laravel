_EliteAiTrade.trades = _EliteAiTrade.trades || {}

_EliteAiTrade.trades.$block = $('.trades-history')

_EliteAiTrade.trades.$list = _EliteAiTrade.trades.$block.find('ul')

_EliteAiTrade.trades.$template = _EliteAiTrade.trades.$block.find('.template')

_EliteAiTrade.trades.show = (trades) ->
  trade = trades[0]

  trades.splice 0, 1

  $template = _EliteAiTrade.trades.$template.clone()

  operation = _EliteAiTrade.lang[trade.operation]

  currency = operation + ' ' + trade.amount + ' ' + (if trade.operation == 'sold' then trade.currency_from else trade.currency_to)

  currency_rate = _EliteAiTrade.lang.for + ' ' +
    (if trade.operation == 'sold' then trade.currency_to else trade.currency_from) + ' ' +
    trade.currency_rate + ' / ' +
    (if trade.operation == 'sold' then trade.currency_from else trade.currency_to)

  $template
    .find('.currency').text(currency)
    .end()
    .find('.currency-rate').text(currency_rate)

  _EliteAiTrade.trades.$list.prepend($template)

  $template.fadeIn(300, () ->
      if trades.length > 0
        _EliteAiTrade.trades.renderTrades(trades)
  )

_EliteAiTrade.trades.renderTrades = (trades) ->
  if _EliteAiTrade.trades.$list.find('li').length >= 5
    $last = _EliteAiTrade.trades.$list.find('li:last-child')

    $last.fadeOut(300, () ->
        $last.remove()

        _EliteAiTrade.trades.show(trades)
    )
  else
    _EliteAiTrade.trades.show(trades)

_EliteAiTrade.trades.initTradesHistory = () ->
  setInterval () ->
      $.ajax
        method: 'get'
        dataType: 'json'
        url: _EliteAiTrade.trades.$block.attr('data-url')
        error: (response) ->
          _EliteAiTrade.ajax.processError(response)
        success: (response) ->
          if response.status is 'success'
            if response.trades.length
              _EliteAiTrade.trades.renderTrades(response.trades)
    , _EliteAiTrade.trades.updateTimeout * 1000

$(window).on 'load', ->
  if _EliteAiTrade.trades.$block.length > 0
    _EliteAiTrade.trades.initTradesHistory()

    console.log 'init trades'