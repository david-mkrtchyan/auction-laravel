_EliteAiTrade.files = _EliteAiTrade.files || {}

_EliteAiTrade.files.itemClass = '.user-verification-file'

_EliteAiTrade.files._initFilesUpload = () ->
  $(document).on 'change', _EliteAiTrade.files.itemClass, (e) ->
    e.preventDefault()

    $item = $(this)
    $form = $item.closest('form')

    data = _EliteAiTrade.form.getFormData($form)

    $.ajax
      method: $form.attr('method')
      dataType: 'json'
      url: $form.attr('action')
      data: data
      processData: false
      contentType: false
      error: (response) ->
        $item.closest('.documents__item')
          .find('.documents__item__status')
          .removeClass('uploaded disclined')
          .addClass('error')
          .text(_EliteAiTrade.lang.errorUpload)

        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        $item.closest('.documents__item')
          .find('.documents__item__status')
          .removeClass('uploaded disclined error')
          .text('')

        flashMessage(response.message, response.status)

        if response.status is 'success'
          $form.attr('action', response.action)

        $item.closest('.documents__item')
          .find('.documents__item__status')
          .addClass('uploaded')
          .text(_EliteAiTrade.lang.uploaded)

        return false

    return false

$(window).on 'load', ->
  if $(_EliteAiTrade.files.itemClass).length
    _EliteAiTrade.files._initFilesUpload()

    console.log 'init files'