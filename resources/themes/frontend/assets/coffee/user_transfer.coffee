_EliteAiTrade.userTransfer = _EliteAiTrade.userTransfer || {}

_EliteAiTrade.contract.userTransfer = false

_EliteAiTrade.userTransfer.$page = $('.user-transfer-page')

_EliteAiTrade.userTransfer.$form = _EliteAiTrade.userTransfer.$page.find('form')

_EliteAiTrade.userTransfer.$walletSelect = _EliteAiTrade.userTransfer.$page.find('[name="wallet_id"]')

_EliteAiTrade.userTransfer.$amountField = _EliteAiTrade.userTransfer.$page.find('[name="amount"]')

_EliteAiTrade.userTransfer.$dollarAmount = _EliteAiTrade.userTransfer.$page.find('.dollar-amount')

_EliteAiTrade.userTransfer.dealingRate = parseFloat(_EliteAiTrade.userTransfer.$dollarAmount.attr('data-dealing_rate'))

_EliteAiTrade.userTransfer.$modalButton = _EliteAiTrade.userTransfer.$page.find('.user-transfer-modal-button')

_EliteAiTrade.userTransfer.$modal = $('#modal_confirm')

_EliteAiTrade.userTransfer.$walletBalance = _EliteAiTrade.userTransfer.$page.find('.calculator .wallet-balance')
_EliteAiTrade.userTransfer.$transferAmount = _EliteAiTrade.userTransfer.$page.find('.calculator .transfer-amount')
_EliteAiTrade.userTransfer.$transferCommission = _EliteAiTrade.userTransfer.$page.find('.calculator .transfer-commission')
_EliteAiTrade.userTransfer.$totalAmount = _EliteAiTrade.userTransfer.$page.find('.calculator .total-amount')

_EliteAiTrade.userTransfer.calculate = (amount) ->
  _EliteAiTrade.userTransfer.$transferAmount.text(amount)

  commission = amount / 100 * _EliteAiTrade.userTransfer.$transferCommission.attr('data-commission')

  _EliteAiTrade.userTransfer.$transferCommission.text(commission.toFixed(8))

  totalAmount =  parseFloat(amount) + commission

  _EliteAiTrade.userTransfer.$totalAmount.text(totalAmount.toFixed(8))

_EliteAiTrade.userTransfer.initWithdrawPage = () ->
  _EliteAiTrade.userTransfer.$walletSelect.on 'change', (e) ->
    _EliteAiTrade.userTransfer.$walletBalance.text($(this).find('option:selected').attr('data-balance'))

  _EliteAiTrade.userTransfer.$amountField.on 'keyup', (e) ->
    e.preventDefault()

    value = $(this).val()

    if value == ''
      _EliteAiTrade.userTransfer.$dollarAmount.text('= 0 $')

      return

    value = parseFloat(value)

    if value < 0 or value == ''
      _EliteAiTrade.userTransfer.$dollarAmount.text('= 0 $')

      return

    amount = _EliteAiTrade.userTransfer.dealingRate * value;

    amount = if amount < 100 then _EliteAiTrade.helpers.format(amount, 2) else _EliteAiTrade.helpers.format(amount, 0)

    _EliteAiTrade.userTransfer.$dollarAmount.text('= ' + amount + ' $')

    _EliteAiTrade.userTransfer.calculate(value)

    return false;

  _EliteAiTrade.userTransfer.$form.on 'click', '.submit-user-transfer-form', (e) ->
    e.preventDefault()

    if _EliteAiTrade.userTransfer.blocked == true
      return

    $that = $(this)

    _EliteAiTrade.userTransfer.blocked = true

    $that.attr('disabled', 'disabled');

    data = _EliteAiTrade.form.getData(_EliteAiTrade.userTransfer.$form)

    $.ajax
      method: 'post',
      url: _EliteAiTrade.userTransfer.$form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)

        _EliteAiTrade.userTransfer.blocked = false

        $that.removeAttr('disabled');
      success: (response) ->
        _EliteAiTrade.userTransfer.blocked = false

        $that.removeAttr('disabled');

        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.userTransfer.$modal.find('[name=\'wallet_id\']').val(response.wallet_id)
          _EliteAiTrade.userTransfer.$modal.find('[name=\'user\']').val(response.user)
          _EliteAiTrade.userTransfer.$modal.find('[name=\'amount\']').val(response.amount)
          _EliteAiTrade.userTransfer.$modal.find('[name=\'_token\']').val(response._token)

          _EliteAiTrade.userTransfer.$modalButton.click()

          return false

    return false

  _EliteAiTrade.userTransfer.$modal.on 'click', '.submit-confirm-form', (e) ->
    e.preventDefault()

    if _EliteAiTrade.userTransfer.blocked == true
      return

    $that = $(this)

    _EliteAiTrade.userTransfer.blocked = true

    $that.attr('disabled', 'disabled');

    $form = _EliteAiTrade.userTransfer.$modal.find('form')

    data = _EliteAiTrade.form.getData($form)

    $.ajax
      method: 'post',
      url: $form.attr('action')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)

        _EliteAiTrade.userTransfer.blocked = false

        $that.removeAttr('disabled');
      success: (response) ->
        _EliteAiTrade.userTransfer.blocked = false

        $that.removeAttr('disabled');

        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.userTransfer.$modal.find('.close').click()

          setTimeout () ->
              window.location.reload()
            , 1000

        return false

    return false

$(window).on 'load', ->
  if _EliteAiTrade.userTransfer.$page.length
    _EliteAiTrade.userTransfer.initWithdrawPage()

    console.log 'init user transfer'