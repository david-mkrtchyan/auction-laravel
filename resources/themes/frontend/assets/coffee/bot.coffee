_EliteAiTrade.bot = _EliteAiTrade.bot || {}

_EliteAiTrade.bot.$modalButton = $('.run-bot')

_EliteAiTrade.bot.$button = $('.run-bot-button')

_EliteAiTrade.bot.$modal = $('.run-bot-modal')

_EliteAiTrade.bot.init = () ->
  _EliteAiTrade.bot.$button.on 'click', (e) ->
    e.preventDefault()

    data = {
      _token: _EliteAiTrade.bot.$button.attr('data-_token')
    }

    $.ajax
      method: 'post',
      url: _EliteAiTrade.bot.$button.attr('href')
      dataType: 'json'
      data: data
      error: (response) ->
        _EliteAiTrade.ajax.processError(response)
      success: (response) ->
        if response.message
          flashMessage(response.message, response.status)

        if response.status is 'success'
          _EliteAiTrade.bot.$modalButton.addClass('spin')
            .text(_EliteAiTrade.lang.running)
            .removeAttr('data-toggle')
            .removeAttr('data-target')

          _EliteAiTrade.bot.$modal.find('.close').click()

        return false

    return false

$(window).on "load", () ->
  if _EliteAiTrade.bot.$button.length
    unless _EliteAiTrade.bot.$button.hasClass('.spin')
      _EliteAiTrade.bot.init()

      console.log('init bot')