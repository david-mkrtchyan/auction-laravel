@if (isset($email) && isset($code))
    <div class="modal fade password-modal" id="modal22" tabindex="-1" role="dialog" aria-labelledby="modal22" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h3>Password Recovery</h3>
                    <div class="align-center"><p>@lang('front_texts.enter new password')</p></div>
                    <div class="login-form">
                        {!! Form::open(['route' => 'reminder.complete.email', 'method' => 'post', 'class' => 'auth-form']) !!}
                        {!! Form::hidden('email', $email) !!}
                        {!! Form::hidden('code', $code) !!}
                        <div class="forinput form-group">
                            {{ Form::password('password', ['placeholder' => trans('front_labels.password'),'class' => 'form-control']) }}
                        </div>
                        <div class="forinput form-group">
                            {{ Form::password('password_confirmation', ['placeholder' => trans('front_labels.password_confirmation'),'class' => 'form-control']) }}
                        </div>
                        @if($g2fa)
                            <div class="forinput">
                                <input required type="text" name="otp" placeholder="@lang('front_labels.otp placeholder')" id="register_otp"/>
                            </div>
                        @endif
                        <div class="error-text hide"></div>
                        <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit"
                               value="@lang('front_labels.new password')"/>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
@endif

<div class="modal fade" id="modal21" tabindex="-1" role="dialog" aria-labelledby="modal21" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3>Password Recovery</h3>
                <div class="align-center">
                    <div class="align-center"><p>@lang('front_texts.password restore helper text')</p></div>
                </div>
                <div class="login-form">
                    {!! Form::open(['route' => 'reminder.store.email', 'method' => 'post', 'class' => 'auth-form']) !!}
                    <div class="forinput form-group">
                        {{ Form::email('email', null, ['placeholder' => trans('front_labels.email placeholder'),'class' => 'form-control']) }}
                    </div>
                    <div class="error-text hide"></div>
                    <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit"
                           value="@lang('front_labels.send')"/>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>