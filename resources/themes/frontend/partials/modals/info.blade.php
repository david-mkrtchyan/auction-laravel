@php($id = isset($id) && !empty($id) ? $id : random_string('alpha'))
@php($title = isset($title) && !empty($title) ? $title : '<strong>AI</strong>'.trans('front_labels.info'))
@php($text = isset($text) && !empty($text) ? $text : 'Welcome')
@php($hide = isset($hide) && $hide == true ? true : false)

<div class="modal izimodal" id="infomodal-{!! $id !!}" data-izimodal-transitionin="fadeInDown" tabindex="-1"
     role="dialog" aria-hidden="true" data-izimodal-group="info">
    <div class="_modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3>{!! $title !!}</h3>
                <div>
                    <p>{!! $text !!}</p>
                </div>
            </div>
            @if ($hide)
                <div class="modal-footer">

                    <div class="forcheck">
                        {{ Form::checkbox('hide_modal_'.$id, true, false, ['id' => 'hide_modal_checkbox_'.$id, 'class' => 'button-hide']) }}
                        <label for="hide_modal_checkbox_{!! $id !!}">
                            <span>@lang('front_labels.do not show this again')</span>
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-block close-modal">
                        @lang('front_labels.got it')
                    </button>
                </div>
            @else
                <div class="modal-footer"></div>
            @endif
        </div>
    </div>
</div>