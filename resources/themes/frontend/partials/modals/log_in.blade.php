<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3><strong>AI</strong> Trade Login</h3>
                <div class="login-form">
                    {!! Form::open(['route' => 'session.store', 'method' => 'post', 'class' => 'auth-form']) !!}
                        <div class="forinput form-group">
                            {{ Form::email('email', null, ['placeholder' => trans('front_labels.email placeholder'),'class' => 'form-control']) }}
                        </div>
                        <div class="forinput form-group">
                            {{ Form::password('password', ['placeholder' => trans('front_labels.password'),'class' => 'form-control']) }}
                        </div>
                        <div class="error-text hide"></div>
                        <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit" value="@lang('front_labels.log in')" />
                    {!! Form::close() !!}
					<p> </p>
					<div data-toggle="modal" data-target="#modal21" class="align-center"><p data-toggle="modal" data-target="#modal2">@lang('front_labels.forgot password')</p></div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade two-factor-login-modal" id="modal20" tabindex="-1" role="dialog" aria-labelledby="modal20" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3>@lang('front_labels.two factor login form title')</h3>
                <div class="align-center">
                    <div class="align-center"><p>@lang('front_texts.two factor login helper text')</p></div>
                </div>
                <div class="login-form">
                    {!! Form::open(['route' => 'session.two_factor.check', 'method' => 'post', 'class' => 'auth-form']) !!}
                    <div class="forinput">
                        {{ Form::text('otp', null, ['placeholder' => trans('front_labels.otp placeholder')]) }}
                    </div>
                    <div class="error-text hide"></div>
                    <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit"
                           value="@lang('front_labels.send')"/>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>