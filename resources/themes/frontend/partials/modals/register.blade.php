<div class="modal fade modal-register @if (isset($referral_key)) active @endif" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-reg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3>Register <strong>AI</strong> Trade</h3>
                <div class="registr-form">
                    {!! Form::open(['route' => 'user.store', 'method' => 'post', 'class' => 'register-form']) !!}
                        <div class="row">
                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group forinput">
                                    {{ Form::email('email', null, ['placeholder' => trans('front_labels.email placeholder'),'class' => 'form-control']) }}
                                </div>
                                <div class="form-group forinput">
                                    {{ Form::text('first_name', null, ['class' => 'form-control name', 'placeholder' => trans('front_labels.first_name'),'class' => 'form-control']) }}
                                </div>
                                <div class="form-group forinput">
                                    {{ Form::password('password', ['placeholder' => trans('front_labels.password'),'class' => 'form-control']) }}
                                </div>
                                <div class="form-group forinput">
                                    {{ Form::text('user_name',  null, ['class' => 'form-control user_name', 'placeholder' => trans('front_labels.user_name'),'class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group forinput">
                                    {{ Form::select('country_id', $countries, null,['class' => 'form-control']) }}
                                </div>
                                <div class="form-group forinput">
                                    {{ Form::text('last_name', null, ['class' => 'form-control name', 'placeholder' => trans('front_labels.last_name'),'class' => 'form-control']) }}
                                </div>

                                <div class="form-group forinput">
                                    {{ Form::password('password_confirmation', ['placeholder' => trans('front_labels.password_confirmation'),'class' => 'form-control']) }}
                                </div>

                                <div class="form-group forinput">
                                    {{ Form::tel('phone', null, ['id'=>'mobile-number','class' => 'form-control phone', 'placeholder' => trans('front_labels.phone'),'class' => 'form-control']) }}
                                    {{ Form::hidden('phone_validate', '', array('id' => 'validate_phone')) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 offset-xl-3 offset-lg-0 offset-md-0 offset-sm-0">
                                <input type="submit" name="submit" class="btn btn2 btn-block btn-primary register-form-submit" value="@lang('front_labels.register title')" />
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

@push('phone')
    @component('partials.components.intlTelInput',['id' => 'mobile-number']) @endcomponent
@endpush
