@if ($questions->count())
    <div class="questions">
        <h3>@lang('front_labels.faq title'):</h3>

        <ul class="questions-list">
            @foreach($questions as $question)
                <li>
                    <a class="question"
                       data-text="{!! $question->answer !!}"
                       href="#">{!! $question->question !!}</a>
                </li>
            @endforeach
        </ul>

        <p class="answer">{!! $questions->first()->answer !!}</p>
    </div>
@endif