<ul>
    @php($viber = variable('support_link_viber'))
    @if ($viber)
        <li>
            <a href="{!! variable('support_link_viber') !!}"
               title="@lang('front_labels.support service') Viber">
                <img src="{!! theme_asset('images/ico-6.png') !!}"
                     alt="@lang('front_labels.support service') Viber"
                     title="@lang('front_labels.support service') Viber"/>
            </a>
        </li>
    @endif

    @php($viber = variable('support_link_messenger'))
    @if ($viber)
        <li>
            <a href="{!! variable('support_link_messenger') !!}"
               title="@lang('front_labels.support service') Messenger">
                <img src="{!! theme_asset('images/ico-7.png') !!}"
                     alt="@lang('front_labels.support service') Messenger"
                     title="@lang('front_labels.support service') Messenger"/>
            </a>
        </li>
    @endif

    @php($viber = variable('support_link_telegram'))
    @if ($viber)
        <li>
            <a href="{!! variable('support_link_telegram') !!}"
               title="@lang('front_labels.support service') Telegram">
                <img src="{!! theme_asset('images/ico-8.png') !!}"
                     alt="@lang('front_labels.support service') Telegram"
                     title="@lang('front_labels.support service') Telegram"/>
            </a>
        </li>
    @endif
</ul>