<div class="language">
    <a class="dropdown-toggle" href="#" role="button" id="dropdownLanguageLink"
       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                class="flag-icon flag-icon-{!! $locale !!}"></span></a>
    <div class="dropdown-menu" aria-labelledby="dropdownLanguageLink">
        @foreach($locales as $locale)
            <a class="dropdown-item dropdown-item-lang" href="{!! localize_url(null, $locale) !!}" data-lang="flag-icon-{!! $locale !!}"><span
                        class="flag-icon flag-icon-{!! $locale !!}"></span></a>
        @endforeach
    </div>
</div>