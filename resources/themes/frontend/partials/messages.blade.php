<script type="text/javascript">window.messages_timeout = 500;</script>

@if (isset($messages))
    @if ($messages->count())
        @foreach ($messages->getMessages() as $key => $group)
            @foreach ($group as $message)
                <script type="text/javascript">
                    $(document).ready(function () {
                        window.messages_timeout = parseInt(window.messages_timeout) + 500;
                        setTimeout('flashMessage(\'{!! $message !!}\', \'{!! $key !!}\');', window.messages_timeout);
                    });
                </script>
            @endforeach
        @endforeach
    @endif
@endif


@if (isset($profile_modal) && $profile_modal == true)
    @php($profile_modal_text = variable('profile_modal_text'))
    @if ($profile_modal_text)
        @include(
            'partials.modals.info',
            [
                'id' => 'profile',
                'title' => trans('front_labels.profile modal title'),
                'text' => $profile_modal_text,
            ]
        )
    @endif
@endif