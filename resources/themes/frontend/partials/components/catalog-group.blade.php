@if($groups)

    @push('countDown')
        <script type="text/javascript" src="{!! theme_asset('js/countDown.js') !!}"></script>
    @endpush

    <div class="group-catalog" id="app">
        <h1 class="group-catalog-h1">Lorem ipsum dolor sit amet.</h1>
        <h2 class="group-catalog-h2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, saepe.</h2>
        <div class="cardd" v-match-heights="{el: ['.img-product'],disabled: [768,[920, 1000], ],}">
            @foreach($groups as $group)
                <div class="one">
                    <div class="box1 group-catalog-title">
                        <h3>{!! $group->name !!}</h3>
                    </div>
                    <div class="box2">
                        <h4>@lang('front_messages.ends') {!! $group->setFormatDate($group->end_time, "d.m") !!}</h4>
                    </div>
                    <div class="box3">
                        <h5>{!! count($group->product) !!} @lang('front_messages.directory numbers')</h5>
                    </div>

                    <div class="box4 group-catalog-image">
                        <a href="{!! route('group.show',$group->id) !!}" class="group-url">
                            @if($image = $group->checkImageExists())
                                <img src="{!! $image !!}" alt="" class="img-product">
                            @endif


                            @if($group->checkTime())
                                @component('partials.components.timer',['id' => $group->id, 'group' => $group])
                                @endcomponent

                                @push('countDown')
                                    <script>
                                        countDown("{!! $group->id !!}", "{!! $group->setFormatDate($group->end_time, "M d, Y H:i:s") !!}")
                                    </script>
                                @endpush

                            @else
                                <div class="removed_item">
                                    @lang('front_messages.removed')
                                </div>
                            @endif
                        </a>
                    </div>
                    <div class="box5">
                        <a href="{!! route('group.show',$group->id) !!}"
                           class="see-all">@lang('front_messages.see all')</a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="group-pagination">
            {{ $groups->links() }}
        </div>
    </div>
@endif



@push('vue')
    <script type="text/javascript" src="{!! theme_asset('js/vue.js') !!}"></script>
@endpush

@push('vue-match-heights')
    <script type="text/javascript" src="{!! theme_asset('js/vue-match-heights.js') !!}"></script>

    <script>
        Vue.use(VueMatchHeights, {
            disabled: [768],
        })

        new Vue({
            el: '#app'
        })
    </script>

@endpush


