<link rel="stylesheet" href="{!! asset('assets/components/intl-tel-input/build/css/intlTelInput.min.css') !!}">

<script type="text/javascript"
        src="{!! asset('assets/components/intl-tel-input/build/js/intlTelInput.min.js') !!}"></script>

<script>
    var input = document.querySelector("#{!! $id !!}"),
        validatePhone = document.querySelector("#validate_phone");
    /*errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg"),*/
    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

    // initialise plugin
    var iti = window.intlTelInput(input, {
        allowExtensions: true,
        preferredCountries: ['lt'],
        nationalMode: false,
        preventInvalidNumbers: true,
        autoHideDialCode: false,
        utilsScript: "{!! asset('assets/components/intl-tel-input/build/js/utils.js') !!}"
    });

    var reset = function () {
        input.classList.remove("error");
        /*errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");*/
    };

    // on blur: validate
    input.addEventListener('blur', function () {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                /*validMsg.classList.remove("hide");*/
                input.classList.remove("error");
                validatePhone.value = true;
            } else {
                input.classList.add("error");
                validatePhone.value = false;
                /*var errorCode = iti.getValidationError();
                errorMsg.innerHTML = errorMap[errorCode];
                errorMsg.classList.remove("hide");*/
            }
        }
    });

    // on keyup / change flag: reset
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);
</script>