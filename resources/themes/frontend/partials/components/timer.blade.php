
<div class="ends-in clearfix" id="ends-in{!! $id !!}">
    <div class="time-container">
        <div class="days text-time" id="days{!! $id !!}"></div>
        <div class="label">дней</div>
    </div>
    <div class="time-container">
        <div class="hours text-time" id="hours{!! $id !!}"></div>
        <div class="label">часов</div>
    </div>
    <div class="time-container">
        <div class="minutes text-time" id="minutes{!! $id !!}"></div>
        <div class="label">минут</div>
    </div>
    <div class="time-container">
        <div class="seconds text-time" id="seconds{!! $id !!}"></div>
        <div class="label">секунд</div>
    </div>
</div>