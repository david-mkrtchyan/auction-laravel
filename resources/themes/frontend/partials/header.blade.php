{{--
<header>
    <div class="container-fluid-my">
        <div class="my-row">
            <div class="row-left">
                @if(!$_user):
                    @include('partials.modals.register')
                    <a href="#" class="btn btn-primary modal-register-button" data-toggle="modal" data-target="#modal1"
                       id="openReg">front_landing.registration</a>

                    @include('partials.modals.log_in')
                    <a href="#" class="btn btn-text" data-toggle="modal" data-target="#modal2" id="openEnter">front_landing.login</a>

                    @include('partials.modals.password_recovery')
                @endif

                @include('partials.logo', ['with_title' => true])
            </div>
            @if ($_user)
                <div class="row-center">
                    <div class="header-center">

                        @include('partials.menus.main')

                        <div class="header-center__right">
                            @include('partials.modules.language')

                            <div class="account">
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownAccountLink"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
                                            src="{!! thumb($_user->avatar, 40) !!}"
                                            alt="{!! $_user->getFullName() !!}"
                                            title="{!! $_user->getFullName() !!}"/><span>{!! $_user->getFullName() !!}</span></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownAccountLink">
                                    <a class="dropdown-item"
                                       title="@lang('front_labels.verification')"
                                       href="{!! route('verification.edit') !!}">@lang('front_labels.verification')</a>
                                    <a class="dropdown-item"
                                       title="@lang('front_labels.personal data')"
                                       href="{!! route('personal_data.edit') !!}">@lang('front_labels.personal data')</a>
                                    <a class="dropdown-item"
                                       title="@lang('front_labels.withdraw wallet')"
                                       href="{!! route('withdraw_wallet.show') !!}">@lang('front_labels.withdraw wallet')</a>
                                    <a class="dropdown-item"
                                       title="@lang('front_labels.change password')"
                                       href="{!! route('password.edit') !!}">@lang('front_labels.change password')</a>
                                    <a class="dropdown-item"
                                       title="@lang('front_labels.withdraw funds')"
                                       href="{!! route('withdraw.create') !!}">@lang('front_labels.withdraw funds')</a>
                                    <a class="dropdown-item"
                                       title="@lang('front_labels.transfer funds')"
                                       href="{!! route('transfer.user.create') !!}">@lang('front_labels.transfer funds')</a>
                                    <a class="dropdown-item"
                                       title="@lang('front_labels.log out')"
                                       href="{!! route('session.delete') !!}">@lang('front_labels.log out')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</header>
--}}

<div class="head">
    <div class="header">
        <div class="head-left">
            @if(!$_user)

                {{-- register,recovery,login --}}
                @include('partials.modals.register')
                @include('partials.modals.password_recovery')
                @include('partials.modals.log_in')

                <a href="#" data-toggle="modal"  data-target="#modal2" id="openEnter" class="btn-primary">
                    <i class="fas fa-unlock"></i>@lang('front_landing.login')
                </a>

                <a href="#" class="two btn btn-primary modal-register-button" data-toggle="modal" data-target="#modal1" id="openReg">
                    <i class="fas fa-plus"></i> @lang('front_landing.create_account')
                </a>

            @else

                <a  href="{!! route('session.delete') !!}"  title="@lang('front_landing.log out')" id="openEnter" class="btn-primary">
                    <i class="fas fa-unlock"></i></i>@lang('front_landing.logout')
                </a>

                <a href="{!! route('user.edit') !!}"  class="{!! front_active_class('user.*') !!} two btn btn-primary modal-register-button"  id="openReg">
                    <i class="fas fa-plus"></i> @lang('front_landing.my details')
                </a>

            @endif

            @include('partials.logo', ['with_title' => true])
        </div>

        <div class="head-right">
            <a href="#"><i class="fab fa-facebook-f "></i></a>
            <input type="search" class="form-control">
            <i class="fab fa-cc-visa"></i>
            <i class="fab fa-cc-mastercard"></i>
            <i class="fab fa-cc-visa"></i>
        </div>

    </div>
</div>

@include('partials.menus.main')

@include('partials.modals.catalog')