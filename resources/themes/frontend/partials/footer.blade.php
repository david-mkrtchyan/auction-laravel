
<footer class="footer">
    @include('partials.menus.bottom')

    <div class="info">
        <p>jysknetauktion.dk - 24 48 48 45 - info@jyskauktion.dk</p>
    </div>
</footer>

@include('partials.metrics.facebook')

@include('partials.metrics.google')

@include('partials.metrics.chimpstatic')


{{--
<footer>
    <div class="container-fluid-my">
        <div class="my-row">
            <div class="row-left">
                <div class="support">
                    <div class="title">
                        <img src="{!! theme_asset('images/ico-5.png') !!}"
                             alt="@lang('front_labels.support')"
                             title="@lang('front_labels.support')" />@lang('front_labels.support') 24/7
                    </div>

                    @include('partials.modules.support_links')
                </div>

                @include('partials.modules.language')
            </div>
            <div class="row-center">
                <div class="row-center__inner">
                    @include('partials.menus.social_pages')
                </div>
            </div>
            <div class="row-right">
                @include('partials.menus.footer_pages')
            </div>
        </div>
    </div>
</footer>


--}}
