@if (!empty(config('metrics.google.analytics_id')))

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={!! config('metrics.google.analytics_id') !!}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', '{!! config('metrics.google.analytics_id') !!}');
    </script>

@endif