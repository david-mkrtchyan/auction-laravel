<div class="menu">
    <div class="navbar">
        <div class="logo">
            @include('partials.logo', ['with_title' => true])
        </div>
        <div class="nav">
            <ul id="men">
                @if($pagesTop)
                    @foreach($pagesTop as $page)
                        @if($page->slug == $catalog)
                            <li class="item"><a id="myBtn" data-toggle="modal" data-target="#catalog-list" href="#">{{$page->name}}</a></li>
                        @else
                            <li class="item"><a href="{!! route('page.show',$page->slug) !!}">{{$page->name}}</a></li>
                        @endif
                    @endforeach
                @endif
                @if($_user)
                        <li class="item"><a href="{!! route('page.show',$page->slug) !!}">{{$page->name}}</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>