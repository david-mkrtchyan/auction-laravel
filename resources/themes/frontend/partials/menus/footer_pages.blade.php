<div class="footer-menu">
    <ul>
        <li>
            <a href="{!! route('support_page.show',['manuals']) !!}"
               title="@lang('front_labels.support')">
                @lang('front_labels.support')
            </a>
        </li>
        <li>
            @php($file = variable('terms_and_conditions_file'))
            <a target="_blank" href="{!! url($file ? $file : '/uploads/files/AI_(Global)_Trade_Terms_and_Conditions.pdf') !!}"
               title="@lang('front_labels.terms of use')">
                @lang('front_labels.terms of use')
            </a>
        </li>
    </ul>
</div>