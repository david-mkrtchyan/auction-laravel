<div class="footer-social-pages-menu">
    <ul>
        @php($link = variable('social_page_youtube'))
        @if ($link)
            <li>
                <a target="_blank" href="{{ $link }}"
                   title="@lang('front_labels.social page youtube')">
                    <img src="{!! theme_asset('images/youtube.png') !!}"
                         alt="@lang('front_labels.social page youtube')">
                </a>
            </li>
        @endif

        @php($link = variable('social_page_instagram'))
        @if ($link)
            <li>
                <a target="_blank" href="{{ $link }}"
                   title="@lang('front_labels.social page instagram')">
                    <img src="{!! theme_asset('images/instagram.png') !!}"
                         alt="@lang('front_labels.social page instagram')">
                </a>
            </li>
        @endif

        @php($link = variable('social_page_facebook'))
        @if ($link)
            <li>
                <a target="_blank" href="{{ $link }}"
                   title="@lang('front_labels.social page facebook')">
                    <img src="{!! theme_asset('images/facebook.png') !!}"
                         alt="@lang('front_labels.social page facebook')">
                </a>
            </li>
        @endif

        @php($link = variable('social_page_twitter'))
        @if ($link)
            <li>
                <a target="_blank" href="{{ $link }}"
                   title="@lang('front_labels.social page twitter')">
                    <img src="{!! theme_asset('images/twitter.png') !!}"
                         alt="@lang('front_labels.social page twitter')">
                </a>
            </li>
        @endif
    </ul>
</div>