<ul class="menu">
    <li class="menu__item"> <a href="#hero">
            @lang('front_landing.platform')
        </a> </li>
    <li class="menu__item"> <a href="#about">
            @lang('front_landing.what we do')
        </a> </li>
    <li class="menu__item"> <a href="#products">
            @lang('front_landing.products')
        </a> </li>
    <li class="menu__item"> <a href="#team">
            @lang('front_landing.command')
        </a> </li>

    @if ($_user && $_user->hasAccess('superuser'))        
        <li class="menu__item"> <a href="#calculator">
                @lang('front_landing.calculator')
            </a> </li>
    @endif
</ul>