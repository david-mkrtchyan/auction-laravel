
@if($pagesBottom)
    <div class="mein">
        <div class="bot">
            @foreach($pagesBottom as $page)
                    <a class="line" href="{!! route('page.show',$page->slug) !!}">{{$page->name}}</a>
            @endforeach
        </div>
    </div>
@endif
