<ul class="lang-list">
    @foreach($locales as $_locale)
        @unless($_locale == $locale)
            <li class="lang-list__item">
                <a href="{!! localize_url(null, $_locale) !!}">
                    <figure class="flag-icon flag-icon-{!! $_locale !!}"></figure>
                </a>
            </li>
        @endif
    @endforeach
</ul>