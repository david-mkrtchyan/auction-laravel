<!--Meta Fix IE-->
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

{!! Meta::render() !!}

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="/favicon.png" type="image/x-icon">

<link rel="stylesheet" href="{!! theme_asset('css/bootstrap.css') !!}">
<link rel="stylesheet" href="{!! theme_asset('fonts/stylesheet.css') !!}">
<link rel="stylesheet" href="{!! theme_asset('css/flag-icon.css') !!}">

<link rel="stylesheet" href="{!! asset('assets/components/alertifyjs/dist/css/alertify.css') !!}" id="alertifyCSS">
<link rel="stylesheet" href="{!! asset('assets/components/csspin/css/csspin-skeleton.css') !!}">
<link rel="stylesheet" href="{!! asset('assets/components/izimodal/css/iziModal.min.css') !!}">

<script type="text/javascript" src="{!! theme_asset('js/jquery-migrate-1.4.1.min.js') !!}"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

@stack('assets.css.top')

<link rel="stylesheet" href="{!! theme_asset('css/style.css') !!}">

<link rel="stylesheet" href="{!! theme_asset('css/media.css') !!}">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,latin-ext"
      rel="stylesheet">

<link rel="stylesheet" href="{!! theme_asset('css/app.css') !!}">

@stack('landing.css')

@include('partials.vars')

<script type="text/javascript" src="{!! theme_asset('js/jquery-3.2.1.min.js') !!}"></script>
<script type="text/javascript" src="{!! theme_asset('js/jquery-migrate-1.4.1.min.js') !!}"></script>

<script type="text/javascript" src="{!! asset('assets/components/izimodal/js/iziModal.min.js') !!}"></script>

@stack('countDown')