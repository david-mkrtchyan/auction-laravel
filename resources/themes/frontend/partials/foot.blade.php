
<script type="text/javascript" src="{!! theme_asset('js/popper.js') !!}"></script>
<script type="text/javascript" src="{!! theme_asset('js/jquery.validate.min.js') !!}"></script>
<script type="text/javascript" src="{!! theme_asset('js/tooltip.js') !!}"></script>
<script type="text/javascript" src="{!! theme_asset('js/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! theme_asset('js/jquery.maskedinput.js') !!}"></script>
<script type="text/javascript" src="{!! theme_asset('js/selectbox.js') !!}"></script>

<script type="text/javascript" src="{!! asset('assets/components/alertifyjs/dist/js/alertify.js') !!}"></script>
<script type="text/javascript" src="{!! asset('assets/components/clipboard/dist/clipboard.min.js') !!}"></script>

@stack('assets.bottom')

<script type="text/javascript" src="{!! theme_asset('js/js.js') !!}"></script>
<script type="text/javascript" src="{!! theme_asset('js/app.js') !!}"></script>

@stack('vue')
@stack('vue-match-heights')
@stack('phone')
@stack('default')
@stack('lightgallery')