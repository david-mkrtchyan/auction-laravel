@extends('layouts.app')

@section('main')

    <div class="main-page__center">
        <h1>{!! $model->name !!}</h1>

        {!! $model->content !!}
    </div>

@endsection

