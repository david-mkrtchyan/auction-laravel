@extends('layouts.profile')

@section('_sidebar')

    @include('support_page.partials.menu')

@endsection

@section('_content')

    <div class="content-block__header">
        <h2>{!! $model->name !!}</h2>
    </div>

    <div class="faq-page">
        <div class="menu">
            <ul class="questions-departments">
                <li class="question-category active" data-category="all">
                    @lang('front_labels.all')
                </li>
                @foreach($departments as $department)
                    <li class="question-category" data-category="{!! $department->id !!}">
                        {!! $department->name !!}
                    </li>
                @endforeach
            </ul>
        </div>

        <ul class="questions-list">
            @foreach($questions as $question)
                <li class="question-item category-{!! $question->department_id !!}">
                    <a data-question="{!! $question->id !!}" class="question">
                        {!! $question->question !!}
                    </a>
                    <div id="answer-{!! $question->id !!}" class="answer">
                        {!! $question->answer !!}
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

@endsection