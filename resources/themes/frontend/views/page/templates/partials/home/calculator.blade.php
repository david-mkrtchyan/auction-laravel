<section class="landing-calculator" id="calculator">
    <div class="container calculator__inner">
        <div class="row">
            <div class="calculator__coll calculator__left col-5">
                <div class="calculator-text-block">
                    <p class="section-title">@lang('front_landing.calculator title')</p>

                    <p class="font-weight-bold calculator-text">@lang('front_landing.calculator text 1 :site_name', ['site_name' => config('app.name')])</p>

                    <p class="calculator-text calculator-text-2">@lang('front_landing.calculator text 2')</p>
                </div>
            </div>
            <div class="calculator__coll calculator__right col-7">
                <div class="calculator-block" id="calculator-block">
                    <div class="row">
                        <div class="col-5 calculator-block-coll">
                            <div class="slider-block">
                                <p class="section-title">
                                    @lang('front_landing.agreement amount')
                                </p>
                                <div id="text_amount" class="slider-value">1</div>
                                <div class="slider">
                                    <input id="agreement_amount" min="{!! config('contracts.min_amount') !!}" max="5" step="0.0001" value="1" type="range">
                                </div>
                            </div>
                        </div>

                        <div class="col-7 calculator-block-coll">
                            <div class="row calculator-block-row calculator-block-normal">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6 label-text">@lang('front_landing.profit per day'):</div>
                                        <div class="col-6 font-weight-bold value-text" id="profit_per_day_normal">
                                            <span class="value-text-value"></span> <span class="currency-title">BTC</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 label-text">@lang('front_landing.profit per year'):</div>
                                        <div class="col-6 font-weight-bold value-text" id="profit_per_year">
                                            <span class="value-text-value"></span> <span class="currency-title">BTC</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 label-text font-weight-bold">@lang('front_landing.clear profit'):</div>
                                        <div class="col-6 font-weight-bold value-text value-text-percentage" id="profit_clear_normal">
                                            <span class="value-text-value"></span> %
                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-text">
                                    @lang('front_landing.not reinvest')
                                </div>
                            </div>

                            <div class="row calculator-block-row calculator-block-reinvest">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6 label-text">@lang('front_landing.profit per day'):</div>
                                        <div class="col-6 font-weight-bold value-text" id="profit_per_day_reinvest">
                                            <span class="value-text-value"></span> <span class="currency-title">BTC</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 label-text">@lang('front_landing.profit and reinvest'):</div>
                                        <div class="col-6 font-weight-bold value-text" id="profit_reinvest">
                                            <span class="value-text-value"></span> <span class="currency-title">BTC</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 label-text font-weight-bold">@lang('front_landing.clear profit'):</div>
                                        <div class="col-6 font-weight-bold value-text value-text-percentage" id="profit_clear_reinvest">
                                            <span class="value-text-value"></span> %
                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-text">
                                    @lang('front_landing.reinvest')
                                </div>
                            </div>

                            <div class="row calculator-block-row calculator-block-difference">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6 label-text label-text-difference">@lang('front_landing.difference'):</div>
                                        <div class="col-6 value-text" id="profit_difference">
                                            <span class="value-text-value"></span> %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>