@extends('layouts.app')

@section('main')
    @if($product)
        @push('countDown')
            <script type="text/javascript" src="{!! theme_asset('js/countDown.js') !!}"></script>
        @endpush

        <div class="product-view">
            <div class="product-title">
                <h3 class="text-center">{!! $product->title !!}</h3>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="group-catalog-image2">
                        @if($images = $product->images)
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @if($imageCount = count($images))
                                        @for($i=0;$i < $imageCount;$i++)
                                            <li data-target="#carouselExampleIndicators" data-slide-to="{!! $i !!}"
                                                class="@if($i == 0) active @endif"></li>
                                        @endfor
                                    @endif
                                </ol>

                                <ul id="lightgallery" class="list-unstyled row carousel-inner">
                                    @foreach($images as $key => $image)
                                        @if($image = $product->checkImageExists($image->name))
                                            <li class="carousel-item @if($key == 0) active @endif"
                                                data-src="{!! $image !!}"
                                                data-sub-html="{!! $key+1 !!}">
                                                <a href="">
                                                    <i class="icon-zoom-in"></i> <img class="img-responsive"
                                                                                      src="{!! $image !!}">
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>

                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-description">
                        {!! $product->description !!}
                    </div>

                    @php($group = $product->group)

                    @if($group->checkTime())
                        <div class="offered">@lang('front_messages.offered'): € {!! $product->price !!}</div>

                        @component('partials.components.timer',['id' => $product->id])
                        @endcomponent

                        @push('countDown')
                            <script>
                                countDown("{!! $product->id !!}", "{!! $group->setFormatDate($group->end_time, "M d, Y H:i:s") !!}")
                            </script>
                        @endpush
                    @endif


                    <div class="bet-now text-center">
                        @if($_user)
                            <a href="" class="btn btn-danger make-bet"><i
                                        class="fa fa-plus"></i> @lang('front_messages.make bet now')</a>
                        @else
                            <a href="#" data-toggle="modal" data-target="#modal2" id="openEnter" class="btn btn-danger make-bet">
                                <i class="fa fa-plus"></i>@lang('front_messages.make bet now') </a>
                        @endif
                    </div>
                </div>

            </div>

        </div>

        @push('lightgallery')
            <link type="text/css" rel="stylesheet"
                  href="{!! asset('assets/components/lightgallery/dist/css/lightgallery.css') !!}"/>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
            <script src="{!! asset('assets/components/lightgallery/dist/js/lightgallery-all.min.js') !!}"></script>
            <script src="{!! asset('assets/components/lightgallery/modules/lg-thumbnail.min.js') !!}"></script>
            <script src="{!! asset('assets/components/lightgallery/modules/lg-fullscreen.min.js') !!}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#lightgallery").lightGallery();
                });
            </script>
        @endpush
    @endif
@endsection



<link type="text/css" rel="stylesheet" href="{!! asset('assets/lightGallery/lightgallery.css') !!}"/>
<style type="text/css">
    body {
        background-color: #152836
    }

    .demo-gallery > ul {
        margin-bottom: 0;
    }

    .demo-gallery > ul > li {
        float: left;
        margin-bottom: 15px;
        margin-right: 20px;
        width: 200px;
    }

    .demo-gallery > ul > li a {
        border: 3px solid #FFF;
        border-radius: 3px;
        display: block;
        overflow: hidden;
        position: relative;
        float: left;
    }

    .demo-gallery > ul > li a > img {
        -webkit-transition: -webkit-transform 0.15s ease 0s;
        -moz-transition: -moz-transform 0.15s ease 0s;
        -o-transition: -o-transform 0.15s ease 0s;
        transition: transform 0.15s ease 0s;
        -webkit-transform: scale3d(1, 1, 1);
        transform: scale3d(1, 1, 1);
        height: 100%;
        width: 100%;
    }

    .demo-gallery > ul > li a:hover > img {
        -webkit-transform: scale3d(1.1, 1.1, 1.1);
        transform: scale3d(1.1, 1.1, 1.1);
    }

    .demo-gallery > ul > li a:hover .demo-gallery-poster > img {
        opacity: 1;
    }

    .demo-gallery > ul > li a .demo-gallery-poster {
        background-color: rgba(0, 0, 0, 0.1);
        bottom: 0;
        left: 0;
        position: absolute;
        right: 0;
        top: 0;
        -webkit-transition: background-color 0.15s ease 0s;
        -o-transition: background-color 0.15s ease 0s;
        transition: background-color 0.15s ease 0s;
    }

    .demo-gallery > ul > li a .demo-gallery-poster > img {
        left: 50%;
        margin-left: -10px;
        margin-top: -10px;
        opacity: 0;
        position: absolute;
        top: 50%;
        -webkit-transition: opacity 0.3s ease 0s;
        -o-transition: opacity 0.3s ease 0s;
        transition: opacity 0.3s ease 0s;
    }

    .demo-gallery > ul > li a:hover .demo-gallery-poster {
        background-color: rgba(0, 0, 0, 0.5);
    }

    .demo-gallery .justified-gallery > a > img {
        -webkit-transition: -webkit-transform 0.15s ease 0s;
        -moz-transition: -moz-transform 0.15s ease 0s;
        -o-transition: -o-transform 0.15s ease 0s;
        transition: transform 0.15s ease 0s;
        -webkit-transform: scale3d(1, 1, 1);
        transform: scale3d(1, 1, 1);
        height: 100%;
        width: 100%;
    }

    .demo-gallery .justified-gallery > a:hover > img {
        -webkit-transform: scale3d(1.1, 1.1, 1.1);
        transform: scale3d(1.1, 1.1, 1.1);
    }

    .demo-gallery .justified-gallery > a:hover .demo-gallery-poster > img {
        opacity: 1;
    }

    .demo-gallery .justified-gallery > a .demo-gallery-poster {
        background-color: rgba(0, 0, 0, 0.1);
        bottom: 0;
        left: 0;
        position: absolute;
        right: 0;
        top: 0;
        -webkit-transition: background-color 0.15s ease 0s;
        -o-transition: background-color 0.15s ease 0s;
        transition: background-color 0.15s ease 0s;
    }

    .demo-gallery .justified-gallery > a .demo-gallery-poster > img {
        left: 50%;
        margin-left: -10px;
        margin-top: -10px;
        opacity: 0;
        position: absolute;
        top: 50%;
        -webkit-transition: opacity 0.3s ease 0s;
        -o-transition: opacity 0.3s ease 0s;
        transition: opacity 0.3s ease 0s;
    }

    .demo-gallery .justified-gallery > a:hover .demo-gallery-poster {
        background-color: rgba(0, 0, 0, 0.5);
    }

    .demo-gallery .video .demo-gallery-poster img {
        height: 48px;
        margin-left: -24px;
        margin-top: -24px;
        opacity: 0.8;
        width: 48px;
    }

    .demo-gallery.dark > ul > li a {
        border: 3px solid #04070a;
    }

    .home .demo-gallery {
        padding-bottom: 80px;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $('#lightgallery').lightGallery();
    });
</script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="{!! asset('assets/lightGallery/lightgallery-all.min.js') !!}"></script>
<script src="{!! asset('assets/lightGallery/jquery.mousewheel.min.js') !!}"></script>


