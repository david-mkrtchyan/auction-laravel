@extends('layouts.app')
@section('main')
    @if($products)

        @push('countDown')
            <script type="text/javascript" src="{!! theme_asset('js/countDown.js') !!}"></script>
        @endpush


        <div class="group-catalog">
            <h1 class="text-center">{!! $group->name !!}</h1>
            <span class="text-center">{!! $group->description !!}</span>
            <div class="cardd">
                @foreach($products as $key => $product)

                    <div class="one">

                        <div class="box4 group-catalog-image">
                            <a href="{!! route('product.show',$product->id) !!}" class="group-url">
                                @if(($productImage = $product->image) && ($image = $product->checkImageExists($productImage->name)))
                                    <img src="{!! $image !!}" alt="" class="img-product">
                                @endif

                                @if($group->checkTime())
                                        @component('partials.components.timer',['id' => $product->id])
                                        @endcomponent

                                        @push('countDown')
                                            <script>
                                                countDown("{!! $product->id !!}", "{!! $group->setFormatDate($group->end_time, "M d, Y H:i:s") !!}")
                                            </script>
                                        @endpush
                                @endif

                                <div class="infoline">
                                    <div class="icon">
                                        <i class="fa fa-bookmark theme"></i>
                                    </div>
                                    @lang('front_messages.catalog') {!! $key+1 !!}
                                </div>
                                <div class="infoline">
                                    <div class="icon">
                                        <i class="fa fa-tag theme"></i>
                                    </div>
                                    {!! $product->price !!} EUR
                                </div>
                                <div class="infoline">
                                    <div class="icon">
                                        <i class="fa fa-comment theme"></i>
                                    </div>
                                    <span>@lang('front_messages.description')</span>
                                    <div class="text">
                                        {!! Text::cutText($product->description,50) !!}
                                    </div>
                                </div>
                                <div class="make-bet">
                                    <a href="{!! route('product.show',$product->id) !!}" class="btn btn-danger">
                                        <i class="fa fa-plus"> </i>@lang('front_messages.make bet now')</a>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="group-pagination">
                {{ $products->links() }}
            </div>
        </div>
    @endif

@endsection