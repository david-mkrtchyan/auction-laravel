@extends('layouts.app')

@section('main')
    <div class="user-info">
        <h3 class="title-into">@lang('front_messages.Update Your Details') </h3>
        <div class="registr-form">
            {!! Form::open(['route' => 'user.update', 'method' => 'post', 'class' => 'register-form']) !!}
            <div class="row">
                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group forinput">
                        {{ Form::email('email', $model->email, ['placeholder' => trans('front_labels.email placeholder'),'class' => 'form-control']) }}
                    </div>
                    <div class="form-group forinput">
                        {{ Form::text('first_name',  $model->first_name, ['class' => 'form-control name', 'placeholder' => trans('front_labels.first_name'),'class' => 'form-control']) }}
                    </div>
                    <div class="form-group forinput">
                        {{ Form::password('password', ['placeholder' => trans('front_labels.password'),'class' => 'form-control']) }}
                    </div>

                    <div class="form-group forinput">
                        {{ Form::text('user_name',  $model->user_name, [  'disabled' => 'disabled','class' => 'form-control user_name', 'placeholder' => trans('front_labels.user_name'),'class' => 'form-control']) }}
                    </div>

                   {{-- @include('views.user.user_avatar')--}}
                </div>

                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group forinput">
                        {{ Form::select('country_id', $countries, null,['class' => 'form-control']) }}
                    </div>

                    <div class="form-group forinput">
                        {{ Form::text('last_name', $model->last_name, ['class' => 'form-control name', 'placeholder' => trans('front_labels.last_name'),'class' => 'form-control']) }}
                    </div>

                    <div class="form-group forinput">
                        {{ Form::password('password_confirmation', ['placeholder' => trans('front_labels.password_confirmation'),'class' => 'form-control']) }}
                    </div>

                    <div class="form-group forinput">
                        {{ Form::tel('phone',  $model->phone, ['id'=>'mobile-number','class' => 'form-control phone', 'placeholder' => trans('front_labels.phone'),'class' => 'form-control']) }}
                        {{ Form::hidden('phone_validate', '', array('id' => 'validate_phone')) }}
                        {{--<span id="valid-msg" class="hide">✓ Valid</span>
                        <span id="error-msg" class="hide"></span>--}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 offset-xl-3 offset-lg-0 offset-md-0 offset-sm-0">
                    <input type="submit" name="submit" class="btn btn2 btn-block btn-primary register-form-submit"
                           value="@lang('front_labels.update')"/>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@push('phone')
    @component('partials.components.intlTelInput',['id' => 'mobile-number']) @endcomponent
@endpush
