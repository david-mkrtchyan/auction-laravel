<?php

use App\Entities\Variable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateVariablesTableAddIsHidden extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'variables',
            function (Blueprint $table) {
                $table->boolean('is_hidden')->default(false)->after('type');
            }
        );
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws \Exception
     */
    public function down()
    {
        Variable::where('is_hidden', true)->delete();
        
        Schema::table(
            'variables',
            function (Blueprint $table) {
                $table->dropColumn('is_hidden');
            }
        );
    }
}
