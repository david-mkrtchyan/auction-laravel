<?php

use Illuminate\Database\Migrations\Migration;

class PopulateUsersTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * @var \App\Entities\User $user
         */
        $user = Sentinel::registerAndActivate(
            [
                'email'       => 'admin@admin.com',
                'password'    => 'admin',
                'permissions' => ['superuser' => true],
                'first_name'  => 'Admin',
                'last_name'   => 'Administrator',
                'user_name'   => 'admin',
                'phone'       => '+37477757629',
            ]
        );

        $role = Sentinel::findRoleBySlug('administrators');

        $role->users()->attach($user);
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}