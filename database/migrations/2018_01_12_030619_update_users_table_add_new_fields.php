<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTableAddNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->string('phone')->nullable()->after('email');
                $table->string('user_name')->nullable()->index()->after('email');

                $table->string('avatar')->nullable()->after('last_name');
                //->index()
                $table->string('two_factor_secret', 255)->nullable()->after('password');

                $table->boolean('verified')->default(false)->index()->after('permissions');
            }
        );

        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->tinyInteger('gender')->default(0)->after('avatar');
            }
        );

        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->integer('birth_year')->nullable()->after('gender');
                $table->tinyInteger('birth_month')->nullable()->after('gender');
                $table->tinyInteger('birth_day')->nullable()->after('gender');
            }
        );

        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->string('postal_code')->nullable()->after('birth_year');
                $table->integer('country_id')->nullable()->unsigned()->index()->after('birth_year');
                $table->string('city')->nullable()->after('birth_year');
                $table->string('address')->nullable()->after('birth_year');

                $table->foreign('country_id')->references('id')->on('countries')
                    ->onUpdate('cascade')->onDelete('set null');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropColumn('phone');
                $table->dropColumn('avatar');
                $table->dropColumn('gender');
                $table->dropColumn('birth_year');
                $table->dropColumn('birth_month');
                $table->dropColumn('birth_day');
                $table->dropColumn('postal_code');
                $table->dropColumn('city');
                $table->dropColumn('address');

               //$table->dropIndex('users_two_factor_secret_index');
                $table->dropColumn('two_factor_secret');

                $table->dropIndex('users_user_name_index');
                $table->dropColumn('user_name');

                $table->dropForeign('users_country_id_foreign');
                $table->dropIndex('users_country_id_index');
                $table->dropColumn('country_id');
            }
        );
    }
}
