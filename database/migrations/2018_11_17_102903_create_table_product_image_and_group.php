<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductImageAndGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'groups',
            function (Blueprint $table) {
                $table->increments('id');
                $table->dateTime('start_time');
                $table->dateTime('end_time');
                $table->boolean('status')->default(false);
                $table->timestamps();
            }
        );

        Schema::create(
            'group_translations',
            function (Blueprint $table) {
                $table->increments('id');

                $table->string('name', 255);

                $table->integer('group_id')->unsigned();

                $table->text('description')->nullable();

                $table->string('locale')->index();

                $table->unique(['group_id', 'locale']);

                $table->foreign('group_id')->references('id')
                    ->on('groups')->onDelete('cascade')->onUpdate('cascade');
            }
        );



        Schema::create(
            'products',
            function (Blueprint $table) {
                $table->increments('id');

                $table->integer('position')->unsigned()->default(0);

                $table->integer('view_count')->default(0);

                $table->boolean('status')->default(true);

                $table->integer('group_id')->unsigned();

                $table->timestamps();

                $table->foreign('group_id')->references('id')
                    ->on('groups')->onDelete('cascade')->onUpdate('cascade');

            }
        );

        Schema::create(
            'product_translations',
            function (Blueprint $table) {

                $table->increments('id');

                $table->integer('product_id')->unsigned();

                $table->string('title', 255);

                $table->text('description');

                $table->string('locale')->index();

                $table->unique(['product_id', 'locale']);

                $table->foreign('product_id')->references('id')
                    ->on('products')->onDelete('cascade')->onUpdate('cascade');
            }
        );

        Schema::create(
            'images',
            function (Blueprint $table) {

                $table->increments('id');

                $table->string('name');

                $table->integer('object_id');

                $table->enum('type', ['group', 'product']);

                $table->timestamps();
            }
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_translations');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('product_translations');
        Schema::dropIfExists('products');
        Schema::dropIfExists('images');
    }
}
