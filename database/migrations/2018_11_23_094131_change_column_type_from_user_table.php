<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypeFromUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('user_name')->nullable(false)->change();
            $table->string('phone')->nullable(false)->change();
            $table->unique('user_name');
            $table->unique('phone');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('user_name')->nullable(true)->change();
            $table->string('phone')->nullable(true)->change();
            $table->dropUnique('user_name');
            $table->dropUnique('phone');
        });
    }
}
