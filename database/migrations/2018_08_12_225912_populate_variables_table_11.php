<?php

use App\Entities\Variable;
use Illuminate\Database\Migrations\Migration;

class PopulateVariablesTable11 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $variables = [

        ];
        
        foreach ($variables as $variable) {
            if (!Variable::where('key', $variable['key'])->first()) {
                $model = new Variable();
                
                $model->fill($variable);
                $model->type = app(Variable::class)->getTypeIdByKey($variable['type']);
                
                $model->save();
            }
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
