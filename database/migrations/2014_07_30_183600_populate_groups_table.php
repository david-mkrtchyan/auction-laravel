<?php

use Illuminate\Database\Migrations\Migration;

class PopulateGroupsTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Sentinel::getRoleRepository()->createModel()->create(
            [
                'name'        => 'Администраторы',
                'slug'        => 'administrators',
                'permissions' => ['administrator' => true],
            ]
        );
        
        Sentinel::getRoleRepository()->createModel()->create(
            [
                'name'        => 'Клиенты',
                'slug'        => 'clients',
                'permissions' => [],
            ]
        );
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}