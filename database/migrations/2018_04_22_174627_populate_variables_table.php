<?php

use App\Entities\Variable;
use Illuminate\Database\Migrations\Migration;

class PopulateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $variables = [
            [
                'key'         => 'support_link_telegram',
                'type'        => 'text',
                'name'        => 'Службы поддержки в Telegram',
                'description' => 'Ссылка на службу поддержки в Telegram',
                'status'      => true,
                'value'       => '',
            ],
            [
                'key'         => 'support_link_messenger',
                'type'        => 'text',
                'name'        => 'Службы поддержки в Messenger',
                'description' => 'Ссылка на службу поддержки в Messenger',
                'status'      => true,
                'value'       => '',
            ],
            [
                'key'         => 'support_link_viber',
                'type'        => 'text',
                'name'        => 'Службы поддержки в Viber',
                'description' => 'Ссылка на службу поддержки в Viber',
                'status'      => true,
                'value'       => '',
            ],
            [
                'key'          => 'admin_emails',
                'type'         => 'text',
                'name'         => 'Верификационные E-mail адреса',
                'description'  => 'E-mail адреса, на которые будут приходить уведомления об изменении верификационных данных пользователей (если несколько, то через кому)',
                'status'       => true,
                'multilingual' => false,
                'value'        => '',
            ],
            [
                'key'          => 'telegram_channel_link',
                'type'         => 'text',
                'name'         => 'Ссылка на телеграм канал',
                'description'  => null,
                'status'       => true,
                'multilingual' => false,
                'value'        => '',
            ],
            [
                'key'          => 'social_page_youtube',
                'type'         => 'text',
                'name'         => 'Ссылка на страницу в Youtube',
                'description'  => null,
                'status'       => true,
                'multilingual' => false,
            ],
            [
                'key'          => 'social_page_instagram',
                'type'         => 'text',
                'name'         => 'Ссылка на страницу в Instagram',
                'description'  => null,
                'status'       => true,
                'multilingual' => false,
            ],
            [
                'key'          => 'social_page_facebook',
                'type'         => 'text',
                'name'         => 'Ссылка на страницу в Facebook',
                'description'  => null,
                'status'       => true,
                'multilingual' => false,
            ],
            [
                'key'          => 'social_page_twitter',
                'type'         => 'text',
                'name'         => 'Ссылка на страницу в Twitter',
                'description'  => null,
                'status'       => true,
                'multilingual' => false,
            ],
            [
                'key'          => 'languages',
                'type'         => 'multi_select',
                'name'         => 'Языки для отображения на сайте',
                'description'  => 'Языки, которые будут доступны для отображения на сайте',
                'status'       => true,
                'multilingual' => false,
                'value'        => '["en","ru","lt"]',
            ],
            [
                'key'          => 'terms_and_conditions_file',
                'type'         => 'file',
                'name'         => 'Условия и положения',
                'description'  => 'Файл условий и положений',
                'status'       => true,
                'multilingual' => true,
                'value'        => null,
            ],
        ];
        
        foreach ($variables as $variable) {
            if (!Variable::where('key', $variable['key'])->first()) {
                $model = new Variable($variable);
                $model->type = app(Variable::class)->getTypeIdByKey($variable['type']);
                
                $model->save();
            }
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
