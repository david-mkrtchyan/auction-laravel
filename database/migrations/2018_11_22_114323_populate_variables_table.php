<?php

use Illuminate\Database\Migrations\Migration;
use App\Entities\Variable;

class PopulateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $variable = [
            'key' => 'price_for_activate_account',
            'type' => 'number',
            'name' => 'Цена активации счета',
            'description' => 'Цена активации счета',
            'status' => true,
            'value' => 1000,
        ];

        if (!Variable::where('key', $variable['key'])->first()) {
            $model = new Variable($variable);
            $model->type = app(Variable::class)->getTypeIdByKey($variable['type']);

            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
