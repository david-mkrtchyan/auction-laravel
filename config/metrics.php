<?php

return [
    
    'facebook' => [
        'pixel_code' => env('METRICS_FACEBOOK_PIXEL_CODE', ''),
    ],
    
    'google' => [
        'analytics_id' => env('GOOGLE_ANALYTICS_ID', ''),
    ],
    
    'chimpstatic' => [
        'script' => env('CHIMPSTATIC_SCRIPT', ''),
    ],

];