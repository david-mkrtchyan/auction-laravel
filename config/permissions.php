<?php

return array (

    /*
    |--------------------------------------------------------------------------
    | superuser
    |--------------------------------------------------------------------------
    |
    | Any user with superuser permissions automatically has access to everything,
    | regardless of the user permissions and role permissions.
    |
    */

    'superuser',

    /*
    |--------------------------------------------------------------------------
    | administrator
    |--------------------------------------------------------------------------
    |
    | Разрешает доступ в админку
    |
    */

    'administrator',

    /*
    |--------------------------------------------------------------------------
    | special
    |------------------------------------------------------------------------
    */
    
    'verified',
    
    /*
    |--------------------------------------------------------------------------
    | menu
    |------------------------------------------------------------------------
    */
    
    'user.create',
    'user.read',
    'user.write',
    'user.approve',
    
    'userfile.read',
    'userfile.write',
    
    'user.role.write',
    
    'wallettransaction.read',
    
    'contract.read',
    'contract.transaction.read',
    
    'deposit.read',
    
    'spend.create',
    'spend.read',
    'spend.write',
    
    'role.create',
    'role.read',
    'role.write',
    'role.delete',
    
    'page.create',
    'page.read',
    'page.write',
    'page.delete',
    
    'supportpage.read',
    'supportpage.write',
    
    'question.create',
    'question.read',
    'question.write',
    'question.delete',
    
    'variable.value.read',
    'variable.value.write',
    'variable.read',
    'variable.create',
    'variable.create',
    'variable.read',
    'variable.read',
    'variable.write',
    'variable.delete',
    
    'translation.read',
    'translation.write',
    
    'dashboard.general',
    'dashboard.contracts',
    'dashboard.report',
    'dashboard.withdraw',
    'dashboard.transfer',
);