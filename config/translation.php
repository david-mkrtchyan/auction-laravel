<?php

return [
    
    'visible_groups' => [
        'front_landing',
        'front_labels',
        'front_tooltips',
        'front_messages',
        'front_texts',
        'front_subjects',
        'front_emails',
    ],
    
    'per_page' => 50,

];