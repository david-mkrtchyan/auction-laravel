<?php

Route::group(
    [
        'prefix' => '9a654138bc7c1c48fba1d0b5ca526a28',
        'as'     => 'admin.',
    ],
    function () {

        Route::group(
            [
                'middleware' => 'admin.auth',
            ],
            function () {
                Route::any('/', ['as' => 'home', 'uses' => 'BackendController@getIndex']);

                // users
                Route::get(
                    'user/find',
                    [
                        'middleware' => 'ajax',
                        'as'         => 'user.find',
                        'uses'       => 'UserController@find',
                    ]
                );
                Route::post(
                    'user/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'user.ajax_field',
                        'uses'       => 'UserController@localAjaxFieldChange',
                    ]
                );
                Route::get(
                    'user/new_password/{id}',
                    ['as' => 'user.new_password.get', 'uses' => 'UserController@getNewPassword']
                );
                Route::post(
                    'user/new_password/{id}',
                    ['as' => 'user.new_password.post', 'uses' => 'UserController@postNewPassword']
                );
                Route::get(
                    'user/{id}/two-factor-authentication/reset',
                    [
                        'as'   => 'user.two_factor_authentication.reset',
                        'uses' => 'UserController@twoFactorAuthenticationReset',
                    ]
                );
                Route::get(
                    'user/unverified',
                    ['as' => 'user.unverified', 'uses' => 'UserController@indexUnverified']
                );
                Route::resource('user', 'UserController', ['except' => 'delete']);

                //user_file
                Route::get(
                    'user/{user_id}/file/{file_id}/show',
                    [
                        'as'   => 'user.file.show',
                        'uses' => 'UserFileController@show',
                    ]
                );
                Route::post(
                    'user/{user_id}/file/{file_id}/approve',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'user.file.approve',
                        'uses'       => 'UserFileController@approve',
                    ]
                );
                Route::post(
                    'user/{user_id}/file/{file_id}/decline',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'user.file.decline',
                        'uses'       => 'UserFileController@decline',
                    ]
                );

                // user wallet transactions
                Route::get(
                    'user/{user_id}/wallet/{wallet_id}/transaction',
                    ['as' => 'user.wallet.transaction.index', 'uses' => 'WalletTransactionController@index']
                );

                // levels
                Route::resource('level', 'LevelController', ['except' => ['create', 'store', 'destroy']]);

                // contracts
                Route::get(
                    'contract',
                    ['as' => 'contract.index', 'uses' => 'ContractController@index']
                );
                Route::get(
                    'user/{user_id}/contract',
                    ['as' => 'contract.index.user', 'uses' => 'ContractController@indexUser']
                );
                Route::get(
                    'contract/{contract_id}',
                    ['as' => 'contract.show', 'uses' => 'ContractController@show']
                );

                // deposit
                Route::get(
                    'deposit',
                    ['as' => 'deposit.index', 'uses' => 'DepositController@index']
                );

                // spends
                Route::resource('spend', 'SpendController');

                // roles
                Route::resource('role', 'RoleController');

                // pages
                Route::post(
                    'page/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'page.ajax_field',
                        'uses'       => 'PageController@ajaxFieldChange',
                    ]
                );
                Route::resource('page', 'PageController');

                // support pages - subpage
                Route::post(
                    'supportpage/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'supportpage.ajax_field',
                        'uses'       => 'SupportPageController@ajaxFieldChange',
                    ]
                );
                Route::resource('supportpage', 'SupportPageController', ['except' => ['destroy', 'create', 'store']]);

                // departments
                Route::post(
                    'department/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'department.ajax_field',
                        'uses'       => 'DepartmentController@ajaxFieldChange',
                    ]
                );
                Route::resource('department', 'DepartmentController');

                // questions
                Route::post(
                    'question/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'question.ajax_field',
                        'uses'       => 'QuestionController@ajaxFieldChange',
                    ]
                );
                Route::resource('question', 'QuestionController');

                // variables
                Route::post(
                    'variable/{id}/ajax_field',
                    array (
                        'middleware' => ['ajax'],
                        'as'         => 'variable.ajax_field',
                        'uses'       => 'VariableController@ajaxFieldChange',
                    )
                );
                Route::get(
                    'variable/value/index',
                    ['as' => 'variable.value.index', 'uses' => 'VariableController@indexValues']
                );
                Route::post(
                    'variable/value/update',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'variable.value.update',
                        'uses'       => 'VariableController@updateValue',
                    ]
                );
                Route::resource('variable', 'VariableController');

                // translations
                Route::get(
                    'translation/{group}',
                    ['as' => 'translation.index', 'uses' => 'TranslationController@index']
                );
                Route::post(
                    'translation/{group}',
                    ['as' => 'translation.update', 'uses' => 'TranslationController@update']
                );

                //files download
                Route::get(
                    'download-file',
                    ['as' => 'download.file', 'uses' => 'BackendController@downloadFile']
                );

                // ticket type
                Route::resource('ticket_type', 'TicketTypeController', ['except' => ['destroy']]);

                // event
                Route::get(
                    'event/{id}/sold',
                    ['as' => 'event.sold_tickets', 'uses' => 'EventController@sold']
                );
                Route::resource('event', 'EventController');

                // exchanges
                Route::post(
                    'exchange/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'exchange.ajax_field',
                        'uses'       => 'ExchangeController@ajaxFieldChange',
                    ]
                );

                Route::resource('exchange', 'ExchangeController', ['except' => ['destroy']]);


                // group
                Route::post(
                    'group/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'group.ajax_field',
                        'uses'       => 'GroupController@ajaxFieldChange',
                    ]
                );

                Route::resource('group', 'GroupController');

                //Image
                Route::post(
                    'image/ajax_delete_image',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'image.ajax_delete_image',
                        'uses'       => 'ImageController@ajaxDeleteImage',
                    ]
                );

                Route::post(
                    'image/sort',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'image.sort',
                        'uses'       => 'ImageController@sort',
                    ]
                );

                // product
                Route::post(
                    'product/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'product.ajax_field',
                        'uses'       => 'ProductController@ajaxFieldChange',
                    ]
                );


                Route::resource('product', 'ProductController');

            }
        );


        Route::group(
            [
                'prefix' => 'auth',
                'as'     => 'auth.',
            ],
            function () {
                Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
                Route::post('login', ['as' => 'login.post', 'uses' => 'AuthController@postLogin']);
                Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
            }
        );
    }
);
