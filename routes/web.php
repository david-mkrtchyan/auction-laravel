<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale(),
        'middleware' => ['localizationRedirect'],
    ],
    function () {
        Route::get('/', ['as' => 'home', 'uses' => 'PageController@home']);

        Route::get('pages/{slug}', ['as' => 'page.show', 'uses' => 'PageController@show'])
            ->where(['slug' => '[0-9a-zA-Z\-]+']);

        /** Group controller*/
        Route::get(
            'group/{id}',
            [
                'as'         => 'group.show',
                'uses'       => 'GroupController@show',
            ]
        )->where('id', '[0-9]+');

        /** Product controller*/
        Route::get(
            'product/{id}',
            [
                'as'         => 'product.show',
                'uses'       => 'ProductController@show',
            ]
        )->where('id', '[0-9]+');


        Route::group(
            [
                'prefix' => 'auth',
            ],
            function () {
                Route::group(
                    [
                        'middleware' => 'guest',
                    ],
                    function () {
                        Route::post(
                            'register',
                            ['as' => 'user.store', 'middleware' => 'ajax', 'uses' => 'UserController@store']
                        );

                        Route::get(
                            'activate/{email}/{code}',
                            ['as' => 'user.activate', 'uses' => 'UserController@activate']
                        );

                        Route::post(
                            'restore/email',
                            ['as' => 'reminder.store.email', 'uses' => 'EmailReminderController@store']
                        );
                        Route::get(
                            'reset/email/{email}/{code}/check',
                            ['as' => 'reminder.check.email', 'uses' => 'EmailReminderController@check']
                        );
                        Route::get(
                            'reset/email/{email}/{code}',
                            [
                                'as'   => 'reminder.show.email',
                                'uses' => 'EmailReminderController@show',
                            ]
                        );
                        Route::post(
                            'reset/email',
                            [
                                'as'   => 'reminder.complete.email',
                                'uses' => 'EmailReminderController@complete',
                            ]
                        );

                        Route::group(
                            [
                                'prefix' => 'log-in',
                            ],
                            function () {
                                Route::post(
                                    '/',
                                    [
                                        'as'         => 'session.store',
                                        'middleware' => 'ajax',
                                        'uses'       => 'SessionController@store',
                                    ]
                                );
                            }
                        );

                        Route::group(
                            [
                                'prefix'     => 'two-factor',
                                'middleware' => 'session_allowed_access:session_check_two_factor',
                            ],
                            function () {
                                Route::post(
                                    '/',
                                    ['as' => 'session.two_factor.check', 'uses' => 'SessionController@twoFactorCheck']
                                );
                            }
                        );
                    }
                );

                Route::group(
                    [
                        'middleware' => 'auth',
                    ],
                    function () {
                        Route::get('log-out', ['as' => 'session.delete', 'uses' => 'SessionController@delete']);

                        Route::get(
                            'admin-login/{user_id}',
                            [
                                'as'         => 'session.admin_login',
                                'middleware' => 'admin.auth',
                                'uses'       => 'SessionController@adminLogin',
                            ]
                        )->where('user_id', '[0-9]+');
                    }
                );

                Route::group(
                    [
                        'middleware' => 'auth',
                    ],
                    function () {
                        Route::get('edit', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
                        Route::post('update', ['as' => 'user.update', 'uses' => 'UserController@update']);
                    }
                );
            }
        );



        Route::group(
            [
                'middleware' => 'auth',
            ],
            function () {
                Route::group(
                    [
                        'prefix' => 'dashboard',
                    ],
                    function () {
                        Route::get(
                            '/',
                            [
                                'as'   => 'dashboard.index',
                                'uses' => 'DashboardController@index',
                            ]
                        );

                        Route::get(
                            'updates',
                            [
                                'as'         => 'dashboard.updates',
                                'middleware' => 'ajax',
                                'uses'       => 'DashboardController@updates',
                            ]
                        );
                    }
                );

                Route::group(
                    [
                        'prefix' => 'profile',
                    ],
                    function () {
                        Route::group(
                            [
                                'prefix' => 'verification',
                            ],
                            function () {
                                Route::get('/', ['as' => 'verification.edit', 'uses' => 'VerificationController@edit']);
                                Route::post(
                                    '/',
                                    [
                                        'as'         => 'verification.update',
                                        'middleware' => 'ajax',
                                        'uses'       => 'VerificationController@update',
                                    ]
                                );
                            }
                        );

                        Route::group(
                            [
                                'prefix'     => 'files',
                                'middleware' => 'ajax',
                            ],
                            function () {
                                Route::post('/', ['as' => 'file.store', 'uses' => 'FileController@store']);
                                Route::post('/{id}', ['as' => 'file.update', 'uses' => 'FileController@update'])
                                    ->where(['id' => '[0-9]+']);
                            }
                        );

                        Route::group(
                            [
                                'prefix' => 'personal-data',
                            ],
                            function () {
                                Route::get(
                                    '/',
                                    ['as' => 'personal_data.edit', 'uses' => 'PersonalDataController@edit']
                                );
                                Route::post(
                                    '/',
                                    [
                                        'as'         => 'personal_data.update',
                                        'middleware' => 'prepare.phone',
                                        'uses'       => 'PersonalDataController@update',
                                    ]
                                );
                            }
                        );


                        Route::group(
                            [
                                'prefix' => 'password',
                            ],
                            function () {
                                Route::get('/', ['as' => 'password.edit', 'uses' => 'PasswordController@edit']);
                                Route::post('/', ['as' => 'password.update', 'uses' => 'PasswordController@update']);
                            }
                        );

                        Route::group(
                            [
                                'prefix' => 'two-factor-authentication',
                            ],
                            function () {
                                Route::get(
                                    '/show',
                                    [
                                        'as'   => 'two_factor_authentication.show',
                                        'uses' => 'TwoFactorAuthenticationController@show',
                                    ]
                                );
                                Route::get(
                                    '/create',
                                    [
                                        'as'   => 'two_factor_authentication.create',
                                        'uses' => 'TwoFactorAuthenticationController@create',
                                    ]
                                );
                                Route::post(
                                    '/store',
                                    [
                                        'as'   => 'two_factor_authentication.store',
                                        'uses' => 'TwoFactorAuthenticationController@store',
                                    ]
                                );
                                Route::get(
                                    '/edit',
                                    [
                                        'as'   => 'two_factor_authentication.edit',
                                        'uses' => 'TwoFactorAuthenticationController@edit',
                                    ]
                                );
                                Route::delete(
                                    '/destroy',
                                    [
                                        'as'   => 'two_factor_authentication.destroy',
                                        'uses' => 'TwoFactorAuthenticationController@destroy',
                                    ]
                                );
                            }
                        );

                    }
                );

                Route::get('qr-code/{entity}/{value}', ['as' => 'qr_code.show', 'uses' => 'QrCodeController@show'])
                    ->where(['entity' => 'deposit_address|ticket']);


            }
        );
    }
);
