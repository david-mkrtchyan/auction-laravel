<?php

Route::group(
    [
        'prefix'     => 'admin',
        'middleware' => 'admin.auth',
    ],
    function () {
        Route::get(
            'elfinder',
            array ('as' => 'admin.elfinder', 'uses' => '\Barryvdh\Elfinder\ElfinderController@showIndex')
        );
        Route::any(
            'elfinder/connector',
            array (
                'as'   => 'admin.elfinder.connector',
                'uses' => '\Barryvdh\Elfinder\ElfinderController@showConnector',
            )
        );
        Route::any(
            'elfinder/ckeditor4',
            array (
                'as'   => 'admin.elfinder.ckeditor4',
                'uses' => '\Barryvdh\Elfinder\ElfinderController@showCKeditor4',
            )
        );
    }
);