<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php if($user->hasAccess(['roles', 'user.read'])): ?>
                <li class="header"><?php echo app('translator')->getFromJson('labels.users'); ?></li>
            <?php endif; ?>

            <?php if($user->hasAccess('user.read')): ?>
                <li class="<?php echo active_class('admin.user.unverified*'); ?>">
                    <a href="<?php echo route('admin.user.unverified'); ?>">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <span><?php echo app('translator')->getFromJson('labels.unverified users'); ?></span>
                    </a>
                </li>
                <li class="<?php echo active_class('admin.user.index*'); ?>">
                    <a href="<?php echo route('admin.user.index'); ?>">
                        <i class="fa fa-user"></i>
                        <span><?php echo app('translator')->getFromJson('labels.users'); ?></span>

                        <?php if($user->hasAccess('user.create')): ?>
                            <small class="label create-label pull-right bg-green" title="<?php echo app('translator')->getFromJson('labels.add_user'); ?>"
                                   data-href="<?php echo route('admin.user.create'); ?>">
                                <i class="fa fa-plus"></i>
                            </small>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if($user->hasAccess('group')): ?>
                <li class="<?php echo active_class('admin.role.index*'); ?>">
                    <a href="<?php echo route('admin.role.index'); ?>">
                        <i class="fa fa-users"></i>
                        <span><?php echo app('translator')->getFromJson('labels.roles'); ?></span>

                        <?php if($user->hasAccess('group.create')): ?>
                            <small class="label create-label pull-right bg-green" title="<?php echo app('translator')->getFromJson('labels.add_group'); ?>"
                                   data-href="<?php echo route('admin.role.create'); ?>">
                                <i class="fa fa-plus"></i>
                            </small>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endif; ?>


            <?php if(
                    $user->hasAccess(['page.read']) ||
                    $user->hasAccess(['supportpage.read']) ||
                    $user->hasAccess(['question.read']) ||
                    $user->hasAccess(['department.read'])
                ): ?>
                <li class="header"><?php echo app('translator')->getFromJson('labels.content'); ?></li>
            <?php endif; ?>

            <?php if($user->hasAccess('page.read')): ?>
                <li class="<?php echo active_class('admin.page*'); ?>">
                    <a href="<?php echo route('admin.page.index'); ?>">
                        <i class="fa fa-file-text"></i>
                        <span><?php echo app('translator')->getFromJson('labels.pages'); ?></span>

                        <?php if($user->hasAccess('page.create')): ?>
                            <small class="label create-label pull-right bg-green" title="<?php echo app('translator')->getFromJson('labels.add_page'); ?>"
                                   data-href="<?php echo route('admin.page.create'); ?>">
                                <i class="fa fa-plus"></i>
                            </small>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endif; ?>



          <?php if($user->hasAccess(['group.read'])): ?>
              <li class="header"><?php echo app('translator')->getFromJson('labels.groups'); ?></li>
          <?php endif; ?>

          <?php if($user->hasAccess('product')): ?>
              <li class="<?php echo active_class('admin.product.index*'); ?>">
                  <a href="<?php echo route('admin.product.index'); ?>">
                      <i class="fa fa-tasks" aria-hidden="true"></i>
                      <span><?php echo app('translator')->getFromJson('labels.products'); ?></span>

                      <?php if($user->hasAccess('product.create')): ?>
                          <small class="label create-label pull-right bg-green" title="<?php echo app('translator')->getFromJson('labels.add_product'); ?>"
                                 data-href="<?php echo route('admin.product.create'); ?>">
                              <i class="fa fa-plus"></i>
                          </small>
                      <?php endif; ?>
                  </a>
              </li>
          <?php endif; ?>


          <?php if($user->hasAccess('group')): ?>
              <li class="<?php echo active_class('admin.group.index*'); ?>">
                  <a href="<?php echo route('admin.group.index'); ?>">
                      <i class="fa fa-users"></i>
                      <span><?php echo app('translator')->getFromJson('labels.groups'); ?></span>

                      <?php if($user->hasAccess('group.create')): ?>
                          <small class="label create-label pull-right bg-green" title="<?php echo app('translator')->getFromJson('labels.add_group'); ?>"
                                 data-href="<?php echo route('admin.group.create'); ?>">
                              <i class="fa fa-plus"></i>
                          </small>
                      <?php endif; ?>
                  </a>
              </li>
          <?php endif; ?>




            <?php if($user->hasAccess(['variablevalue.read', 'translation.read'])): ?>
                <li class="header"><?php echo app('translator')->getFromJson('labels.settings'); ?></li>
            <?php endif; ?>

            <?php if($user->hasAccess('variablevalue.read')): ?>
                <li class="<?php echo active_class('admin.variable*'); ?>">
                    <a href="<?php echo route('admin.variable.value.index'); ?>">
                        <i class="fa fa-cog"></i>
                        <span><?php echo app('translator')->getFromJson('labels.variables'); ?></span>
                    </a>
                </li>
            <?php endif; ?>

            <?php if($user->hasAccess('translation.read')): ?>
                <li class="treeview <?php echo active_class('admin.translation.index*'); ?>">
                    <a href="#">
                        <i class="fa fa-language"></i>
                        <span><?php echo app('translator')->getFromJson('labels.translations'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php $__currentLoopData = config('translation.visible_groups'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="<?php echo front_active_class(route('admin.translation.index', $group)); ?>">
                                <a href="<?php echo route('admin.translation.index', $group); ?>">
                                    <span><?php echo app('translator')->getFromJson('labels.translation_group_' . $group); ?></span>
                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </section>
</aside>