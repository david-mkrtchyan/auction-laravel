<div class="col-md-12 no-padding">
    <div class="input-group">
        <?php echo Form::text($name, $value, array_merge(['class' => 'form-control input-sm'], $params)); ?>


        <div class="input-group-addon show-elfinder-button" data-title="<?php echo app('translator')->getFromJson('labels.please_select_image'); ?>" data-target="[elfinder-link='<?php echo $elfinder_link_name; ?>']">
            <i class="fa fa-folder"></i>
        </div>
        <div class="input-group-addon download-file-button" data-title="<?php echo app('translator')->getFromJson('labels.download'); ?>">
            <a target="_blank"
               data-href="<?php echo route('admin.download.file', ['file']); ?>"
               href="<?php echo $value ? route('admin.download.file', ['file' => $value]) : '#'; ?>">
                <i class="fa fa-download" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>

<div class="clearfix"></div>