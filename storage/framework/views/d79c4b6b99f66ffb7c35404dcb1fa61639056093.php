<?php $__env->startSection('main'); ?>
    <div class="user-info">
        <h3 class="title-into"><?php echo app('translator')->getFromJson('front_messages.Update Your Details'); ?> </h3>
        <div class="registr-form">
            <?php echo Form::open(['route' => 'user.update', 'method' => 'post', 'class' => 'register-form']); ?>

            <div class="row">
                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group forinput">
                        <?php echo e(Form::email('email', $model->email, ['placeholder' => trans('front_labels.email placeholder'),'class' => 'form-control'])); ?>

                    </div>
                    <div class="form-group forinput">
                        <?php echo e(Form::text('first_name',  $model->first_name, ['class' => 'form-control name', 'placeholder' => trans('front_labels.first_name'),'class' => 'form-control'])); ?>

                    </div>
                    <div class="form-group forinput">
                        <?php echo e(Form::password('password', ['placeholder' => trans('front_labels.password'),'class' => 'form-control'])); ?>

                    </div>

                    <div class="form-group forinput">
                        <?php echo e(Form::text('user_name',  $model->user_name, [  'disabled' => 'disabled','class' => 'form-control user_name', 'placeholder' => trans('front_labels.user_name'),'class' => 'form-control'])); ?>

                    </div>

                   
                </div>

                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group forinput">
                        <?php echo e(Form::select('country_id', $countries, null,['class' => 'form-control'])); ?>

                    </div>

                    <div class="form-group forinput">
                        <?php echo e(Form::text('last_name', $model->last_name, ['class' => 'form-control name', 'placeholder' => trans('front_labels.last_name'),'class' => 'form-control'])); ?>

                    </div>

                    <div class="form-group forinput">
                        <?php echo e(Form::password('password_confirmation', ['placeholder' => trans('front_labels.password_confirmation'),'class' => 'form-control'])); ?>

                    </div>

                    <div class="form-group forinput">
                        <?php echo e(Form::tel('phone',  $model->phone, ['id'=>'mobile-number','class' => 'form-control phone', 'placeholder' => trans('front_labels.phone'),'class' => 'form-control'])); ?>

                        <?php echo e(Form::hidden('phone_validate', '', array('id' => 'validate_phone'))); ?>

                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 offset-xl-3 offset-lg-0 offset-md-0 offset-sm-0">
                    <input type="submit" name="submit" class="btn btn2 btn-block btn-primary register-form-submit"
                           value="<?php echo app('translator')->getFromJson('front_labels.update'); ?>"/>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('phone'); ?>
    <?php $__env->startComponent('partials.components.intlTelInput',['id' => 'mobile-number']); ?> <?php echo $__env->renderComponent(); ?>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>