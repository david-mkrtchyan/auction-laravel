<?php $__env->startSection('main'); ?>

    <div class="main-page__center">
        <h1><?php echo $model->name; ?></h1>

        <?php echo $model->content; ?>

    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>