<?php echo $__env->make('group.partials._buttons', ['class' => 'buttons-top'], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="row">
    <div class="col-md-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php $__currentLoopData = config('app.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li <?php if($key == 0): ?> class="active" <?php endif; ?>>
                        <a aria-expanded="false" href="#tab_<?php echo $locale; ?>" data-toggle="tab">
                            <i class="flag flag-<?php echo $locale; ?>"></i>
                            <?php echo app('translator')->getFromJson('labels.tab_'.$locale); ?>
                        </a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <li>
                    <a aria-expanded="false" href="#images" data-toggle="tab"><?php echo app('translator')->getFromJson('labels.tab_images'); ?></a>
                </li>

                <li>
                    <a aria-expanded="false" href="#general" data-toggle="tab"><?php echo app('translator')->getFromJson('labels.tab_general'); ?></a>
                </li>

            </ul>

            <div class="tab-content">
                <?php $__currentLoopData = config('app.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="tab-pane fade in <?php if($key == 0): ?> active <?php endif; ?>" id="tab_<?php echo $locale; ?>">
                        <?php echo $__env->make('views.group.tabs.locale', array('errors' => $errors, 'model' => $model , 'locale' => $locale), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <div class="tab-pane" id="images">
                    <?php echo $__env->make('group.tabs.images', array('errors' => $errors, 'model' => $model,'images' => $images, 'imagesConfig' => $imagesConfig ), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>


                <div class="tab-pane" id="general">
                    <?php echo $__env->make('group.tabs.general', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>


            </div>
        </div>

    </div>
</div>

<?php echo $__env->make('group.partials._buttons', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>