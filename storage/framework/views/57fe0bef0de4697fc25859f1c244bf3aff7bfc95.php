<div class="menu">
    <div class="navbar">
        <div class="logo">
            <?php echo $__env->make('partials.logo', ['with_title' => true], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div class="nav">
            <ul id="men">
                <?php if($pagesTop): ?>
                    <?php $__currentLoopData = $pagesTop; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($page->slug == $catalog): ?>
                            <li class="item"><a id="myBtn" data-toggle="modal" data-target="#catalog-list" href="#"><?php echo e($page->name); ?></a></li>
                        <?php else: ?>
                            <li class="item"><a href="<?php echo route('page.show',$page->slug); ?>"><?php echo e($page->name); ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <?php if($_user): ?>
                        <li class="item"><a href="<?php echo route('page.show',$page->slug); ?>"><?php echo e($page->name); ?></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>