<div class="row form-group">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <?php echo Form::number('value', null, ['id' => 'value', 'placeholder' => trans('labels.number'), 'required' => true, 'class' => 'form-control input-sm']); ?>

    </div>
</div>
