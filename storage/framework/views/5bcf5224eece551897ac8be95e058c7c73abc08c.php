
<div class="form-group required <?php if($errors->has('images')): ?> has-error <?php endif; ?>">

    <div class="col-md-12">

        <?php echo Form::file('images[]', ['required' => true, 'id' => 'input-1a','accept' => 'image/*','multiple' => true]); ?>


        <?php echo $errors->first('images', '<p class="help-block error">:message</p>'); ?>


    </div>

    <?php echo $__env->make('partials.tabs.fileInput', ['id' => 'input-1a', 'images' => $images, 'imagesConfig' => $imagesConfig ], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>