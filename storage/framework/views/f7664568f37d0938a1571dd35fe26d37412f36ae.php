<?php if($groups): ?>

    <?php $__env->startPush('countDown'); ?>
        <script type="text/javascript" src="<?php echo theme_asset('js/countDown.js'); ?>"></script>
    <?php $__env->stopPush(); ?>

    <div class="group-catalog" id="app">
        <h1 class="group-catalog-h1">Lorem ipsum dolor sit amet.</h1>
        <h2 class="group-catalog-h2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, saepe.</h2>
        <div class="cardd" v-match-heights="{el: ['.img-product'],disabled: [768,[920, 1000], ],}">
            <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="one">
                    <div class="box1 group-catalog-title">
                        <h3><?php echo $group->name; ?></h3>
                    </div>
                    <div class="box2">
                        <h4><?php echo app('translator')->getFromJson('front_messages.ends'); ?> <?php echo $group->setFormatDate($group->end_time, "d.m"); ?></h4>
                    </div>
                    <div class="box3">
                        <h5><?php echo count($group->product); ?> <?php echo app('translator')->getFromJson('front_messages.directory numbers'); ?></h5>
                    </div>

                    <div class="box4 group-catalog-image">
                        <a href="<?php echo route('group.show',$group->id); ?>" class="group-url">
                            <?php if($image = $group->checkImageExists()): ?>
                                <img src="<?php echo $image; ?>" alt="" class="img-product">
                            <?php endif; ?>


                            <?php if($group->checkTime()): ?>
                                <?php $__env->startComponent('partials.components.timer',['id' => $group->id, 'group' => $group]); ?>
                                <?php echo $__env->renderComponent(); ?>

                                <?php $__env->startPush('countDown'); ?>
                                    <script>
                                        countDown("<?php echo $group->id; ?>", "<?php echo $group->setFormatDate($group->end_time, "M d, Y H:i:s"); ?>")
                                    </script>
                                <?php $__env->stopPush(); ?>

                            <?php else: ?>
                                <div class="removed_item">
                                    <?php echo app('translator')->getFromJson('front_messages.removed'); ?>
                                </div>
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="box5">
                        <a href="<?php echo route('group.show',$group->id); ?>"
                           class="see-all"><?php echo app('translator')->getFromJson('front_messages.see all'); ?></a>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="group-pagination">
            <?php echo e($groups->links()); ?>

        </div>
    </div>
<?php endif; ?>



<?php $__env->startPush('vue'); ?>
    <script type="text/javascript" src="<?php echo theme_asset('js/vue.js'); ?>"></script>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('vue-match-heights'); ?>
    <script type="text/javascript" src="<?php echo theme_asset('js/vue-match-heights.js'); ?>"></script>

    <script>
        Vue.use(VueMatchHeights, {
            disabled: [768],
        })

        new Vue({
            el: '#app'
        })
    </script>

<?php $__env->stopPush(); ?>


