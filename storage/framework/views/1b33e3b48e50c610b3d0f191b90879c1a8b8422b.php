

<div class="image-block">

    <div class="preview-image profile-user-img img-circle"
         id="preview_image">

        <?php echo $__env->make('views.user.image', ['src' => $model->avatar, 'attributes' => ['width' => 128, 'height' => 128, 'class' => 'img-responsive img-circle']], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo Form::file('avatar', ['onchange' => 'loadImagePreview(this, \'preview_image\')']); ?>

    </div>

    <div class="text-muted input-sm remove-image <?php if(!$model->avatar): ?> hidden <?php endif; ?>" data-preview_id="preview_image" data-image_input_id="avatar">
        <?php echo app('translator')->getFromJson('labels.remove_avatar'); ?>
    </div>

    <?php echo Form::hidden('avatar', $model->avatar, ['id' => 'avatar']); ?>

</div>

<?php $__env->startPush('default'); ?>
    <script src="<?php echo theme_asset('js/default.js'); ?>"></script>
<?php $__env->stopPush(); ?>