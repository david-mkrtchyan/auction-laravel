<!doctype html>
<html lang="<?php echo $locale; ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <?php echo $__env->make('partials.head', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</head>

<body>

<?php $__env->startPush('landing.css'); ?>
    <meta name="format-detection" content="telephone=no">
<?php $__env->stopPush(); ?>

<?php echo $__env->make('partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="content container">
    <div class="col-md-3">
        <?php echo $__env->make('partials.menus.dashboard.aside', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
    <div class="col-md-9">
        <?php echo $__env->yieldContent('main'); ?>
    </div>
</div>


<?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startPush('assets.bottom'); ?>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('partials.messages', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('partials.foot', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


</body>

</html>
