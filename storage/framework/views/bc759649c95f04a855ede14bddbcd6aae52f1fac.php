
<?php if($pagesBottom): ?>
    <div class="mein">
        <div class="bot">
            <?php $__currentLoopData = $pagesBottom; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a class="line" href="<?php echo route('page.show',$page->slug); ?>"><?php echo e($page->name); ?></a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php endif; ?>
