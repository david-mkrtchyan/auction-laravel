<?php if(isset($email) && isset($code)): ?>
    <div class="modal fade password-modal" id="modal22" tabindex="-1" role="dialog" aria-labelledby="modal22" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h3>Password Recovery</h3>
                    <div class="align-center"><p><?php echo app('translator')->getFromJson('front_texts.enter new password'); ?></p></div>
                    <div class="login-form">
                        <?php echo Form::open(['route' => 'reminder.complete.email', 'method' => 'post', 'class' => 'auth-form']); ?>

                        <?php echo Form::hidden('email', $email); ?>

                        <?php echo Form::hidden('code', $code); ?>

                        <div class="forinput form-group">
                            <?php echo e(Form::password('password', ['placeholder' => trans('front_labels.password'),'class' => 'form-control'])); ?>

                        </div>
                        <div class="forinput form-group">
                            <?php echo e(Form::password('password_confirmation', ['placeholder' => trans('front_labels.password_confirmation'),'class' => 'form-control'])); ?>

                        </div>
                        <?php if($g2fa): ?>
                            <div class="forinput">
                                <input required type="text" name="otp" placeholder="<?php echo app('translator')->getFromJson('front_labels.otp placeholder'); ?>" id="register_otp"/>
                            </div>
                        <?php endif; ?>
                        <div class="error-text hide"></div>
                        <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit"
                               value="<?php echo app('translator')->getFromJson('front_labels.new password'); ?>"/>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="modal fade" id="modal21" tabindex="-1" role="dialog" aria-labelledby="modal21" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3>Password Recovery</h3>
                <div class="align-center">
                    <div class="align-center"><p><?php echo app('translator')->getFromJson('front_texts.password restore helper text'); ?></p></div>
                </div>
                <div class="login-form">
                    <?php echo Form::open(['route' => 'reminder.store.email', 'method' => 'post', 'class' => 'auth-form']); ?>

                    <div class="forinput form-group">
                        <?php echo e(Form::email('email', null, ['placeholder' => trans('front_labels.email placeholder'),'class' => 'form-control'])); ?>

                    </div>
                    <div class="error-text hide"></div>
                    <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit"
                           value="<?php echo app('translator')->getFromJson('front_labels.send'); ?>"/>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>