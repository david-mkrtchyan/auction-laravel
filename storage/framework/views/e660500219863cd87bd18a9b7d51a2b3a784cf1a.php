
<footer class="footer">
    <?php echo $__env->make('partials.menus.bottom', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="info">
        <p>jysknetauktion.dk - 24 48 48 45 - info@jyskauktion.dk</p>
    </div>
</footer>

<?php echo $__env->make('partials.metrics.facebook', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('partials.metrics.google', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('partials.metrics.chimpstatic', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>



