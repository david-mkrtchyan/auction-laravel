<?php ($list = isset($list) ? $list : ['1' => trans('labels.status_on'), '0' => trans('labels.status_off')]); ?>

<div class="col-sm-10">
    <select class="select2 input-sm col-xs-16 form-control ajax-field-changer <?php echo $field; ?>-select"
               data-id="<?php echo $model->id; ?>"
               data-token="<?php echo csrf_token(); ?>"
               data-url="<?php echo route('admin.' . $type . '.ajax_field', [$model->id]); ?>"
               data-field="<?php echo $field; ?>"
    >
        <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option <?php if($model->{$field} == $key): ?> selected="selected" <?php endif; ?> value="<?php echo $key; ?>"><?php echo $item; ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>
