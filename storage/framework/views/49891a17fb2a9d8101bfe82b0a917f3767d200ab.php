<?php $__env->startSection('assets.top'); ?>
    ##parent-placeholder-1ff61875b36adf1c43eca5c25e092b19647f1874##

    <link rel="stylesheet" href="<?php echo asset('assets/components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css'); ?>"/>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('assets.bottom'); ?>
    ##parent-placeholder-a9a07541009abdaaa4217e18e14eaac1744c2437##

    <script src="<?php echo asset('assets/components/bootstrap-switch/dist/js/bootstrap-switch.min.js'); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>