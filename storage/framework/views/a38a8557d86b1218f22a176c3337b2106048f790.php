

<div class="form-group required <?php if($errors->has('start_price')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('start_price', trans('labels.start_price'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']); ?>


    <div class="col-xs-12 col-sm-7 col-md-4">
        <?php echo Form::text('start_price', null, ['placeholder' => trans('labels.start_price'), 'required' => true, 'class' => 'form-control input-sm']); ?>


        <?php echo $errors->first('start_price', '<p class="help-block error">:message</p>'); ?>

    </div>
</div>

<div class="form-group required <?php if($errors->has('price')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('price', trans('labels.price'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']); ?>


    <div class="col-xs-12 col-sm-7 col-md-4">
        <?php echo Form::text('price', null, ['placeholder' => trans('labels.price'), 'required' => true,'class' => 'form-control input-sm']); ?>


        <?php echo $errors->first('price', '<p class="help-block error">:message</p>'); ?>

    </div>
</div>

<div class="form-group required <?php if($errors->has('price_step')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('price_step', trans('labels.price_step'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']); ?>


    <div class="col-xs-12 col-sm-7 col-md-4">
        <?php echo Form::text('price_step', null, ['placeholder' => trans('labels.price_step'), 'required' => true,'class' => 'form-control input-sm']); ?>


        <?php echo $errors->first('price_step', '<p class="help-block error">:message</p>'); ?>

    </div>
</div>



