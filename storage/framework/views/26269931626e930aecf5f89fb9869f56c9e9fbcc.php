<script type="text/javascript">window.messages_timeout = 500;</script>

<?php if(isset($messages)): ?>
    <?php if($messages->count()): ?>
        <?php $__currentLoopData = $messages->getMessages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        window.messages_timeout = parseInt(window.messages_timeout) + 500;
                        setTimeout('flashMessage(\'<?php echo $message; ?>\', \'<?php echo $key; ?>\');', window.messages_timeout);
                    });
                </script>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
<?php endif; ?>


<?php if(isset($profile_modal) && $profile_modal == true): ?>
    <?php ($profile_modal_text = variable('profile_modal_text')); ?>
    <?php if($profile_modal_text): ?>
        <?php echo $__env->make(
            'partials.modals.info',
            [
                'id' => 'profile',
                'title' => trans('front_labels.profile modal title'),
                'text' => $profile_modal_text,
            ]
        , \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
<?php endif; ?>