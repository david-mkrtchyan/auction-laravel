<?php if($item->multilingual): ?>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <?php $__currentLoopData = config('app.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li <?php if($key == 0): ?> class="active" <?php endif; ?>>
                    <a aria-expanded="false" href="#tab_<?php echo $item->id; ?>_<?php echo $locale; ?>" data-toggle="tab">
                        <i class="flag flag-<?php echo $locale; ?>"></i>
                        <?php echo app('translator')->getFromJson('labels.tab_'.$locale); ?>
                    </a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>

        <div class="tab-content">
            <?php $__currentLoopData = config('app.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="tab-pane fade in <?php if($key == 0): ?> active <?php endif; ?>" id="tab_<?php echo $item->id; ?>_<?php echo $locale; ?>">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <?php echo Form::text($locale.'[text]', isset($item->translate($locale)->text) ? $item->translate($locale)->text : '', ['id' => $locale.'_text', 'placeholder' => trans('labels.text'), 'required' => true, 'class' => 'form-control input-sm']); ?>

                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>

<?php else: ?>

    <div class="row form-group">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?php echo Form::text('value', null, ['id' => 'value', 'placeholder' => trans('labels.value'), 'required' => true, 'class' => 'form-control input-sm']); ?>

        </div>
    </div>

<?php endif; ?>