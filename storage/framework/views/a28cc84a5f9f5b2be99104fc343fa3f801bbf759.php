<?php $__env->startSection('main'); ?>
    <?php if($product): ?>
        <?php $__env->startPush('countDown'); ?>
            <script type="text/javascript" src="<?php echo theme_asset('js/countDown.js'); ?>"></script>
        <?php $__env->stopPush(); ?>

        <div class="product-view">
            <div class="product-title">
                <h3 class="text-center"><?php echo $product->title; ?></h3>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="group-catalog-image2">
                        <?php if($images = $product->images): ?>
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <?php if($imageCount = count($images)): ?>
                                        <?php for($i=0;$i < $imageCount;$i++): ?>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>"
                                                class="<?php if($i == 0): ?> active <?php endif; ?>"></li>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </ol>
                                <div class="carousel-inner">
                                    <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($image = $product->checkImageExists($image->name)): ?>
                                            <div class="carousel-item <?php if($key == 0): ?> active <?php endif; ?>">
                                                <img class="d-block w-100" src="<?php echo $image; ?>" alt="First slide">
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-description">
                        <?php echo $product->description; ?>

                    </div>

                    <hr/>
                    <?php ($group = $product->group); ?>

                    <?php if($group->checkTime()): ?>
                        <div class="offered"><?php echo app('translator')->getFromJson('front_messages.offered'); ?>: € <?php echo $product->price; ?></div>

                        <?php $__env->startComponent('partials.components.timer',['id' => $product->id]); ?>
                        <?php echo $__env->renderComponent(); ?>

                        <?php $__env->startPush('countDown'); ?>
                            <script>
                                countDown("<?php echo $product->id; ?>", "<?php echo $group->setFormatDate($group->end_time, "M d, Y H:i:s"); ?>")
                            </script>
                        <?php $__env->stopPush(); ?>
                    <?php endif; ?>


                </div>

            </div>

        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>