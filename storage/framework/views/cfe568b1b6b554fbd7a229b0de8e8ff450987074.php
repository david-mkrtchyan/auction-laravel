<?php if(empty($src)): ?>
    <?php $src = $no_image; ?>
<?php endif; ?>

<?php if(empty($attributes['width'])): ?>
    <?php $attributes['width'] = 50; ?>
<?php endif; ?>

<?php if(empty($attributes['height'])): ?>
    <?php $attributes['height'] = $attributes['width']; ?>
<?php endif; ?>

<img src="<?php echo thumb($src, $attributes['width'], $attributes['height']); ?>"
    <?php if(!empty($attributes) && count($attributes)): ?>
        <?php $__currentLoopData = $attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo $key; ?>="<?php echo $value; ?>"
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
/>