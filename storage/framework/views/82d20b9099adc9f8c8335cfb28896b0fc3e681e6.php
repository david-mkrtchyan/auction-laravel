<?php $__env->startSection('main'); ?>
    <?php if($products): ?>

        <?php $__env->startPush('countDown'); ?>
            <script type="text/javascript" src="<?php echo theme_asset('js/countDown.js'); ?>"></script>
        <?php $__env->stopPush(); ?>


        <div class="group-catalog">
            <h1 class="text-center"><?php echo $group->name; ?></h1>
            <span class="text-center"><?php echo $group->description; ?></span>
            <div class="cardd">
                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="one">

                        <div class="box4 group-catalog-image">
                            <a href="<?php echo route('product.show',$product->id); ?>" class="group-url">
                                <?php if(($productImage = $product->image) && ($image = $product->checkImageExists($productImage->name))): ?>
                                    <img src="<?php echo $image; ?>" alt="" class="img-product">
                                <?php endif; ?>

                                <?php if($group->checkTime()): ?>
                                        <?php $__env->startComponent('partials.components.timer',['id' => $product->id]); ?>
                                        <?php echo $__env->renderComponent(); ?>

                                        <?php $__env->startPush('countDown'); ?>
                                            <script>
                                                countDown("<?php echo $product->id; ?>", "<?php echo $group->setFormatDate($group->end_time, "M d, Y H:i:s"); ?>")
                                            </script>
                                        <?php $__env->stopPush(); ?>
                                <?php endif; ?>

                                <div class="infoline">
                                    <div class="icon">
                                        <i class="fa fa-bookmark theme"></i>
                                    </div>
                                    <?php echo app('translator')->getFromJson('front_messages.catalog'); ?> <?php echo $key+1; ?>

                                </div>
                                <div class="infoline">
                                    <div class="icon">
                                        <i class="fa fa-tag theme"></i>
                                    </div>
                                    <?php echo $product->price; ?> EUR
                                </div>
                                <div class="infoline">
                                    <div class="icon">
                                        <i class="fa fa-comment theme"></i>
                                    </div>
                                    <span><?php echo app('translator')->getFromJson('front_messages.description'); ?></span>
                                    <div class="text">
                                        <?php echo Text::cutText($product->description,50); ?>

                                    </div>
                                </div>
                                <div class="make-bet">
                                    <a href="<?php echo route('product.show',$product->id); ?>" class="btn btn-danger">
                                        <i class="fa fa-plus"> </i><?php echo app('translator')->getFromJson('front_messages.make bet now'); ?></a>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="group-pagination">
                <?php echo e($products->links()); ?>

            </div>
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>