

<div class="head">
    <div class="header">
        <div class="head-left">
            <?php if(!$_user): ?>

                
                <?php echo $__env->make('partials.modals.register', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('partials.modals.password_recovery', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('partials.modals.log_in', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <a href="#" data-toggle="modal"  data-target="#modal2" id="openEnter" class="btn-primary">
                    <i class="fas fa-unlock"></i><?php echo app('translator')->getFromJson('front_landing.login'); ?>
                </a>

                <a href="#" class="two btn btn-primary modal-register-button" data-toggle="modal" data-target="#modal1" id="openReg">
                    <i class="fas fa-plus"></i> <?php echo app('translator')->getFromJson('front_landing.create_account'); ?>
                </a>

            <?php else: ?>

                <a  href="<?php echo route('session.delete'); ?>"  title="<?php echo app('translator')->getFromJson('front_landing.log out'); ?>" id="openEnter" class="btn-primary">
                    <i class="fas fa-unlock"></i></i><?php echo app('translator')->getFromJson('front_landing.logout'); ?>
                </a>

                <a href="<?php echo route('user.edit'); ?>"  class="<?php echo front_active_class('user.*'); ?> two btn btn-primary modal-register-button"  id="openReg">
                    <i class="fas fa-plus"></i> <?php echo app('translator')->getFromJson('front_landing.my details'); ?>
                </a>

            <?php endif; ?>

            <?php echo $__env->make('partials.logo', ['with_title' => true], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="head-right">
            <a href="#"><i class="fab fa-facebook-f "></i></a>
            <input type="search" class="form-control">
            <i class="fab fa-cc-visa"></i>
            <i class="fab fa-cc-mastercard"></i>
            <i class="fab fa-cc-visa"></i>
        </div>

    </div>
</div>

<?php echo $__env->make('partials.menus.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('partials.modals.catalog', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>