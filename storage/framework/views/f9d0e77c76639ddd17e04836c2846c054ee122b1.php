
<script type="text/javascript" src="<?php echo theme_asset('js/popper.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_asset('js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_asset('js/tooltip.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_asset('js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_asset('js/jquery.maskedinput.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_asset('js/selectbox.js'); ?>"></script>

<script type="text/javascript" src="<?php echo asset('assets/components/alertifyjs/dist/js/alertify.js'); ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/components/clipboard/dist/clipboard.min.js'); ?>"></script>

<?php echo $__env->yieldPushContent('assets.bottom'); ?>

<script type="text/javascript" src="<?php echo theme_asset('js/js.js'); ?>"></script>
<script type="text/javascript" src="<?php echo theme_asset('js/app.js'); ?>"></script>

<?php echo $__env->yieldPushContent('vue'); ?>
<?php echo $__env->yieldPushContent('vue-match-heights'); ?>
<?php echo $__env->yieldPushContent('phone'); ?>
<?php echo $__env->yieldPushContent('default'); ?>