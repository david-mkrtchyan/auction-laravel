<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3><strong>AI</strong> Trade Login</h3>
                <div class="login-form">
                    <?php echo Form::open(['route' => 'session.store', 'method' => 'post', 'class' => 'auth-form']); ?>

                        <div class="forinput form-group">
                            <?php echo e(Form::email('email', null, ['placeholder' => trans('front_labels.email placeholder'),'class' => 'form-control'])); ?>

                        </div>
                        <div class="forinput form-group">
                            <?php echo e(Form::password('password', ['placeholder' => trans('front_labels.password'),'class' => 'form-control'])); ?>

                        </div>
                        <div class="error-text hide"></div>
                        <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit" value="<?php echo app('translator')->getFromJson('front_labels.log in'); ?>" />
                    <?php echo Form::close(); ?>

					<p> </p>
					<div data-toggle="modal" data-target="#modal21" class="align-center"><p data-toggle="modal" data-target="#modal2"><?php echo app('translator')->getFromJson('front_labels.forgot password'); ?></p></div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade two-factor-login-modal" id="modal20" tabindex="-1" role="dialog" aria-labelledby="modal20" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3><?php echo app('translator')->getFromJson('front_labels.two factor login form title'); ?></h3>
                <div class="align-center">
                    <div class="align-center"><p><?php echo app('translator')->getFromJson('front_texts.two factor login helper text'); ?></p></div>
                </div>
                <div class="login-form">
                    <?php echo Form::open(['route' => 'session.two_factor.check', 'method' => 'post', 'class' => 'auth-form']); ?>

                    <div class="forinput">
                        <?php echo e(Form::text('otp', null, ['placeholder' => trans('front_labels.otp placeholder')])); ?>

                    </div>
                    <div class="error-text hide"></div>
                    <input type="submit" name="submit" class="btn btn2 btn-block btn-primary auth-form-submit"
                           value="<?php echo app('translator')->getFromJson('front_labels.send'); ?>"/>
                    <?php echo Form::close(); ?>

                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>