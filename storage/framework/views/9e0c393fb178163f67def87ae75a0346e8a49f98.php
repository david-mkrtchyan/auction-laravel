<div class="form-group required <?php if($errors->has($locale.'.title')): ?> has-error <?php endif; ?>">
    <?php echo Form::label($locale . '[title]', trans('labels.title'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']); ?>


    <div class="col-xs-12 col-sm-7 col-md-10">
        <?php echo Form::text($locale.'[title]', isset($model->translate($locale)->title) ? $model->translate($locale)->title : '', ['placeholder'=> trans('labels.title'), 'required' => true, 'class' => 'form-control input-sm title_'.$locale]); ?>


        <?php echo $errors->first($locale.'.title', '<p class="help-block error">:message</p>'); ?>

    </div>
</div>

<div class="form-group <?php if($errors->has($locale . '[description]')): ?> has-error <?php endif; ?>">
    <?php echo Form::label($locale . '[description]', trans('labels.description'), ['class' => 'control-label col-xs-4 col-sm-3 col-md-2']); ?>


    <div class="col-xs-8 col-sm-7 col-md-10">
        <?php echo Form::textarea($locale . '[description]', isset($model->translate($locale)->description) ? $model->translate($locale)->description : '', ['rows' => '3', 'placeholder' => trans('labels.description'), 'class' => 'form-control input-sm description' . $locale]); ?>


        <?php echo $errors->first($locale . '[description]', '<p class="help-block error">:message</p>'); ?>

    </div>
</div>
<?php echo $__env->make('partials.tabs.ckeditor', ['id' => $locale . '[description]'], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
