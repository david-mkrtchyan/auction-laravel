<link rel="stylesheet" href="<?php echo asset('vendor/css/fileinput/fileinput.min.css'); ?>"/>
<script src="<?php echo asset('vendor/js/fileinput/sortable.min.js'); ?>"></script>
<script src="<?php echo asset('vendor/js/fileinput/purify.min.js'); ?>"></script>
<script src="<?php echo asset('vendor/js/fileinput/fileinput.min.js'); ?>"></script>

<script>
    $("#<?php echo $id; ?>").fileinput({
        uploadUrl: "#",
        autoReplace: true,
        overwriteInitial: false,
        showUploadedThumbs: false,
        maxFileCount: 40,
        ajaxSettings: {
            beforeSend: function (xhr) {
               // xhr.setRequestHeader('csrf-token', 'DbfkmIfLnnDhEuAdLIeVWDB1r9RiaLDhqtwjZ3XR');
            },
        },
        initialPreview: [
            <?php echo $images; ?>

        ],
        initialPreviewConfig: [
            <?php echo $imagesConfig; ?>

        ],
        initialPreviewAsData: true,
        initialPreviewFileType: 'image',
        initialPreviewShowDelete: true,
        showRemove: true,
        showClose: true,
        showUpload: false,
        language: 'ru',
        showCaption: true,
        showPreview: true,
        showCancel: true,
        uploadLabel: false,
        uploadAsync: false,
        uploadExtraData: {
            'static_variable_1': 'static_value_1',
            'static_variable_2': 'static_value_2'
        },
        layoutTemplates: {/*actionDelete: ''*/}, // disable thumbnail deletion
        allowedFileExtensions: ["jpg", "png", "gif"]
    }).on('filebeforedelete', function (event, key, data) {
        //data.form = {"videoname": "your_video_name_here"};
    }).on("filepredelete", function (event, key, jqXHR, data) {
        var abort = true;

        if (confirm("Are you sure you want to delete this file?")) {
            abort = false;
        }

        return abort;

    }).on('filesorted', function (e, params) {
        $.ajax({
            url: "<?php echo route('admin.image.sort'); ?>",
            type: "post",
            data: {
                previewId: params.stack[params.newIndex].key,
                oldIndex: params.oldIndex,
                newIndex: params.newIndex,
                stack: params.stack,
                _token: '<?php echo csrf_token(); ?>',
            },
        }).done(function (msg) {

        });
    })/*.on('fileuploaded', function(e, params) {
        console.log('File uploaded params', params);
    })*/;


</script>