
<div class="ends-in clearfix" id="ends-in<?php echo $id; ?>">
    <div class="time-container">
        <div class="days text-time" id="days<?php echo $id; ?>"></div>
        <div class="label">дней</div>
    </div>
    <div class="time-container">
        <div class="hours text-time" id="hours<?php echo $id; ?>"></div>
        <div class="label">часов</div>
    </div>
    <div class="time-container">
        <div class="minutes text-time" id="minutes<?php echo $id; ?>"></div>
        <div class="label">минут</div>
    </div>
    <div class="time-container">
        <div class="seconds text-time" id="seconds<?php echo $id; ?>"></div>
        <div class="label">секунд</div>
    </div>
</div>