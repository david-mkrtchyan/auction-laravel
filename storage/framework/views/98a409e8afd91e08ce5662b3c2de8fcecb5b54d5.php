<?php if(!empty(config('metrics.google.analytics_id'))): ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo config('metrics.google.analytics_id'); ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', '<?php echo config('metrics.google.analytics_id'); ?>');
    </script>

<?php endif; ?>