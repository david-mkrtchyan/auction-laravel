<ol class="breadcrumb">
    <li class="first">
        <a href="<?php echo route('admin.home'); ?>"><i class="fa fa-home"></i> <?php echo app('translator')->getFromJson('labels.home'); ?></a>
    </li>
    <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <li <?php echo ($i == count($breadcrumbs)-1 ? 'class="active"':''); ?>>
            <?php if($data['url']): ?>
                <a href="<?php echo $data['url']; ?>"><?php echo $data['name']; ?></a>
            <?php else: ?>
                <?php echo $data['name']; ?>

            <?php endif; ?>
        </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ol>

