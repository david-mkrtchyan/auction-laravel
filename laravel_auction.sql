-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2018 at 09:20 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_auction`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'aV91Qy1uIY7lkmyoBJtffTH1oMJlCsIh', 1, '2018-10-22 02:12:43', '2018-10-22 02:12:43', '2018-10-22 02:12:43'),
(2, 4, 'hnUYzXbpSlHsXRlrKHdwuo69YABUTjK8', 1, '2018-10-22 02:16:13', '2018-10-22 02:16:13', '2018-10-22 02:16:13'),
(3, 6, 'bSAfqpMwn1jrLb90sYflNujf8TsoVWUa', 1, '2018-10-26 04:45:26', '2018-10-26 04:45:26', '2018-10-26 04:45:26'),
(4, 7, 'FMljuYUXyXmNlilZFjbq3y3BUuG1dpsX', 1, '2018-10-26 05:25:33', '2018-10-26 05:25:33', '2018-10-26 05:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `slug`, `title`, `native`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', 'غانستان', NULL, NULL),
(2, 'AX', 'Åland Islands', 'Åland', NULL, NULL),
(3, 'AL', 'Albania', 'Shqipëri', NULL, NULL),
(4, 'DZ', 'Algeria', 'الجزائر', NULL, NULL),
(5, 'AS', 'American Samoa', '', NULL, NULL),
(6, 'AD', 'Andorra', '', NULL, NULL),
(7, 'AO', 'Angola', '', NULL, NULL),
(8, 'AI', 'Anguilla', '', NULL, NULL),
(9, 'AQ', 'Antarctica', '', NULL, NULL),
(10, 'AG', 'Antigua and Barbuda', '', NULL, NULL),
(11, 'AR', 'Argentina', '', NULL, NULL),
(12, 'AM', 'Armenia', 'Հայաստան', NULL, NULL),
(13, 'AW', 'Aruba', '', NULL, NULL),
(14, 'AC', 'Ascension Island', '', NULL, NULL),
(15, 'AU', 'Australia', '', NULL, NULL),
(16, 'AT', 'Austria', 'Österreich', NULL, NULL),
(17, 'AZ', 'Azerbaijan', 'Azərbaycan', NULL, NULL),
(18, 'BS', 'Bahamas', '', NULL, NULL),
(19, 'BH', 'Bahrain', 'البحرين', NULL, NULL),
(20, 'BD', 'Bangladesh', 'বাংলাদেশ', NULL, NULL),
(21, 'BB', 'Barbados', '', NULL, NULL),
(22, 'BY', 'Belarus', 'Беларусь', NULL, NULL),
(23, 'BE', 'Belgium', 'België', NULL, NULL),
(24, 'BZ', 'Belize', '', NULL, NULL),
(25, 'BJ', 'Benin', 'Bénin', NULL, NULL),
(26, 'BM', 'Bermuda', '', NULL, NULL),
(27, 'BT', 'Bhutan', 'འབྲུག', NULL, NULL),
(28, 'BO', 'Bolivia', '', NULL, NULL),
(29, 'BA', 'Bosnia and Herzegovina', 'Босна и Херцеговина', NULL, NULL),
(30, 'BW', 'Botswana', '', NULL, NULL),
(31, 'BV', 'Bouvet Island', '', NULL, NULL),
(32, 'BR', 'Brazil', 'Brasil', NULL, NULL),
(33, 'IO', 'British Indian Ocean Territory', '', NULL, NULL),
(34, 'VG', 'British Virgin Islands', '', NULL, NULL),
(35, 'BN', 'Brunei', '', NULL, NULL),
(36, 'BG', 'Bulgaria', 'България', NULL, NULL),
(37, 'BF', 'Burkina Faso', '', NULL, NULL),
(38, 'BI', 'Burundi', 'Uburundi', NULL, NULL),
(39, 'KH', 'Cambodia', 'កម្ពុជា', NULL, NULL),
(40, 'CM', 'Cameroon', 'Cameroun', NULL, NULL),
(41, 'CA', 'Canada', '', NULL, NULL),
(42, 'IC', 'Canary Islands', 'islas Canarias', NULL, NULL),
(43, 'CV', 'Cape Verde', 'Kabu Verdi', NULL, NULL),
(44, 'BQ', 'Caribbean Netherlands', '', NULL, NULL),
(45, 'KY', 'Cayman Islands', '', NULL, NULL),
(46, 'CF', 'Central African Republic', 'République centrafricaine', NULL, NULL),
(47, 'EA', 'Ceuta and Melilla', 'Ceuta y Melilla', NULL, NULL),
(48, 'TD', 'Chad', 'Tchad', NULL, NULL),
(49, 'CL', 'Chile', '', NULL, NULL),
(50, 'CN', 'China', '中国', NULL, NULL),
(51, 'CX', 'Christmas Island', '', NULL, NULL),
(52, 'CP', 'Clipperton Island', '', NULL, NULL),
(53, 'CC', 'Cocos (Keeling) Islands', 'Kepulauan Cocos (Keeling)', NULL, NULL),
(54, 'CO', 'Colombia', '', NULL, NULL),
(55, 'KM', 'Comoros', 'جزر القمر', NULL, NULL),
(56, 'CD', 'Congo (DRC)', 'Jamhuri ya Kidemokrasia ya Kongo', NULL, NULL),
(57, 'CG', 'Congo (Republic)', 'Congo-Brazzaville', NULL, NULL),
(58, 'CK', 'Cook Islands', '', NULL, NULL),
(59, 'CR', 'Costa Rica', '', NULL, NULL),
(60, 'CI', 'Côte d’Ivoire', '', NULL, NULL),
(61, 'HR', 'Croatia', 'Hrvatska', NULL, NULL),
(62, 'CU', 'Cuba', '', NULL, NULL),
(63, 'CW', 'Curaçao', '', NULL, NULL),
(64, 'CY', 'Cyprus', 'Κύπρος', NULL, NULL),
(65, 'CZ', 'Czech Republic', 'Česká republika', NULL, NULL),
(66, 'DK', 'Denmark', 'Danmark', NULL, NULL),
(67, 'DG', 'Diego Garcia', '', NULL, NULL),
(68, 'DJ', 'Djibouti', '', NULL, NULL),
(69, 'DM', 'Dominica', '', NULL, NULL),
(70, 'DO', 'Dominican Republic', 'República Dominicana', NULL, NULL),
(71, 'EC', 'Ecuador', '', NULL, NULL),
(72, 'EG', 'Egypt', 'مصر', NULL, NULL),
(73, 'SV', 'El Salvador', '', NULL, NULL),
(74, 'GQ', 'Equatorial Guinea', 'Guinea Ecuatorial', NULL, NULL),
(75, 'ER', 'Eritrea', '', NULL, NULL),
(76, 'EE', 'Estonia', 'Eesti', NULL, NULL),
(77, 'ET', 'Ethiopia', '', NULL, NULL),
(78, 'FK', 'Falkland Islands', 'Islas Malvinas', NULL, NULL),
(79, 'FO', 'Faroe Islands', 'Føroyar', NULL, NULL),
(80, 'FJ', 'Fiji', '', NULL, NULL),
(81, 'FI', 'Finland', 'Suomi', NULL, NULL),
(82, 'FR', 'France', '', NULL, NULL),
(83, 'GF', 'French Guiana', 'Guyane française', NULL, NULL),
(84, 'PF', 'French Polynesia', 'Polynésie française', NULL, NULL),
(85, 'TF', 'French Southern Territories', 'Terres australes françaises', NULL, NULL),
(86, 'GA', 'Gabon', '', NULL, NULL),
(87, 'GM', 'Gambia', '', NULL, NULL),
(88, 'GE', 'Georgia', 'საქართველო', NULL, NULL),
(89, 'DE', 'Germany', 'Deutschland', NULL, NULL),
(90, 'GH', 'Ghana', 'Gaana', NULL, NULL),
(91, 'GI', 'Gibraltar', '', NULL, NULL),
(92, 'GR', 'Greece', 'Ελλάδα', NULL, NULL),
(93, 'GL', 'Greenland', 'Kalaallit Nunaat', NULL, NULL),
(94, 'GD', 'Grenada', '', NULL, NULL),
(95, 'GP', 'Guadeloupe', '', NULL, NULL),
(96, 'GU', 'Guam', '', NULL, NULL),
(97, 'GT', 'Guatemala', '', NULL, NULL),
(98, 'GG', 'Guernsey', '', NULL, NULL),
(99, 'GN', 'Guinea', 'Guinée', NULL, NULL),
(100, 'GW', 'Guinea-Bissau', 'Guiné Bissau', NULL, NULL),
(101, 'GY', 'Guyana', '', NULL, NULL),
(102, 'HT', 'Haiti', '', NULL, NULL),
(103, 'HM', 'Heard & McDonald Islands', '', NULL, NULL),
(104, 'HN', 'Honduras', '', NULL, NULL),
(105, 'HK', 'Hong Kong', '香港', NULL, NULL),
(106, 'HU', 'Hungary', 'Magyarország', NULL, NULL),
(107, 'IS', 'Iceland', 'Ísland', NULL, NULL),
(108, 'IN', 'India', 'भारत', NULL, NULL),
(109, 'ID', 'Indonesia', '', NULL, NULL),
(110, 'IR', 'Iran', 'ایران', NULL, NULL),
(111, 'IQ', 'Iraq', 'العراق', NULL, NULL),
(112, 'IE', 'Ireland', '', NULL, NULL),
(113, 'IM', 'Isle of Man', '', NULL, NULL),
(114, 'IL', 'Israel', 'ישראל', NULL, NULL),
(115, 'IT', 'Italy', 'Italia', NULL, NULL),
(116, 'JM', 'Jamaica', '', NULL, NULL),
(117, 'JP', 'Japan', '日本', NULL, NULL),
(118, 'JE', 'Jersey', '', NULL, NULL),
(119, 'JO', 'Jordan', 'الأردن', NULL, NULL),
(120, 'KZ', 'Kazakhstan', 'Казахстан', NULL, NULL),
(121, 'KE', 'Kenya', '', NULL, NULL),
(122, 'KI', 'Kiribati', '', NULL, NULL),
(123, 'XK', 'Kosovo', 'Kosovë', NULL, NULL),
(124, 'KW', 'Kuwait', 'الكويت', NULL, NULL),
(125, 'KG', 'Kyrgyzstan', 'Кыргызстан', NULL, NULL),
(126, 'LA', 'Laos', 'ລາວ', NULL, NULL),
(127, 'LV', 'Latvia', 'Latvija', NULL, NULL),
(128, 'LB', 'Lebanon', 'لبنان', NULL, NULL),
(129, 'LS', 'Lesotho', '', NULL, NULL),
(130, 'LR', 'Liberia', '', NULL, NULL),
(131, 'LY', 'Libya', 'ليبيا', NULL, NULL),
(132, 'LI', 'Liechtenstein', '', NULL, NULL),
(133, 'LT', 'Lithuania', 'Lietuva', NULL, NULL),
(134, 'LU', 'Luxembourg', '', NULL, NULL),
(135, 'MO', 'Macau', '澳門', NULL, NULL),
(136, 'MK', 'Macedonia (FYROM)', 'Македонија', NULL, NULL),
(137, 'MG', 'Madagascar', 'Madagasikara', NULL, NULL),
(138, 'MW', 'Malawi', '', NULL, NULL),
(139, 'MY', 'Malaysia', '', NULL, NULL),
(140, 'MV', 'Maldives', '', NULL, NULL),
(141, 'ML', 'Mali', '', NULL, NULL),
(142, 'MT', 'Malta', '', NULL, NULL),
(143, 'MH', 'Marshall Islands', '', NULL, NULL),
(144, 'MQ', 'Martinique', '', NULL, NULL),
(145, 'MR', 'Mauritania', 'موريتانيا', NULL, NULL),
(146, 'MU', 'Mauritius', 'Moris', NULL, NULL),
(147, 'YT', 'Mayotte', '', NULL, NULL),
(148, 'MX', 'Mexico', '', NULL, NULL),
(149, 'FM', 'Micronesia', '', NULL, NULL),
(150, 'MD', 'Moldova', 'Republica Moldova', NULL, NULL),
(151, 'MC', 'Monaco', '', NULL, NULL),
(152, 'MN', 'Mongolia', 'Монгол', NULL, NULL),
(153, 'ME', 'Montenegro', 'Crna Gora', NULL, NULL),
(154, 'MS', 'Montserrat', '', NULL, NULL),
(155, 'MA', 'Morocco', 'المغرب', NULL, NULL),
(156, 'MZ', 'Mozambique', 'Moçambique', NULL, NULL),
(157, 'MM', 'Myanmar (Burma)', 'မြန်မာ', NULL, NULL),
(158, 'NA', 'Namibia', 'Namibië', NULL, NULL),
(159, 'NR', 'Nauru', '', NULL, NULL),
(160, 'NP', 'Nepal', 'नेपाल', NULL, NULL),
(161, 'NL', 'Netherlands', 'Nederland', NULL, NULL),
(162, 'NC', 'New Caledonia', 'Nouvelle-Calédonie', NULL, NULL),
(163, 'NZ', 'New Zealand', '', NULL, NULL),
(164, 'NI', 'Nicaragua', '', NULL, NULL),
(165, 'NE', 'Niger', 'Nijar', NULL, NULL),
(166, 'NG', 'Nigeria', '', NULL, NULL),
(167, 'NU', 'Niue', '', NULL, NULL),
(168, 'NF', 'Norfolk Island', '', NULL, NULL),
(169, 'MP', 'Northern Mariana Islands', '', NULL, NULL),
(170, 'KP', 'North Korea', '조선 민주주의 인민 공화국', NULL, NULL),
(171, 'NO', 'Norway', 'Norge', NULL, NULL),
(172, 'OM', 'Oman', 'عُمان', NULL, NULL),
(173, 'PK', 'Pakistan', 'پاکستان', NULL, NULL),
(174, 'PW', 'Palau', '', NULL, NULL),
(175, 'PS', 'Palestine', 'فلسطين', NULL, NULL),
(176, 'PA', 'Panama', '', NULL, NULL),
(177, 'PG', 'Papua New Guinea', '', NULL, NULL),
(178, 'PY', 'Paraguay', '', NULL, NULL),
(179, 'PE', 'Peru', 'Perú', NULL, NULL),
(180, 'PH', 'Philippines', '', NULL, NULL),
(181, 'PN', 'Pitcairn Islands', '', NULL, NULL),
(182, 'PL', 'Poland', 'Polska', NULL, NULL),
(183, 'PT', 'Portugal', '', NULL, NULL),
(184, 'PR', 'Puerto Rico', '', NULL, NULL),
(185, 'QA', 'Qatar', 'قطر', NULL, NULL),
(186, 'RE', 'Réunion', 'La Réunion', NULL, NULL),
(187, 'RO', 'Romania', 'România', NULL, NULL),
(188, 'RU', 'Russia', 'Россия', NULL, NULL),
(189, 'RW', 'Rwanda', '', NULL, NULL),
(190, 'BL', 'Saint Barthélemy', 'Saint-Barthélemy', NULL, NULL),
(191, 'SH', 'Saint Helena', '', NULL, NULL),
(192, 'KN', 'Saint Kitts and Nevis', '', NULL, NULL),
(193, 'LC', 'Saint Lucia', '', NULL, NULL),
(194, 'MF', 'Saint Martin', '', NULL, NULL),
(195, 'PM', 'Saint Pierre and Miquelon', 'Saint-Pierre-et-Miquelon', NULL, NULL),
(196, 'WS', 'Samoa', '', NULL, NULL),
(197, 'SM', 'San Marino', '', NULL, NULL),
(198, 'ST', 'São Tomé and Príncipe', 'São Tomé e Príncipe', NULL, NULL),
(199, 'SA', 'Saudi Arabia', 'المملكة العربية السعودية', NULL, NULL),
(200, 'SN', 'Senegal', 'Sénégal', NULL, NULL),
(201, 'RS', 'Serbia', 'Србија', NULL, NULL),
(202, 'SC', 'Seychelles', '', NULL, NULL),
(203, 'SL', 'Sierra Leone', '', NULL, NULL),
(204, 'SG', 'Singapore', '', NULL, NULL),
(205, 'SX', 'Sint Maarten', '', NULL, NULL),
(206, 'SK', 'Slovakia', 'Slovensko', NULL, NULL),
(207, 'SI', 'Slovenia', 'Slovenija', NULL, NULL),
(208, 'SB', 'Solomon Islands', '', NULL, NULL),
(209, 'SO', 'Somalia', 'Soomaaliya', NULL, NULL),
(210, 'ZA', 'South Africa', '', NULL, NULL),
(211, 'GS', 'South Georgia & South Sandwich Islands', '', NULL, NULL),
(212, 'KR', 'South Korea', '대한민국', NULL, NULL),
(213, 'SS', 'South Sudan', 'جنوب السودان', NULL, NULL),
(214, 'ES', 'Spain', 'España', NULL, NULL),
(215, 'LK', 'Sri Lanka', 'ශ්‍රී ලංකාව', NULL, NULL),
(216, 'VC', 'St. Vincent & Grenadines', '', NULL, NULL),
(217, 'SD', 'Sudan', 'السودان', NULL, NULL),
(218, 'SR', 'Suriname', '', NULL, NULL),
(219, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard og Jan Mayen', NULL, NULL),
(220, 'SZ', 'Swaziland', '', NULL, NULL),
(221, 'SE', 'Sweden', 'Sverige', NULL, NULL),
(222, 'CH', 'Switzerland', 'Schweiz', NULL, NULL),
(223, 'SY', 'Syria', 'سوريا', NULL, NULL),
(224, 'TW', 'Taiwan', '台灣', NULL, NULL),
(225, 'TJ', 'Tajikistan', '', NULL, NULL),
(226, 'TZ', 'Tanzania', '', NULL, NULL),
(227, 'TH', 'Thailand', 'ไทย', NULL, NULL),
(228, 'TL', 'Timor-Leste', '', NULL, NULL),
(229, 'TG', 'Togo', '', NULL, NULL),
(230, 'TK', 'Tokelau', '', NULL, NULL),
(231, 'TO', 'Tonga', '', NULL, NULL),
(232, 'TT', 'Trinidad and Tobago', '', NULL, NULL),
(233, 'TA', 'Tristan da Cunha', '', NULL, NULL),
(234, 'TN', 'Tunisia', 'تونس', NULL, NULL),
(235, 'TR', 'Turkey', 'Türkiye', NULL, NULL),
(236, 'TM', 'Turkmenistan', '', NULL, NULL),
(237, 'TC', 'Turks and Caicos Islands', '', NULL, NULL),
(238, 'TV', 'Tuvalu', '', NULL, NULL),
(239, 'UM', 'U.S. Outlying Islands', '', NULL, NULL),
(240, 'VI', 'U.S. Virgin Islands', '', NULL, NULL),
(241, 'UG', 'Uganda', '', NULL, NULL),
(242, 'UA', 'Ukraine', 'Україна', NULL, NULL),
(243, 'AE', 'United Arab Emirates', 'الإمارات العربية المتحدة', NULL, NULL),
(244, 'GB', 'United Kingdom', '', NULL, NULL),
(245, 'US', 'United States', '', NULL, NULL),
(246, 'UY', 'Uruguay', '', NULL, NULL),
(247, 'UZ', 'Uzbekistan', 'Oʻzbekiston', NULL, NULL),
(248, 'VU', 'Vanuatu', '', NULL, NULL),
(249, 'VA', 'Vatican City', 'Città del Vaticano', NULL, NULL),
(250, 'VE', 'Venezuela', '', NULL, NULL),
(251, 'VN', 'Vietnam', 'Việt Nam', NULL, NULL),
(252, 'WF', 'Wallis and Futuna', '', NULL, NULL),
(253, 'EH', 'Western Sahara', 'الصحراء الغربية', NULL, NULL),
(254, 'YE', 'Yemen', 'اليمن', NULL, NULL),
(255, 'ZM', 'Zambia', '', NULL, NULL),
(256, 'ZW', 'Zimbabwe', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `src` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2016_02_16_225952_create_pages_table', 2),
(3, '2018_01_25_001000_create_variables_table', 3),
(4, '2018_04_22_174627_populate_variables_table', 3),
(5, '2018_08_12_225912_populate_variables_table_11', 3),
(6, '2018_08_29_013414_update_variables_table_add_is_hidden', 3),
(7, '2016_02_16_215952_create_countries_table', 4),
(8, '2016_02_16_215953_populate_countries_table', 4),
(9, '2014_07_30_183600_populate_groups_table', 5),
(11, '2014_07_30_184000_populate_users_table', 6),
(12, '2018_01_12_030619_update_users_table_add_new_fields', 7),
(13, '2018_04_15_161945_create_files_table', 8),
(14, '2018_10_28_153049_create_table_translations', 9);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `external_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `view_count` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `slug`, `template`, `image`, `external_url`, `position`, `view_count`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'home', 'templates\\home', NULL, NULL, 0, 0, 1, NULL, '2018-11-08 15:07:30'),
(2, NULL, 'asdsad', 'templates\\default', NULL, '2ea989403ccd75f4fd28bbb652223d8d', 1, 0, 1, '2018-10-28 10:08:15', '2018-10-29 02:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `page_translations`
--

CREATE TABLE `page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_content` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `h1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_translations`
--

INSERT INTO `page_translations` (`id`, `page_id`, `locale`, `name`, `short_content`, `content`, `h1`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 'en', 'home', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 'en', 'home', '<p>test</p>', NULL, NULL, 'test', 'tes', 'test'),
(3, 2, 'ru', '222dsadsd222', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 2, 'lt', '22222222', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 1, 'ru', 'home', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 1, 'lt', 'home', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 4, '9Qg56aVcDn9iCGQCyVqyGaEVSulcfFyF', '2018-10-22 02:21:36', '2018-10-22 02:21:36'),
(2, 4, 'OKCEOjRzIDAminGGBkHqNjPbcDnAkar8', '2018-10-22 03:11:29', '2018-10-22 03:11:29'),
(3, 4, 'p9TmZWj3vOXlGHrQ8lecN29MHHv0spRJ', '2018-10-22 03:29:37', '2018-10-22 03:29:37'),
(4, 4, 'GgqgXj46yP15hNoE3Gmi7ymQZhOsQZdw', '2018-10-22 03:30:14', '2018-10-22 03:30:14'),
(5, 4, 'QijozBIHEOenEOua6c2jcYKB0SPsdLUB', '2018-10-22 03:31:11', '2018-10-22 03:31:11'),
(6, 4, 'okY2mNO3Y2rTrsWrW6EwjwnHPXLTMnrV', '2018-10-22 03:33:15', '2018-10-22 03:33:15'),
(7, 4, 'AklagG4P88P3uTVxH20Q5pNaVabw7wi6', '2018-10-22 04:18:43', '2018-10-22 04:18:43'),
(8, 4, 'R07foHOeqo5daMvBo3gcn58HZUaAMyz1', '2018-10-22 04:20:19', '2018-10-22 04:20:19'),
(9, 4, 'j35YgHU3GcF4YvfRj1GEiCUegrCca9jw', '2018-10-22 14:18:49', '2018-10-22 14:18:49'),
(10, 4, 'oXYDYlhegV1pmcDDRQBLJkJimJ3iXXw3', '2018-10-22 14:18:50', '2018-10-22 14:18:50'),
(11, 4, 'H12ODiphp0sE7UaW0rPlhwGos34gWe5L', '2018-10-22 14:46:09', '2018-10-22 14:46:09'),
(12, 4, 'ox9Mh7gFGw85edKw3J5P2IGVVOPGKTCv', '2018-10-22 15:27:07', '2018-10-22 15:27:07'),
(13, 4, 'DNd8xdThLjQkz9ASbfgzNgZRmzPtl05y', '2018-10-23 07:45:34', '2018-10-23 07:45:34'),
(14, 4, 'xPiwETx72pk8fOBhqkVa1LOCuMhvdoPO', '2018-10-23 08:14:40', '2018-10-23 08:14:40'),
(15, 4, 'xlX5v96pUA1u8WiCpz925nefLHFwE6jo', '2018-10-23 08:30:06', '2018-10-23 08:30:06'),
(16, 4, 'HTlZBfEDoKWJ5Kll1BveAAiRA6cqHh7K', '2018-10-24 15:14:05', '2018-10-24 15:14:05'),
(17, 4, 't5duIkMorm575vTuoM3rzk9pXGqySCAD', '2018-10-25 01:58:16', '2018-10-25 01:58:16'),
(18, 4, 'J8Np641GnBZN5kzUwBrQy7wllWQu50Rj', '2018-10-26 01:56:33', '2018-10-26 01:56:33'),
(20, 4, 'UGZ7QZ31z4mVH6ARxV704wWiIRttEjRk', '2018-10-26 06:30:49', '2018-10-26 06:30:49'),
(21, 4, 'U0QLNjLgS2CfI3RUMBhE2ySnjgXGt4zM', '2018-10-26 06:51:25', '2018-10-26 06:51:25'),
(22, 4, '7ken7ioldhrJevxflwXT0ZgVEQTM8AQt', '2018-10-26 06:52:22', '2018-10-26 06:52:22'),
(24, 4, 'nvzBwSLWv1jhNhKo9pKaGwPEng0eIuka', '2018-10-26 07:08:43', '2018-10-26 07:08:43'),
(25, 4, '6uRAeN29zQ1FgfEsMon0yErRNpQdvaZD', '2018-10-26 07:42:43', '2018-10-26 07:42:43'),
(26, 4, 'wdlLRYk9hImgIwggWr2AqBEZ6ZU2tDcp', '2018-10-28 07:48:15', '2018-10-28 07:48:15'),
(27, 4, 'vFahUsTkhReD4Mb8ANrzqbFzcYICHcvA', '2018-10-28 08:16:05', '2018-10-28 08:16:05'),
(28, 4, 'yWgtxplSzQ4ND6wwzoThlz8BUBoC50PG', '2018-10-28 08:30:54', '2018-10-28 08:30:54'),
(29, 4, 'lgqQAhDD2FtunKTyX5gDkpgB9mzYg3y7', '2018-10-29 01:45:36', '2018-10-29 01:45:36'),
(30, 4, 'HYyJS4NdacCuOGJP4MVqsOu4fb7F46Z9', '2018-10-29 01:54:48', '2018-10-29 01:54:48'),
(31, 4, 'ZYGTAGxdX7SYInqlNanFNe2U4szXIkcv', '2018-11-08 14:57:01', '2018-11-08 14:57:01'),
(32, 4, 'qwPMLFbbOT12xeHxm23loy7iI4AmrMUF', '2018-11-11 13:51:27', '2018-11-11 13:51:27'),
(33, 4, 'o5xGzf5DJDAwaaKnXrQ54sljk7mkioYE', '2018-11-11 14:03:22', '2018-11-11 14:03:22'),
(34, 4, 'wwsPbTHgm8bUhZYYAAb0X7sCwwHfAY8t', '2018-11-11 14:10:45', '2018-11-11 14:10:45'),
(35, 4, 'qZfRgPvlc01umyWN6ZQ1TOAdENfBbjLP', '2018-11-14 15:10:09', '2018-11-14 15:10:09'),
(36, 4, 'MZH4VHN3riz2bzd22TeTx6kowp23FLW2', '2018-11-15 04:32:04', '2018-11-15 04:32:04');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'administrators', 'Администраторы', '{\"administrator\":true}', '2018-10-22 02:14:42', '2018-10-22 02:14:42'),
(2, 'clients', 'Клиенты', '{\"superuser\":true,\"administrator\":false,\"verified\":false,\"user.create\":false,\"user.read\":false,\"user.write\":false,\"user.approve\":false,\"user.role.write\":false,\"userfile.read\":false,\"userfile.write\":false,\"wallettransaction.read\":false,\"contract.read\":false,\"contract.transaction.read\":false,\"deposit.read\":false,\"spend.create\":false,\"spend.read\":false,\"spend.write\":false,\"role.create\":false,\"role.read\":false,\"role.write\":false,\"role.delete\":false,\"page.create\":false,\"page.read\":false,\"page.write\":false,\"page.delete\":false,\"supportpage.read\":false,\"supportpage.write\":false,\"question.create\":false,\"question.read\":false,\"question.write\":false,\"question.delete\":false,\"variable.value.read\":false,\"variable.value.write\":false,\"variable.read\":false,\"variable.create\":false,\"variable.write\":false,\"variable.delete\":false,\"translation.read\":false,\"translation.write\":false,\"dashboard.general\":false,\"dashboard.contracts\":false,\"dashboard.report\":false,\"dashboard.withdraw\":false,\"dashboard.transfer\":false}', '2018-10-22 02:14:42', '2018-10-28 08:51:21'),
(3, 'manager', 'Manager', '{\"superuser\":true,\"administrator\":false,\"verified\":false,\"user.create\":true,\"user.read\":false,\"user.write\":false,\"user.approve\":false,\"user.role.write\":false,\"userfile.read\":false,\"userfile.write\":false,\"wallettransaction.read\":false,\"contract.read\":false,\"contract.transaction.read\":false,\"deposit.read\":false,\"spend.create\":false,\"spend.read\":false,\"spend.write\":false,\"role.create\":false,\"role.read\":false,\"role.write\":false,\"role.delete\":false,\"page.create\":false,\"page.read\":false,\"page.write\":false,\"page.delete\":false,\"supportpage.read\":false,\"supportpage.write\":false,\"question.create\":false,\"question.read\":false,\"question.write\":false,\"question.delete\":false,\"variable.value.read\":false,\"variable.value.write\":false,\"variable.read\":false,\"variable.create\":false,\"variable.write\":false,\"variable.delete\":false,\"translation.read\":false,\"translation.write\":false,\"dashboard.general\":false,\"dashboard.contracts\":false,\"dashboard.report\":false,\"dashboard.withdraw\":false,\"dashboard.transfer\":false}', '2018-10-26 07:25:30', '2018-10-26 07:25:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(4, 1, '2018-10-22 02:16:13', '2018-10-22 02:16:13'),
(6, 1, '2018-10-26 04:45:25', '2018-10-26 04:45:25'),
(6, 2, '2018-10-26 04:45:26', '2018-10-26 04:45:26'),
(7, 1, '2018-10-26 05:25:33', '2018-10-26 05:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'en', 'front_landing', 'agreement amount', 'Agreement amount', '2018-10-28 11:33:02', '2018-10-28 11:33:29'),
(2, 'en', 'front_landing', 'altcoins', NULL, '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(3, 'en', 'front_landing', 'altcoins text', NULL, '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(4, 'en', 'front_landing', 'arbitrage', NULL, '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(5, 'en', 'front_landing', 'arbitrage text', NULL, '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(6, 'en', 'front_landing', 'atm', NULL, '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(7, 'en', 'front_landing', 'atm text', NULL, '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(8, 'en', 'front_landing', 'calculator', 'Calculator', '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(9, 'en', 'front_landing', 'calculator text 1 :site_name', 'Find out how much you earn per day/month/ year with :site_name', '2018-10-28 11:33:02', '2018-10-28 11:33:02'),
(10, 'en', 'front_landing', 'calculator text 2', 'Invest in the technology of the future and «skim the best» right now', '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(11, 'en', 'front_landing', 'calculator title', 'Profit calculator', '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(12, 'en', 'front_landing', 'clear profit', 'Clear profit', '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(13, 'en', 'front_landing', 'command', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(14, 'en', 'front_landing', 'command text 1 desktop', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(15, 'en', 'front_landing', 'command text 1 mobile', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(16, 'en', 'front_landing', 'command text 2 desktop', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(17, 'en', 'front_landing', 'command text 2 mobile', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(18, 'en', 'front_landing', 'command text 3 desktop', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(19, 'en', 'front_landing', 'command text 3 mobile', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(20, 'en', 'front_landing', 'command text 4 desktop', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(21, 'en', 'front_landing', 'command text 4 mobile', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(22, 'en', 'front_landing', 'difference', 'Difference', '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(23, 'en', 'front_landing', 'ico', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(24, 'en', 'front_landing', 'ico text', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(25, 'en', 'front_landing', 'login', NULL, '2018-10-28 11:33:03', '2018-10-28 11:33:03'),
(26, 'en', 'front_landing', 'mining', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(27, 'en', 'front_landing', 'mining text', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(28, 'en', 'front_landing', 'not reinvest', 'Not reinvest', '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(29, 'en', 'front_landing', 'platform', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(30, 'en', 'front_landing', 'platform text 1', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(31, 'en', 'front_landing', 'platform text 2', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(32, 'en', 'front_landing', 'platform text 3', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(33, 'en', 'front_landing', 'platform text 4', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(34, 'en', 'front_landing', 'products', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(35, 'en', 'front_landing', 'products of ai trade', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(36, 'en', 'front_landing', 'products text 1', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(37, 'en', 'front_landing', 'products text 2 desktop', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(38, 'en', 'front_landing', 'products text 2 mobile', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(39, 'en', 'front_landing', 'products text 3 desktop', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(40, 'en', 'front_landing', 'products text 3 mobile', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(41, 'en', 'front_landing', 'products text 4', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(42, 'en', 'front_landing', 'products text 5 desktop', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(43, 'en', 'front_landing', 'products text 5 mobile', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(44, 'en', 'front_landing', 'products text 6', NULL, '2018-10-28 11:33:04', '2018-10-28 11:33:04'),
(45, 'en', 'front_landing', 'profile', NULL, '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(46, 'en', 'front_landing', 'profit and reinvest', 'Profit + Reinvest', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(47, 'en', 'front_landing', 'profit per day', 'Profit/Day', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(48, 'en', 'front_landing', 'profit per year', '1 Year profit', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(49, 'en', 'front_landing', 'registration', NULL, '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(50, 'en', 'front_landing', 'reinvest', 'Reinvest', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(51, 'en', 'front_landing', 'telegram channel', '', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(52, 'en', 'front_landing', 'trading', '', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(53, 'en', 'front_landing', 'trading text', '', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(54, 'en', 'front_landing', 'umbrellas title', '', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(55, 'en', 'front_landing', 'what we do', '', '2018-10-28 11:33:05', '2018-10-28 11:33:05'),
(56, 'en', 'front_landing', 'what we do question', '', '2018-10-28 11:33:05', '2018-10-28 11:33:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `birth_day` tinyint(4) DEFAULT NULL,
  `birth_month` tinyint(4) DEFAULT NULL,
  `birth_year` int(11) DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `user_name`, `phone`, `password`, `two_factor_secret`, `permissions`, `verified`, `last_login`, `first_name`, `last_name`, `avatar`, `gender`, `birth_day`, `birth_month`, `birth_year`, `address`, `city`, `country_id`, `postal_code`, `created_at`, `updated_at`) VALUES
(4, 'admin@admin.com', 'admin', NULL, '$2y$10$ID7drjiHLWhSt9tKtkNpd.uVGxcWIhQwIcCEX9Ib9IPoeuHanPkli', NULL, '{\"superuser\":true}', 1, '2018-11-15 04:32:04', 'Admin', 'Administrator', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-22 02:16:13', '2018-11-15 04:32:04'),
(6, 'david.mkrtchyan121992@gmail.com', 'vazgen', '71231232131', '$2y$10$ARvW8knLL8YulW2XBLMZoe2mUXI7tfDf89wX0dmWiya.tqDhJes3O', NULL, NULL, 0, NULL, 'vazgen', 'mkrtchyan', NULL, 1, 15, 3, NULL, 'kotayq 2 street 2 apratment', 'erevan', 70, 'wq', '2018-10-26 04:45:25', '2018-10-26 04:45:25'),
(7, 'david.mkrtasdchyan121992@gmail.com', 'vazgen21322', '71231232131', '$2y$10$n./rlex.HwxkZyz4Hs8kfO/wXgxAoVc7jlVbHv.d.4Pmvh91T88w6', NULL, NULL, 1, '2018-10-26 07:08:01', 'vazgen', 'mkrtchyan', '/uploads/images/user/7/1f3bfdb9dee567bb4fb73c6d2604230c.png', 1, 1, 2, 2018, 'kotayq 2 street 2 apratment', 'erevan', 1, '231', '2018-10-26 05:25:33', '2018-10-28 14:04:23');

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE `variables` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multilingual` tinyint(1) NOT NULL DEFAULT '0',
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`id`, `type`, `is_hidden`, `key`, `name`, `description`, `multilingual`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'support_link_telegram', 'Службы поддержки в Telegram', 'Ссылка на службу поддержки в Telegram', 0, '', 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(2, 1, 0, 'support_link_messenger', 'Службы поддержки в Messenger', 'Ссылка на службу поддержки в Messenger', 0, '', 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(3, 1, 0, 'support_link_viber', 'Службы поддержки в Viber', 'Ссылка на службу поддержки в Viber', 0, '', 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(4, 1, 0, 'admin_emails', 'Верификационные E-mail адреса', 'E-mail адреса, на которые будут приходить уведомления об изменении верификационных данных пользователей (если несколько, то через кому)', 0, '', 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(5, 1, 0, 'telegram_channel_link', 'Ссылка на телеграм канал', NULL, 0, '', 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(6, 1, 0, 'social_page_youtube', 'Ссылка на страницу в Youtube', NULL, 0, NULL, 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(7, 1, 0, 'social_page_instagram', 'Ссылка на страницу в Instagram', NULL, 0, NULL, 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(8, 1, 0, 'social_page_facebook', 'Ссылка на страницу в Facebook', NULL, 0, NULL, 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(9, 1, 0, 'social_page_twitter', 'Ссылка на страницу в Twitter', NULL, 0, NULL, 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(10, 8, 0, 'languages', 'Языки для отображения на сайте', 'Языки, которые будут доступны для отображения на сайте', 0, '[\"en\",\"ru\",\"lt\"]', 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57'),
(11, 9, 0, 'terms_and_conditions_file', 'Условия и положения', 'Файл условий и положений', 1, NULL, 1, '2018-10-21 07:24:57', '2018-10-21 07:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `variable_translations`
--

CREATE TABLE `variable_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `variable_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_user_id_index` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_parent_id_index` (`parent_id`);

--
-- Indexes for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  ADD KEY `page_translations_locale_index` (`locale`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_user_name_index` (`user_name`),
  ADD KEY `users_verified_index` (`verified`),
  ADD KEY `users_country_id_index` (`country_id`);

--
-- Indexes for table `variables`
--
ALTER TABLE `variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variable_translations`
--
ALTER TABLE `variable_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `variable_translations_variable_id_locale_unique` (`variable_id`,`locale`),
  ADD KEY `variable_translations_variable_id_index` (`variable_id`),
  ADD KEY `variable_translations_locale_index` (`locale`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `variables`
--
ALTER TABLE `variables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `variable_translations`
--
ALTER TABLE `variable_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `pages` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `variable_translations`
--
ALTER TABLE `variable_translations`
  ADD CONSTRAINT `variable_translations_variable_id_foreign` FOREIGN KEY (`variable_id`) REFERENCES `variables` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
